import { createBottomTabNavigator } from 'react-navigation';
import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import { greyColor, globalStyles } from '../assets/styles';

import DashboardRouter from './dashboard-router';
import CompetitionRouter from './createCompetiation-router';
import MyCompetitionRouter from './mycompetition-router';

class Placeholder extends Component {
    render() {
        return (
            <TouchableOpacity
                style={[
                    globalStyles.flexContainer,
                    globalStyles.flexHCenter,
                    globalStyles.flexVCenter
                ]}
                onPress={() => this.props.navigation.goBack()}
            >
                <Text>Go Back</Text>
            </TouchableOpacity>
        );
    }
}

const CreateCompetitionButton = args => {
    const buttonStyle = {
        borderRadius: 50,
        height: '100%',
        width: '100%'
    };
    const textStyle = {
        textAlign: 'center',
        fontSize: 10,
        fontWeight: 'bold',
        color: 'white'
    };
    return (
        <LinearGradient
            colors={['#4d6bc2', '#3695e3']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={[
                globalStyles.flexHCenter,
                globalStyles.flexVCenter,
                buttonStyle
            ]}
        >
            <Text style={textStyle}>{'Create\nCompetition'}</Text>
        </LinearGradient>
    );
};

const screensWithoutTab = [
    'Search',
    'PlayIt',
    'WalletTransationScreen',
    'CompetitionDetail',
    'MyWalletSearchScreen',
    'GroupCreateScreen',
    'GroupInfo',
    'GroupEditScreen',
    'NotificationScreen'
]; // hide tab from screen

const drawerScreens = [
    'MySportsListing',
    'Home',
    'ReferFriend',
    'MyProfile',
    'GroupScreenStack',
    'MyFriendsList',
    'MyWalletScreen'
]; // no tint color on tabs

const showTintColor = routes => {
    return !routes.some(route => drawerScreens.includes(route.routeName));
};

const showTabBar = routes => {
    return !routes.some(route => screensWithoutTab.includes(route.routeName));
};

export default createBottomTabNavigator(
    {
        Dashboard: {
            screen: DashboardRouter,
            navigationOptions: ({ navigation }) => {
                return {
                    tabBarOnPress: ({ navigation, defaultHandler }) => {
                        navigation.navigate('DashboardScreen');
                    },
                    tabBarVisible: showTabBar(navigation.state.routes),
                    tabBarIcon: ({ tintColor }) => (
                        <Icon
                            name="home"
                            type="font-awesome"
                            size={16}
                            color={
                                showTintColor(navigation.state.routes)
                                    ? tintColor
                                    : null
                            }
                        />
                    )
                };
            }
        },
        CreateCompetitions: {
            title: 'Create Competition',
            screen: CompetitionRouter,
            navigationOptions: {
                tabBarVisible: false,
                tabBarLabel: CreateCompetitionButton
            }
        },
        MyCompetitions: {
            screen: MyCompetitionRouter,
            navigationOptions: ({ navigation }) => ({
                tabBarVisible: showTabBar(navigation.state.routes),
                tabBarLabel: 'My Competitions',
                tabBarIcon: ({ tintColor }) => (
                    <Image
                        resizeMode="contain"
                        source={require('../assets/images/competition.png')}
                        style={{ tintColor, width: 25, height: 16 }}
                    />
                )
            })
        }
    },
    {
        tabBarOptions: {
            activeTintColor: '#4287e1',
            inactiveTintColor: greyColor,
            labelStyle: {
                fontWeight: 'bold',
                top: -8,
                color: 'black'
            },
            style: {
                justifyContent: 'center'
            }
        }
    }
);
