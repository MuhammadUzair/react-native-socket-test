import { createSwitchNavigator } from 'react-navigation';

import LoginScreen from '../screens/Login/LoginScreen';
import SignupScreen from '../screens/Signup/SignupScreen';

import onBoardRouter from './onboard-router';
import DashboardRouter from './drawer-router';

const RootStack = createSwitchNavigator(
    {
        Login: LoginScreen,
        Signup: SignupScreen,
        onBoard: onBoardRouter,
        Dashboard: {
            screen: DashboardRouter,
            navigationOptions: {
                header: () => null
            }
        }
    },
    {
        initialRouteName: 'Login',
        headerMode: 'none'
    }
);

export default RootStack;
