import React from 'react';

import { createStackNavigator } from 'react-navigation';
import { Image, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

import { os } from '../utils';
import { globalStyles, greyColor, baseBlack } from '../assets/styles';

import MyCompetition from '../screens/MyCompetition';
import CompetitionDetail from '../screens/CompetitionDetail';
import Amount from '../components/Amount';

const amount = {
    textAlign: 'left',
    fontSize: 10,
    color: baseBlack
};

export default createStackNavigator(
    {
        MyCompetition,
        CompetitionDetail
    },
    {
        navigationOptions: {
            headerTintColor: greyColor,
            headerStyle: {
                borderBottomWidth: 0,
                paddingRight: os() ? 10 : 20
            },
            headerTitle: (
                <View
                    style={[
                        globalStyles.flexHCenter,
                        globalStyles.flexVCenter,
                        globalStyles.flexContainer
                    ]}
                >
                    <Image
                        resizeMode="contain"
                        style={{ width: '50%', height: '50%' }}
                        source={require('../assets/images/logo.png')}
                    />
                </View>
            ),
            headerRight: <Amount />
        }
    }
);
