import React, { Component } from 'react';
import { View, Text, Image, Platform, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { createStackNavigator } from 'react-navigation';

import { os } from '../utils';
import { globalStyles, greyColor, baseBlack } from '../assets/styles';

import MyWallet from '../screens/MyWallet/MyWallet';
import WalletTransaction from '../screens/WalletTransaction/WalletTransaction';
import Amount from '../components/Amount';

const amount = {
    textAlign: 'left',
    fontSize: 10,
    color: baseBlack
};

export default createStackNavigator(
    {
        MyWallet,
        WalletTransaction
    },
    {
        navigationOptions: () => ({
            headerTintColor: greyColor,
            headerStyle: {
                borderBottomWidth: 0,
                paddingRight: os() ? 10 : 20
            },
            headerTitle: (
                <View
                    style={[
                        globalStyles.flexHCenter,
                        globalStyles.flexVCenter,
                        globalStyles.flexContainer
                    ]}
                >
                    <Image
                        resizeMode="contain"
                        style={{ width: '50%', height: '50%' }}
                        source={require('../assets/images/logo.png')}
                    />
                </View>
            ),
            headerRight: <Amount />
        })
    }
);
