import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
    ScrollView,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import { createDrawerNavigator } from 'react-navigation';

import { globalStyles } from '../assets/styles';
import Drawer from '../components/Drawer';
import DashboarTabs from './tab-router';
import MyWallet from '../screens/MyWallet/MyWallet';
import Notifications from '../screens/Notifications';
import ReferFriend from '../screens/ReferFriend';

class Placeholder extends Component {
    render() {
        return (
            <TouchableOpacity
                style={[
                    globalStyles.flexContainer,
                    globalStyles.flexHCenter,
                    globalStyles.flexVCenter
                ]}
                onPress={() => this.props.navigation.goBack()}
            >
                <Text>Go Back</Text>
            </TouchableOpacity>
        );
    }
}

export default createDrawerNavigator(
    {
        Home: DashboarTabs,
        Competitions: Placeholder,
        MySports: {
            screen: Placeholder,
            navigationOptions: {
                drawerLabel: 'My Sports'
            }
        },
        MyGroups: {
            screen: Placeholder,
            navigationOptions: {
                drawerLabel: 'My Groups'
            }
        },
        MyFriends: {
            screen: Placeholder,
            navigationOptions: {
                drawerLabel: 'My Friends'
            }
        },
        MyWallet: {
            screen: Placeholder,
            navigationOptions: {
                drawerLabel: 'My Wallet'
            }
        },
        MyProfile: {
            screen: Placeholder,
            navigationOptions: {
                drawerLabel: 'My Profile'
            }
        },
        // Notifications: Placeholder,
        Notifications: {
            screen: Placeholder,
            navigationOptions: {
                drawerLabel: 'Notifications'
            }
        },
        ReferFriend: {
            screen: ReferFriend,
            navigationOptions: {
                drawerLabel: 'ReferFriend'
            }
        },
        // Settings: Placeholder,

        Logout: Placeholder
    },
    {
        headerMode: 'none',
        drawerWidth: Dimensions.get('window').width,
        contentComponent: props => <Drawer {...props} />,
        contentOptions: { activeBackgroundColor: 'rgba(0, 0, 0,0)' }
    }
);
