import { createStackNavigator } from 'react-navigation';

import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { Icon } from 'react-native-elements';

import FriendListing from '../screens/MyFriendsListing/';
import PendingFriendListing from '../screens/MyFriendsPendingRequestListing/';

import { os } from '../utils';
import { globalStyles, greyColor, baseBlack } from '../assets/styles';
import Amount from '../components/Amount';

const amount = {
    textAlign: 'left',
    fontSize: 10,
    color: baseBlack
};

const MyFriendsList = createStackNavigator(
    {
        FriendListing: FriendListing
        // PendingFriendList: PendingFriendListing
    },
    {
        initialRouteName: 'FriendListing',
        navigationOptions: {
            headerTintColor: greyColor,
            headerStyle: {
                borderBottomWidth: 0,
                paddingRight: os() ? 10 : 20
            },
            headerTitle: (
                <View
                    style={[
                        globalStyles.flexHCenter,
                        globalStyles.flexVCenter,
                        globalStyles.flexContainer
                    ]}
                >
                    <Image
                        resizeMode="contain"
                        style={{ width: '50%', height: '50%' }}
                        source={require('../assets/images/logo.png')}
                    />
                </View>
            ),
            headerRight: <Amount />
        }
    }
);

export default MyFriendsList;
