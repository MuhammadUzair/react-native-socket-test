import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { onboardGrey } from '../assets/styles';

import MySports from '../screens/MySports';
import MyTeams from '../screens/MyTeams';
import MyFriends from '../screens/MyFriends';

export default createStackNavigator(
    {
        MySports,
        MyTeams,
        MyFriends
    },
    {
        initialRouteName: 'MySports',
        navigationOptions: ({ navigation }) => {
            return {
                headerTintColor: onboardGrey,
                headerStyle: {
                    elevation: 0,
                    backgroundColor: '#E9E9EF',
                    borderBottomWidth: 0
                }
            };
        }
    }
);
