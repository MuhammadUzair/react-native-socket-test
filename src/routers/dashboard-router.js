import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import { os } from '../utils';
import { globalStyles, greyColor } from '../assets/styles';

import DashboardScreen from '../screens/Dashboard';
import Search from '../screens/Search';
import MySportsListing from '../screens/MySportsListing';
import ReferFriend from '../screens/ReferFriend';
import MyFriendsList from '../routers/myfriends-router';
import PlayIt from '../routers/playit-router';
import MyProfile from '../screens/MyProfile';
import MyWalletScreen from '../screens/MyWallet';
import WalletTransationScreen from '../screens/WalletTransaction/WalletTransaction';
import MyWalletSearchScreen from '../screens/MyWalletSearch';
import CreateGroup from '../screens/CreateGroup';
import GroupScreenListing from '../screens/MyGroupsListing';
import GroupCreateInfo from '../screens/GroupCreateInfo';
import GroupInfo from '../screens/GroupInfo';
import GroupScreen from '../screens/MyGroups';
import GroupEditScreen from '../screens/MyGroupsEdit';
import NotificationScreen from '../screens/Notifications';
import AddFundsScreen from '../screens/AddFunds';
import MyFriendsPendingRequestListing from '../screens/MyFriendsPendingRequestListing';
import GatewayTransaction from '../screens/GatewayTransaction';

import Amount from '../components/Amount';

export default createStackNavigator(
    {
        DashboardScreen,
        MyFriendsList: {
            screen: MyFriendsList,
            navigationOptions: {
                header: () => null
            }
        },
        PendingFriendList: {
            screen: MyFriendsPendingRequestListing
        },
        MySportsListing,
        ReferFriend,
        PlayIt: {
            screen: PlayIt,
            // screen: ({ navigation }) => <PlayIt screenProps={{ navigation }} />,
            navigationOptions: {
                header: () => null
            }
        },
        MyProfile,
        MyWalletScreen,
        WalletTransationScreen,
        MyWalletSearchScreen,
        GroupCreateScreen: CreateGroup,
        GroupScreenListing,
        GroupCreateInfo,
        GroupInfo,
        GroupScreen,
        GroupEditScreen,
        NotificationScreen,
        AddFundsScreen,
        GatewayTransaction
    },
    {
        initialRoute: 'Dashboard',
        navigationOptions: {
            headerTintColor: greyColor,
            headerStyle: {
                borderBottomWidth: 0,
                paddingRight: os() ? 10 : 20
            },
            headerTitle: (
                <View
                    style={[
                        globalStyles.flexHCenter,
                        globalStyles.flexVCenter,
                        globalStyles.flexContainer
                    ]}
                >
                    <Image
                        resizeMode="contain"
                        style={{ width: '50%', height: '50%' }}
                        source={require('../assets/images/logo.png')}
                    />
                </View>
            ),
            headerRight: <Amount />
        }
    }
);
