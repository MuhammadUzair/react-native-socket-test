import { createStackNavigator } from 'react-navigation';

import React, { Component } from 'react';
import { View, Text, Image, Platform } from 'react-native';
import { Icon } from 'react-native-elements';

import CompetitionScreen from '../screens/createCompetition/CompetitionScreen';
import CompetitionDetailScreen from '../screens/createCompetition/CompetitionDetailScreen';
import CompetitionConfirmationScreen from '../screens/createCompetition/CompetitionConfirmationScreen';
import CompetitionPaymentScreen from '../screens/createCompetition/CompetitionPaymentScreen';

import { os } from '../utils';
import { globalStyles, greyColor, baseBlack } from '../assets/styles';

import Amount from '../components/Amount';

const amount = {
    textAlign: 'left',
    fontSize: 10,
    color: baseBlack
};

const CompetitionStack = createStackNavigator(
    {
        createCompetition: CompetitionScreen,
        competitionDetail: CompetitionDetailScreen,
        competitionConfirmation: CompetitionConfirmationScreen,
        competitionPayment: CompetitionPaymentScreen
    },
    {
        initialRouteName: 'createCompetition',
        navigationOptions: ({ navigation }) => {
            return {
                headerTintColor: greyColor,
                headerStyle: {
                    borderBottomWidth: 0,
                    paddingRight: os() ? 10 : 20
                },
                headerTitle: (
                    <View
                        style={[
                            globalStyles.flexHCenter,
                            globalStyles.flexVCenter,
                            globalStyles.flexContainer
                        ]}
                    >
                        <Image
                            resizeMode="contain"
                            style={{ width: '50%', height: '50%' }}
                            source={require('../assets/images/logo.png')}
                        />
                    </View>
                ),
                headerRight: <Amount />
            };
        }
    }
);

export default CompetitionStack;
