import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';

import { globalStyles, baseBlack } from '../../assets/styles';

const styles = {
    email: {
        fontSize: 12,
        color: 'white'
    },
    amount: {
        fontSize: 14,
        color: 'white',
        textAlign: 'right'
    },
    nonDarwerAmount: {
        textAlign: 'right',
        fontSize: 10
    }
};

const HeaderRightIcon = props => {
    return (
        <TouchableOpacity
            style={[{ alignItems: 'flex-end' }]}
            onPress={() => props.drawerProps.navigate('MyWalletScreen')}
        >
            <View style={[globalStyles.flexVBottom, { flex: 0 }]}>
                <Text
                    ellipsizeMode="tail"
                    style={[
                        props.drawer ? styles.amount : styles.nonDarwerAmount,
                        props.drawer ? null : globalStyles.highlightBlueColor
                    ]}
                >
                    $
                    {(props.user.code >= 200 &&
                        props.user.code < 300 &&
                        props.user.data &&
                        props.user.data.amount.toFixed(2)) ||
                        `00.00`}
                </Text>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        { justifyContent: 'space-between' }
                    ]}
                >
                    <Icon
                        name="money"
                        size={props.drawer ? 14 : 10}
                        type="font-awesome"
                        color={props.drawer ? '#fff' : '#000'}
                    />
                    <Text
                        style={[
                            props.drawer
                                ? styles.email
                                : styles.nonDarwerAmount,
                            { marginLeft: 5 }
                        ]}
                    >
                        Wallet
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(HeaderRightIcon);
