import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { globalStyles } from '../../assets/styles';
import { getDate, getTime } from '../../utils';

export default ({ days, startTime, timeLeft, owner, alternate }) => {
    const time = timeLeft.split(':');
    return (
        <View
            style={[
                globalStyles.flexHorizontal,
                globalStyles.fullWidth,
                alternate ? {} : globalStyles.blueBackgroudColor,
                globalStyles.flexHCenter,
                {
                    height: 30,
                    paddingHorizontal: 10,
                    borderTopWidth: alternate ? StyleSheet.hairlineWidth : 0,
                    borderColor: '#aaa'
                }
            ]}
        >
            <Text
                style={[
                    globalStyles.flexContainer,
                    globalStyles.whiteColor,
                    globalStyles.baseFontSize,
                    { textAlign: 'center' }
                ]}
            >
                {days} D, {time[0]} H, {time[1]} M Left
            </Text>
        </View>
    );
};
