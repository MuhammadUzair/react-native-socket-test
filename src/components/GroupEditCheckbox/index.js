import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

import CustomCheckBox from '../CustomCheckBox';

import { globalStyles } from '../../assets/styles';

export default class GroupEditCheckbox extends Component {
    onPress = () => {
        this.props.onPress(this.props.index);
    };

    render() {
        const { item, stateText, checked } = this.props;
        return (
            <TouchableOpacity onPress={this.onPress}>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        styles.checkbox,
                        globalStyles.hairLineUnderBorder
                    ]}
                >
                    <CustomCheckBox
                        uri={item.avatar}
                        style={styles.checkboxStyle}
                        checked={checked}
                        rounded
                    />
                    <View style={styles.nameHolder}>
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseFontSize,
                                globalStyles.blackColor,
                                styles.paddingText
                            ]}
                        >
                            {item.name}
                        </Text>
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                styles.paddingText
                            ]}
                        >
                            {item.username}
                        </Text>
                    </View>

                    <View style={styles.stateHolder}>{stateText}</View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = {
    checkbox: {
        width: '100%',
        paddingHorizontal: 5,
        marginBottom: 20,
        alignItems: 'center'
    },

    checkboxStyle: {
        marginRight: 10
    },

    nameHolder: {
        flex: 4
    },

    stateHolder: {
        flex: 2.5,
        alignItems: 'flex-end'
    },

    paddingText: {
        paddingLeft: 10
    }
};
