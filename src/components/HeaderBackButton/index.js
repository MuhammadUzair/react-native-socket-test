import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

import { connect } from 'react-redux';

import { greyColor } from '../../assets/styles';

import { os, retrieveToken } from '../../utils';

import { fetchUser } from '../../redux/epics/fetch-user-epic';
import { readAllNotification } from '../../redux/epics/read-all-notifications';
import { resetFetchSpecificCompetition } from '../../redux/epics/fetch-specific-competition-epic';

class HeaderBackButton extends Component {
    onPress = async () => {
        const {
            onPress,
            fetchForUpdate,
            fetchUser,
            isNotificationScreen,
            readAllNotification,
            resetFetchSpecificCompetition
        } = this.props;
        onPress();
        let token = await retrieveToken();
        if (isNotificationScreen) {
            resetFetchSpecificCompetition();
            readAllNotification({ token });
        }
        if (fetchForUpdate) {
            fetchUser(token);
        }
    };
    render() {
        const { type = 'back', fetchForUpdate } = this.props;
        let style = {};
        if (fetchForUpdate) {
            style = {
                paddingLeft: os() ? 10 : 12
            };
        }
        return (
            <TouchableOpacity style={style} onPress={this.onPress}>
                <Icon
                    name={
                        type === 'back'
                            ? os()
                                ? 'angle-left'
                                : 'arrow-back'
                            : 'close'
                    }
                    type={
                        type === 'back'
                            ? os()
                                ? 'font-awesome'
                                : 'material'
                            : 'evilicon'
                    }
                    size={type === 'back' ? (os() ? 39 : 25) : os() ? 30 : 25}
                    color={greyColor}
                />
            </TouchableOpacity>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    fetchUser: data => dispatch(fetchUser(data)),
    readAllNotification: data => dispatch(readAllNotification(data)),
    resetFetchSpecificCompetition: () =>
        dispatch(resetFetchSpecificCompetition())
});

export default connect(
    null,
    mapDispatchToProps
)(HeaderBackButton);
