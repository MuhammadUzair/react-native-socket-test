import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale } from 'react-native-size-matters';
import { Icon } from 'react-native-elements';

import { globalStyles } from '../../assets/styles';

export default class BlueSmallCards extends Component {
    onPressButton = () => {
        const { onPress, secondaryAction, item } = this.props;
        onPress(item);
        typeof secondaryAction === 'function' && secondaryAction();
    };
    render() {
        const {
            style,
            selected,
            text,
            first,
            containerStyle,
            index,
            textStyle
        } = this.props;
        return (
            <View
                style={[
                    !first ? styles.marginBottomTen : null,
                    index % 2 !== 0 ? styles.marginLeftTen : null,
                    style
                ]}
                key={index}
            >
                <LinearGradient
                    colors={['#4d6bc2', '#3695e3']}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={[
                        styles.container,
                        styles.selectedContainer,
                        containerStyle
                    ]}
                >
                    <Text
                        style={[
                            styles.text,
                            globalStyles.subTextFontSize,
                            textStyle
                        ]}
                    >
                        {text}
                    </Text>

                    <TouchableOpacity
                        style={styles.deleteSelectedItemWrap}
                        onPress={this.onPressButton}
                    >
                        <Icon
                            type="FontAwesome"
                            name="close"
                            iconStyle={styles.deleteSelectedItemIcon}
                        />
                    </TouchableOpacity>
                </LinearGradient>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    selectedContainer: {
        borderWidth: 0
    },
    container: {
        minWidth: 140,
        borderWidth: StyleSheet.hairlineWidth,
        paddingHorizontal: scale(15),
        paddingVertical: 4,
        borderRadius: 50
    },
    text: {
        maxHeight: 20,
        maxWidth: '80%',
        color: 'white'
    },
    deleteSelectedItemWrap: {
        position: 'absolute',
        top: 0,
        right: 10
    },
    deleteSelectedItemIcon: {
        color: 'white'
    },
    marginBottomTen: {
        marginBottom: 10
    },
    marginLeftTen: {
        marginLeft: 10
    }
});
