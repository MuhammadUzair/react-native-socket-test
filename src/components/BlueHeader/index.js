import React, { Component } from 'react';
import { Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements';
import { globalStyles } from '../../assets/styles';
import styles from './styles';

class BlueHeader extends Component {
    render() {
        const { title, navigation, onPress } = this.props;
        return (
            <LinearGradient
                colors={['#4d6bc2', '#3695e3']}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={[
                    globalStyles.flexHorizontal,
                    styles.gradientHeader,
                    globalStyles.flexSpaceBetween
                ]}
            >
                <Text
                    style={[
                        globalStyles.boldFontFace,
                        globalStyles.whiteColor,
                        styles.gradientHeaderFont
                    ]}
                >
                    {title}
                </Text>
                <Icon
                    name="close"
                    type="font-awesome"
                    iconStyle={styles.gradientHeaderFont}
                    // onPress={() => {
                    //     navigation.goBack();
                    // }}
                    onPress={this.props.onPress}
                    underlayColor={'#3695e3'}
                />
            </LinearGradient>
        );
    }
}

export default BlueHeader;
