import { StyleSheet, Dimensions } from 'react-native';
import { scale } from 'react-native-size-matters';

export default StyleSheet.create({
    gradientHeader: {
        padding: 15
    },
    gradientHeaderFont: {
        fontSize: scale(12),
        color: 'white'
    }
});
