import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Icon, Badge } from 'react-native-elements';
import { fetchLeagues } from '../../redux/epics/fetch-leagues-epic';
import { retrieveToken } from '../../utils';
class DrawerIconButton extends Component {
    componentDidMount() {
        retrieveToken().then(token => {
            fetchLeagues(token);
        });
    }

    render() {
        const { isNotification, count } = this.props.getNotification;
        if (isNotification) {
            return (
                <TouchableOpacity
                    onPress={() => this.props.onPress()}
                    style={styles.header}
                >
                    <Image
                        style={styles.drawerIcon}
                        resizeMode="contain"
                        source={require('../../assets/images/drawer_icon.png')}
                    />

                    {count == 0 ? (
                        <View
                            style={{ position: 'absolute', top: -2, right: 0 }}
                        >
                            <Icon
                                name="circle"
                                type="font-awesome"
                                iconStyle={styles.dotIcon}
                            />
                        </View>
                    ) : (
                        <Badge containerStyle={styles.notificationCountWrap}>
                            <Text style={styles.notificationCount}>
                                {count}
                            </Text>
                        </Badge>
                    )}
                </TouchableOpacity>
            );
        } else {
            return (
                <TouchableOpacity
                    onPress={() => this.props.onPress()}
                    style={styles.drawerIconWrap}
                >
                    <Image
                        style={styles.drawerIcon}
                        resizeMode="contain"
                        source={require('../../assets/images/drawer_icon.png')}
                    />
                </TouchableOpacity>
            );
        }
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row'
    },
    dotIcon: {
        color: 'red',
        fontSize: 11
    },
    dotIconWrap: {
        position: 'absolute',
        top: -2,
        right: 0
    },
    notificationCountWrap: {
        backgroundColor: 'red',
        marginLeft: -10
    },
    notificationCount: {
        color: 'white',
        fontSize: 11
    },
    drawerIconWrap: {
        paddingTop: 5,
        paddingBottom: 5
    },
    drawerIcon: {
        width: 45,
        height: 20
    }
});

const mapStateToProps = ({ getNotification }) => ({
    getNotification
});

const mapDispatchToProps = dispatch => ({
    fetchLeagues: data => dispatch(fetchLeagues(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DrawerIconButton);
