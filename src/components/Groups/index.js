import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { globalStyles } from '../../assets/styles';

export default class GroupCard extends Component {
    viewDetailsPressed = () => {
        const { viewDetail, item } = this.props;
        viewDetail(item);
    };

    joinGroupPressed = () => {
        const { joinGroup, item } = this.props;
        joinGroup(item);
    };

    render() {
        const { item, index } = this.props;
        return (
            <View
                style={[
                    globalStyles.whiteBackgroudColor,
                    styles.flatListContainer,
                    styles.flatList
                ]}
            >
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.hairLineUnderBorder,
                        styles.paddingButtom
                    ]}
                >
                    <View style={[globalStyles.flexVCenter]}>
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.navIconGreyColor,
                                styles.counterFontSize,
                                styles.marginRight
                            ]}
                        >
                            {index < 10 ? `0${index}` : index}
                        </Text>
                    </View>
                    <View style={[globalStyles.flexContainer]}>
                        <View style={styles.marginBottom}>
                            <Text
                                style={[
                                    globalStyles.blackColor,
                                    globalStyles.boldFontFace
                                ]}
                            >
                                {item.name}
                            </Text>
                        </View>

                        <View style={[globalStyles.flexHorizontal]}>
                            <View
                                style={{
                                    flex: 0,
                                    borderRadius: 50,
                                    overflow: 'hidden',
                                    marginRight: 6
                                }}
                            >
                                <Text
                                    style={[
                                        styles.featuredBadge,
                                        globalStyles.paleRedBackgroudColor,
                                        globalStyles.whiteColor,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    {item.total_members} Members
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.marginTop}>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexVBottom,
                                styles.btnHolder
                            ]}
                            onPress={this.joinGroupPressed}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.buttonStyle
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.boldFontFace,
                                        globalStyles.whiteColor,
                                        globalStyles.baseFontSize
                                    ]}
                                >
                                    Join
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                <View
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.flexHorizontal,
                        globalStyles.flexVCenter,
                        globalStyles.flexHCenter,
                        styles.paddingTop
                    ]}
                >
                    <View style={[{ flex: 0.19 }]} />
                    <View
                        style={[
                            globalStyles.flexContainer,
                            globalStyles.flexHorizontal
                        ]}
                    >
                        <View style={[styles.paddingLeft]}>
                            <Text>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Open{' '}
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.baseBlueColor
                                    ]}
                                >
                                    {item.open}
                                </Text>
                            </Text>
                        </View>
                        <View style={[styles.paddingLeft]}>
                            <Text>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    In-Play{' '}
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.highlightPinkColor
                                    ]}
                                >
                                    {item.in_play}
                                </Text>
                            </Text>
                        </View>

                        <View style={[styles.paddingLeft]}>
                            <Text>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Settled{' '}
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.blackColor
                                    ]}
                                >
                                    {item.settled}
                                </Text>
                            </Text>
                        </View>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{ padding: 5 }}
                        onPress={this.viewDetailsPressed}
                    >
                        <Text
                            style={[
                                globalStyles.baseBlueColor,
                                globalStyles.subTextFontSize,
                                globalStyles.boldFontFace
                            ]}
                        >
                            View Detail
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    flatListContainer: {
        flex: 0,
        marginBottom: 10
    },
    marginRight: { marginRight: 10 },
    marginTop: { marginTop: 5 },
    marginBottom: { marginBottom: 5 },
    flatList: {
        padding: 10,
        borderRadius: 10
    },

    hairLineDivider: {
        borderTopColor: '#f0f0f0',
        borderTopWidth: 0.5,
        marginTop: 10
    },

    widthSixty: {
        width: '60%'
    },

    widthForty: {
        width: '40%'
    },

    roundTextCard: {
        flex: 0,
        borderRadius: 50,
        overflow: 'hidden'
    },

    counterFontSize: {
        fontSize: 30
    },

    featuredBadge: {
        borderRadius: 50,
        paddingHorizontal: 10
    },

    paddingLeft: {
        paddingLeft: 5
    },
    paddingRight: {
        paddingRight: 10
    },
    paddingButtom: {
        paddingBottom: 5
    },
    paddingTop: {
        paddingTop: 5
    },
    btnHolder: {
        paddingLeft: 2,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 50
    }
});
