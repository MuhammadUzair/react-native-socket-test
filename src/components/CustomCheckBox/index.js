import React from 'react';
import { Image, View } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Icon } from 'react-native-elements';

import { globalStyles } from '../../assets/styles';
import { BASEURL_DEV } from '../../redux/epics/base-url';
import CustomAvatar from '../CustomAvatar';

const styles = {
    imageView: {
        alignItems: 'center',
        marginBottom: 5
    },
    overlay: {
        height: '100%',
        width: 50,
        borderRadius: 50,
        position: 'absolute',
        left: 0,
        top: 0,
        backgroundColor: '#4879d5aa'
    }
};

const applyOverlay = (apply, size) =>
    apply ? (
        <View
            style={[
                size === 'small'
                    ? { ...styles.overlay, width: 34 }
                    : styles.overlay,
                globalStyles.flexHCenter,
                globalStyles.flexVCenter
            ]}
        >
            <Icon color="#FFF" type="font-awesome" name="check" size={15} />
        </View>
    ) : null;

const selectSize = (size, title, uri) => {
    switch (size) {
        case 'small':
            return <CustomAvatar uri={uri} title={title} small rounded />;
        case 'xsmall':
            return <CustomAvatar uri={uri} title={title} xsmall rounded />;
        case 'large':
            return <CustomAvatar uri={uri} title={title} large rounded />;
        default:
            return <CustomAvatar uri={uri} title={title} medium rounded />;
    }
};

export default ({ checked, style, uri, title, size }) => {
    return (
        <View style={[styles.imageView, style]}>
            {selectSize(size, title, uri)}
            {applyOverlay(checked, size)}
        </View>
    );
};
