import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { globalStyles } from '../../assets/styles';
import { getDate } from '../../utils';

export default class Transaction extends Component {
    onPress = () => {
        this.props.onPress(this.props.item);
    };
    render() {
        const { item } = this.props;
        return (
            <TouchableOpacity onPress={this.onPress}>
                <View>
                    <View style={[styles.card]}>
                        <View style={[globalStyles.flexHorizontal, styles.mb]}>
                            <View style={globalStyles.flexContainer}>
                                <View
                                    style={[
                                        globalStyles.flexHorizontal,
                                        globalStyles.fullWidth,
                                        globalStyles.flexSpaceBetween,
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.boldFontFace,
                                            globalStyles.baseBlueColor,
                                        ]}
                                    >
                                        {item.fee_type}
                                    </Text>
                                    <Text
                                        style={[
                                            globalStyles.boldFontFace,
                                            item.type === 'Debit'
                                                ? globalStyles.highlightPinkColor
                                                : globalStyles.baseBlueColor,
                                            { textAlign: 'right' },
                                        ]}
                                    >
                                        ${parseFloat(item.amount).toFixed(2)}
                                    </Text>
                                </View>
                                <View
                                    style={[
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.subTextFontSize,
                                            globalStyles.blackColor,
                                            globalStyles.flexContainer,
                                        ]}
                                    >
                                        {item.description}
                                    </Text>
                                    <Text
                                        style={[
                                            globalStyles.subTextFontSize,
                                            globalStyles.greyColor,
                                            globalStyles.flexContainer,
                                            { textAlign: 'right' },
                                        ]}
                                    >
                                        {getDate(item.created_at)}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = {
    card: {
        padding: 5,
    },
};
