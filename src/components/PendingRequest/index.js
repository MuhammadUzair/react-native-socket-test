import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { globalStyles } from '../../assets/styles';

import CustomAvatar from '../CustomAvatar';

export default class PendingRequest extends Component {
    accept = () => {
        this.props.accept(this.props.item);
    };

    reject = () => {
        this.props.reject(this.props.item);
    };

    render() {
        const { item } = this.props;
        return (
            <View
                style={[globalStyles.flexHorizontal, styles.checkbox]}
                key={item.friend_id}
            >
                <CustomAvatar
                    uri={item.avatar}
                    containerStyle={styles.checkboxStyle}
                    medium
                    rounded
                />
                <View style={styles.nameHolder}>
                    <Text
                        style={[
                            styles.checkboxText,
                            globalStyles.boldFontFace,
                            globalStyles.blackColor
                        ]}
                    >
                        {item.name && item.name}
                    </Text>
                    <Text
                        style={[
                            globalStyles.baseFontSize,
                            globalStyles.placeHolderGreyColor
                        ]}
                    >
                        {item.username && item.username}
                    </Text>
                </View>
                <TouchableOpacity onPress={this.accept}>
                    <Text
                        style={[
                            globalStyles.boldFontFace,
                            globalStyles.highlightBlueColor,
                            styles.paddingRight
                        ]}
                    >
                        Accept
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.reject}>
                    <Text
                        style={[
                            globalStyles.boldFontFace,
                            globalStyles.paleRedColor
                        ]}
                    >
                        Decline
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    nameHolder: {
        flex: 4
    },
    paddingRight: {
        paddingRight: 5
    }
};
