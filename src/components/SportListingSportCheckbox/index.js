import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

import { globalStyles } from '../../assets/styles';

import CustomCheckBox from '../CustomCheckBox';

export default class SportListingSportCheckbox extends Component {
    onPress = () => {
        this.props.onPress(this.props.item, 'mySports');
    };

    renderStateText = item => {
        return item ? (
            <Text style={[styles.checkboxText, globalStyles.paleRedColor]}>
                Remove
            </Text>
        ) : (
            <Text style={styles.checkboxText}>Add to favorite</Text>
        );
    };

    render() {
        const { item } = this.props;
        return (
            <TouchableOpacity
                onPress={this.onPress}
                style={[
                    globalStyles.flexHorizontal,
                    styles.checkbox,
                    globalStyles.hairLineUnderBorder
                ]}
            >
                <CustomCheckBox
                    style={styles.checkboxStyle}
                    checked={item.isAdded}
                    uri={item.avatar}
                    title={item.short_code}
                    rounded
                />
                <View style={styles.nameHolder}>
                    <Text
                        style={[
                            globalStyles.blackColor,
                            globalStyles.subTextFontSize
                        ]}
                    >
                        {item.name && item.name}
                    </Text>
                    <Text
                        style={[
                            styles.checkboxText,
                            globalStyles.highlightColor
                        ]}
                    >
                        {item.short_code && item.short_code}
                    </Text>
                </View>
                <View style={styles.stateHolder}>
                    {this.renderStateText(item.isAdded)}
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = {
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    nameHolder: {
        flex: 4
    },
    stateHolder: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'flex-end'
    }
};
