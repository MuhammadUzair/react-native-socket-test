import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { scale } from 'react-native-size-matters';

import { globalStyles } from '../../assets/styles';

export default class Smallcards extends Component {
    onPressButton = () => {
        const { onPress, secondaryAction, item } = this.props;
        onPress(item);
        typeof secondaryAction === 'function' && secondaryAction();
    };
    render() {
        const {
            style,
            selected,
            text,
            first,
            containerStyle,
            index,
            textStyle,
        } = this.props;
        if (selected) {
            return (
                <TouchableOpacity
                    style={[!first ? { marginLeft: 20 } : null, style]}
                    onPress={this.onPressButton}
                    key={index}
                >
                    <LinearGradient
                        colors={['#4d6bc2', '#3695e3']}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={[
                            styles.container,
                            styles.selectedContainer,
                            containerStyle,
                        ]}
                    >
                        <Text
                            style={[
                                styles.text,
                                styles.selectedText,
                                globalStyles.baseFontSize,
                                textStyle,
                            ]}
                        >
                            {text}
                        </Text>
                    </LinearGradient>
                </TouchableOpacity>
            );
        } else {
            return (
                <TouchableOpacity
                    style={[!first ? { marginLeft: 20 } : null, style]}
                    onPress={this.onPressButton}
                    key={index}
                >
                    <View
                        style={[
                            styles.container,
                            globalStyles.whiteBackgroudColor,
                            containerStyle,
                        ]}
                    >
                        <Text
                            style={[
                                styles.text,
                                globalStyles.blackColor,
                                globalStyles.baseFontSize,
                                textStyle,
                            ]}
                        >
                            {text}
                        </Text>
                    </View>
                </TouchableOpacity>
            );
        }
    }
}

const styles = StyleSheet.create({
    selectedContainer: {
        borderWidth: 0,
    },
    container: {
        minWidth: 60,
        borderWidth: StyleSheet.hairlineWidth,
        paddingHorizontal: scale(15),
        paddingVertical: 4,
        borderRadius: 50,
    },
    text: {
        textAlign: 'center',
    },
    selectedText: {
        color: '#fff',
    },
});
