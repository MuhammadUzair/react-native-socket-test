import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';

import { globalStyles, baseBlack } from '../../assets/styles';

const styles = {
    email: {
        fontSize: 12,
        color: 'white'
    },
    amount: {
        fontSize: 14,
        color: 'white',
        textAlign: 'right'
    },
    nonDarwerAmount: {
        textAlign: 'right',
        fontSize: 10
    }
};

const Amount = props => {
    // if (props.drawer)
    return (
        <View style={[{ alignItems: 'flex-end' }]}>
            <View style={[globalStyles.flexVBottom, { flex: 0 }]}>
                <Text
                    ellipsizeMode="tail"
                    style={[
                        props.drawer ? styles.amount : styles.nonDarwerAmount,
                        props.drawer ? null : globalStyles.highlightBlueColor
                    ]}
                >
                    $
                    {(props.user.code >= 200 &&
                        props.user.code < 300 &&
                        props.user.data &&
                        props.user.data.amount.toFixed(2)) ||
                        `00.00`}
                </Text>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        { justifyContent: 'space-between' }
                    ]}
                >
                    <Icon
                        name="money"
                        size={props.drawer ? 14 : 10}
                        type="font-awesome"
                        color={props.drawer ? '#fff' : '#000'}
                    />
                    <Text
                        style={[
                            props.drawer
                                ? styles.email
                                : styles.nonDarwerAmount,
                            { marginLeft: 5 }
                        ]}
                    >
                        Wallet
                    </Text>
                </View>
            </View>
        </View>
    );
    // else
    //     return (
    //         <View style={[{ alignItems: 'flex-end' }]}>
    //             <View style={[globalStyles.flexVBottom, { flex: 0 }]}>
    //                 <Text
    //                     ellipsizeMode="tail"
    //                     style={[
    //                         styles.nonDarwerAmount,
    //                         globalStyles.highlightColor
    //                     ]}
    //                 >
    //                     ${props.user.amount}
    //                 </Text>
    //                 <View
    //                     style={[
    //                         globalStyles.flexHorizontal,
    //                         { justifyContent: 'space-between' }
    //                     ]}
    //                 >
    //                     <Icon
    //                         name="money"
    //                         size={14}
    //                         type="font-awesome"
    //                         color="#000"
    //                     />
    //                     <Text style={styles.nonDarwerAmount}>Wallet</Text>
    //                 </View>
    //             </View>
    //         </View>
    //     );
    // return (
    //     <View
    //         style={[
    //             globalStyles.flexVCenter,
    //             globalStyles.flexHCenter,
    //             { flex: 1 }
    //         ]}
    //     >
    //         <Text
    //             style={[
    //                 styles.nonDarwerAmount,
    //                 globalStyles.highlightColor,
    //                 globalStyles.fullWidth
    //             ]}
    //             ellipsizeMode="tail"
    //         >
    //             ${props.user.amount}
    //         </Text>
    //         <View
    //             style={[
    //                 globalStyles.flexHorizontal,
    //                 globalStyles.fullWidth,
    //                 { width: '100%', justifyContent: 'space-between' }
    //             ]}
    //         >
    //             <Icon
    //                 color={baseBlack}
    //                 size={10}
    //                 name="money"
    //                 type="font-awesome"
    //             />
    //             <Text style={styles.nonDarwerAmount}>Wallet</Text>
    //         </View>
    //     </View>
    // );
};

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(Amount);
