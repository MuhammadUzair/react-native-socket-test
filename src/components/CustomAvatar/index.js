import React, { Component } from 'react';
import { Avatar } from 'react-native-elements';
import { globalStyles } from '../../assets/styles';
import { BASEURL_DEV } from '../../redux/epics/base-url';

export default props => {
    if (props.showBase64) {
        return <Avatar source={{ uri: props.base64Image }} {...props} />;
    }
    if (props.uri) {
        return (
            <Avatar
                source={{
                    uri: props.uri.startsWith('http')
                        ? props.uri
                        : `${BASEURL_DEV}${props.uri}`
                }}
                {...props}
            />
        );
    } else {
        return (
            <Avatar
                overlayContainerStyle={globalStyles.blueBackgroudColor}
                titleStyle={{ fontSize: 10 }}
                title={props.title}
                {...props}
            />
        );
    }
};
