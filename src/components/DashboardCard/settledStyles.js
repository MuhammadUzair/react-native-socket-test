import { StyleSheet, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    main: {
        marginBottom: 5,
        padding: 10,
        paddingLeft: 13,
        paddingRight: 13,
        backgroundColor: 'white'
    },
    contentRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10
    },
    contentRowForTwoElements: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10
    },
    leftContentWrap: {
        width: width * 0.3
    },
    leftContent: {
        flexDirection: 'row'
    },
    centerContent: {
        position: 'absolute',
        top: 5,
        left: width * 0.49
    },
    centerText: {
        color: '#3695e3'
    },
    postedByWrap: {
        marginTop: 8
    },
    footerWrap: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    teamNamewidth: {
        width: width * 0.38
    },
    footerLeftContent: {
        width: width * 0.7
    },
    amount: {
        textAlign: 'right'
    },
    date: {
        fontSize: 12
    },
    gradiantButnText: {
        fontSize: scale(8)
    },
    openCardfooterWrap: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    myCompettionButtonWidth: {
        width: '80%'
    },
    openCardMargin: {
        padding: 10,
        paddingTop: 5
    },
    openCardFooterLeftContent: {
        width: width * 0.5
    }
});
