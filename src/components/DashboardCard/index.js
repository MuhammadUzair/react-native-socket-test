import React, { Component, Fragment } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements';

import styles from './style';
import TimeSection from '../TimeSection';
import CustomAvatar from '../CustomAvatar';
import { globalStyles } from '../../assets/styles';
import {
    renderSubText,
    renderSpread,
    renderSpreadWithPlusSign,
    getDate,
    getTime
} from '../../utils';
import style from './style';
import settledStyles from './settledStyles';

const { height } = Dimensions.get('window');

export default class DashboardCard extends Component {
    onPress = () => {
        this.props.onPress(this.props.item);
    };

    isFeatured = () => (
        <View style={styles.feturedBadgeHolder}>
            <Text style={[styles.featuredBadge, styles.smallFont]}>
                Featured
            </Text>
        </View>
    );

    renderAmountCard = () => {
        const {
            isOpenCard,
            isInplayCard,
            isSettledCard,
            isNoActionCard,
            isDashboardCard,
            item
        } = this.props;
        if (isOpenCard || isNoActionCard)
            return (
                <View>
                    <Text
                        style={[
                            globalStyles.highlightPinkColor,
                            globalStyles.boldFontFace,
                            globalStyles.baseFontSize
                        ]}
                    >
                        ${item.bidding_amount}
                    </Text>
                    <Text
                        style={[
                            globalStyles.highlightPinkColor,
                            globalStyles.boldFontFace,
                            globalStyles.baseFontSize
                        ]}
                    >
                        /$
                        {item.amount}
                    </Text>
                </View>
            );
        if (isInplayCard)
            return (
                <Text
                    style={[
                        styles.amount,
                        globalStyles.boldFontFace,
                        globalStyles.baseFontSize
                    ]}
                >
                    ${item.amount}
                </Text>
            );
        if (isSettledCard)
            return (
                <Text
                    style={[
                        styles.amount,
                        globalStyles.boldFontFace,
                        globalStyles.baseFontSize
                    ]}
                >
                    ${item.amount}
                </Text>
            );
        if (isDashboardCard)
            return (
                <Text
                    style={[
                        styles.amount,
                        globalStyles.boldFontFace,
                        globalStyles.baseFontSize
                    ]}
                >
                    ${item.remaining_amount}
                </Text>
            );
        return (
            <Text
                style={[
                    styles.amount,
                    globalStyles.boldFontFace,
                    globalStyles.baseFontSize
                ]}
            >
                ${item.remaining_amount}
            </Text>
        );
    };

    renderAvatar = () => {
        const {
            item,
            isOpenCard,
            isInplayCard,
            isSettledCard,
            isDashboardCard
        } = this.props;
        if (isOpenCard || isInplayCard || isSettledCard) {
            return (
                <CustomAvatar
                    small
                    rounded
                    uri={item[item.my_team].avatar}
                    title={item[item.my_team].short_code}
                    rounded
                />
            );
        }
        return (
            <CustomAvatar
                uri={item.competitor.avatar}
                title={item.competitor.short_code}
                containerStyle={styles.image}
                small
                rounded
            />
        );
    };

    nameToUse = () => {
        const {
            item,
            isOpenCard,
            isInplayCard,
            isSettledCard,
            isDashboardCard
        } = this.props;
        if (isOpenCard || isInplayCard || isSettledCard)
            return item[item.my_team].name;
        return item.competitor.name;
    };

    subTextToRender = () => {
        const { item, isOpenCard, isInplayCard, isDashboardCard } = this.props;
        if (isInplayCard) return '';
        if (isOpenCard) return renderSubText(item.spread, true);
        if (isDashboardCard) return renderSubText(item.spread, true);
        return renderSubText(item.spread);
    };

    renderSettledRow = () => {
        const { item } = this.props;
        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.flexSpaceBetween,
                    { paddingTop: 4, alignItems: 'flex-end' }
                ]}
            >
                <Text style={[{ fontSize: 10 }, globalStyles.blackColor]}>
                    Score{' '}
                    <Text
                        style={[{ fontSize: 10 }, globalStyles.baseBlueColor]}
                    >
                        {item.result !== 'Waiting For Result' &&
                            item.score &&
                            item.score}
                    </Text>
                </Text>
                <Text style={[{ fontSize: 10 }, globalStyles.blackColor]}>
                    <Text
                        style={[
                            globalStyles.blackColor,
                            item.result === 'WIN'
                                ? globalStyles.highlightGreenColor
                                : null,
                            item.result === 'Lose'
                                ? globalStyles.paleRedColor
                                : null,
                            item.result === 'LOSS'
                                ? globalStyles.paleRedColor
                                : null,
                            { fontSize: 10 }
                        ]}
                    >
                        {item.result == 'Waiting For Result'
                            ? 'Waiting For Result'
                            : 'Result ' + item.result}
                    </Text>
                </Text>
                <Text style={[{ fontSize: 10 }, globalStyles.blackColor]}>
                    Result Amount{' '}
                    <Text
                        style={[
                            item.result === 'WIN'
                                ? globalStyles.highlightGreenColor
                                : globalStyles.paleRedColor,
                            { fontSize: 10 }
                        ]}
                    >
                        {item.result == 'Waiting For Result'
                            ? ''
                            : '$' + item.result_amount && item.result_amount}
                    </Text>
                </Text>
            </View>
        );
    };

    renderButton = () => {
        const { isDashboardCard } = this.props;
        return (
            <TouchableOpacity
                onPress={this.onPress}
                activeOpacity={0.5}
                style={[
                    globalStyles.flexVBottom,
                    globalStyles.flexHCenter,
                    styles.btnHolder
                ]}
            >
                <LinearGradient
                    colors={['#4d6bc2', '#3695e3']}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexHCenter,
                        globalStyles.flexSpaceBetween,
                        styles.buttonStyle
                    ]}
                >
                    <Text
                        style={[
                            globalStyles.subTextFontSize,
                            globalStyles.whiteColor,
                            globalStyles.boldFontFace
                        ]}
                    >
                        {isDashboardCard ? 'Play It' : 'View Detail'}
                    </Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    };
    getTimeZone = date => {
        if (date) {
            let zone = new Date(date).toString().substr(25, 3);
            return zone;
        }
    };

    renderGradientButton = () => {
        const {
            isDashboardCard,
            isCreateCompetitionCard,
            isOpenCard,
            isInplayCard
        } = this.props;
        return (
            <TouchableOpacity
                onPress={this.onPress}
                activeOpacity={0.5}
                style={[
                    globalStyles.flexVBottom,
                    globalStyles.flexHCenter,
                    styles.dashboardBtnWrap
                ]}
            >
                <LinearGradient
                    colors={['#4d6bc2', '#3695e3']}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexHCenter,
                        globalStyles.flexSpaceBetween,
                        styles.dashboardBtn,
                        (isOpenCard || isInplayCard) &&
                            settledStyles.myCompettionButtonWidth
                    ]}
                >
                    <Text
                        style={[
                            globalStyles.baseFontSize,
                            globalStyles.whiteColor,
                            globalStyles.boldFontFace
                        ]}
                    >
                        {isDashboardCard || isCreateCompetitionCard
                            ? 'Play It'
                            : 'View Detail'}
                    </Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    };

    renderAmountAndResult = () => {
        const { item, isSettledCard } = this.props;

        if (isSettledCard) {
            return (
                <View>
                    <Text
                        style={[
                            globalStyles.headingBlackColor,
                            styles.alignSelfCenter,
                            globalStyles.subTextFontSize
                        ]}
                    >
                        Amount/Result
                    </Text>
                    <Text
                        style={[
                            globalStyles.headingBlackColor,
                            settledStyles.amount,
                            globalStyles.subTextFontSize,
                            item.result === 'WIN'
                                ? globalStyles.highlightGreenColor
                                : null,
                            item.result === 'Lose'
                                ? globalStyles.paleRedColor
                                : null,
                            item.result === 'LOSS'
                                ? globalStyles.paleRedColor
                                : null,
                            style.marginTopFive
                        ]}
                    >
                        $ {item.amount && item.amount}
                        {item.result == 'Waiting For Result'
                            ? ' N/A'
                            : ' ' + item.result}
                    </Text>
                </View>
            );
        } else {
            return (
                <View>
                    <Text
                        style={[
                            globalStyles.headingBlackColor,
                            styles.alignSelfCenter,
                            globalStyles.subTextFontSize
                        ]}
                    >
                        Amount/Result
                    </Text>
                    <Text
                        style={[
                            settledStyles.amount,
                            globalStyles.subTextFontSize,
                            globalStyles.paleRedColor,
                            style.marginTopFive
                        ]}
                    >
                        {item.amount &&
                            '$ ' + item.bidding_amount + '/$ ' + item.amount}
                    </Text>
                </View>
            );
        }
    };

    renderleagueName = league_name => {
        if (league_name) {
            return (
                <Text
                    style={[
                        globalStyles.baseFontSize,
                        globalStyles.baseBlueColor,
                        styles.leagueName
                    ]}
                >
                    {league_name}
                </Text>
            );
        }
    };

    renderAmount = () => {
        const { isDashboardCard, item, isOpenCard, isInplayCard } = this.props;
        if (isDashboardCard) {
            return (
                <View style={[styles.rightSection]}>
                    <Text
                        style={[
                            styles.amount,
                            globalStyles.subTextFontSize,
                            globalStyles.paleRedColor
                        ]}
                    >
                        $ {item.amount && item.amount}
                    </Text>
                </View>
            );
        }
        if (isOpenCard) {
            return (
                <View style={[styles.rightSection, styles.paddingRightTen]}>
                    <Text
                        style={[
                            styles.amount,
                            globalStyles.subTextFontSize,
                            globalStyles.greyColor
                        ]}
                    >
                        Amount
                    </Text>
                    <Text
                        style={[
                            styles.amount,
                            globalStyles.subTextFontSize,
                            globalStyles.paleRedColor
                        ]}
                    >
                        $ {item.bidding_amount && item.bidding_amount}
                    </Text>
                </View>
            );
        }
        if (isInplayCard) {
            return (
                <View style={[styles.rightSection, styles.paddingRightTen]}>
                    <Text
                        style={[
                            styles.amount,
                            globalStyles.subTextFontSize,
                            globalStyles.greyColor
                        ]}
                    >
                        Amount
                    </Text>
                    <Text
                        style={[
                            styles.amount,
                            globalStyles.subTextFontSize,
                            globalStyles.paleRedColor
                        ]}
                    >
                        {item.amount &&
                            item.bidding_amount &&
                            '$ ' + item.bidding_amount + '/$ ' + item.amount}
                    </Text>
                </View>
            );
        }
    };

    renderHomeTeamAvatar = () => {
        const { item, isDashboardCard } = this.props;
        if (isDashboardCard) {
            return (
                <CustomAvatar
                    small
                    rounded
                    uri={item.homeTeam.avatar}
                    title={item.homeTeam.short_code}
                    rounded
                    containerStyle={styles.avatarRound}
                />
            );
        }
        return (
            <CustomAvatar
                uri={item.competitor.avatar}
                title={item.competitor.short_code}
                containerStyle={styles.image}
                small
                rounded
            />
        );
    };

    renderAwayTeamAvatar = () => {
        const { item, isDashboardCard } = this.props;
        if (isDashboardCard) {
            return (
                <CustomAvatar
                    small
                    rounded
                    uri={item.awayTeam.avatar}
                    title={item.awayTeam.short_code}
                    rounded
                    containerStyle={styles.avatarRound}
                />
            );
        }
        return (
            <CustomAvatar
                uri={item.team.avatar}
                title={item.team.short_code}
                containerStyle={styles.image}
                small
                rounded
            />
        );
    };

    dashboardCard = () => {
        const {
            item,
            isDashboardCard,
            isInplayCard,
            isOpenCard,
            isSettledCard,
            isNoActionCard,
            fullCard,
            isCreateCompetitionCard
        } = this.props;

        let startdate = isCreateCompetitionCard
            ? item.start_date_time
            : item.start_time;

        return (
            <View style={styles.mainCardWrap}>
                <View
                    style={[
                        styles.mainCard,
                        {
                            height: isSettledCard
                                ? height * 0.17
                                : height * 0.16,

                            backgroundColor: 'white'
                        }
                    ]}
                >
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexContainer,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <View style={styles.leftSection}>
                            {fullCard &&
                                this.renderleagueName(item.league_name)}
                            <View style={globalStyles.flexHorizontal}>
                                <View
                                    opacity={
                                        !this.props.teamStatus.awayTeam &&
                                        isDashboardCard
                                            ? 0.5
                                            : 1
                                    }
                                >
                                    {this.renderAwayTeamAvatar()}
                                </View>
                                <Text
                                    style={[
                                        globalStyles.baseFontSize,
                                        styles.homeTeamName,
                                        !this.props.teamStatus.awayTeam &&
                                            isDashboardCard &&
                                            styles.greyColor
                                    ]}
                                >
                                    {isDashboardCard
                                        ? item.awayTeam.name
                                        : item.team.name}
                                    {this.props.teamStatus.awayTeam &&
                                    typeof item.spread === 'number'
                                        ? ' ' +
                                          renderSpreadWithPlusSign(item.spread)
                                        : null}
                                </Text>
                            </View>

                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.marginTopTen
                                ]}
                            >
                                <View
                                    opacity={
                                        !this.props.teamStatus.homeTeam &&
                                        isDashboardCard
                                            ? 0.5
                                            : 1
                                    }
                                >
                                    {this.renderHomeTeamAvatar()}
                                </View>
                                <Text
                                    style={[
                                        globalStyles.baseFontSize,
                                        styles.homeTeamName,
                                        !this.props.teamStatus.homeTeam &&
                                            isDashboardCard &&
                                            styles.greyColor
                                    ]}
                                >
                                    {isDashboardCard
                                        ? item.homeTeam.name
                                        : item.competitor.name}
                                    {this.props.teamStatus.homeTeam &&
                                    typeof item.spread === 'number'
                                        ? ' ' +
                                          renderSpreadWithPlusSign(item.spread)
                                        : null}
                                </Text>
                            </View>
                        </View>
                        {this.renderAmount()}
                    </View>
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexContainer,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <View style={styles.leftSection}>
                            <Text
                                style={[
                                    globalStyles.subTextFontSize,
                                    styles.greyColor,
                                    styles.timeMargin,
                                    fullCard
                                        ? styles.timeMarginFullCard
                                        : styles.timeMargin,
                                    isCreateCompetitionCard &&
                                        style.createCompetitionTimeMargin
                                ]}
                            >
                                {getDate(startdate) +
                                    ' ' +
                                    getTime(startdate) +
                                    ' ' +
                                    this.getTimeZone(startdate)}
                            </Text>
                        </View>
                        <View
                            style={[
                                styles.rightSection,
                                globalStyles.flexVBottom
                            ]}
                        >
                            {this.renderGradientButton()}
                        </View>
                    </View>
                </View>
                <TimeSection
                    owner={
                        isSettledCard || isNoActionCard || isInplayCard
                            ? item.participants[0]
                            : null
                    }
                    startTime={startdate}
                    days={item.days_left}
                    timeLeft={item.time_left}
                    alternate={isSettledCard || isNoActionCard || isInplayCard}
                />
            </View>
        );
    };

    myCompettionCard = () => {
        const {
            item,
            isDashboardCard,
            isInplayCard,
            isOpenCard,
            isSettledCard,
            isNoActionCard,
            fullCard,
            isCreateCompetitionCard
        } = this.props;

        let startdate = isCreateCompetitionCard
            ? item.start_date_time
            : item.start_time;

        return (
            <View style={styles.mainCardWrap}>
                <View
                    style={[
                        styles.mainCard,
                        {
                            height: height * 0.1,
                            backgroundColor: 'white'
                        }
                    ]}
                >
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexContainer,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <View style={styles.leftSection}>
                            <View style={globalStyles.flexHorizontal}>
                                <View
                                    opacity={
                                        !this.props.teamStatus.awayTeam
                                            ? 0.5
                                            : 1
                                    }
                                >
                                    {this.renderHomeTeamAvatar()}
                                </View>
                                <Text
                                    style={[
                                        globalStyles.baseFontSize,
                                        styles.homeTeamName,
                                        !this.props.teamStatus.awayTeam &&
                                            styles.greyColor
                                    ]}
                                >
                                    {item.awayTeam.name}
                                    {this.props.teamStatus.awayTeam &&
                                    typeof item.spread === 'number'
                                        ? ' ' +
                                          renderSpreadWithPlusSign(item.spread)
                                        : null}
                                </Text>
                            </View>

                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.marginTopTen
                                ]}
                            >
                                <View
                                    opacity={
                                        !this.props.teamStatus.homeTeam
                                            ? 0.5
                                            : 1
                                    }
                                >
                                    {this.renderAwayTeamAvatar()}
                                </View>

                                <Text
                                    style={[
                                        globalStyles.baseFontSize,
                                        styles.homeTeamName,
                                        !this.props.teamStatus.homeTeam &&
                                            styles.greyColor
                                    ]}
                                >
                                    {item.homeTeam.name}
                                    {this.props.teamStatus.homeTeam &&
                                    typeof item.spread === 'number'
                                        ? ' ' +
                                          renderSpreadWithPlusSign(item.spread)
                                        : null}
                                </Text>
                            </View>
                        </View>
                        {this.renderAmount()}
                    </View>
                </View>

                <View
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        settledStyles.openCardMargin
                    ]}
                >
                    <View style={settledStyles.openCardFooterLeftContent}>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                settledStyles.postedByWrap
                            ]}
                        >
                            <Text
                                style={[
                                    globalStyles.subTextFontSize,
                                    styles.greyColor
                                ]}
                            >
                                {getDate(startdate) +
                                    ' ' +
                                    getTime(startdate) +
                                    ' ' +
                                    this.getTimeZone(startdate)}
                            </Text>
                        </View>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                settledStyles.postedByWrap
                            ]}
                        >
                            <Text style={globalStyles.subTextFontSize}>
                                Posted by
                            </Text>
                            <Text
                                style={[
                                    globalStyles.baseBlueColor,
                                    globalStyles.subTextFontSize
                                ]}
                            >
                                {item.owner && item.owner.name
                                    ? ' ' + item.owner.name
                                    : null}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={[styles.rightSection, globalStyles.flexVBottom]}
                    >
                        {this.renderGradientButton()}
                    </View>
                </View>
                {!isInplayCard ? (
                    <TimeSection
                        owner={item.participants[0]}
                        startTime={startdate}
                        days={item.days_left}
                        timeLeft={item.time_left}
                        alternate={
                            isSettledCard || isNoActionCard || isInplayCard
                        }
                    />
                ) : null}
            </View>
        );
    };

    settledCard = () => {
        const { item, isSettledCard } = this.props;

        return (
            <View style={[globalStyles.flexContainer, settledStyles.main]}>
                <View style={settledStyles.contentRow}>
                    <View style={settledStyles.leftContentWrap}>
                        <View style={settledStyles.leftContent}>
                            <View
                                opacity={
                                    !this.props.teamStatus.awayTeam ? 0.5 : 1
                                }
                            >
                                <CustomAvatar
                                    uri={item.awayTeam.avatar}
                                    title={item.awayTeam.short_code}
                                    small
                                    containerStyle={styles.avatar}
                                />
                            </View>

                            <Text
                                style={[
                                    styles.alignSelfCenter,
                                    styles.leftTeamNameMargin,
                                    settledStyles.teamNamewidth,
                                    globalStyles.subTextFontSize,
                                    !this.props.teamStatus.awayTeam &&
                                        styles.greyColor
                                ]}
                            >
                                {item.awayTeam.name}
                                {this.props.teamStatus.awayTeam &&
                                typeof item.spread === 'number'
                                    ? ' ' +
                                      renderSpreadWithPlusSign(item.spread)
                                    : null}
                            </Text>
                        </View>
                    </View>
                    {isSettledCard ? (
                        <View style={settledStyles.centerContent}>
                            <Text
                                style={[
                                    globalStyles.baseBlueColor,
                                    settledStyles.centerText,
                                    globalStyles.subTextFontSize
                                ]}
                            >
                                {item.detail &&
                                    item.detail.payload &&
                                    item.detail.payload.awaySpread &&
                                    item.detail.payload.awaySpread}
                            </Text>
                        </View>
                    ) : null}
                    {this.renderAmountAndResult()}
                </View>

                <View style={settledStyles.contentRowForTwoElements}>
                    <View style={settledStyles.leftContentWrap}>
                        <View style={settledStyles.leftContent}>
                            <View
                                opacity={
                                    !this.props.teamStatus.homeTeam ? 0.5 : 1
                                }
                            >
                                <CustomAvatar
                                    uri={item.homeTeam.avatar}
                                    title={item.homeTeam.short_code}
                                    small
                                    containerStyle={styles.avatar}
                                />
                            </View>

                            <Text
                                style={[
                                    styles.alignSelfCenter,
                                    styles.leftTeamNameMargin,
                                    settledStyles.teamNamewidth,
                                    globalStyles.subTextFontSize,
                                    !this.props.teamStatus.homeTeam &&
                                        styles.greyColor
                                ]}
                            >
                                {item.homeTeam.name}
                                {this.props.teamStatus.homeTeam &&
                                typeof item.spread === 'number'
                                    ? ' ' +
                                      renderSpreadWithPlusSign(item.spread)
                                    : null}
                            </Text>
                        </View>
                    </View>
                    <View style={settledStyles.centerContent}>
                        <Text
                            style={[
                                globalStyles.baseBlueColor,
                                settledStyles.centerText,
                                globalStyles.subTextFontSize
                            ]}
                        >
                            {item.detail &&
                                item.detail.payload &&
                                item.detail.payload.homeSpread &&
                                item.detail.payload.homeSpread}
                        </Text>
                    </View>
                </View>

                <View
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.flexSpaceBetween,
                        globalStyles.flexHorizontal,
                        style.marginTopTen
                    ]}
                >
                    <View>
                        <Text
                            style={[
                                globalStyles.subTextFontSize,
                                styles.greyColor
                            ]}
                        >
                            {getDate(item.start_time) +
                                ' ' +
                                getTime(item.start_time) +
                                ' ' +
                                this.getTimeZone(item.start_time)}
                        </Text>
                    </View>
                    {isSettledCard ? (
                        <View>
                            <Text>ID # {item.id}</Text>
                        </View>
                    ) : null}
                </View>

                <View style={settledStyles.footerWrap}>
                    <View style={settledStyles.footerLeftContent}>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                settledStyles.postedByWrap
                            ]}
                        >
                            <Text style={globalStyles.subTextFontSize}>
                                Posted by
                            </Text>
                            <Text
                                style={[
                                    globalStyles.baseBlueColor,
                                    globalStyles.subTextFontSize
                                ]}
                            >
                                {item.owner && item.owner.name
                                    ? ' ' + item.owner.name
                                    : null}
                            </Text>
                        </View>
                    </View>
                    {isSettledCard ? (
                        <TouchableOpacity
                            onPress={this.onPress}
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexVBottom,
                                globalStyles.flexHCenter,
                                styles.btnHolder,
                                style.marginTopTen
                            ]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.buttonStyle
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.whiteColor,
                                        globalStyles.boldFontFace
                                    ]}
                                >
                                    View Detail
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    ) : null}
                </View>
            </View>
        );
    };

    render() {
        const {
            item,
            isDashboardCard,
            isInplayCard,
            isOpenCard,
            isSettledCard,
            isNoActionCard,
            isCreateCompetitionCard
        } = this.props;
        const subText = this.subTextToRender();

        if (isDashboardCard || isCreateCompetitionCard) {
            return this.dashboardCard();
        }
        if (isOpenCard || isInplayCard) {
            return this.myCompettionCard();
        }
        if (isSettledCard || isNoActionCard) {
            return this.settledCard();
        }
        return null;
    }
}
