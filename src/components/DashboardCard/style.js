import { StyleSheet, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    card: {
        justifyContent: 'space-between',
        overflow: 'hidden',
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 20,
        height: 160
    },
    avatarColumn: {
        flex: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 5
    },
    paddingTop: { paddingTop: 10 },
    nameAndButton: {
        height: 40,
        marginBottom: 5,
        alignItems: 'flex-start'
    },
    versusHolder: {
        justifyContent: 'flex-end'
    },
    versusTeam: { flex: 4, fontSize: 12 },
    smallFont: {
        fontSize: 10
    },
    black: {
        color: '#000'
    },
    feturedBadgeHolder: {
        flex: 0,
        borderRadius: 50,
        overflow: 'hidden',
        paddingHorizontal: 10,
        marginRight: 5,
        backgroundColor: '#ff2458'
    },
    featuredBadge: {
        borderRadius: 50,
        backgroundColor: '#ff2458',
        color: 'white'
    },
    btnHolder: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderRadius: 50
    },
    amount: {
        color: '#ff59a7',
        width: 40,
        textAlign: 'center'
    },
    alignSelfCenter: {
        alignSelf: 'center'
    },
    dashboardAmount: {
        flex: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 9
    },
    rightTeamNameMargin: {
        // marginRight: 40
        marginLeft: 10
        // // marginTop: 5,
        // backgroundColor: 'red',
        // width: width * 0.8
    },
    leftTeamNameMargin: {
        marginLeft: 10
    },

    marginTopFive: {
        marginTop: 5
    },
    paddingTopFive: {
        paddingTop: 5
    },

    // Dashboard Car - start

    mainCardWrap: {
        marginBottom: 2,
        paddingBottom: 2,
        backgroundColor: 'white'
    },
    mainCard: {
        justifyContent: 'space-between',
        overflow: 'hidden',
        backgroundColor: 'white',
        height: 160,
        paddingTop: 10,
        paddingBottom: 5,
        paddingRight: 10,
        paddingLeft: 10
    },

    leftSection: {
        alignItems: 'flex-start'
    },

    rightSection: {
        alignItems: 'flex-end'
    },

    homeTeamName: {
        textAlignVertical: 'center',
        marginLeft: 10,
        marginTop: verticalScale(4),
        width: width * 0.5
    },
    marginTopTen: {
        marginTop: 10
    },
    paddingRightTen: {
        paddingRight: 10
    },
    paddingBottomTen: {
        paddingBottom: 10
    },

    timeMargin: {
        marginTop: verticalScale(25)
    },
    timeMarginFullCard: {
        marginTop: verticalScale(35)
    },
    createCompetitionTimeMargin: {
        marginTop: verticalScale(30)
    },
    amount: {
        textAlign: 'right',
        marginTop: verticalScale(5)
    },

    leagueName: {
        marginBottom: verticalScale(5)
    },

    dashboardBtnWrap: {
        // flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        // alignItems: 'center',
        width: width * 0.3
    },
    dashboardBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderRadius: 50,
        width: '60%'
    },
    greyColor: {
        color: '#BAC5CD'
    },
    avatar: {
        height: 25,
        width: 25
    },
    avatarRound: {
        height: 25,
        width: 25,
        borderRadius: 15
    }

    // Dashboard Car - end
});
