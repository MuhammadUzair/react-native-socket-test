import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { verticalScale } from 'react-native-size-matters';
import BlueBadge from '../../components/BlueBadge';

import { globalStyles } from '../../assets/styles';

import { getTime, getDate, getTimeZone } from '../../utils';

import CustomAvatar from '../CustomAvatar';

export default class CompetitionDetailUserCard extends Component {
    render() {
        const { item, index } = this.props;
        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.fullWidth,
                    styles.card
                ]}
            >
                <View
                    style={[globalStyles.flexHCenter, globalStyles.flexVCenter]}
                >
                    <CustomAvatar
                        rounded
                        small
                        uri={item.avatar}
                        containerStyle={{ marginRight: 10 }}
                    />
                </View>
                <View style={[globalStyles.flexContainer]}>
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexContainer
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexHCenter,
                                globalStyles.flexContainer
                            ]}
                        >
                            <Text
                                style={[
                                    globalStyles.boldFontFace,
                                    globalStyles.blackColor,
                                    globalStyles.baseFontSize
                                ]}
                            >
                                {item.name}
                            </Text>
                        </View>
                    </View>
                    <Text
                        style={[
                            globalStyles.subTextFontSize,
                            globalStyles.baseBlueColor,
                            styles.spreadText
                        ]}
                    >
                        {item.spread_text}
                    </Text>
                    <Text
                        style={[
                            globalStyles.subTextFontSize,
                            globalStyles.greyColor
                        ]}
                    >
                        {getDate(item.pivot.created_at) +
                            ' ' +
                            getTime(item.pivot.created_at) +
                            ' ' +
                            getTimeZone(item.pivot.created_at)}
                    </Text>
                </View>
                <View
                    style={[globalStyles.flexHCenter, globalStyles.flexVCenter]}
                >
                    <BlueBadge
                        textStyle={[
                            globalStyles.baseFontSize,
                            styles.badgeText
                        ]}
                        text="Participant"
                        isGrayColor
                    />

                    <Text
                        style={[
                            styles.ownerAmount,
                            globalStyles.subTextFontSize
                        ]}
                    >
                        ${item.pivot.amount}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = {
    card: {
        paddingHorizontal: 10,
        paddingVertical: verticalScale(10),
        marginVertical: verticalScale(5)
    },
    cardContainer: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: 'white'
    },
    ownerAmount: {
        marginTop: 20,
        textAlign: 'right',
        marginRight: -80
    },
    badgeText: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        textAlign: 'center'
    },
    spreadText: {
        marginTop: -10
    }
};
