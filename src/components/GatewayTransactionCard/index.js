import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';

import { globalStyles, baseBlue } from '../../assets/styles';
import { getDate, retrieveToken } from '../../utils';
import { BASEURL_DEV } from '../../redux/epics/base-url';

export default class GatewayTransactionCard extends Component {
    state = {
        fetching: false,
        message: ''
    };

    onPressCheckStatus = () => {
        this.setState({ fetching: true });
        fetch(`${BASEURL_DEV}/api/v1/view/transactionStatus`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.token}` },
            body: JSON.stringify({
                merchantTransactionID: this.props.item.merchantTransactionID
            })
        })
            .then(res => res.json())
            .then(res => this.setState({ message: res.messages[0] }))
            .finally(() => this.setState({ fetching: false }));
    };

    renderStatus = () => {
        if (this.state.fetching) {
            return <ActivityIndicator size="small" color={baseBlue} />;
        }
        if (this.state.message) {
            return (
                <Text
                    style={[
                        globalStyles.blackColor,
                        globalStyles.subTextFontSize
                    ]}
                >
                    {this.state.message}
                </Text>
            );
        }
        return (
            <TouchableOpacity
                activeOpacity={0.5}
                style={{ paddingVertical: 10 }}
                onPress={this.onPressCheckStatus}
            >
                <Text
                    style={[
                        globalStyles.boldFontFace,
                        globalStyles.baseFontSize
                    ]}
                >
                    Check Status
                </Text>
            </TouchableOpacity>
        );
    };

    render() {
        const { item } = this.props;
        return (
            <View>
                <View style={[styles.card]}>
                    <View style={[globalStyles.flexHorizontal, styles.mb]}>
                        <View style={globalStyles.flexContainer}>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.boldFontFace,
                                        globalStyles.baseBlueColor
                                    ]}
                                >
                                    {item.paymentMethodType}
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.boldFontFace,
                                        globalStyles.highlightPinkColor,
                                        { textAlign: 'right' }
                                    ]}
                                >
                                    ${parseFloat(item.amount).toFixed(2)}
                                </Text>
                            </View>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.blackColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    {item.paymentMethodAccount}
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer,
                                        { textAlign: 'right' }
                                    ]}
                                >
                                    {getDate(item.created_at)}
                                </Text>
                            </View>
                            <View>{this.renderStatus()}</View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    card: {
        padding: 10
    }
};
