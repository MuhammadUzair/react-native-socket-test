import React from 'react';
import { View, Text } from 'react-native';

import { globalStyles } from '../../assets/styles';

const styles = {
    txt: {
        borderRadius: 50,
        fontSize: 10,
        paddingHorizontal: 2,
        color: 'white'
    },
    container: {
        flex: 0,
        borderRadius: 50,
        overflow: 'hidden'
    }
};

export default ({ style, text, textStyle, isGrayColor }) => {
    return (
        <View style={[styles.container, style]}>
            <Text
                style={[
                    styles.txt,
                    isGrayColor
                        ? globalStyles.darkGrayBackgroudColor
                        : globalStyles.blueBackgroudColor,
                    globalStyles.subTextFontSize,
                    textStyle
                ]}
            >
                {text}
            </Text>
        </View>
    );
};
