import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { globalStyles } from '../../assets/styles';
import CustomCheckBox from '../CustomCheckBox';

export default class FriendsListingListItem extends Component {
    onPress = () => {
        this.props.onPress();
    };

    renderStateText = item => {
        return item ? (
            <Text
                style={[
                    styles.checkboxText,
                    globalStyles.boldFontFace,
                    globalStyles.paleRedColor,
                    globalStyles.flexContainer
                ]}
            >
                Cancel
            </Text>
        ) : (
            <Text style={styles.checkboxText}>Add Friend</Text>
        );
    };

    render() {
        const { item } = this.props;

        return (
            <TouchableOpacity
                onPress={this.onPress}
                style={[globalStyles.flexHorizontal, styles.checkbox]}
            >
                <CustomCheckBox
                    uri={item.avatar}
                    style={styles.checkboxStyle}
                    medium
                    checked={item.isAdded}
                    rounded
                />
                <View style={styles.nameHolder}>
                    <Text style={styles.checkboxText}>
                        {item.name && item.name}
                    </Text>
                    <Text style={styles.checkboxText}>
                        {item.username && item.username}
                    </Text>
                </View>
                <View style={styles.stateHolder}>
                    {this.renderStateText(item.isAdded)}
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = {
    unFriendBtn: {
        borderWidth: 1,
        borderColor: '#ff2458',
        padding: 8,
        borderRadius: 20
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    nameHolder: {
        flex: 4
    }
};
