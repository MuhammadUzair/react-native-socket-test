import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

import { globalStyles } from '../../assets/styles';

import CustomCheckBox from '../CustomCheckBox';

export default class OnboardSportCheckbox extends Component {
    onPress = () => {
        this.props.onPress(this.props.index);
    };

    render() {
        const { item, index } = this.props;
        return (
            <View key={index} style={[styles.checkbox, { width: '50%' }]}>
                <TouchableOpacity
                    onPress={this.onPress}
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.flexHCenter
                    ]}
                >
                    <CustomCheckBox
                        title={item.short_code}
                        uri={item.avatar || ''}
                        checked={item.isAdded}
                    />
                    <Text style={styles.checkboxText}>{item.short_code}</Text>
                    <Text style={styles.checkboxText}>{item.name}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    checkbox: {
        paddingVertical: 5,
        paddingHorizontal: 2.5
    },
    checkboxText: {
        color: 'black',
        textAlign: 'center',
        fontSize: 12
    }
};
