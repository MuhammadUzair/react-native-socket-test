import React from 'react';
import { View, StyleSheet } from 'react-native';

import { globalStyles, seperatorColor } from '../../assets/styles';

const seperatorStyle = {
    height: StyleSheet.hairlineWidth,
    backgroundColor: seperatorColor,
    marginVertical: 2.5
};

export default ({ style }) => (
    <View style={[globalStyles.fullWidth, seperatorStyle, style]} />
);
