import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Text,
    ImageBackground,
    StyleSheet,
    TouchableOpacity,
    Image,
    Platform,
    AsyncStorage,
    Alert
} from 'react-native';
import { DrawerItems, SafeAreaView } from 'react-navigation';
import { Icon, Avatar } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager } from 'react-native-fbsdk';
import { connect } from 'react-redux';
import DeviceInfo from 'react-native-device-info';

import Amount from '../Amount';

import { logoutUserAction } from '../../redux/epics/allReset-epic';
import { logoutRequest } from '../../redux/epics/logout-epic';
import { globalStyles } from '../../assets/styles';
import styles from './styles';
import {
    removeFromStorage,
    configureGoogleAuth,
    retrieveToken,
    retrieveNotificationTokenAndOS
} from '../../utils';
import store from '../../redux/store';

import CustomAvatar from '../CustomAvatar';

const icons = [
    <Icon name="home" type="font-awesome" size={16} color="#c8d0ef" />,
    <Image
        resizeMethod="resize"
        resizeMode="contain"
        style={{ tintColor: 'white', height: 16, width: 25, left: -4.5 }}
        source={require('../../assets/images/competition.png')}
    />,
    <Icon name="sports-club" type="entypo" size={16} color="#c8d0ef" />,
    <Icon name="group" type="font-awesome" size={16} color="#c8d0ef" />,
    <Icon name="group" type="material-icon" size={16} color="#c8d0ef" />,
    <Icon name="wallet" type="entypo" size={16} color="#c8d0ef" />,
    <Icon name="user-circle-o" type="font-awesome" size={16} color="#c8d0ef" />,
    <Icon
        name="notifications"
        type="material-icon"
        size={16}
        color="#c8d0ef"
    />,
    <Icon name="cog" type="font-awesome" size={16} color="#c8d0ef" />,
    <Icon name="logout" type="material-community" size={16} color="#c8d0ef" />
];

const hackyRoutes = [
    'Home',
    'Competitions',
    'MySports',
    'MyGroups',
    'MyWallet',
    'MyFriends',
    'Logout',
    'Notifications',
    'ReferFriend'
];

const routeMap = {
    Home: 'DashboardScreen',
    Competitions: 'MyCompetitions',
    MySports: 'MySportsListing',
    MyGroups: 'GroupScreen',
    MyWallet: 'MyWalletScreen',
    MyFriends: 'MyFriendsList',
    Notifications: 'NotificationScreen',
    ReferFriend: 'ReferFriend',
    Logout: 'Login'
};

/** ACTUAL COMPONENT */

class Drawer extends Component {
    state = {
        showLogoutAlert: false
    };

    // static getDerivedStateFromProps(nextProps, nextState) {
    // console.log('getDerivedStateFromProps ', nextProps);
    // if (nextProps.logout.code >= 200 && nextProps.logout.code < 300) {
    //     AsyncStorage.removeItem('token');
    //     return store.dispatch(logoutUserAction());
    // } else {
    // if (nextState.showLogoutAlert) {
    //     let msg = nextProps.logout.messages
    //         ? nextProps.messages[0]
    //         : ' when logout.';
    //     Alert.alert('There was a problem', msg);
    //     return {
    //         showLogoutAlert: false
    //     };
    // }
    // }

    //     return null;
    // }

    renderItem = (scene, alert, isNotificationActive) => {
        // return <View style={styles.iconContainer}>{icons[scene.index]}</View>;
        return (
            <View style={styles.iconContainer}>
                {this.renderIcon(scene.index, isNotificationActive)}
            </View>
        );
    };
    renderIcon = (index, isNotificationActive) => {
        switch (index) {
            case 0:
                return (
                    <Icon
                        name="home"
                        type="font-awesome"
                        size={16}
                        color="#c8d0ef"
                    />
                );
            case 1:
                return (
                    <Image
                        resizeMethod="resize"
                        resizeMode="contain"
                        style={{
                            tintColor: 'white',
                            height: 16,
                            width: 25,
                            left: -4.5
                        }}
                        source={require('../../assets/images/competition.png')}
                    />
                );
            case 2:
                return (
                    <Icon
                        name="sports-club"
                        type="entypo"
                        size={16}
                        color="#c8d0ef"
                    />
                );
            case 3:
                return (
                    <Icon
                        name="group"
                        type="font-awesome"
                        size={16}
                        color="#c8d0ef"
                    />
                );
            case 4:
                return (
                    <Icon
                        name="group"
                        type="material-icon"
                        size={16}
                        color="#c8d0ef"
                    />
                );
            case 5:
                return (
                    <Icon
                        name="wallet"
                        type="entypo"
                        size={16}
                        color="#c8d0ef"
                    />
                );
            case 6:
                return (
                    <Icon
                        name="user-circle-o"
                        type="font-awesome"
                        size={16}
                        color="#c8d0ef"
                    />
                );
            case 7:
                return (
                    <View style={styles.iconWrap}>
                        <Icon
                            name="notifications"
                            type="material-icon"
                            size={16}
                            color="#c8d0ef"
                        />
                        {isNotificationActive && (
                            <View style={styles.dotIconWrap}>
                                <Icon
                                    name="circle"
                                    type="font-awesome"
                                    iconStyle={styles.dotIcon}
                                />
                            </View>
                        )}
                    </View>
                );
            case 8:
                return (
                    <Icon
                        name="user-circle-o"
                        type="font-awesome"
                        size={16}
                        color="#c8d0ef"
                    />
                );
            case 9:
                return (
                    <Icon
                        name="logout"
                        type="material-community"
                        size={16}
                        color="#c8d0ef"
                    />
                );
        }
    };
    render() {
        const { props } = this;
        const { navigation } = this.props;

        return (
            <ImageBackground
                style={[globalStyles.fullScreen]}
                source={require('../../assets/images/drawer.png')}
            >
                <SafeAreaView style={[globalStyles.flexContainer]}>
                    <View style={[styles.header]}>
                        <TouchableOpacity
                            onPress={() => navigation.closeDrawer()}
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexVBottom,
                                styles.cross
                            ]}
                        >
                            <Icon
                                name="close"
                                type="evilicon"
                                color="rgba(255,255,255,.5)"
                            />
                        </TouchableOpacity>
                        <View style={globalStyles.flexHorizontal}>
                            <CustomAvatar
                                xsmall={Platform.OS === 'ios'}
                                small={Platform.OS === 'android'}
                                rounded
                                containerStyle={styles.image}
                                uri={
                                    props.user.data !== undefined &&
                                    props.user.data.avatar
                                }
                            />
                            <View
                                style={[
                                    globalStyles.flexContainer,
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween
                                ]}
                            >
                                <View
                                    style={[
                                        globalStyles.flexVCenter,
                                        { flex: 3 }
                                    ]}
                                >
                                    <Text
                                        numberOfLines={1}
                                        ellipsizeMode="tail"
                                        style={styles.name}
                                    >
                                        {props.user.data &&
                                            props.user.data.name}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        ellipsizeMode="tail"
                                        style={styles.email}
                                    >
                                        {props.user.data &&
                                            props.user.data.email}
                                    </Text>
                                </View>
                                <Amount drawer navigation={navigation} />
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 4 }}>
                        <DrawerItems
                            {...props}
                            onItemPress={async ({ route }) => {
                                if (route.routeName === 'Logout') {
                                    let token = '';
                                    let device = {
                                        device_token: '',
                                        device_type: '',
                                        device_id: DeviceInfo.getUniqueID(),
                                        requestFrom: 'Mobile',
                                        token: ''
                                    };
                                    await AsyncStorage.getItem('token').then(
                                        getToken => {
                                            const storeToken = JSON.parse(
                                                getToken
                                            );
                                            if (
                                                storeToken !== null &&
                                                storeToken.data
                                            ) {
                                                token = storeToken.data;
                                                device.token = storeToken.data;
                                            }
                                        }
                                    );

                                    await retrieveNotificationTokenAndOS().then(
                                        res => {
                                            if (res && res.token && res.os) {
                                                device.device_token = res.token;
                                                device.device_type = res.os;
                                            }
                                        }
                                    );

                                    try {
                                        await removeFromStorage('token');
                                        await configureGoogleAuth();
                                        const user = await GoogleSignin.currentUserAsync();
                                        user && (await GoogleSignin.signOut());
                                        await LoginManager.logOut();
                                    } catch (e) {
                                        console.log('err ', e);
                                    } finally {
                                        this.setState({
                                            showLogoutAlert: true
                                        });
                                        store.dispatch(
                                            logoutRequest({
                                                token: token,
                                                device: device
                                            })
                                        );
                                        store.dispatch(logoutUserAction());
                                    }
                                }
                                if (hackyRoutes.includes(route.routeName)) {
                                    navigation.navigate(
                                        routeMap[route.routeName]
                                    );
                                } else {
                                    navigation.navigate(route.routeName);
                                }
                                route.routeName === 'Home' &&
                                    navigation.closeDrawer();
                            }}
                            getLabel={scene => {
                                return (
                                    <View
                                        style={[
                                            styles.item,
                                            globalStyles.flexHorizontal,
                                            globalStyles.flexHCenter
                                        ]}
                                    >
                                        {this.renderItem(
                                            scene,
                                            scene.index % 3 === 0,
                                            props.getNotification.isNotification
                                        )}
                                        <Text style={styles.itemText}>
                                            {props.getLabel(scene)}
                                        </Text>
                                    </View>
                                );
                            }}
                        />
                    </View>
                    <View
                        style={[
                            globalStyles.flexContainer,
                            globalStyles.flexHorizontal
                        ]}
                    >
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate('CreateCompetitions')
                            }
                            style={[
                                globalStyles.flexVCenter,
                                styles.buttonHolder
                            ]}
                            activeOpacity={0.5}
                        >
                            <LinearGradient
                                colors={['#4286f4', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.buttonStyle
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        styles.buttonText
                                    ]}
                                >
                                    Create Competition
                                </Text>
                                <Icon
                                    size={20}
                                    color="#FFF"
                                    name="arrow-forward"
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                        {/* <TouchableOpacity
                        onPress={() => navigation.navigate('ReferFriend')}
                        style={[globalStyles.flexVCenter, styles.referButton]}
                        activeOpacity={0.5}
                    >
                        <Text
                            style={[
                                styles.referText,
                                globalStyles.baseFontSize
                            ]}
                        >
                            Refer A Friend
                        </Text>
                    </TouchableOpacity> */}
                    </View>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

const mapStateToProps = ({ user, getNotification, logout }) => ({
    user,
    getNotification,
    logout
});

export default connect(mapStateToProps)(Drawer);
