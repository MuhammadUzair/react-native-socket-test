import { StyleSheet, Platform } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { os } from '../../utils';

export default StyleSheet.create({
    header: {
        paddingHorizontal: 20,
        marginBottom: '1%'
    },
    cross: {
        paddingTop: 10,
        paddingLeft: 10,
        marginBottom: '2%'
    },
    image: {
        marginRight: 10
    },
    item: {
        paddingVertical: verticalScale(14),
        paddingHorizontal: 20,
        width: '100%'
    },
    itemText: {
        color: 'white'
    },
    iconContainer: {
        marginRight: 20,
        width: 16
    },
    name: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16
    },
    email: {
        width: '90%',
        fontSize: 12,
        color: 'white'
    },
    amount: {
        fontSize: 14,
        color: 'white',
        textAlign: 'right'
    },
    amountContainer: {
        alignItems: 'flex-end'
    },
    buttonHolder: {
        paddingHorizontal: 10,
        // marginLeft: 20,
        flex: 3
    },
    buttonStyle: {
        paddingVertical: verticalScale(15),
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row'
    },
    buttonText: {
        fontSize: 14
    },
    alert: {
        position: 'absolute',
        top: 0,
        right: 0,
        borderRadius: 50,
        backgroundColor: 'red',
        width: 10,
        height: 10
    },
    referButton: {
        flex: 1.5
    },
    referText: {
        textAlign: 'center',
        color: 'white'
    },
    referText: {
        textAlign: 'center',
        color: 'white'
    },
    iconWrap: {
        flexDirection: 'row'
    },
    dotIconWrap: {
        marginLeft: -8
    },
    dotIcon: {
        color: 'red',
        fontSize: 9
    }
});
