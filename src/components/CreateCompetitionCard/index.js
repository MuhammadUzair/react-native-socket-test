import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements';

import CustomAvatar from '../CustomAvatar';
import BlueBadge from '../BlueBadge';

import { globalStyles } from '../../assets/styles';
import { os, getTime, getDate } from '../../utils';

export default class CreateCompetitionCard extends Component {
    onPress = () => {
        this.props.onPress(this.props.item);
    };
    render() {
        const { item } = this.props;
        const time = item.time_left.split(':');
        return (
            <View style={styles.flatListItemHolder}>
                <View style={styles.flatListContainer}>
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexHCenter,
                            styles.images
                        ]}
                    >
                        <CustomAvatar
                            small
                            title={item.competitor.short_code}
                            uri={item.competitor.avatar}
                            rounded
                        />
                        <Text
                            style={[
                                globalStyles.blackColor,
                                globalStyles.subTextFontSize,
                                { marginHorizontal: 4 }
                            ]}
                        >
                            VS
                        </Text>
                        <CustomAvatar
                            small
                            title={item.team.short_code}
                            uri={item.team.avatar}
                            rounded
                        />
                    </View>
                    <View
                        style={[
                            globalStyles.whiteBackgroudColor,
                            styles.flatList
                        ]}
                    >
                        <View style={{ padding: 0 }}>
                            <Text style={globalStyles.boldFontFace}>
                                <Text style={globalStyles.baseBlueColor}>
                                    {item.competitor.short_code}{' '}
                                </Text>
                                VS
                                <Text style={globalStyles.baseBlueColor}>
                                    {` ${item.team.short_code}`}
                                </Text>
                            </Text>
                            <Text
                                style={[
                                    globalStyles.highlightPinkColor,
                                    globalStyles.boldFontFace,
                                    globalStyles.subTextFontSize
                                ]}
                            >
                                {item.payload.name}, {item.payload.city}{' '}
                                {item.payload.country}
                            </Text>

                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween
                                ]}
                            >
                                <View style={[globalStyles.flexHorizontal]}>
                                    <Icon
                                        name="unlock-alt"
                                        type="font-awesome"
                                        size={12}
                                    />
                                    <Text
                                        style={[
                                            globalStyles.blackColor,
                                            globalStyles.subTextFontSize
                                        ]}
                                    >
                                        {getTime(item.start_date_time)}
                                    </Text>
                                </View>
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    {getDate(item.start_date_time)}
                                </Text>
                            </View>

                            <View style={styles.hairLineDivider} />

                            <View>
                                <Text
                                    style={[
                                        styles.noteFontFace,
                                        styles.widthSixty,
                                        globalStyles.flexHCenter
                                    ]}
                                >
                                    Competition Start in
                                </Text>
                                <View
                                    style={[
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.flexHCenter
                                    ]}
                                >
                                    <View
                                        style={{
                                            flex: 0,
                                            borderRadius: 50,
                                            overflow: 'hidden'
                                        }}
                                    >
                                        <BlueBadge
                                            textStyle={[
                                                globalStyles.baseFontSize,
                                                { paddingHorizontal: 5 }
                                            ]}
                                            text={
                                                item.days_left +
                                                ' D ' +
                                                time[0] +
                                                ' H ' +
                                                time[1] +
                                                ' M Left'
                                            }
                                        />
                                    </View>
                                    <TouchableOpacity
                                        onPress={this.onPress}
                                        activeOpacity={0.5}
                                        style={[
                                            globalStyles.flexVBottom,
                                            globalStyles.flexHCenter,
                                            styles.btnHolder
                                        ]}
                                    >
                                        <LinearGradient
                                            colors={['#4d6bc2', '#3695e3']}
                                            start={{ x: 0, y: 0 }}
                                            end={{ x: 1, y: 0 }}
                                            style={[
                                                globalStyles.flexHorizontal,
                                                globalStyles.flexHCenter,
                                                globalStyles.flexSpaceBetween,
                                                styles.buttonStyle
                                            ]}
                                        >
                                            <Text
                                                style={[
                                                    globalStyles.fontFace,
                                                    globalStyles.whiteColor,
                                                    globalStyles.boldFontFace,
                                                    globalStyles.baseFontSize
                                                ]}
                                            >
                                                Post It
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    flatListItemHolder: {
        width: '100%',
        padding: 5,
        paddingHorizontal: os() ? 3 : 5
    },
    flatListContainer: {
        flex: 0
    },
    flatList: {
        top: -10,
        paddingTop: 25,
        padding: 10,
        borderRadius: 10
    },
    images: {
        marginLeft: 5,
        zIndex: 3,
        top: 10
    },
    noteFontFace: {
        fontSize: 8,
        color: 'grey'
    },
    hairLineDivider: {
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 0.5,
        paddingTop: 10,
        marginBottom: 10
    },
    widthSixty: {
        width: '60%'
    },
    btnHolder: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 8,
        borderRadius: 50
    }
};
