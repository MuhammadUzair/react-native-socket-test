import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { globalStyles } from '../../assets/styles';
import CustomAvatar from '../CustomAvatar';

export default class FriendsListingListItem extends Component {
    onPress = () => {
        this.props.onPress(this.props.item, 'deleteFriend');
    };
    render() {
        const { item } = this.props;

        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.flexContainer,
                    styles.checkbox
                ]}
            >
                <CustomAvatar
                    uri={item.avatar}
                    containerStyle={styles.checkboxStyle}
                    medium
                    rounded
                />
                <View style={styles.nameHolder}>
                    <Text
                        style={[styles.checkboxText, globalStyles.boldFontFace]}
                    >
                        {item.name}
                    </Text>
                    <Text style={styles.checkboxText}>
                        {item.username && item.username}
                    </Text>
                </View>
                <TouchableOpacity
                    onPress={this.onPress}
                    style={styles.unFriendBtn}
                >
                    <Text
                        style={[
                            globalStyles.boldFontFace,
                            globalStyles.paleRedColor,
                            { backgroundColor: 'transparent' }
                        ]}
                    >
                        {item.status == 'Accepted' ? 'Unfriend' : 'Cancel'}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    unFriendBtn: {
        borderWidth: 1,
        borderColor: '#ff2458',
        padding: 8,
        borderRadius: 20
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    nameHolder: {
        flex: 4
    }
};
