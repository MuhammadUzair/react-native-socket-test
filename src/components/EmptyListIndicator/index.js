import React, { Component } from 'react';
import { View, ActivityIndicator, Text } from 'react-native';
import { globalStyles, baseBlue } from '../../assets/styles';

export default ({ message, showIndicator, style }) => {
    if (showIndicator)
        return (
            <View
                style={[
                    globalStyles.flexVCenter,
                    globalStyles.flexHCenter,
                    style,
                ]}
            >
                <ActivityIndicator size="small" color={baseBlue} />
            </View>
        );
    return (
        <View style={[style]}>
            <Text style={[globalStyles.greyColor]}>{message}</Text>
        </View>
    );
};
