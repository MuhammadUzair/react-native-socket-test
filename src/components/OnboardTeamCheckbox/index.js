import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { globalStyles } from '../../assets/styles';
import CustomCheckBox from '../CustomCheckBox';

export default class OnboardTeamCheckbox extends Component {
    renderStateText = item => {
        return item ? (
            <Text style={[styles.checkboxText, globalStyles.highlightColor]}>
                Added
            </Text>
        ) : (
            <Text style={styles.checkboxText}>Add to favorite</Text>
        );
    };

    onPress = () => {
        this.props.onPress(this.props.item.id);
    };

    render() {
        const { item, index, isAdded } = this.props;
        return (
            <TouchableOpacity
                onPress={this.onPress}
                key={index}
                style={[globalStyles.flexHorizontal, styles.checkbox]}
            >
                <CustomCheckBox
                    style={styles.checkboxStyle}
                    checked={isAdded}
                    title={item.short_code}
                    uri={item.avatar}
                />
                <View style={styles.nameHolder}>
                    <Text style={styles.checkboxText}>{item.name}</Text>
                    <Text
                        style={[
                            styles.checkboxText,
                            globalStyles.highlightColor,
                        ]}
                    >
                        {item.address ? item.address.city : ''}
                    </Text>
                </View>
                <View style={styles.stateHolder}>
                    {this.renderStateText(isAdded)}
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = {
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center',
    },
    checkboxText: {
        color: 'black',
        fontSize: 12,
    },
    checkboxStyle: {
        marginRight: 10,
    },
    buttonStyle: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row',
        width: 60 + '%',
    },
    buttonText: {
        fontSize: 18,
        paddingLeft: 15,
    },
    nameHolder: {
        flex: 4,
    },
    stateHolder: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    scrollView: {
        flex: 0,
    },
};
