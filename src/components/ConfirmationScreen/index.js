import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Alert } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { globalStyles } from '../../assets/styles';
import styles from './styles';
import CompetitionStyles from '../../screens/createCompetition/styles';
import BlueHeader from '../BlueHeader';
import CustomCheckbox from '../../components/CustomCheckBox';
import {
    getTime,
    getDate,
    getTimeZone,
    renderSubTextWithChangeSign
} from '../../utils';
import CustomAvatar from '../CustomAvatar';

class ConfirmationScreen extends Component {
    renderMyTeamRow = () => {
        const {
            item,
            spreadTitle,
            teamStatus,
            selectedTeam,
            isDashBoard
        } = this.props;
        let title = '',
            uri = '',
            name = '',
            spreadMsg = renderSubTextWithChangeSign(item.spread, true),
            condition = false;

        if (isDashBoard) {
            condition = teamStatus && !teamStatus.homeTeam ? true : false;
        } else {
            condition = selectedTeam !== 'competitor' ? true : false;
        }

        if (condition) {
            title =
                item && item.competitor && item.competitor.short_code
                    ? item.competitor.short_code
                    : '';

            uri =
                item && item.competitor && item.competitor.avatar
                    ? item.competitor.avatar
                    : '';
            name =
                item && item.competitor && item.competitor.name
                    ? ' ' + item.competitor.name + ' '
                    : '';
        } else {
            title =
                item && item.team && item.team.short_code
                    ? item.team.short_code
                    : '';

            uri = item && item.team && item.team.avatar ? item.team.avatar : '';
            name =
                item && item.team && item.team.name
                    ? ' ' + item.team.name + ' '
                    : '';
        }

        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.flexSpaceBetween,
                    globalStyles.flexContainer,
                    CompetitionStyles.teamRow
                ]}
            >
                <View style={[globalStyles.flexHorizontal]}>
                    <CustomAvatar
                        size="small"
                        checked={false}
                        title={title}
                        uri={uri}
                    />
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexVertical
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                styles.paddingLeftFive
                            ]}
                        >
                            {name}
                        </Text>
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                globalStyles.baseBlueColor,
                                CompetitionStyles.confirmScreenSpread
                            ]}
                        >
                            {name && spreadMsg ? name + ' ' + spreadMsg : null}
                        </Text>
                    </View>
                </View>
                <View>
                    <Text
                        style={[
                            globalStyles.baseFontSize,
                            globalStyles.paleRedColor
                        ]}
                    >
                        {item && item.remaining_amount
                            ? '$' + item.remaining_amount
                            : null}
                    </Text>
                </View>
            </View>
        );
    };

    render() {
        const {
            title,
            navigation,
            onPressBackButton,
            onPressSubmit,
            item,
            enteredAmount,
            serviceFee,
            totalAmount,
            isSubmitBtnPressed,
            selectedTeam,
            isDashBoard,
            teamStatus
        } = this.props;
        return (
            <ScrollView>
                <BlueHeader
                    title={title}
                    navigation={navigation}
                    onPress={() => onPressBackButton()}
                />

                <View style={[globalStyles.whiteBackgroudColor]}>
                    <View
                        style={[
                            styles.confrirmationHeadingRow,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <View>
                            <Text
                                style={[
                                    styles.topSpacing,
                                    globalStyles.subTextFontSize,
                                    globalStyles.boldFontFace
                                ]}
                            >
                                Confirmation
                            </Text>
                            <Text
                                style={[
                                    globalStyles.subTextFontSize,
                                    globalStyles.greyColor,
                                    styles.paddingTopFive
                                ]}
                            >
                                Your Choosen
                            </Text>
                        </View>
                    </View>

                    <View
                        style={[globalStyles.flexHorizontal, styles.teamRow]}
                        opacity={
                            isDashBoard
                                ? !teamStatus.awayTeam
                                    ? 0.5
                                    : 1
                                : selectedTeam == 'competitor'
                                ? 0.5
                                : 1
                        }
                    >
                        <View>
                            <CustomCheckbox
                                size="small"
                                checked={false}
                                uri={
                                    item &&
                                    item.competitor &&
                                    item.competitor.avatar
                                        ? item.competitor.avatar
                                        : ''
                                }
                                title={
                                    item &&
                                    item.competitor &&
                                    item.competitor.short_code
                                        ? item.competitor.short_code
                                        : ''
                                }
                                style={styles.avatarMarigin}
                            />
                        </View>
                        <View style={styles.alignSelfCenter}>
                            <Text style={[globalStyles.baseFontSize]}>
                                {item && item.competitor && item.competitor.name
                                    ? ' ' + item.competitor.name + ' '
                                    : null}
                            </Text>
                        </View>
                    </View>

                    <View
                        style={[globalStyles.flexHorizontal, styles.teamRow]}
                        opacity={
                            isDashBoard
                                ? !teamStatus.homeTeam
                                    ? 0.5
                                    : 1
                                : selectedTeam == 'team'
                                ? 0.5
                                : 1
                        }
                    >
                        <View>
                            <CustomCheckbox
                                size="small"
                                checked={false}
                                title={
                                    item && item.team && item.team.short_code
                                        ? item.team.short_code
                                        : ''
                                }
                                uri={
                                    item && item.team && item.team.avatar
                                        ? item.team.avatar
                                        : ''
                                }
                                style={styles.avatarMarigin}
                            />
                        </View>
                        <View style={styles.alignSelfCenter}>
                            <Text style={[globalStyles.baseFontSize]}>
                                {item && item.team && item.team.name
                                    ? ' ' + item.team.name + ' '
                                    : null}
                            </Text>
                        </View>
                    </View>

                    <View
                        style={[
                            globalStyles.hairLineUnderBorder,
                            styles.teamBorder,
                            styles.teamRow
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.subTextFontSize,
                                globalStyles.greyColor,
                                styles.marginTop,
                                styles.paddingBottom
                            ]}
                        >
                            {getDate(item.start_date_time) +
                                ' ' +
                                getTime(item.start_date_time) +
                                ' ' +
                                getTimeZone(item.start_date_time)}
                        </Text>
                    </View>

                    {/* My teams - start */}
                    <View style={[globalStyles.flexHorizontal, styles.teamRow]}>
                        <Text
                            style={[
                                globalStyles.subTextFontSize,
                                globalStyles.boldFontFace,
                                styles.marginTopTen
                            ]}
                        >
                            My Teams & Spread
                        </Text>
                    </View>

                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            styles.marginTopTen
                        ]}
                    >
                        {this.renderMyTeamRow()}
                    </View>

                    {/* My teams - end */}

                    {/* Amount section - start */}

                    <View
                        style={[
                            styles.rowContainer,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.subTextFontSize,
                                globalStyles.boldFontFace
                            ]}
                        >
                            Amount
                        </Text>
                        <View style={[globalStyles.flexHorizontal]}>
                            <View style={[globalStyles.flexHorizontal]}>
                                <Text style={[globalStyles.baseFontSize]}>
                                    $ {enteredAmount}
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View
                        style={[
                            styles.totalAmountSection,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <View style={globalStyles.flexVertical}>
                            <View style={globalStyles.flexHorizontal}>
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        styles.totalHeading,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    Service Charges:
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.highlightPinkColor,
                                        styles.textAlignRight,
                                        styles.amountWidth
                                    ]}
                                >
                                    {serviceFee * 100}%
                                </Text>
                            </View>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.marginTopTen
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        styles.totalHeading,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    Total Amount:
                                </Text>

                                <Text
                                    style={[
                                        globalStyles.highlightPinkColor,
                                        styles.amountWidth,
                                        styles.textAlignRight
                                    ]}
                                >
                                    {totalAmount && totalAmount}
                                </Text>
                            </View>
                        </View>
                    </View>

                    {/* Amount Section - end */}

                    {/* Footer  - start*/}
                    <View
                        style={[
                            styles.rowContainer,
                            globalStyles.hairLineUnderBorder,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <View style={globalStyles.flexVTop}>
                            <TouchableOpacity
                                onPress={() => this.props.onPressBackButton()}
                                activeOpacity={0.5}
                                style={[styles.marginTop]}
                            >
                                <LinearGradient
                                    colors={['#F5F5F5', '#F5F5F5']}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={[
                                        globalStyles.flexHCenter,
                                        globalStyles.flexSpaceBetween,
                                        styles.backButtonWrap
                                    ]}
                                >
                                    <Text
                                        style={[
                                            styles.footerButonText,
                                            globalStyles.greyColor
                                        ]}
                                    >
                                        Back
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>

                        <View style={globalStyles.flexVBottom}>
                            <TouchableOpacity
                                disabled={isSubmitBtnPressed ? true : false}
                                onPress={() => onPressSubmit()}
                                activeOpacity={0.5}
                                style={[styles.marginTop]}
                            >
                                <LinearGradient
                                    colors={
                                        isSubmitBtnPressed
                                            ? ['#b2bec3', '#b2bec3']
                                            : ['#4d6bc2', '#3695e3']
                                    }
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={[
                                        globalStyles.flexHCenter,
                                        globalStyles.flexSpaceBetween,
                                        styles.backButtonWrap
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.whiteColor,
                                            styles.footerButonText
                                        ]}
                                    >
                                        Confirm
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* Footer  - end */}
                </View>
            </ScrollView>
        );
    }
}

export default ConfirmationScreen;
