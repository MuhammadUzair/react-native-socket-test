import { StyleSheet, Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

import { os } from '../../utils';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
    whiteBG: {
        backgroundColor: 'white'
    },
    heading: {
        fontSize: scale(24)
        // height: verticalScale(30)
    },
    flatListItemHolder: {
        width: '100%',
        padding: 0,
        paddingHorizontal: os() ? 3 : 5
    },
    flatListContainer: {
        flex: 0,
        borderRadius: 10
    },

    flatList: {
        paddingTop: 15,
        padding: 10,
        borderRadius: 10,
        top: -7.5
    },

    imagess: {
        marginLeft: 5,
        zIndex: 3,
        top: 7.5
    },

    postGameCard: {
        width: '100%',
        height: '10%',
        top: verticalScale(40),
        position: 'absolute'
        // marginLeft: os() ? 10 : 15
    },

    noteFontFace: {
        fontSize: 8,
        color: 'grey'
    },

    roundTimer: {
        alignItems: 'center',
        top: verticalScale(0),
        padding: 2
    },

    hairLineDivider: {
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 0.5,
        paddingTop: verticalScale(10),
        marginBottom: verticalScale(10)
    },

    widthSixty: {
        width: '60%'
    },

    widthForty: {
        width: '40%'
    },

    paddingBottom: {
        marginVertical: verticalScale(5)
    },

    paddingTop: {
        paddingTop: 10
    },

    rowContainer: {
        paddingVertical: os() ? verticalScale(15) : verticalScale(12),
        paddingHorizontal: os() ? scale(18) : scale(10),
        borderBottomColor: '#bdc3c7'
    },

    borderRadius: {
        borderRadius: 10
    },

    borderRadiusTop: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },

    roundedAmount: {
        borderRadius: 50,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#88c3ff',
        padding: 5,
        width: 60,
        alignSelf: 'flex-end'
    },

    inputText: {
        width: 80,
        height: 20,
        padding: scale(10),
        paddingBottom: 0,
        paddingTop: 7,
        color: 'black',
        marginLeft: -20,
        borderWidth: 0,
        textAlign: 'right'
    },

    circularPosition: {
        position: 'absolute'
    },

    bottomSpacing: {
        marginBottom: verticalScale(10)
    },

    topSpacing: {
        marginTop: verticalScale(10)
    },
    leftSpacing: {
        marginLeft: scale(13)
    },

    rightSpacing: {
        marginRight: scale(13)
    },
    textCenter: {
        textAlign: 'center'
    },

    bigPrimaryButtonText: {
        fontSize: scale(18)
    },

    bigPrimaryButton: {
        paddingVertical: verticalScale(15),
        paddingHorizontal: scale(20),
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },

    marginTop: {
        marginTop: verticalScale(10)
    },
    paddingBottom: {
        paddingBottom: verticalScale(10)
    },

    amountTextSize: {
        fontSize: scale(18),
        width
    },

    cancelCenterButton: {
        marginTop: verticalScale(25),
        fontSize: scale(15)
    },

    btnHolder: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },

    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 8,
        borderRadius: 50
    },
    paddedContainer: {
        marginBottom: verticalScale(4)
    },
    searchHolder: {
        paddingHorizontal: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        marginVertical: 5
    },
    searchBar: {
        height: 40,
        backgroundColor: 'white'
    },
    paddingAll: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5
    },
    marginTopPlaybtn: {
        marginTop: verticalScale(5)
    },
    rowHeading: {
        fontSize: scale(14)
    },
    spreadInputHeading: {
        position: 'absolute',
        top: 0,
        right: 0
    },
    spreadInputWrap: {
        marginTop: 20
    },
    gradientHeader: {
        padding: 15
    },
    gradientHeaderFont: {
        fontSize: scale(12),
        color: 'white'
    },
    locationSection: {
        justifyContent: 'flex-end',
        width: width * 0.3,
        marginTop: 20
    },
    locationText: {
        // textAlign: 'right'
        lineHeight: 20
    },
    marginTopTen: {
        marginTop: 10
    },
    amountWidth: {
        width: 60
    },
    totalAmountSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 20
        // paddingBottom: 10
    },
    totalHeading: {
        width: 150,
        textAlign: 'right',
        marginRight: 20
    },
    smallDropDown: {
        width: 130
    },
    backButtonWrap: {
        paddingVertical: verticalScale(8),
        paddingHorizontal: scale(18),
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    teamNameButtonWrap: {
        paddingVertical: verticalScale(8),
        paddingHorizontal: scale(10),
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    avatarMarigin: {
        marginRight: 10,
        paddingTop: 10
    },
    footerButonText: {
        fontSize: scale(13)
    },
    leagueSpreadMarign: {
        marginTop: -10
    },
    textAlignRight: {
        textAlign: 'right'
    },
    selectTeamError: {
        paddingTop: 10
    },
    errorAmount: {
        width: width * 0.42,
        marginTop: -20,
        paddingBottom: 10,
        textAlign: 'left'
    },
    totalAmountText: {
        width: 60,
        height: 40
    },
    chooseTeamSection: {
        width: width * 0.5
    },

    // Confrirmation screen styles   - start
    confrirmationHeadingRow: {
        paddingHorizontal: os() ? scale(18) : scale(10)
    },
    teamRow: {
        paddingHorizontal: os() ? scale(18) : scale(10)
    },
    teamBorder: {
        borderBottomColor: '#bdc3c7'
    },
    alignSelfCenter: {
        alignSelf: 'center'
    },
    paddingTopFive: {
        paddingTop: 5
    },
    confirmScreenSpread: {
        paddingTop: 5,
        paddingLeft: 4
    },
    paddingLeftFive: {
        paddingLeft: 5
    }
    // Confrirmation screen styles   - end
});
