import React, { Component } from 'react';

import { generateAndPrintErrorMessage } from '../utils';

export default (ComponentToWrap, propsToCheck) => {
    class AlertOnError extends Component {
        componentDidUpdate(prevProps, prevState) {
            let errorShown = false;
            for (let { propName, actionOnFailure } of propsToCheck) {
                if (!errorShown) {
                    errorShown = generateAndPrintErrorMessage(
                        this.props[propName],
                        typeof this.props[actionOnFailure] === 'function'
                            ? this.props[actionOnFailure]
                            : null
                    );
                } else {
                    break;
                }
            }
        }

        render() {
            return <ComponentToWrap {...this.props} />;
        }
    }
    AlertOnError.navigationOptions = ComponentToWrap.navigationOptions;
    return AlertOnError;
};
