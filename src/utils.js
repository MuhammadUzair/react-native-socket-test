import {
    Platform,
    AsyncStorage,
    Alert,
    PermissionsAndroid,
    PushNotificationIOS
    // Clipboard
} from 'react-native';
import FBSDK, {
    AccessToken,
    LoginManager,
    GraphRequest,
    GraphRequestManager
} from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import moment from 'moment';
import PushNotification from 'react-native-push-notification';

import store from './redux/store';
import { socialSignup } from './redux/epics/social-signup-epic';
import { fetchFBFriends } from './redux/epics/fb-friends-epic';
import { BASEURL_DEV } from './redux/epics/base-url';

export const os = () => Platform.OS === 'ios';

export const validateDOB = date => {
    const match = /([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})/g.exec(date);
    if (match !== null) {
        const day = parseInt(match[2]);
        const month = parseInt(match[1]);
        const year = parseInt(match[3]);
        if (day <= 31 && day >= 1 && (month <= 12 && month >= 1)) {
            return true;
        }
    }
    return false;
};

export const storeToStorage = (key, data) => {
    try {
        return AsyncStorage.setItem(key, data);
    } catch (error) {
        return error;
    }
};

export const removeFromStorage = key => {
    try {
        return AsyncStorage.removeItem(key);
    } catch (error) {
        return error;
    }
};

export const loginFB = device => {
    getLocation(({ coords: { longitude, latitude, altitude } }) => {
        LoginManager.logInWithReadPermissions([
            'public_profile',
            'email',
            'user_friends'
        ])
            .then(function(result) {
                if (!result.isCancelled) {
                    AccessToken.getCurrentAccessToken().then(
                        ({ accessToken: token, userID: id }) => {
                            const infoRequest = new GraphRequest(
                                '/me',
                                {
                                    accessToken: token,
                                    httpMethod: 'GET',
                                    version: 'v3.0',
                                    parameters: {
                                        fields: {
                                            string:
                                                'email, name, picture, friends'
                                        }
                                    }
                                },
                                (
                                    err,
                                    {
                                        email,
                                        picture: {
                                            data: { url }
                                        },
                                        name
                                    }
                                ) => {
                                    const data = {
                                        social_data: {
                                            id,
                                            token,
                                            email,
                                            avatar: url,
                                            name,
                                            birthday: '',
                                            gender: '',
                                            device_token: '',
                                            device_type: '',
                                            requestFrom: 'Mobile'
                                        },
                                        social_type: 'facebook',
                                        longitude,
                                        latitude,
                                        altitude
                                    };
                                    if (device && device.device_token) {
                                        data.device_token = device.device_token;
                                    }
                                    if (device && device.device_type) {
                                        data.device_type = device.device_type;
                                    }

                                    if (device && device.device_id) {
                                        data.device_id = device.device_id;
                                    }
                                    // complianceCheck(token);
                                    store.dispatch(socialSignup(data));
                                }
                            );
                            new GraphRequestManager()
                                .addRequest(infoRequest)
                                .start();
                        }
                    );
                }
            })
            .catch(error => {
                Alert.alert('Failure', error.message);
            });
    });
};

export const configureGoogleAuth = () => {
    return GoogleSignin.configure({
        iosClientId:
            '924289564409-voo7sm772np3ud86jfp8bk6dikfkbgm9.apps.googleusercontent.com',
        webClientId:
            '924289564409-1h8smq94ae5i1ciji3ua3r762om2cvnc.apps.googleusercontent.com',
        scopes: [
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile'
        ]
    });
};

export const loginGoogle = async device => {
    try {
        await GoogleSignin.hasPlayServices({
            autoResolve: true
        });
        await configureGoogleAuth();
        const user = await GoogleSignin.signIn().catch(e => console.log(e));
        if (user) {
            getLocation(({ coords: { longitude, latitude, altitude } }) => {
                const {
                    accessToken: token,
                    email,
                    id,
                    photo: avatar,
                    name
                } = user;
                const data = {
                    social_type: 'google',
                    social_data: {
                        token,
                        email,
                        id,
                        avatar: avatar !== null ? avatar : '',
                        name,
                        birthday: '',
                        gender: ''
                    },
                    device_token: '',
                    device_type: '',
                    device_id: '',
                    requestFrom: 'Mobile',
                    longitude,
                    latitude,
                    altitude
                };

                if (device && device.device_token) {
                    data.device_token = device.device_token;
                }
                if (device && device.device_type) {
                    data.device_type = device.device_type;
                }
                if (device && device.device_id) {
                    data.device_id = device.device_id;
                }
                //complianceCheck(token);
                store.dispatch(socialSignup(data));
            });
        }
    } catch (e) {
        Alert.alert('There was a problem', e.message);
    }
};

export const onboard = (data, navigation) => {
    if (data.login_yet === 0 || data.login_yet === undefined)
        navigation.navigate('MySports');
    if (data.login_yet === 1)
        navigation.navigate('MyTeams', { disableBack: true });
    if (data.login_yet === 2)
        navigation.navigate('MyFriends', { disableBack: true });
    if (data.login_yet >= 3)
        navigation.navigate('Dashboard', { disableBack: true });
};

export const addBasicHeaders = token => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('device', 'Mobile');
    token && headers.append('Authorization', `Bearer ${token}`);
    return headers;
};

export const retrieveToken = () => {
    return AsyncStorage.getItem('token').then(data => {
        const token = JSON.parse(data);
        if (token !== null && token.data) return token.data;
        else throw new Error('token not found on the phone.');
    });
};

export const importFriends = token => {
    LoginManager.logInWithReadPermissions([
        'public_profile',
        'email',
        'user_friends'
    ])
        .then(function(result) {
            if (!result.isCancelled) {
                AccessToken.getCurrentAccessToken().then(data => {
                    const infoRequest = new GraphRequest(
                        '/me',
                        {
                            accessToken: data.accessToken,
                            httpMethod: 'GET',
                            version: 'v3.0',
                            parameters: {
                                fields: {
                                    string: 'email, name, picture, friends'
                                }
                            }
                        },
                        (err, res) => {
                            store.dispatch(
                                fetchFBFriends({
                                    token,
                                    friends:
                                        res.friends !== undefined
                                            ? res.friends.data
                                            : []
                                })
                            );
                        }
                    );
                    new GraphRequestManager().addRequest(infoRequest).start();
                });
            }
        })
        .catch(error => {
            Alert.alert('Failure', error.message);
        });
};

export const generateErrorMessage = messages =>
    messages.reduce((msg, item) => `${msg}\n${item}`);

export const generateAndPrintErrorMessage = (obj, callback) => {
    if (obj.code >= 300) {
        const msg = obj.messages.reduce((msg, item) => `${msg}\n${item}`);
        Alert.alert('There was a problem', msg);
        typeof callback === 'function' && callback();
        return true;
    }
    if (typeof obj === 'string' && obj !== 'inprogress' && obj !== '') {
        Alert.alert('There was a problem', obj);
        typeof callback === 'function' && callback();
        return true;
    }
    return false;
};

export const getDate = date => {
    return moment(date).format('MMM DD, YYYY');
};

export const getTime = time => {
    return moment(time).format('hh:mm A');
};

export const renderSpread = spread => {
    if (spread < 0) return `+${Math.abs(spread)}`;
    if (spread > 0) return `-${spread}`;
    return 0;
};

export const renderSpreadWithPlusSign = spread => {
    if (spread > 0) return `+${spread}`;
    return spread;
};

export const renderSubText = (spread, inverse) => {
    if (inverse) {
        if (spread > 0) return `to win or lose by less than ${spread}`;
        if (spread < 0) return `to win by more than ${Math.abs(spread)}`;
    } else {
        if (spread < 0)
            return `to win or lose by less than ${Math.abs(spread)}`;
        if (spread > 0) return `to win by more than ${spread}`;
    }
    return 'to win the match';
};
export const renderSubTextWithChangeSign = (spread, inverse) => {
    if (inverse) {
        if (spread > 0) return `to win or lose by less than ${spread}`;
        if (spread < 0) return `to win by more than ${spread}`;
    } else {
        if (spread < 0) return `to win or lose by less than ${spread}`;
        if (spread > 0) return `to win by more than ${spread}`;
    }
    return 'to win the match 0';
};

export const permissionToGetGeolocation = async () => {
    return await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );
};

export const getPermissionToGetGeolocation = async (title, message) => {
    try {
        const permission = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: 'Hello there',
                message:
                    "We need your geolocation, just to make sure that you're from US"
            }
        );
        if (permission === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
        } else {
            return false;
        }
    } catch (e) {
        Alert.alert('There was a problem', e.message);
    }
};

export const retrieveNotificationTokenAndOS = () => {
    return AsyncStorage.getItem('pushNotification').then(data => {
        const tokenAndOS = JSON.parse(data);
        if (tokenAndOS !== null) return tokenAndOS;
        else console.log('notification token And OS not found on the phone.');
    });
};

export const getLocation = (success, error) => {
    navigator.geolocation.getCurrentPosition(
        success,
        error
            ? error
            : e => {
                  Alert.alert(
                      'There was a problem',
                      e.code === 2
                          ? 'Please turn on your location services'
                          : e.code === 1
                          ? 'Please allow PostItPlayIt to access your ' +
                            'location to Create Competitions, Participate ' +
                            'in Competitions and Add/Withdraw Funds ' +
                            'from your Wallet.'
                          : 'Something went wrong, please try again.'
                  );
              },
        {
            enableHighAccuracy: false,
            timeout: 20000,
            maximumAge: 5000
        }
    );
};

export const changeRoute = async (
    navigation,
    token,
    notification,
    fetchSpecificCompetition
) => {
    // console.log('changeRoute name ', notification);
    let name = '';
    let detail = '';
    if (Platform.OS == 'ios') {
        if (
            notification.data &&
            notification.data.extraPayLoad &&
            notification.data.extraPayLoad.name
        ) {
            name = notification.data.extraPayLoad.name;
        }
        if (
            notification.data &&
            notification.data.extraPayLoad &&
            notification.data.extraPayLoad.detail
        ) {
            detail = notification.data.extraPayLoad.detail;
        }
    } else {
        name = notification.name;
        detail = parseInt(notification.detail);
    }
    let storeName = await AsyncStorage.getItem('notificationName');
    let storeDetail = await AsyncStorage.getItem('notificationDetail');

    if (storeName) {
        name = storeName;
    }
    if (storeDetail) {
        detail = parseInt(storeDetail);
    }
    // console.log('changeRoute ', name);
    // console.log('name ', notification);
    // console.log('detail ', detail);
    await AsyncStorage.removeItem('notificationName');
    await AsyncStorage.removeItem('notificationDetail');

    switch (name) {
        case 'pipi-friend-request-accepted':
            return navigation.push('MyFriendsList');
        case 'pipi-friend-request-sent':
            return navigation.navigate('PendingFriendList', {
                token
            });
        case 'pipi-competition-created':
            if (detail) {
                return fetchSpecificCompetition({
                    token: token,
                    competitionId: detail,
                    isDashboard: true
                });
            } else {
                return null;
            }

        case 'pipi-join-group':
            return navigation.navigate('GroupScreen', {
                isMyGroup: true
            });
        case 'pipi-group-member-added':
            return navigation.navigate('GroupScreen', {
                isMyGroup: true
            });
        case 'pipi-competition-settled':
            return navigation.navigate('MyCompetition', {
                showCompetitions: 'Settled'
            });
        case 'pipi-competition-participated':
            if (detail) {
                return navigation.navigate('CompetitionDetail', {
                    competitionId: detail,
                    isDashboard: true
                });
            } else {
                return null;
            }
    }
};

export const runLocationValidations = async (
    dispatchAction,
    locationNotNecessary = false
) => {
    if (os() || locationNotNecessary) {
        dispatchAction();
    } else {
        const havePermission = await permissionToGetGeolocation();
        if (havePermission) {
            dispatchAction();
        } else {
            const gotPermission = await getPermissionToGetGeolocation(
                'Hello there',
                'We need your location to authorize you'
            );
            if (gotPermission) {
                dispatchAction();
            } else {
                Alert.alert(
                    'There was a problem',
                    'Access to location was denied'
                );
            }
        }
    }
};

export const complianceCheck = token => {
    return new Promise((resolve, reject) => {
        runLocationValidations(() => {
            getLocation(
                ({ coords: { longitude, latitude, altitude } }) => {
                    resolve(
                        fetch(
                            `${BASEURL_DEV}/api/v1/view/my-competitions/check-compliances?longitude=${longitude}&latitude=${latitude}`,
                            { headers: { Authorization: `Bearer ${token}` } }
                        )
                            .then(res => res.json())
                            .then(
                                res =>
                                    res.data.includes('LL-BLOCK') ||
                                    res.data.includes('LL-FAIL')
                            )
                    );
                },
                () =>
                    reject(
                        new Error(
                            "Couldn't get the location of your device, make sure your location is turned on and the app has proper permissions to access your location"
                        )
                    )
            );
        });
    });
};

export const PushNotificationControler = async (
    navigation,
    activeNotification,
    fetchSpecificCompetition
) => {
    const token = await retrieveToken();
    PushNotification.configure({
        onNotification: async notification => {
            // Clipboard.setString(JSON.stringify(notification));
            // console.log('notification ', notification);

            // Store  Name and detail property in AsyncStorage because of libary issue - Start
            if (Platform.OS == 'ios') {
                if (
                    notification.data &&
                    notification.data.extraPayLoad &&
                    notification.data.extraPayLoad.name
                ) {
                    await storeToStorage(
                        'notificationName',
                        notification.data.extraPayLoad.name
                    );
                }
                if (
                    notification.data &&
                    notification.data.extraPayLoad &&
                    notification.data.extraPayLoad.detail
                ) {
                    let detail = notification.data.extraPayLoad.detail.toString();
                    await storeToStorage('notificationDetail', detail);
                }
            } else {
                if (notification.name) {
                    await storeToStorage('notificationName', notification.name);
                }
                if (notification.detail) {
                    await storeToStorage(
                        'notificationDetail',
                        notification.detail
                    );
                }
            }
            // Store  Name and detail property in AsyncStorage because of libary issue - End

            // Cancel Notifications - start
            AsyncStorage.getItem('notificationIds').then(res => {
                if (res) {
                    let allNotificationIds = [];
                    let cancellNotification = false;
                    allNotificationIds = JSON.parse(res);
                    for (let i = 0; i < allNotificationIds.length; i++) {
                        if (allNotificationIds[i].id === notification.id) {
                            cancellNotification = true;
                            PushNotification.cancelLocalNotifications({
                                id: notification.id
                            });
                            if (notification.foreground) {
                                return true;
                            }
                        }
                    }
                    if (!cancellNotification) {
                        let notification_id;
                        if (Platform.OS == 'ios') {
                            notification_id = notification.data.notificationId;
                        } else {
                            notification_id = notification.id;
                        }
                        allNotificationIds.push({ id: notification_id });
                        storeToStorage(
                            'notificationIds',
                            JSON.stringify(allNotificationIds)
                        );
                    }
                } else {
                    let notificationId = [];
                    if (Platform.OS == 'ios') {
                        notificationId.push({
                            id: notification.data.notificationId
                        });
                    } else {
                        notificationId.push({ id: notification.id });
                    }
                    storeToStorage(
                        'notificationIds',
                        JSON.stringify(notificationId)
                    );
                }
            });
            // Cancel Notifications - end

            // Show Notification in Android and IOS - start
            if (notification.badge) {
                let badge =
                    Platform.os == 'ios'
                        ? notification.badge
                        : parseInt(notification.badge);
                PushNotification.setApplicationIconBadgeNumber(badge);
            }

            if (notification.userInteraction) {
                // console.log('userInteraction ', notification);

                await changeRoute(
                    navigation,
                    token,
                    notification,
                    fetchSpecificCompetition
                );

                return true;
            } else {
                // Show Notification in Android and IOS - start
                let message;
                let title;
                if (Platform.OS == 'android') {
                    message = notification.message || ' ';
                    title = notification.title || 'Notification';
                    // console.log('Platform.OS android ', notification);
                    if (notification.hasOwnProperty('userInteraction')) {
                        PushNotification.localNotificationSchedule({
                            title: title,
                            message: message,
                            date: new Date(Date.now()),
                            id: notification.id
                        });
                    } else {
                        // Fix libarary issue in android - start
                        // In some cases (foreground) , this libary delete property in notification.
                        // In this block we get deleted values from AsyncStorage.

                        let storeName = await AsyncStorage.getItem(
                            'notificationName'
                        );
                        let storeDetail = await AsyncStorage.getItem(
                            'notificationDetail'
                        );
                        if (storeName) {
                            notification.name = storeName;
                            notification.detail = storeDetail;
                            await changeRoute(
                                navigation,
                                token,
                                notification,
                                fetchSpecificCompetition
                            );
                        }

                        // Fix libarary issue in android - end
                    }
                } else {
                    if (!notification.foreground) {
                        PushNotification.localNotificationSchedule({
                            title: notification.data.extraPayLoad.title,
                            message: notification.data.extraPayLoad.message,
                            date: new Date(Date.now()),
                            id: notification.data.notificationId
                        });
                    }
                }
                // Show Notification in Android and IOS - end
            }

            // App in foreground  - start
            if (notification.foreground) {
                let badge = parseInt(notification.badge);
                activeNotification({
                    count: badge ? badge : 0
                });
            }
            // App in foreground  - end

            notification.finish(PushNotificationIOS.FetchResult.NoData);
        }
    });
};

export const getSelectedTeam = item => {
    let status = { homeTeam: false, awayTeam: false };
    if (item.my_team) {
        if (item[item.my_team].name === item.awayTeam.name) {
            status.awayTeam = true;
            return status;
        }
        if (item[item.my_team].name === item.homeTeam.name) {
            status.homeTeam = true;
            return status;
        }
    }
    return status;
};

export const getDashboardSelectedTeam = item => {
    let status = { homeTeam: false, awayTeam: false };
    if (item.competitor.id === item.awayTeam.id) {
        status.awayTeam = true;
        return status;
    }
    if (item.competitor.id === item.homeTeam.id) {
        status.homeTeam = true;
        return status;
    }
    return status;
};

export const getTimeZone = date => {
    if (date) {
        let zone = new Date(date).toString().substr(25, 3);
        return zone;
    }
    return new Date().toString().substr(25, 3);
};
