import { StyleSheet } from 'react-native';
import { os } from '../../utils';

export default StyleSheet.create({
    heading: {
        fontSize: 24
    },

    postGameCard: {
        height: 50,
        width: '100%',
        position: 'absolute',
        top: 40,
        marginLeft: 20
    },

    noteFontFace: {
        fontSize: 8,
        color: 'grey'
    },

    paddingBottom: {
        paddingBottom: 10
    },

    paddingTop: {
        paddingTop: 20
    },

    rowContainer: {
        paddingVertical: os() ? 18 : 12,
        paddingHorizontal: 20,
        borderBottomColor: '#bdc3c7'
    },

    borderRadius: {
        borderRadius: 10
    },

    borderRadiusTop: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },

    roundedAmount: {
        borderRadius: 50,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#88c3ff',
        padding: 5,
        width: 60,
        textAlign: 'center',
        alignSelf: 'flex-end'
    },

    inputText: {
        width: 70,
        height: '100%',
        padding: 10,
        paddingBottom: 0,
        paddingTop: 0,
        backgroundColor: '#EEE',
        color: 'black',
        marginLeft: -20,
        position: 'relative',
        zIndex: 0
    },

    width: {
        width: '60%'
    },

    circularPosition: {
        position: 'absolute'
    },

    bottomSpacing: {
        marginBottom: 12
    },

    topSpacing: {
        marginTop: 12
    },

    bigPrimaryButtonText: {
        fontSize: 18
    },

    bigPrimaryButton: {
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },

    marginTop: {
        marginTop: 10
    },

    amountTextSize: {
        fontSize: 18
    },

    cancelCenterButton: {
        marginTop: 25,
        fontSize: 15
    },

    SmallCards: {
        marginLeft: 0,
        paddingRight: 8
    },
    CircularCard: {
        width: 35,
        height: 35
    },
    gradientCard: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35,
        padding: 8
    }
});
