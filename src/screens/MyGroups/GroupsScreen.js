import React, { Component, Fragment } from 'react';

import { View, Text, FlatList, TouchableOpacity } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import { Icon } from 'react-native-elements';

import { Divider } from 'react-native-elements';

import { os, retrieveToken, generateAndPrintErrorMessage } from '../../utils';
import CompetitionStyles from './styles';
import { globalStyles } from '../../assets/styles';

import SmallCards from '../../components/SmallCards';
import GroupCard from '../../components/Groups';
import MyGroupsCard from '../../components/MyGroups';
import PendingGroupRequest from '../../components/MyGroupsPendingRequest';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import DrawerIconButton from '../../components/DrawerIconButton';
import OverlayIndicator from '../../components/OverlayIndicator';
import HeaderRightIcon from '../../components/HeaderRightIcon';

export default class GroupsScreen extends Component {
    state = {
        isFetching: false,
        group: 'all',
        buttonStates: [
            { id: 'all', text: 'All Groups', state: true },
            { id: 'mygroups', text: 'My Groups', state: false },
            { id: 'pendinggroups', text: 'Pending Requests', state: false }
        ],
        onDetailScreen: false
    };

    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingLeft: os() ? 10 : 15
            },
            headerLeft: (
                <DrawerIconButton onPress={() => navigation.openDrawer()} />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    static getDerivedStateFromProps(nextProps, nextState) {
        generateAndPrintErrorMessage(
            nextProps.myGroups,
            nextProps.resetMyGroups
        );
        generateAndPrintErrorMessage(nextProps.groupJoined);
        generateAndPrintErrorMessage(nextProps.groupAction);
        generateAndPrintErrorMessage(nextProps.groupDeleted);
        return { isFetching: false };
    }

    componentDidMount() {
        retrieveToken().then(token => {
            this.setState({ token }, () => {
                const { group } = this.state;
                const { fetchMyGroups } = this.props;
                fetchMyGroups({ token, group });
            });
        });
        if (
            this.props.navigation.state &&
            this.props.navigation.state.params &&
            this.props.navigation.state.params.isMyGroup
        ) {
            this.onPressCard(1);
        }
    }

    onRefresh = () => {
        const { token, group } = this.state;
        const { refreshMyGroups } = this.props;
        refreshMyGroups({ token, group });
    };

    viewDetail = item => {
        const { navigation } = this.props;
        this.setState({ onDetailScreen: true }, () => {
            navigation.push('GroupInfo', { item });
        });
    };

    deleteGroup = item => {
        const { token } = this.state;
        const { deleteGroup } = this.props;
        deleteGroup({ token, groupId: item.group_id });
    };

    onPressCard = index => {
        const { buttonStates } = this.state;
        const { myGroups } = this.props;
        if (!buttonStates[index].state) {
            const newButtonStates = buttonStates.map(({ text, id }) => ({
                id,
                text,
                state: false
            }));
            newButtonStates[index].state = !newButtonStates[index].state;
            this.setState(
                {
                    buttonStates: newButtonStates,
                    group: newButtonStates[index].id
                },
                () => {
                    const { group, token } = this.state;
                    const { fetchMyGroups } = this.props;
                    myGroups.data[group] === undefined &&
                        fetchMyGroups({ token, group });
                }
            );
        }
    };

    plusPressed = () => {
        this.props.navigation.push('GroupCreateScreen');
    };

    onScrollEnd = () => {
        const { myGroups, fetchMyGroups } = this.props;
        const { group, token, isFetching } = this.state;
        if (myGroups.data[group].next_page && !isFetching) {
            this.setState({ isFetching: true }, () => {
                fetchMyGroups({
                    token,
                    group,
                    nextPage: myGroups.data[group].next_page
                });
            });
        }
    };

    allGroupDetail = item => {
        this.props.navigation.push('GroupInfo', { item, screen: 'all' });
    };

    myGroupDetail = item => {
        const leaveGroupFunction = this.leaveGroup.bind(this, item);
        this.props.navigation.push('GroupInfo', {
            item,
            screen: 'mygroups',
            leaveGroupFunction
        });
    };

    editGroup = item => {
        this.props.navigation.push('GroupEditScreen', { item });
    };

    leaveGroup = item => {
        const { token } = this.state;
        const { leaveGroup, refreshMyGroups } = this.props;
        leaveGroup({ token, groupId: item.group_id });
    };

    joinGroup = item => {
        const { token } = this.state;
        const { joinGroup } = this.props;
        joinGroup({ token, groupId: item.group_id });
    };

    acceptGroupRequest = item => {
        const { token } = this.state;
        const { acceptGroupRequest } = this.props;
        acceptGroupRequest({
            token,
            ownerId: item.owner_id,
            groupId: item.group_id
        });
    };

    rejectGroupRequest = item => {
        const { token } = this.state;
        const { rejectGroupRequest } = this.props;
        rejectGroupRequest({
            token,
            ownerId: item.owner_id,
            groupId: item.group_id
        });
    };

    render() {
        let CardToRender;
        const {
            myGroups,
            groupJoined,
            groupLeft,
            groupDeleted,
            groupAction
        } = this.props;
        const { group, buttonStates } = this.state;
        if (buttonStates[0].state) CardToRender = GroupCard;
        if (buttonStates[1].state) CardToRender = MyGroupsCard;
        if (buttonStates[2].state) CardToRender = PendingGroupRequest;

        let groupsToShow = [];
        let totalMembers = 0;

        if (myGroups.data[group]) {
            groupsToShow = myGroups.data[group].groups;
            totalMembers = myGroups.data[group].total_records;
        }
        return (
            <Fragment>
                <View style={[globalStyles.mainContainer]}>
                    <View style={[CompetitionStyles.paddingBottom]}>
                        <Text
                            style={[
                                globalStyles.highlightBlueColor,
                                globalStyles.headingFontFace,
                                CompetitionStyles.heading
                            ]}
                        >
                            My Groups
                        </Text>
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                globalStyles.blackColor,
                                globalStyles.boldFontFace
                            ]}
                        >
                            Pick a new team for contest and play against your
                            friends all session.
                        </Text>
                    </View>

                    <View style={[globalStyles.flexHorizontal]}>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexContainer
                            ]}
                        >
                            {this.state.buttonStates.map(
                                ({ text, state }, i) => {
                                    return (
                                        <SmallCards
                                            key={i}
                                            text={text}
                                            style={[
                                                CompetitionStyles.SmallCards
                                            ]}
                                            containerStyle={{
                                                minWidth: 0,
                                                paddingHorizontal: 5,
                                                paddingVertical: 3
                                            }}
                                            selected={state}
                                            onPress={this.onPressCard.bind(
                                                this,
                                                i
                                            )}
                                            first={true}
                                        />
                                    );
                                }
                            )}
                        </View>

                        <TouchableOpacity
                            style={[
                                CompetitionStyles.CircularCard,
                                { marginTop: -5 }
                            ]}
                            onPress={this.plusPressed}
                            activeOpacity={0.5}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[CompetitionStyles.gradientCard]}
                            >
                                <Icon
                                    name="plus"
                                    type="feather"
                                    color="#fff"
                                    size={18}
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={[
                            {
                                flexDirection: 'row',
                                paddingBottom: 8,
                                paddingTop: 5
                            },
                            globalStyles.flexSpaceBetween,
                            globalStyles.fullWidth
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.subTextFontSize,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            You have {totalMembers} groups
                        </Text>
                        <Icon
                            name="dots-three-vertical"
                            type="entypo"
                            size={10}
                        />
                    </View>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: 145 }}
                        ItemSeparatorComponent={() => <Divider />}
                        data={groupsToShow}
                        onEndReachedThreshold={0.1}
                        onEndReached={this.onScrollEnd}
                        // refreshing={myGroups.status === 'refreshing'}
                        refreshing={false}
                        onRefresh={this.onRefresh}
                        keyExtractor={(item, index) =>
                            `id: ${item.id}, index: ${index}`
                        }
                        ListEmptyComponent={
                            <EmptyListIndicator
                                // showIndicator={myGroups.status === 'inprogress'}
                                message={
                                    group === 'pendinggroups'
                                        ? 'You have no pending requests'
                                        : 'No groups available'
                                }
                            />
                        }
                        renderItem={({ item, index }) => (
                            <CardToRender
                                item={item}
                                index={index + 1}
                                navigation={this.props.navigation}
                                viewDetail={
                                    buttonStates[0].state
                                        ? this.allGroupDetail
                                        : this.myGroupDetail
                                }
                                editGroup={this.editGroup}
                                leaveGroup={this.leaveGroup}
                                joinGroup={this.joinGroup}
                                rejectGroupRequest={this.rejectGroupRequest}
                                acceptGroupRequest={this.acceptGroupRequest}
                                deleteGroup={this.deleteGroup}
                            />
                        )}
                    />
                    {/* {groupJoined === 'inprogress' ||
                groupLeft === 'inprogress' ||
                groupDeleted === 'inprogress' ||
                groupAction === 'inprogress' ||
                myGroups.status === 'inprogress' ? (
                    <OverlayIndicator />
                ) : null} */}
                </View>
                {groupJoined === 'inprogress' ||
                groupLeft === 'inprogress' ||
                groupDeleted === 'inprogress' ||
                groupAction === 'inprogress' ||
                myGroups.status === 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </Fragment>
        );
    }
}
