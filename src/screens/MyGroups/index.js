import { connect } from 'react-redux';

import MyGroups from './GroupsScreen';
import {
    fetchMyGroups,
    refreshMyGroups,
    resetMyGroups
} from '../../redux/epics/fetch-my-groups-epic';

import { joinGroup } from '../../redux/epics/join-group-epic';
import {
    acceptGroupRequest,
    rejectGroupRequest
} from '../../redux/epics/group-action-epic';

import {
    leaveGroup,
    resetLeaveGroup
} from '../../redux/epics/leave-group-epic';

import { deleteGroup } from '../../redux/epics/delete-group-epic';

const mapStateToProps = ({
    myGroups,
    groupLeft,
    groupJoined,
    groupAction,
    groupDeleted
}) => ({
    myGroups,
    groupLeft,
    groupJoined,
    groupAction,
    groupDeleted
});

const mapDispatchToProps = dispatch => ({
    fetchMyGroups: data => dispatch(fetchMyGroups(data)),
    refreshMyGroups: data => dispatch(refreshMyGroups(data)),
    resetMyGroups: () => dispatch(resetMyGroups()),
    leaveGroup: data => dispatch(leaveGroup(data)),
    resetLeaveGroup: () => dispatch(resetLeaveGroup()),
    joinGroup: data => dispatch(joinGroup(data)),
    acceptGroupRequest: data => dispatch(acceptGroupRequest(data)),
    rejectGroupRequest: data => dispatch(rejectGroupRequest(data)),
    deleteGroup: data => dispatch(deleteGroup(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyGroups);
