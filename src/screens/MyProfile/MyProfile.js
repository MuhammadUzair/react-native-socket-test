import React, { Component, Fragment } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Alert,
    Keyboard,
    Platform
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import { retrieveToken } from '../../utils';
import CustomAvatar from '../../components/CustomAvatar';

import { globalStyles, greyColor } from '../../assets/styles';
import styles from './styles';

import { os } from '../../utils';
import DrawerIconButton from '../../components/DrawerIconButton';
import OverlayIndicator from '../../components/OverlayIndicator';
import HeaderRightIcon from '../../components/HeaderRightIcon';
export default class MyProfile extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20
            },
            headerLeft: (
                <DrawerIconButton onPress={() => navigation.openDrawer()} />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            user: {
                name: '',
                email: '',
                username: '',
                newPassword: '',
                confirmPassword: '',
                date_of_birth: '',
                state: 'Select State',
                homeTown: 'Select Home Town',
                avatar: '',
                gender: 'Select Gender',
                phone_no: ''
            },
            isEdit: false,
            isFromDatePickerVisible: false,
            genderOption: [
                {
                    name: 'Male'
                },
                {
                    name: 'Female'
                }
            ],
            stateOption: {
                status: '',
                data: [
                    {
                        name: 'States not found'
                    }
                ]
            },
            homeTownOption: {
                status: '',
                data: [
                    {
                        name: 'Home Towns not found'
                    }
                ]
            },
            dateOfBirthPrivacyOption: [
                {
                    name: 'Private'
                },
                {
                    name: 'Public'
                }
            ],
            genderPrivacyOption: [
                {
                    name: 'Private'
                },
                {
                    name: 'Public'
                }
            ],
            phoneNumberPrivacyOption: [
                {
                    name: 'Private'
                },
                {
                    name: 'Public'
                }
            ],
            emailPrivacyOption: [
                {
                    name: 'Private'
                },
                {
                    name: 'Public'
                }
            ],
            statePrivacyOption: [
                {
                    name: 'Private'
                },
                {
                    name: 'Public'
                }
            ],
            homeTownPrivacyOption: [
                {
                    name: 'Private'
                },
                {
                    name: 'Public'
                }
            ],
            token: '',
            dateOfBirthPrivacy: 'Privacy',
            genderPrivacy: 'Privacy',
            phoneNumberPrivacy: 'Privacy',
            emailPrivacy: 'Privacy',
            statePrivacy: 'Privacy',
            homeTownPrivacy: 'Privacy',
            showUserImage: true,
            userAvatarFile: '',
            userAvatar: { uri: '' },
            showLoader: false,
            showSaveButton: false,
            datePicker: new Date(),
            isProfileSubmit: false,
            getUser: false
        };

        this.didBlurSubscription = this.props.navigation.addListener(
            'didFocus',
            payload => {
                this.fetchUser();
            }
        );
    }

    fetchUser = async () => {
        //  Call states and cites  api start
        await retrieveToken().then(token => {
            this.setState({ token, showSaveButton: false });
            this.props.fetchSate({ token: token });
            this.props.fetchUser(token);
        });
        //  Call states and cites end
    };
    componentWillUnmount() {
        // Remove the listener navigation
        this.didBlurSubscription.remove();
    }
    componentDidMount() {
        const { user } = this.props;

        if (user.data.date_of_birth) {
            if (Platform.OS == 'ios') {
                let dob = user.data.date_of_birth;
                let momentDate = moment(dob, 'DD-MM-YYYY').format('MM-DD-YYYY');
                this.setState({ datePicker: new Date(momentDate) });
            } else {
                let dob = user.data.date_of_birth;
                let momentDate = moment(dob, 'DD-MM-YYYY').format('MM-DD-YYYY');
                let formatted = new Date(momentDate);
                let getDate = formatted.getDate();
                let getMonth = formatted.getMonth() + 1;
                let getYear = formatted.getFullYear();
                this.setState({
                    datePicker: new Date(
                        getMonth + '-' + getDate + '-' + getYear
                    )
                });
            }
        }
    }

    componentDidUpdate() {
        const { profile, uploadImage } = this.props;
        const { token } = this.state;

        // Show messages Start
        if (profile.status && profile.status == 'ERROR') {
            Alert.alert(
                'There was a problem ',
                this.props.profile.messages[0],
                [
                    {
                        text: 'OK',
                        onPress: () => {
                            this.setState({
                                isProfileSubmit: false
                            });
                        }
                    }
                ],
                { cancelable: false }
            );
            this.props.resetProfileSubmit();
            return null;
        }
        if (
            (profile.status && profile.status == 'OK') ||
            profile == 'Unexpected end of JSON input'
        ) {
            Alert.alert(
                '',
                profile.messages && profile.messages[0]
                    ? profile.messages[0]
                    : profile,
                [
                    {
                        text: 'OK',
                        onPress: () => {
                            this.setState({
                                isEdit: !this.state.isEdit,
                                showLoader: false,
                                isProfileSubmit: false
                            });
                            this.props.fetchUser(token);
                        }
                    }
                ],
                { cancelable: false }
            );
            this.props.resetProfileSubmit();
            return null;
        }
        if (uploadImage.status && uploadImage.status == 'Success') {
            this.onChangeText('avatar', uploadImage.data);
            this.profileSubmit(uploadImage.data);
            this.props.resetUploadImageApi();

            return null;
        }
        if (
            (uploadImage.status && uploadImage.status == 'ERROR') ||
            uploadImage == 'Unexpected end of JSON input'
        ) {
            this.profileSubmit();
            this.props.resetUploadImageApi();
        }
        // Show messages End
    }

    static getDerivedStateFromProps(newProps, prevState) {
        let states = prevState.stateOption;
        let cities = prevState.homeTownOption;
        let user = { ...prevState.user };
        let showLoader = prevState.showLoader;
        let datePicker = prevState.datePicker;
        let getUser = prevState.getUser;
        if (newProps.state.data && newProps.state.data.length > 0) {
            states = newProps.state;
        }
        if (newProps.cities.data && newProps.cities.data.length > 0) {
            cities = newProps.cities;
        }
        if (newProps.cities.data.state_id) {
            this.onStateChange(newProps.user.data.state_id);
        }

        if (
            newProps.user.code == 200 &&
            newProps.user.data &&
            !prevState.isProfileSubmit
        ) {
            user.avatar = newProps.user.data.avatar || '';
            user.name = newProps.user.data.name || '';
            user.email = newProps.user.data.email || '';
            user.username = newProps.user.data.username || '';
            user.phone_no = newProps.user.data.phone_no || '';
            user.newPassword = '';
            user.confirmPassword = '';
            user.date_of_birth = newProps.user.data.date_of_birth || '';
            user.state = newProps.user.data.state_id || 'Select State';
            user.homeTown = newProps.user.data.city || 'Select Home Town';
            user.gender = newProps.user.data.gender || 'Select Gender';
            if (newProps.user.data.date_of_birth) {
                if (Platform.OS == 'ios') {
                    let dob = newProps.user.data.date_of_birth;
                    let momentDate = moment(dob, 'DD-MM-YYYY').format(
                        'MM-DD-YYYY'
                    );
                    // console.log('momentDate -', momentDate);
                    datePicker = new Date(momentDate);
                } else {
                    let dob = newProps.user.data.date_of_birth;
                    let momentDate = moment(dob, 'DD-MM-YYYY').format(
                        'MM-DD-YYYY'
                    );
                    let formatted = new Date(momentDate);
                    let getDate = formatted.getDate();
                    let getMonth = formatted.getMonth() + 1;
                    let getYear = formatted.getFullYear();

                    // console.log('momentDate ', momentDate);
                    // console.log('momentDate type', typeof momentDate);
                    // console.log('momentDate -- ', new Date(momentDate));

                    datePicker = new Date(
                        getMonth + '-' + getDate + '-' + getYear
                    );
                }
            }
        }

        if (
            newProps.profile.status &&
            (newProps.profile.status == 'ERROR' ||
                newProps.profile.status == 'OK')
        ) {
            showLoader = false;
        }
        if (newProps.user.code && prevState.getUser) {
            showLoader = false;
            getUser = false;
        }

        return {
            stateOption: states,
            homeTownOption: cities,
            user,
            showLoader,
            datePicker,
            getUser
        };
    }
    _showToDatePicker = () =>
        this.setState({
            isToDatePickerVisible: true
        });

    _hideToDatePicker = () => this.setState({ isToDatePickerVisible: false });
    _handleToDatePicked = date => {
        if (
            new Date(this.state.user.date_of_birth).toString().substr(0, 15) ==
            new Date(date).toString().substr(0, 15)
        ) {
            this._hideToDatePicker();
            return true;
        }
        this.setState({ showSaveButton: true });
        let formatted = new Date(date);
        let getDate = formatted.getDate();
        let getMonth = formatted.getMonth() + 1;
        let getYear = formatted.getFullYear();
        let newDate =
            getDate.toString() +
            '-' +
            getMonth.toString() +
            '-' +
            getYear.toString();
        let datePicker = new Date();
        if (Platform.OS == 'ios') {
            datePicker = new Date(date);
        } else {
            let getUserDob = new Date(date);
            datePicker = getUserDob.getTime();
            datePicker = new Date(date);
        }
        this.setState({ datePicker: datePicker });

        this.onChangeText('date_of_birth', newDate);
        this._hideToDatePicker();
    };

    onChangeText(key, value) {
        this.setState({ showSaveButton: true });
        let user = { ...this.state.user };
        user[key] = value;
        this.setState({ user });
    }
    onChangePrivacy(key, value) {
        this.setState({ showSaveButton: true });
        this.setState({ [key]: value });
    }

    onStateChange = state => {
        this.onChangeText('state', state);
        this.props.fetchCities({ token: this.state.token, state: state });
    };

    profileSubmit = uploadedImage => {
        this.setState({ showLoader: true });
        let data = {
            avatar: uploadedImage ? uploadedImage : this.state.user.avatar,
            email: this.state.user.email,
            name: this.state.user.name,
            password: this.state.user.newPassword,
            password_confirmation: this.state.user.confirmPassword,
            profile: {
                name: {
                    value: '',
                    is_public: 1
                },
                country_id: {
                    value: null,
                    is_public: 0
                },
                city: {
                    value:
                        this.state.user.homeTown !== 'Select Home Town'
                            ? this.state.user.homeTown
                            : '',
                    is_public: this.state.homeTownPrivacy == 'Public' ? 1 : 0
                },
                date_of_birth: {
                    value: this.state.user.date_of_birth,
                    is_public: this.state.dateOfBirthPrivacy == 'Public' ? 1 : 0
                },
                email: {
                    value: this.state.user.email,
                    is_public: this.state.emailPrivacy == 'Public' ? 1 : 0
                },
                gender: {
                    value:
                        this.state.user.gender !== 'Select Gender'
                            ? this.state.user.gender
                            : '',
                    is_public: this.state.genderPrivacy == 'Public' ? 1 : 0
                },
                phone_no: {
                    value: this.state.user.phone_no,
                    is_public: this.state.phoneNumberPrivacy == 'Public' ? 1 : 0
                },
                state_id: {
                    value:
                        this.state.user.state !== 'Select State'
                            ? this.state.user.state
                            : '',
                    is_public: this.state.statePrivacy == 'Public' ? 1 : 0
                }
            },
            username: this.state.user.username
        };
        this.props.profileSubmit({ token: this.state.token, profile: data });
    };

    onImagePickerPress() {
        let options = {
            title: 'Select Avatar',
            quality: 0.4,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
            } else {
                let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    userAvatar: source,
                    showUserImage: false,
                    userAvatarFile: 'data:image/jpeg;base64,' + response.data,
                    showSaveButton: true
                });
            }
        });
    }

    onSubmit = () => {
        this.setState({ showLoader: true, isProfileSubmit: true }, () => {
            if (this.state.userAvatarFile) {
                let image = { avatar: this.state.userAvatarFile };
                this.props.uploadImageApi({
                    token: this.state.token,
                    image: image
                });
            } else {
                this.profileSubmit();
            }
        });
    };

    renderTextInput = (
        value,
        isEdit,
        inputName,
        placeholderName,
        isPassword,
        leftAlign
    ) => {
        return (
            <TextInput
                style={[
                    globalStyles.blackColor,
                    globalStyles.flexContainer,
                    styles.inputStyle,
                    leftAlign && styles.textAlignLeft
                ]}
                underlineColorAndroid="transparent"
                value={value}
                editable={isEdit}
                onChangeText={text => {
                    if (inputName) {
                        this.onChangeText(inputName, text);
                    }
                }}
                placeholder={placeholderName}
                secureTextEntry={isPassword}
                placeholderTextColor="rgba(0, 0, 0, 0.87)"
            />
        );
    };

    renderDropDown = (value, dropdownData, dropdownName) => {
        return (
            <Dropdown
                value={value}
                rippleOpacity={0}
                labelFontSize={0}
                fontSize={14}
                labelHeight={0}
                textColor={'rgba(0, 0, 0, 0.87)'}
                data={dropdownData}
                onChangeText={(value, index, data) =>
                    this.onChangeDropdown(index, data, dropdownName)
                }
                containerStyle={styles.smallDropDown}
                disabled={!this.state.isEdit}
                disabledItemColor={greyColor}
                valueExtractor={value => value.name}
            />
        );
    };

    onChangeDropdown(index, data, dropdownName) {
        switch (dropdownName) {
            case 'gender' || 'homeTown':
                return this.onChangeText(dropdownName, data[index].name);
            case 'state':
                return this.onStateChange(data[index].name);
            default:
                return this.onChangePrivacy(dropdownName, data[index].name);
        }
    }

    editIconPressed = () => {
        if (this.state.isEdit) {
            this.setState({
                showSaveButton: false,
                showLoader: true,
                getUser: true
            });
            this.props.fetchUser(this.state.token);
        }
        this.setState({
            isEdit: !this.state.isEdit
        });
    };

    render() {
        return (
            <Fragment>
                <ScrollView
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.mainContainer,
                        styles.container
                    ]}
                >
                    <Text
                        style={[styles.headerText, globalStyles.baseBlueColor]}
                    >
                        My Profile
                    </Text>
                    {/* <Text style={[styles.subText, globalStyles.baseFontSize]}>
                        Personalize your Post It Play It. Pick a new team for
                        contest and play against your friends all season.
                    </Text> */}
                    <View
                        style={[
                            globalStyles.flexContainer,
                            styles.profileMainWhiteBox
                        ]}
                    >
                        <View style={styles.userProfileAvtarWrap}>
                            <View
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.fullWidth
                                ]}
                            >
                                <TouchableOpacity
                                    onPress={() =>
                                        this.state.isEdit &&
                                        this.onImagePickerPress()
                                    }
                                >
                                    <CustomAvatar
                                        medium
                                        rounded
                                        containerStyle={styles.image}
                                        uri={
                                            this.state.user.avatar !==
                                                undefined &&
                                            this.state.user.avatar
                                        }
                                        showBase64={!this.state.showUserImage}
                                        base64Image={this.state.userAvatar.uri}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View
                            style={[
                                globalStyles.flexContainer,
                                globalStyles.whiteBackgroudColor,
                                styles.rowHolder
                            ]}
                        >
                            <View style={styles.editIconWrap}>
                                <TouchableOpacity
                                    style={styles.editIconPressed}
                                    onPress={() => this.editIconPressed()}
                                >
                                    <Icon
                                        name={
                                            this.state.isEdit ? 'close' : 'edit'
                                        }
                                        type="font-awesome"
                                        size={14}
                                        style={styles.editIcon}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    Your Name
                                </Text>
                                {this.renderTextInput(
                                    this.state.user.name,
                                    false,
                                    'name',
                                    '',
                                    false,
                                    false
                                )}
                            </View>
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text style={[globalStyles.greyColor]}>
                                    Email
                                </Text>
                                {!this.state.isEdit
                                    ? this.renderTextInput(
                                          this.state.user.email,
                                          false,
                                          '',
                                          '',
                                          false,
                                          false
                                      )
                                    : null}
                            </View>
                            {this.state.isEdit && (
                                <View>
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginTopTen
                                        ]}
                                    >
                                        {this.renderTextInput(
                                            this.state.user.email,
                                            false,
                                            '',
                                            '',
                                            false,
                                            true
                                        )}
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginBottomTen
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.emailPrivacy,
                                            this.state.emailPrivacyOption,
                                            'emailPrivacy'
                                        )}
                                    </View>
                                </View>
                            )}
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    Username
                                </Text>
                                {this.state.isEdit ? (
                                    this.renderTextInput(
                                        this.state.user.username,
                                        this.state.isEdit,
                                        'username',
                                        'Enter your Username',
                                        false,
                                        false
                                    )
                                ) : (
                                    <Text
                                        style={[
                                            styles.inputStyle,
                                            styles.textAlignLeft,
                                            styles.textAlignVerticalCenter,
                                            styles.blackText
                                        ]}
                                    >
                                        {this.state.user.username
                                            ? this.state.user.username
                                            : '-'}
                                    </Text>
                                )}
                            </View>
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    New Password
                                </Text>
                                {this.renderTextInput(
                                    this.state.user.newPassword,
                                    this.state.isEdit,
                                    'newPassword',
                                    '* * * * * *',
                                    true,
                                    false
                                )}
                            </View>
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    Confirm Password
                                </Text>
                                {this.renderTextInput(
                                    this.state.user.confirmPassword,
                                    this.state.isEdit,
                                    'confirmPassword',
                                    '* * * * * *',
                                    true,
                                    false
                                )}
                            </View>
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    Phone
                                </Text>
                                {!this.state.isEdit
                                    ? this.renderTextInput(
                                          this.state.user.phone_no,
                                          this.state.isEdit,
                                          'phone_no',
                                          '-',
                                          false,
                                          false
                                      )
                                    : null}
                            </View>
                            {this.state.isEdit && (
                                <View>
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginTopTen
                                        ]}
                                    >
                                        {this.renderTextInput(
                                            this.state.user.phone_no,
                                            this.state.isEdit,
                                            'phone_no',
                                            'Phone no',
                                            false,
                                            true
                                        )}
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginBottomTen
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.phoneNumberPrivacy,
                                            this.state.phoneNumberPrivacyOption,
                                            'phoneNumberPrivacy'
                                        )}
                                    </View>
                                </View>
                            )}

                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    Date Of Birth
                                </Text>
                                {!this.state.isEdit
                                    ? this.renderTextInput(
                                          this.state.user.date_of_birth,
                                          false,
                                          '',
                                          '-',
                                          false,
                                          false
                                      )
                                    : null}
                            </View>
                            {this.state.isEdit && (
                                <View>
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            styles.infoRow,
                                            styles.selectDateWrap,
                                            styles.marginTopTen
                                        ]}
                                    >
                                        <TouchableOpacity
                                            onPress={() =>
                                                this._showToDatePicker()
                                            }
                                        >
                                            <Text
                                                style={[
                                                    styles.textAlignLeft,
                                                    styles.blackText
                                                ]}
                                            >
                                                {this.state.user.date_of_birth
                                                    ? this.state.user
                                                          .date_of_birth
                                                    : 'Select date of birth'}
                                            </Text>
                                        </TouchableOpacity>
                                        <DateTimePicker
                                            mode="date"
                                            isVisible={
                                                this.state.isToDatePickerVisible
                                            }
                                            onConfirm={this._handleToDatePicked}
                                            onCancel={() =>
                                                this._hideToDatePicker()
                                            }
                                            date={this.state.datePicker}
                                        />
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            styles.infoRow,
                                            styles.selectDateWrap,
                                            styles.marginBottomTen
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.dateOfBirthPrivacy,
                                            this.state.dateOfBirthPrivacyOption,
                                            'dateOfBirthPrivacy'
                                        )}
                                    </View>
                                </View>
                            )}

                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow,
                                    { height: 34 }
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    Gender
                                </Text>
                                {!this.state.isEdit
                                    ? this.renderTextInput(
                                          this.state.user.gender !==
                                              'Select Gender'
                                              ? this.state.user.gender
                                              : '-',
                                          false,
                                          '',
                                          '-',
                                          false,
                                          false
                                      )
                                    : null}
                            </View>

                            {this.state.isEdit && (
                                <View>
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginTopTen
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.user.gender,
                                            this.state.genderOption,
                                            'gender'
                                        )}
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginBottomTen
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.genderPrivacy,
                                            this.state.genderPrivacyOption,
                                            'genderPrivacy'
                                        )}
                                    </View>
                                </View>
                            )}
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    State
                                </Text>
                                {!this.state.isEdit
                                    ? this.renderTextInput(
                                          this.state.user.state !==
                                              'Select State'
                                              ? this.state.user.state
                                              : '-',
                                          false,
                                          '',
                                          '-',
                                          false,
                                          false
                                      )
                                    : null}
                            </View>
                            {this.state.isEdit && (
                                <View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginTopTen
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.user.state,
                                            this.state.stateOption.data,
                                            'state'
                                        )}
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow,
                                            styles.marginBottomTen
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.statePrivacy,
                                            this.state.statePrivacyOption,
                                            'statePrivacy'
                                        )}
                                    </View>
                                </View>
                            )}
                            <View
                                style={[
                                    globalStyles.hairLineUnderBorder,
                                    globalStyles.flexHorizontal,
                                    globalStyles.fullWidth,
                                    globalStyles.flexSpaceBetween,
                                    styles.infoRow
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.flexContainer
                                    ]}
                                >
                                    Home Town
                                </Text>
                                {!this.state.isEdit
                                    ? this.renderTextInput(
                                          this.state.user.homeTown !==
                                              'Select Home Town'
                                              ? this.state.user.homeTown
                                              : '-',
                                          false,
                                          '',
                                          '-',
                                          false,
                                          false
                                      )
                                    : null}
                            </View>

                            {this.state.isEdit && (
                                <View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.user.homeTown,
                                            this.state.homeTownOption.data,
                                            'homeTown'
                                        )}
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.hairLineUnderBorder,
                                            globalStyles.flexHorizontal,
                                            globalStyles.fullWidth,
                                            globalStyles.flexSpaceBetween,
                                            styles.infoRow
                                        ]}
                                    >
                                        {this.renderDropDown(
                                            this.state.homeTownPrivacy,
                                            this.state.homeTownPrivacyOption,
                                            'homeTownPrivacy'
                                        )}
                                    </View>
                                </View>
                            )}

                            {this.state.isEdit && (
                                <View style={styles.submitBtn}>
                                    <TouchableOpacity
                                        onPress={() => this.onSubmit()}
                                        activeOpacity={0.5}
                                        disabled={!this.state.showSaveButton}
                                    >
                                        <LinearGradient
                                            colors={
                                                !this.state.showSaveButton
                                                    ? ['#b2bec3', '#b2bec3']
                                                    : ['#4d6bc2', '#3695e3']
                                            }
                                            start={{ x: 0, y: 0 }}
                                            end={{ x: 1, y: 0 }}
                                            style={[
                                                globalStyles.flexHCenter,
                                                globalStyles.flexSpaceBetween,
                                                styles.bigPrimaryButton
                                            ]}
                                        >
                                            <Text
                                                style={[
                                                    globalStyles.fontFace,
                                                    globalStyles.whiteColor,
                                                    styles.bigPrimaryButtonText
                                                ]}
                                            >
                                                Save
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </View>
                    </View>
                </ScrollView>
                {this.state.showLoader ||
                this.state.isProfileSubmit ||
                this.props.user == '' ? (
                    <OverlayIndicator />
                ) : null}
            </Fragment>
        );
    }
}
