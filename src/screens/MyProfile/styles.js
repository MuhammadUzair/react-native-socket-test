import { StyleSheet, Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { greyColor } from '../../assets/styles';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        paddingTop: 10
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    subText: {
        color: 'black',
        marginBottom: verticalScale(10)
    },
    rowHolder: {
        paddingHorizontal: 10,
        borderRadius: 5,
        paddingTop: 30
    },
    infoRow: {
        alignItems: 'center',
        paddingVertical: verticalScale(4)
    },
    inputStyle: {
        height: 35,
        textAlign: 'right',
        paddingVertical: 0
    },
    bigPrimaryButton: {
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    bigPrimaryButtonText: {
        fontSize: 18,
        paddingRight: 10
    },
    submitBtn: {
        marginBottom: 12,
        marginTop: 20
    },
    smallDropDown: {
        flex: 1,
        // width: 130,
        paddingTop: 10
        // backgroundColor: 'red',
        // position: 'absolute',
        // top: 20,
        // right: 0
    },
    disabaleText: {
        color: 'rgba(0, 0, 0, .38)'
    },
    enableText: {
        color: greyColor
    },
    fullWidthDropDown: {
        width: '100%',
        paddingTop: 10
    },
    selectDateWrap: {
        justifyContent: 'flex-start'
    },
    image: {
        marginRight: 10
    },
    image: {
        marginRight: 10
    },
    marginBottomTen: {
        marginBottom: 10
    },
    marginTopTen: {
        marginTop: 10
    },
    textAlignLeft: {
        textAlign: 'left'
    },
    textAlignVerticalCenter: {
        textAlignVertical: 'center'
    },
    blackText: {
        color: 'black'
    },
    profileMainWhiteBox: {
        marginTop: -30
    },
    userProfileAvtarWrap: {
        top: 30,
        zIndex: 10
    },
    editIconWrap: {
        alignItems: 'flex-end',
        zIndex: 20
    },
    editIconPressed: {
        width: width * 0.1,
        height: 20,
        paddingTop: 5
    },
    textAlignVerticalCenter: {
        textAlignVertical: 'center'
    },
    editIcon: {
        marginTop: 10
    }
});
