import MyProfile from './MyProfile';

import { connect } from 'react-redux';
import { fetchSate } from '../../redux/epics/fetch-states';
import { fetchCities } from '../../redux/epics/fetch-cities';
import {
    profileSubmit,
    resetProfileSubmit
} from '../../redux/epics/profile-submit';
import {
    uploadImageApi,
    resetUploadImageApi
} from '../../redux/epics/upload-image';
import { fetchUser } from '../../redux/epics/fetch-user-epic';

const mapStateToProps = ({ user, state, cities, profile, uploadImage }) => ({
    user,
    state,
    cities,
    profile,
    uploadImage
});

const mapDispatchToProps = dispatch => ({
    fetchSate: data => dispatch(fetchSate(data)),
    fetchCities: data => dispatch(fetchCities(data)),
    profileSubmit: data => dispatch(profileSubmit(data)),
    uploadImageApi: data => dispatch(uploadImageApi(data)),
    resetProfileSubmit: data => dispatch(resetProfileSubmit(data)),
    resetUploadImageApi: data => dispatch(resetUploadImageApi(data)),
    fetchUser: data => dispatch(fetchUser(data))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyProfile);
