import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import { os, getDate, getTime } from '../../utils';

import { globalStyles } from '../../assets/styles';
import pdStyles from './styles';
import CompetitionStyles from '../createCompetition/styles';
import playitStyles from '../PlayIt/styles';

import HeaderBackButton from '../../components/HeaderBackButton';

export default class WalletTransaction extends Component {
    state = { check: [true, false] };

    onDoneBtnClick = () => {
        this.props.navigation.goBack();
    };

    static navigationOptions = args => {
        const { navigation, navigationOptions } = args;
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingLeft: os() ? 10 : 15,
                backgroundColor: '#E9E9EF'
            },
            headerLeft: (
                <HeaderBackButton
                    type="close"
                    onPress={() => navigation.goBack()}
                />
            )
        };
    };

    render() {
        const { item } = this.props.navigation.state.params;
        return (
            <View style={[globalStyles.flexContainer]}>
                <View
                    style={[
                        globalStyles.mainContainer,
                        globalStyles.flexContainer
                    ]}
                >
                    <View
                        style={[
                            pdStyles.rowContainer,
                            globalStyles.flexContainer
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.greyBackgroudColor,
                                CompetitionStyles.borderRadiusTop,
                                pdStyles.postGameCard
                            ]}
                        />

                        <View
                            style={[
                                CompetitionStyles.roundTimer,
                                {
                                    alignItems: 'center',
                                    top: 10
                                }
                            ]}
                        >
                            <View
                                style={[
                                    CompetitionStyles.roundTimer,
                                    globalStyles.blueBackgroudColor,
                                    {
                                        flex: 0,
                                        borderRadius: 50,
                                        overflow: 'hidden'
                                    }
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.blueBackgroudColor,
                                        globalStyles.whiteColor,
                                        globalStyles.baseFontSize,
                                        {
                                            paddingTop: 1,
                                            paddingBottom: 1,
                                            paddingLeft: 5,
                                            paddingRight: 5
                                        }
                                    ]}
                                >
                                    Transaction
                                </Text>
                            </View>
                        </View>

                        <View
                            style={[
                                globalStyles.whiteBackgroudColor,
                                pdStyles.marginTop
                            ]}
                        >
                            <View style={[globalStyles.greyBackgroudColor]}>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.greyColor
                                        ]}
                                    >
                                        Transaction ID
                                    </Text>
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.baseFontSize,
                                            globalStyles.blackColor
                                        ]}
                                    >
                                        {item.transaction_id}
                                    </Text>
                                </View>

                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.greyColor
                                        ]}
                                    >
                                        Date & Time
                                    </Text>
                                    <Text
                                        style={[
                                            globalStyles.blackColor,
                                            globalStyles.fontFace,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        {getDate(item.created_at)},{' '}
                                        {getTime(item.created_at)}
                                    </Text>
                                </View>

                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.greyColor
                                        ]}
                                    >
                                        Type
                                    </Text>
                                    <View style={[globalStyles.flexHorizontal]}>
                                        <Text
                                            style={
                                                globalStyles.highlightGreenColor
                                            }
                                        >
                                            {item.type}
                                        </Text>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.greyColor
                                        ]}
                                    >
                                        Fee Type
                                    </Text>
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.baseBlueColor,
                                            playitStyles.bigFontSize
                                        ]}
                                    >
                                        {item.fee_type}
                                    </Text>
                                </View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.flexHCenter,
                                        globalStyles.hairLineUnderBorder,
                                        { marginTop: -7 }
                                    ]}
                                >
                                    <View style={[globalStyles.fullWidth]}>
                                        <Text
                                            style={[
                                                globalStyles.fontFace,
                                                globalStyles.blackColor
                                            ]}
                                        >
                                            Description
                                        </Text>
                                        <Text
                                            style={[
                                                globalStyles.baseGreyColor,
                                                globalStyles.subTextFontSize
                                            ]}
                                        >
                                            {item.description}
                                        </Text>
                                    </View>
                                </View>
                            </View>

                            <View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.blackColor
                                        ]}
                                    >
                                        Credit
                                    </Text>
                                    <View style={[globalStyles.flexHorizontal]}>
                                        <View
                                            style={[
                                                globalStyles.flexHorizontal
                                            ]}
                                        >
                                            <Text
                                                style={[
                                                    globalStyles.boldFontFace,
                                                    globalStyles.highlightPinkColor,
                                                    playitStyles.bigFontSize
                                                ]}
                                            >
                                                {item.type === 'Credit'
                                                    ? `$${item.amount}`
                                                    : '-'}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.flexHCenter
                                    ]}
                                >
                                    <View
                                        style={[CompetitionStyles.widthSixty]}
                                    >
                                        <Text
                                            style={[
                                                globalStyles.fontFace,
                                                globalStyles.blackColor
                                            ]}
                                        >
                                            Debit
                                        </Text>
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            globalStyles.flexHCenter
                                        ]}
                                    >
                                        <View
                                            style={[
                                                globalStyles.flexHorizontal
                                            ]}
                                        >
                                            <Text
                                                style={[
                                                    globalStyles.boldFontFace,
                                                    globalStyles.highlightPinkColor,
                                                    playitStyles.bigFontSize
                                                ]}
                                            >
                                                {item.type === 'Debit'
                                                    ? `$${item.amount}`
                                                    : '-'}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View
                            style={[
                                globalStyles.flexContainer,
                                globalStyles.whiteBackgroudColor,
                                globalStyles.fullScreen
                            ]}
                        />
                    </View>
                </View>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        pdStyles.paddingAll,
                        pdStyles.navIconGreyBackColor
                    ]}
                >
                    <View style={{ marginTop: 6 }}>
                        <Text
                            style={[
                                CompetitionStyles.amountTextSize,
                                globalStyles.baseBlueColor,
                                globalStyles.boldFontFace
                            ]}
                        >
                            ${parseFloat(item.current_balance).toFixed(2)}
                        </Text>
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                globalStyles.blackColor
                            ]}
                        >
                            Balance
                        </Text>
                    </View>

                    <View>
                        <TouchableOpacity
                            onPress={this.onDoneBtnClick}
                            activeOpacity={0.5}
                            style={[
                                pdStyles.bottomSpacing,
                                pdStyles.marginTopPlaybtn
                            ]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    CompetitionStyles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        CompetitionStyles.bigPrimaryButtonText
                                    ]}
                                >
                                    Done
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
