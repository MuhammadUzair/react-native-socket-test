import { StyleSheet } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { os } from '../../utils';

export default StyleSheet.create({
    container: {
        paddingLeft: 20,
        paddingRight: 20
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 1.5
    },
    paddingAll: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    rowContainer: {
        paddingTop: os() ? 18 : 12,
        // paddingHorizontal: os() ? 20 : 10,
        borderBottomColor: '#bdc3c7'
    },
    postGameCard: {
        height: 50,
        width: '100%',
        position: 'absolute',
        top: 40
        // marginLeft: os() ? 20 : 10
    },
    navIconGreyBackColor: {
        backgroundColor: '#F7F8FA'
    },
    bottomSpacing: {
        marginBottom: 8
    },
    marginTop: {
        marginTop: 10
    },
    marginTopPlaybtn: {
        marginTop: verticalScale(5)
    },

    subText: {
        fontSize: 18,
        color: 'black'
    },
    descriptionText: {
        color: 'black',
        fontSize: 12,
        marginBottom: 10
    },
    teamsCard: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: '100%',
        // height: 50,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 40,
        paddingHorizontal: 10,
        flexWrap: 'wrap'
    },
    checkbox: {
        width: '50%',
        paddingLeft: 2.5,
        paddingBottom: os() ? 0 : 8,
        paddingRight: 2.5,
        marginBottom: 10
    },
    checkboxText: {
        color: 'black',
        textAlign: 'center',
        fontSize: 12
    },
    buttonStyle: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row',
        width: 60 + '%'
    },
    buttonText: {
        fontSize: 18,
        paddingLeft: 15
    }
});
