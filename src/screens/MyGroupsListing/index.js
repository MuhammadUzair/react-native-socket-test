import { connect } from 'react-redux';
import {
    fetchMyFriends,
    resetMyFriend
} from '../../redux/epics/fetch-my-friends-epic';
import {
    searchMyFriends,
    resetSearchMyFriend
} from '../../redux/epics/search-my-friends-epic';

import {
    fetchGroupMembers,
    resetGroupMembers
} from '../../redux/epics/fetch-group-members-epic';

import { editGroup } from '../../redux/epics/edit-group-epic';

import MyGroupsListing from './MyGroupsListing';
import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({
    myFriends,
    searchedMyFriends,
    groupMembers,
    groupEdited
}) => ({
    myFriends,
    searchedMyFriends,
    groupMembers,
    groupEdited
});

const mapDispatchToProps = dispatch => ({
    fetchMyFriends: data => dispatch(fetchMyFriends(data)),
    resetMyFriend: () => dispatch(resetMyFriend()),

    searchMyFriends: data => dispatch(searchMyFriends(data)),
    resetSearchMyFriend: () => dispatch(resetSearchMyFriend()),

    fetchGroupMembers: data => dispatch(fetchGroupMembers(data)),
    resetGroupMembers: () => dispatch(resetGroupMembers()),

    editGroup: data => dispatch(editGroup(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(MyGroupsListing, [
        { propName: 'searchedMyFriends' },
        { propName: 'myFriends' },
        { propName: 'groupEdited' }
    ])
);
