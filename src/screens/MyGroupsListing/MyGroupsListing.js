import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    FlatList
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import EmptyListIndicator from '../../components/EmptyListIndicator';

import { retrieveToken } from '../../utils';

import { globalStyles } from '../../assets/styles';
import styles from './styles';

import OverlayIndicator from '../../components/OverlayIndicator';
import GroupEditCheckbox from '../../components/GroupEditCheckbox';

export default class MyGroupsListing extends Component {
    state = {
        search: '',
        isFetching: false,
        added: {},
        removed: {}
    };

    componentDidMount() {
        const { item, isEditing } = this.props.navigation.state.params;

        retrieveToken().then(token => {
            this.setState({ token }, () => {
                if (isEditing) {
                    this.props.resetGroupMembers();
                    this.props.fetchGroupMembers({
                        token,
                        groupId: item.group_id,
                        filter: true
                    });
                } else {
                    this.props.resetMyFriend();
                    this.props.fetchMyFriends({ token });
                }
            });
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { isEditing } = this.props.navigation.state.params;
        const { groupEdited, myFriends, navigation } = this.props;
        // generateAndPrintErrorMessage(myFriends);
        if (isEditing) {
            // generateAndPrintErrorMessage(groupEdited);
            if (groupEdited.code >= 200 && groupEdited.code < 300)
                navigation.navigate('GroupScreen');
        }
    }

    static getDerivedStateFromProps(props, state) {
        return {
            isFetching:
                props.myFriends.status === 'inprogress' ||
                props.searchedMyFriends.status === 'inprogress' ||
                props.groupMembers.status === 'inprogress'
        };
    }

    textEntered = search => {
        this.setState({ search }, () => {
            if (!this.state.search.length) {
                this.props.resetSearchMyFriend();
            }
        });
    };

    onCancelBtnClick = () => {
        this.props.navigation.navigate('GroupScreen');
    };

    onNextBtnClick = () => {
        const { item, isEditing } = this.props.navigation.state.params;
        const { added, token, removed } = this.state;
        const {
            editGroup,
            navigation: {
                state: {
                    params: { privacy, name, description }
                }
            }
        } = this.props;
        if (isEditing) {
            editGroup({
                token,
                privacy,
                name,
                description,
                remove: Object.values(removed).filter(item => item),
                add: Object.values(added)
                    .map(item => item.friend_id)
                    .filter(item => item),
                groupId: item.group_id
            });
        } else {
            this.props.navigation.push('GroupCreateInfo', {
                friends: Object.values(added).filter(
                    item => item !== undefined
                ),
                privacy,
                name,
                description
            });
        }
    };

    onScrollEnd = () => {
        const { isFetching, token, search } = this.state;
        const { item, isEditing } = this.props.navigation.state.params;
        const {
            fetchMyFriends,
            myFriends,

            searchMyFriends,
            searchedMyFriends,

            fetchGroupMembers,
            groupMembers
        } = this.props;
        if (!isFetching) {
            if (search) {
                if (searchedMyFriends.data.next_page)
                    this.setState({ isFetching: true }, () => {
                        searchMyFriends({
                            token,
                            nextPage: searchedMyFriends.data.next_page,
                            search
                        });
                    });
            } else {
                if (isEditing) {
                    if (groupMembers.data.next_page)
                        this.setState({ isFetching: true }, () => {
                            fetchGroupMembers({
                                token,
                                nextPage: groupMembers.data.next_page
                            });
                        });
                } else if (myFriends.data.next_page) {
                    this.setState({ isFetching: true }, () => {
                        fetchMyFriends({
                            token,
                            nextPage: myFriends.data.next_page
                        });
                    });
                }
            }
        }
    };

    onSearch = () => {
        const { isEditing, item } = this.props.navigation.state.params;
        const { searchMyFriends } = this.props;
        const { search, token } = this.state;
        if (search) {
            if (isEditing)
                searchMyFriends({ groupId: item.group_id, token, search });
            else searchMyFriends({ token, search });
        }
    };

    selectFriend = index => {
        const { isEditing } = this.props.navigation.state.params;
        const { added, search, removed } = this.state;
        const { myFriends, searchedMyFriends, groupMembers } = this.props;
        const newAdded = { ...added };
        const newRemoved = { ...removed };
        let friendsToCheck = myFriends;

        if (isEditing) friendsToCheck = groupMembers;
        if (search) friendsToCheck = searchedMyFriends;
        const objBeingChecked =
            !isEditing || search
                ? friendsToCheck.data.friends[index]
                : friendsToCheck.data.members[index];

        if (search || !isEditing) {
            if (newAdded[objBeingChecked.friend_id] === undefined)
                newAdded[objBeingChecked.friend_id] = objBeingChecked;
            else newAdded[objBeingChecked.friend_id] = undefined;
            this.setState({ added: newAdded });
        } else {
            friendsToCheck = groupMembers;
            if (newRemoved[objBeingChecked.id] === undefined)
                newRemoved[objBeingChecked.id] = objBeingChecked.id;
            else newRemoved[objBeingChecked.id] = undefined;
            this.setState({ removed: newRemoved });
        }
    };

    checkboxBehavior = item => {
        const { isEditing } = this.props.navigation.state.params;
        const { searchedMyFriends } = this.props;
        const { removed, added, search } = this.state;
        if (
            (search &&
                searchedMyFriends.status !== '' &&
                searchedMyFriends.status !== 'inprogress') ||
            !isEditing
        ) {
            return added[item.friend_id] !== undefined;
        } else {
            return !removed[item.id];
        }
    };

    searchComponent = () => {
        return (
            <View>
                <View style={styles.paddedContainer}>
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.fullWidth,
                            styles.searchHolder,
                            globalStyles.whiteBackgroudColor
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.flexContainer,
                                styles.searchBar,
                                globalStyles.whiteBackgroudColor
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexContainer,
                                    globalStyles.flexVCenter
                                ]}
                            >
                                <TextInput
                                    value={this.state.search}
                                    placeholder="Search with name"
                                    underlineColorAndroid="transparent"
                                    style={globalStyles.fullHeight}
                                    onChangeText={this.textEntered}
                                    onSubmitEditing={this.onSearch}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            style={[globalStyles.flexVCenter]}
                            activeOpacity={0.5}
                            onPress={this.onSearch}
                        >
                            <Icon name="search" type="evilicon" size={20} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    };

    renderStateText = item => {
        return item ? (
            <Text
                style={[
                    globalStyles.paleRedColor,
                    globalStyles.boldFontFace,
                    globalStyles.baseFontSize
                ]}
            >
                Remove
            </Text>
        ) : (
            <Text
                style={[
                    globalStyles.paleRedColor,
                    globalStyles.boldFontFace,
                    globalStyles.baseFontSize,
                    globalStyles.baseGreyColor
                ]}
            >
                Add to Group
            </Text>
        );
    };

    renderListItem = ({ item, index }) => {
        const {
            itemFromPrevScreen,
            isEditing
        } = this.props.navigation.state.params;
        const { added, removed, search } = this.state;
        return (
            <GroupEditCheckbox
                item={item}
                index={index}
                onPress={this.selectFriend}
                checked={this.checkboxBehavior(item)}
                stateText={this.renderStateText(this.checkboxBehavior(item))}
            />
        );
    };

    render() {
        const { isEditing } = this.props.navigation.state.params;
        const {
            myFriends,
            searchedMyFriends,
            groupMembers,
            groupEdited
        } = this.props;
        const { search } = this.state;
        let friendsToShow = [];
        if (
            search.length &&
            searchedMyFriends.status !== 'inprogress' &&
            searchedMyFriends.status !== ''
        )
            friendsToShow = searchedMyFriends.data.friends;
        else {
            if (isEditing) {
                friendsToShow = groupMembers.data.members;
            } else {
                friendsToShow = myFriends.data.friends;
            }
        }

        return (
            <View
                style={[
                    globalStyles.flexContainer,
                    globalStyles.mainContainer,
                    styles.container
                ]}
            >
                {this.searchComponent()}
                <View
                    style={[
                        globalStyles.flexContainer,
                        { paddingVertical: 10 }
                    ]}
                >
                    <View
                        style={[
                            globalStyles.flexContainer,
                            styles.scrollViewWrapper,
                            globalStyles.flexVCenter,
                            globalStyles.whiteBackgroudColor
                        ]}
                    >
                        <FlatList
                            data={friendsToShow}
                            contentContainerStyle={styles.scrollContainer}
                            ListEmptyComponent={
                                <EmptyListIndicator
                                    message={
                                        isEditing
                                            ? 'There are no group members'
                                            : 'You have no friends'
                                    }
                                    // showIndicator={
                                    //     myFriends.status === 'inprogress' ||
                                    //     searchedMyFriends.status ===
                                    //         'inprogress'
                                    // }
                                />
                            }
                            renderItem={this.renderListItem}
                            onEndReached={this.onScrollEnd}
                            onEndReachedThreshold={0.1}
                            keyExtractor={(item, index) =>
                                `id: ${item.friend_id}, index: ${index}`
                            }
                        />
                    </View>
                </View>

                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        styles.bottomSpacing
                    ]}
                >
                    <View>
                        <TouchableOpacity onPress={this.onCancelBtnClick}>
                            <Text
                                style={[
                                    globalStyles.boldFontFace,
                                    globalStyles.highlightPinkColor,
                                    styles.cancelCenterButton
                                ]}
                            >
                                {isEditing ? 'Back' : 'Cancel Post'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.onNextBtnClick(this.props)}
                            activeOpacity={0.5}
                            style={[globalStyles.flexHCenter]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        { width: '50%' },
                                        globalStyles.whiteColor,
                                        styles.bigPrimaryButtonText
                                    ]}
                                >
                                    {isEditing ? 'Update' : 'Submit'}
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {groupEdited === 'inprogress' ||
                groupEdited.status == 'inprogress' ||
                searchedMyFriends.status == 'inprogress' ||
                myFriends.status == 'inprogress' ||
                groupMembers.status == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
