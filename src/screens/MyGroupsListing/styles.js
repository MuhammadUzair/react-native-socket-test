import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        paddingVertical: 10
    },

    checkbox: {
        width: '100%',
        paddingHorizontal: 5,
        marginBottom: 20,
        alignItems: 'center'
    },

    checkboxStyle: {
        marginRight: 10
    },

    nameHolder: {
        flex: 4
    },

    stateHolder: {
        flex: 2.5,
        alignItems: 'flex-end'
    },

    scrollContainer: {
        paddingHorizontal: 3
    },

    scrollViewWrapper: {
        borderRadius: 15,
        paddingVertical: 10
    },

    paddedContainer: {
        paddingHorizontal: 0
    },

    searchHolder: {
        paddingHorizontal: 20,
        borderRadius: 50,
        marginVertical: 0
    },

    searchBar: {
        height: 40
    },

    paddingText: {
        paddingLeft: 10
    },

    cancelCenterButton: {
        marginTop: 15,
        fontSize: 15
    },

    bottomSpacing: {
        marginBottom: 6
    },

    marginTop: {
        marginTop: 5
    },

    bigPrimaryButtonText: {
        fontSize: 18
    },

    bigPrimaryButton: {
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    }
});
