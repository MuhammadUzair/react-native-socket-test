import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    TextInput,
    FlatList,
    RefreshControl
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements';

import { globalStyles } from '../../assets/styles';
import styles from './styles';

import { os, retrieveToken } from '../../utils';

import DrawerIconButton from '../../components/DrawerIconButton';
import SmallCards from '../../components/SmallCards';
import CustomAvatar from '../../components/CustomAvatar';
import SportListingSportCheckbox from '../../components/SportListingSportCheckbox';
import SportListingTeamCheckbox from '../../components/SportListingTeamCheckbox';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import OverlayIndicator from '../../components/OverlayIndicator';
export default class MySportsListing extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20
            },
            headerLeft: (
                <DrawerIconButton onPress={() => navigation.openDrawer()} />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    state = {
        showTeamsView: false,
        showTeams: false,
        showSports: false,
        tabStates: [
            { text: 'My Sports', state: true },
            { text: 'My Teams', state: false }
        ],
        cards: [],
        check: [false, false, false, false, false, false, false, false],
        added: [],
        teams: '',
        mySports: [],
        token: '',
        selectedTeams: [],
        selectedTeamsAndleagues: [],
        selectedTeamCode: '',
        selectedTeamName: '',
        filterTeams: [],
        isFilterTeams: false,
        isMysportsRefreshing: true,
        isMyTeamsRefreshing: true,
        updateSelectedTeams: true,
        defaultTextInputValue: '',
        searchedTeamNotFound: false
    };

    componentDidMount() {
        //  Call fetchTeams api start
        retrieveToken().then(token => {
            this.setState({ token });
            this.props.resetTeams();
            this.props.fetchLeagues(token);
            this.props.fetchTeams(token);
        });
        //  Call fetchTeams api end

        if (
            this.props.leagues &&
            this.props.leagues.data &&
            this.props.leagues.data.length > 0
        ) {
            let allLeagues = [];
            this.props.leagues.data.map(item => {
                allLeagues.push({
                    text: item.short_code,
                    state: false,
                    id: item.id,
                    name: item.name
                });
            });
            allLeagues[0].state = true;
            this.setState({ cards: allLeagues });
        }
    }

    static getDerivedStateFromProps(newProps, prevState) {
        let teams = prevState.teams,
            selectedteam = prevState.selectedteam,
            mySports = prevState.mySports,
            updateSelectedTeams = prevState.updateSelectedTeams,
            selectedTeamsAndleagues = prevState.selectedTeamsAndleagues,
            selectedTeams = prevState.selectedTeams,
            selectedTeamCode = prevState.selectedTeamCode,
            selectedTeamName = prevState.selectedTeamName,
            isMysportsRefreshing = prevState.isMysportsRefreshing,
            isMyTeamsRefreshing = prevState.isMyTeamsRefreshing;

        // fetch My Sports Start
        if (newProps.leagues && newProps.leagues.data) {
            mySports = newProps.leagues;
        }
        // fetch My Sports end

        // fetch Teams Start

        if (newProps && newProps.teams) {
            teams = newProps.teams;
            selectedteam = [];
        }

        if (
            newProps &&
            newProps.teams &&
            newProps.teams.data &&
            Object.keys(newProps.teams.data).length > 0
        ) {
            // if No team selected  - start
            if (
                Object.values(newProps.teams.data)[0].leagueTeams &&
                Object.values(newProps.teams.data)[0].leagueTeams.length > 0 &&
                updateSelectedTeams
            ) {
                selectedTeams = Object.values(newProps.teams.data)[0]
                    .leagueTeams;
                selectedTeamCode = Object.values(newProps.teams.data)[0]
                    .short_code;
                selectedTeamName = Object.values(newProps.teams.data)[0].name;
                updateSelectedTeams = false;
            }
            // if No team selected  - end

            if (selectedTeamCode) {
                Object.values(newProps.teams.data).map(teams => {
                    if (teams.short_code == selectedTeamCode) {
                        selectedTeams = teams.leagueTeams;
                    }
                });
            }

            // Get selected Teams Andleagues Required for Api - start

            Object.values(newProps.teams.data).map(teams => {
                if (teams.leagueTeams) {
                    teams.leagueTeams.map(leagueTeams => {
                        if (leagueTeams.isAdded) {
                            selectedTeamsAndleagues.push({
                                league_id: teams.id,
                                team_id: leagueTeams.id
                            });
                        }
                    });
                }
            });
            // Get selected Teams Andleagues Required for Api - end

            // fetch Teams End
        }
        return {
            teams: teams,
            selectedteam: selectedteam,
            isMyTeamsRefreshing: isMyTeamsRefreshing,
            mySports: mySports,
            updateSelectedTeams: updateSelectedTeams,
            selectedTeamsAndleagues: selectedTeamsAndleagues,
            selectedTeams: selectedTeams,
            selectedTeamCode: selectedTeamCode,
            selectedTeamName: selectedTeamName,
            isMysportsRefreshing: false,
            isMyTeamsRefreshing: false
        };
    }

    plusPressed = () => {
        this.setState(({ showSports }) => ({
            showSports: !showSports,
            defaultTextInputValue: ''
        }));
        if (this.state.searchedTeamNotFound) {
            this.setState({
                searchedTeamNotFound: false,
                isMyTeamsRefreshing: true
            });
            this.props.fetchTeams(this.state.token);
        }
    };

    onPressCheckbox = (item, listName) => {
        var tempArray;
        // My sports add favorite start
        if (listName == 'mySports') {
            let league_ids = [];
            tempArray = this.state.mySports;
            tempArray.data.map((tempItem, i) => {
                if (tempItem.id == item.id) {
                    tempItem.isAdded = !tempItem.isAdded;
                }
                if (tempItem.isAdded) {
                    league_ids.push(tempItem.id);
                }
            });
            this.props.addLeagues({
                leagueIds: league_ids,
                token: this.state.token
            });
            this.setState({ mySports: tempArray });
        }
        // My sports add favorite end

        // My  teams add favorite start
        else {
            this.setState({ defaultTextInputValue: '' });
            let selectedTeamsAndleagues = [],
                allTeams = this.state.teams;

            Object.values(allTeams.data).map(teams => {
                if (teams.leagueTeams) {
                    teams.leagueTeams.map(leagueTeams => {
                        if (leagueTeams.id == item.id) {
                            leagueTeams.isAdded = !leagueTeams.isAdded;
                        }
                        if (leagueTeams.isAdded) {
                            selectedTeamsAndleagues.push({
                                league_id: teams.id,
                                team_id: leagueTeams.id
                            });
                        }
                    });
                }
            });

            this.props.addTeams({
                team_ids: selectedTeamsAndleagues,
                token: this.state.token
            });
        }
        // My teams add favorite end
    };

    onTabPress = i => {
        let tabStates = [...this.state.tabStates];
        tabStates.forEach(tab => (tab.state = false));
        tabStates[i].state = !tabStates[i].state;
        this.setState({ tabStates });
    };

    onPressCard = (index, item) => {
        const cards = this.state.cards.map(({ text, name }) => ({
            text,
            name,
            state: false
        }));
        cards[index].state = !cards[index].state;
        this.setState({
            cards,
            defaultTextInputValue: '',
            selectedTeams: [],
            selectedTeamCode: cards[index].text,
            selectedTeamName: cards[index].name
        });

        if (
            item.text !== this.state.selectedTeamCode &&
            this.state.teams.data &&
            Object.keys(this.state.teams.data).length > 0
        ) {
            Object.values(this.state.teams.data).map(teams => {
                if (teams.short_code == item.text) {
                    if (teams.leagueTeams && teams.leagueTeams.length > 0) {
                        this.setState({
                            selectedTeams: teams.leagueTeams
                        });
                    }
                }
            });
        }
    };

    secondaryActions = [
        () => {
            this.setState({ showTeamsView: false });
        },
        () => {
            this.setState({ showTeamsView: true });
        }
    ];

    onRefreshMySports = () => {
        this.props.fetchLeagues(this.state.token);
    };
    onRefreshMyTeams = () => {
        this.props.fetchTeams(this.state.token);
    };

    addedMySportsCard(item) {
        if (item.isAdded) {
            return (
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        styles.checkbox,
                        globalStyles.hairLineUnderBorder
                    ]}
                >
                    <CustomAvatar
                        uri={item.avatar}
                        title={item.short_code}
                        containerStyle={styles.avatar}
                        medium
                        rounded
                    />
                    <View style={styles.nameHolder}>
                        <Text
                            style={[
                                globalStyles.blackColor,
                                globalStyles.subTextFontSize
                            ]}
                        >
                            {item.name && item.name}
                        </Text>
                    </View>
                    <View style={styles.stateHolder}>
                        <Text style={globalStyles.highlightColor}>
                            {item.short_code && item.short_code}
                        </Text>
                    </View>
                </View>
            );
        }
    }

    mySportsCard(item) {
        if (item.isAdded) {
            return (
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        styles.checkbox,
                        globalStyles.hairLineUnderBorder
                    ]}
                    key={i}
                >
                    <CustomAvatar
                        uri={item.avatar}
                        title={item.short_code}
                        containerStyle={styles.avatar}
                        medium
                        rounded
                    />
                    <View style={styles.nameHolder}>
                        <Text
                            style={[
                                globalStyles.blackColor,
                                globalStyles.subTextFontSize
                            ]}
                        >
                            {item.name && item.name}
                        </Text>
                    </View>
                    <View style={styles.stateHolder}>
                        <Text style={globalStyles.highlightColor}>
                            {item.short_code && item.short_code}
                        </Text>
                    </View>
                </View>
            );
        }
    }

    emptyAddedTeams = teamName => {
        return (
            <View>
                <EmptyListIndicator
                    message={
                        teamName
                            ? 'There are no teams added in ' + teamName + '.'
                            : 'There are no teams added'
                    }
                    // showIndicator={
                    //     this.state.selectedTeams !== 'inprogress' ||
                    //     this.state.isMyTeamsRefreshing
                    // }
                />
            </View>
        );
    };

    emptyMySports = () => {
        return (
            <View>
                <EmptyListIndicator
                    message="There are no Sports added."
                    // showIndicator={
                    //     this.state.selectedTeams !== 'inprogress' ||
                    //     this.state.isMyTeamsRefreshing
                    // }
                />
            </View>
        );
    };

    renderAddedMyTeams = () => {
        // Render favorite Teams start
        var teamName =
            this.state.selectedTeamName &&
            this.state.selectedTeamName.toLowerCase();
        teamName = teamName.replace(/\b\w/g, l => l.toUpperCase());
        if (teamName == 'Ncaa Football') {
            teamName = `NCAAFB`;
        }
        if (teamName == 'Ncaa Men') {
            teamName = `NCAAMB`;
        }
        let isAddedCount = 0;
        let selectedTeams = [];

        this.state.selectedTeams.filter((item, i) => {
            if (item.isAdded) {
                isAddedCount = isAddedCount + 1;
                selectedTeams.push(item);
            }
        });
        if (isAddedCount === 0) {
            return (
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            onRefresh={this.onRefreshMyTeams}
                            refreshing={this.state.isMyTeamsRefreshing}
                        />
                    }
                >
                    <Text style={styles.emptyListStyle}>
                        {teamName
                            ? 'There are no teams added in ' + teamName + '.'
                            : 'There are no teams added'}
                    </Text>
                </ScrollView>
            );
        }
        return (
            <FlatList
                data={selectedTeams}
                renderItem={({ item }) => {
                    return this.addedMySportsCard(item);
                }}
                onEndReachedThreshold={0.1}
                onEndReached={this.onScrollEnd}
                onRefresh={this.onRefreshMyTeams}
                refreshing={false}
                ListEmptyComponent={this.emptyAddedTeams(teamName)}
                keyExtractor={(item, index) => `id:${item.id},index:${index}`}
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );

        // Render favorite Teams end
    };

    renderAddedMySports() {
        // render favorite My Sports start
        let isAddedCount = 0;
        this.state.mySports.data.map((item, i) => {
            if (item.isAdded) {
                isAddedCount = isAddedCount + 1;
            }
        });

        if (isAddedCount == 0) {
            return (
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            onRefresh={this.onRefreshMySports}
                            refreshing={this.state.isMysportsRefreshing}
                        />
                    }
                >
                    <Text style={styles.emptyListStyle}>
                        There are no Sports added.
                    </Text>
                </ScrollView>
            );
        }
        return (
            <FlatList
                data={this.state.mySports.data}
                renderItem={({ item }) => {
                    return this.addedMySportsCard(item);
                }}
                onEndReachedThreshold={0.1}
                onRefresh={this.onRefreshMySports}
                refreshing={false}
                ListEmptyComponent={this.emptyMySports()}
                keyExtractor={(item, index) => `id:${item.id},index:${index}`}
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
        // render favorite My Sports end
    }

    renderAddedListing = () => {
        if (this.state.showTeamsView) {
            return this.renderAddedMyTeams();
        } else {
            return this.renderAddedMySports();
        }
    };

    renderCardSport = ({ item }) => {
        return (
            <SportListingSportCheckbox
                item={item}
                onPress={this.onPressCheckbox}
            />
        );
    };

    renderCardTeam = ({ item }) => {
        return (
            <SportListingTeamCheckbox
                item={item}
                onPress={this.onPressCheckbox}
            />
        );
    };
    renderAvailableTeams = () => {
        var teamName =
            this.state.selectedTeamName &&
            this.state.selectedTeamName.toLowerCase();
        teamName = teamName.replace(/\b\w/g, l => l.toUpperCase());
        if (teamName == 'Ncaa Football') {
            teamName = `NCAAFB`;
        }
        if (teamName == 'Ncaa Men') {
            teamName = `NCAAMB`;
        }
        if (this.state.searchedTeamNotFound) {
            return (
                <View>
                    <EmptyListIndicator message="No team found in search" />
                </View>
            );
        }

        return (
            <FlatList
                data={this.state.selectedTeams}
                renderItem={this.renderCardTeam}
                onEndReachedThreshold={0.1}
                onRefresh={this.onRefreshMyTeams}
                refreshing={this.state.isMyTeamsRefreshing}
                ListEmptyComponent={this.emptyAddedTeams(teamName)}
                keyExtractor={(item, index) => `id:${item.id},index:${index}`}
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    renderAvailableMySports = () => {
        return (
            <FlatList
                data={this.state.mySports.data}
                renderItem={this.renderCardSport}
                onEndReachedThreshold={0.1}
                onRefresh={this.onRefreshMySports}
                refreshing={this.state.isMysportsRefreshing}
                ListEmptyComponent={this.emptyMySports()}
                keyExtractor={(item, index) => `id:${item.id},index:${index}`}
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    renderAvailableListing = () => {
        if (this.state.showTeamsView) {
            return this.renderAvailableTeams();
        } else {
            return this.renderAvailableMySports();
        }
    };

    searchBarHandler = (text, updateState) => {
        if (updateState) {
            this.setState({ defaultTextInputValue: text });
        }
        let allTeams = this.state.teams,
            selectedteam = [];

        Object.values(allTeams.data).map(teams => {
            if (
                teams.short_code == this.state.selectedTeamCode &&
                teams.leagueTeams &&
                teams.leagueTeams.length > 0
            ) {
                teams.leagueTeams.filter(item => {
                    if (
                        item.name.toLowerCase().indexOf(text.toLowerCase()) >=
                            0 ||
                        item.short_code
                            .toLowerCase()
                            .indexOf(text.toLowerCase()) >= 0 ||
                        item.address.city
                            .toLowerCase()
                            .indexOf(text.toLowerCase()) >= 0 ||
                        item.address.states
                            .toLowerCase()
                            .indexOf(text.toLowerCase()) >= 0 ||
                        item.address.country
                            .toLowerCase()
                            .indexOf(text.toLowerCase()) >= 0
                    ) {
                        selectedteam.push(item);
                        return true;
                    }
                    return false;
                });
            }
        });
        // this.setState({ selectedTeams: selectedteam });
        if (selectedteam.length > 0) {
            this.setState({
                selectedTeams: selectedteam,
                searchedTeamNotFound: false
            });
        } else {
            if (updateState) {
                this.setState({ searchedTeamNotFound: true });
            } else {
                this.setState({ searchedTeamNotFound: false });
            }
        }
    };

    renderTeamsVariant = () => {
        if (this.state.showTeamsView) {
            return (
                <View>
                    <View style={styles.paddedContainer}>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.fullWidth,
                                styles.searchHolder
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexContainer,
                                    styles.searchBar
                                ]}
                            >
                                <View
                                    style={[
                                        globalStyles.flexContainer,
                                        globalStyles.flexVCenter
                                    ]}
                                >
                                    <TextInput
                                        placeholder="Search by Name, City, State and Country"
                                        underlineColorAndroid="transparent"
                                        style={globalStyles.fullHeight}
                                        onChangeText={e =>
                                            this.searchBarHandler(e, true)
                                        }
                                        value={this.state.defaultTextInputValue}
                                        underlineColorAndroid="transparent"
                                    />
                                </View>
                            </View>
                            <Icon name="search" type="evilicon" size={20} />
                        </View>
                    </View>
                    <View>
                        <ScrollView
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={styles.scrollView}
                        >
                            {this.state.cards.map((item, i) => {
                                return (
                                    <SmallCards
                                        text={item.text}
                                        selected={item.state}
                                        onPress={this.onPressCard.bind(
                                            this,
                                            i,
                                            item
                                        )}
                                        first={i === 0}
                                        key={i}
                                    />
                                );
                            })}
                        </ScrollView>
                    </View>
                </View>
            );
        }
    };

    render() {
        const { showSports, showTeams } = this.state;
        return (
            <View
                style={[
                    globalStyles.flexContainer,
                    globalStyles.mainContainer,
                    styles.container
                ]}
            >
                <View>
                    <Text
                        style={[styles.headerText, globalStyles.highlightColor]}
                    >
                        {showTeams ? 'My Teams' : 'My Sports'}
                    </Text>
                    <Text
                        style={styles.descriptionText}
                    >{`Pick a new Sports for contest,\nand play against your friends all season.`}</Text>
                </View>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexHCenter
                    ]}
                >
                    <View style={[globalStyles.flexHorizontal, { flex: 3 }]}>
                        {this.state.tabStates.map(({ text, state }, i) => (
                            <SmallCards
                                selected={state}
                                text={text}
                                onPress={this.onTabPress.bind(this, i)}
                                secondaryAction={this.secondaryActions[i].bind(
                                    this
                                )}
                                first={i === 0}
                                key={i}
                            />
                        ))}
                    </View>
                    <TouchableOpacity
                        onPress={this.plusPressed}
                        activeOpacity={0.5}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 50,
                                padding: 8
                            }}
                        >
                            <Icon
                                name="plus"
                                type="feather"
                                color="#fff"
                                size={18}
                                containerStyle={{
                                    transform: [
                                        {
                                            rotate: showSports
                                                ? '45deg'
                                                : '0deg'
                                        }
                                    ]
                                }}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                {this.renderTeamsVariant()}
                <View
                    style={[
                        globalStyles.flexContainer,
                        { paddingVertical: 10 }
                    ]}
                >
                    <View
                        style={[
                            globalStyles.flexContainer,
                            styles.scrollViewWrapper
                        ]}
                    >
                        <View>
                            {this.state.showSports
                                ? this.renderAvailableListing()
                                : this.renderAddedListing()}
                        </View>
                    </View>
                </View>
                {this.state.isMyTeamsRefreshing ||
                this.state.isMyTeamsRefreshing ||
                this.state.selectedTeams == 'inprogress' ||
                this.state.selectedTeams == 'inprogress' ||
                this.props.teams == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
