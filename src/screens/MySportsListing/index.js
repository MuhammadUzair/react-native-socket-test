import MySportsListing from './MySportsListing';
import { connect } from 'react-redux';
import { fetchTeams, reset } from '../../redux/epics/fetch-teams-epic';
import { fetchLeagues } from '../../redux/epics/fetch-leagues-epic';
import { addLeagues } from '../../redux/epics/add-leagues-epic';
import { addTeams } from '../../redux/epics/add-teams-epic';
import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ leagues, teams, teamsAdded }) => ({
    leagues,
    teams,
    teamsAdded
});

const mapDispatchToProps = dispatch => ({
    fetchLeagues: data => dispatch(fetchLeagues(data)),
    fetchTeams: data => dispatch(fetchTeams(data)),
    resetTeams: data => dispatch(reset(data)),
    addTeams: data => dispatch(addTeams(data)),
    addLeagues: data => dispatch(addLeagues(data))
});
// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(MySportsListing);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(MySportsListing, [{ propName: 'teams' }]));
