import { connect } from 'react-redux';

import AddFunds from './AddFunds';

import { fetchUser } from '../../redux/epics/fetch-user-epic';
import {
    tsevoCheckCompilanceStatus,
    resetTsevoCheckCompilanceStatus
} from '../../redux/epics/tsevo-check-compliances-epic';

const mapStateToProps = ({ user, tsevoCheckCompliances }) => ({
    user,
    tsevoCheckCompliances
});

const mapDispatchToProps = dispatch => ({
    fetchUser: data => dispatch(fetchUser(data)),
    tsevoCheckCompilanceStatus: data =>
        dispatch(tsevoCheckCompilanceStatus(data)),
    resetTsevoCheckCompilanceStatus: () =>
        dispatch(resetTsevoCheckCompilanceStatus())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddFunds);
