import { StyleSheet } from 'react-native';
import { os } from '../../utils';

export default StyleSheet.create({
    tabView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        backgroundColor: 'white'
    },
    inputContainer: {
        borderRadius: 50,
        borderWidth: StyleSheet.hairlineWidth,
        paddingVertical: os() ? 10 : 0,
        paddingHorizontal: os() ? 10 : 0,
        backgroundColor: 'white',
        width: '70%'
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    withdrawLinkText: {
        fontSize: 16,
        textAlign: 'center'
    }
});
