import React, { Component } from 'react';
import {
    View,
    WebView,
    Alert,
    TextInput,
    Text,
    TouchableOpacity,
    Linking
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {
    retrieveToken,
    os,
    permissionToGetGeolocation,
    getPermissionToGetGeolocation
} from '../../utils';
import { BASEURL_DEV } from '../../redux/epics/base-url';
import { globalStyles } from '../../assets/styles';
import styles from './styles';

import SmallCards from '../../components/SmallCards';
import HeaderBackButton from '../../components/HeaderBackButton';

export default class AddFunds extends Component {
    constructor(props) {
        super(props);
        this.amount = 0;
    }

    static navigationOptions = args => {
        const { navigation, navigationOptions } = args;
        return {
            headerLeft: (
                <HeaderBackButton
                    fetchForUpdate
                    onPress={() => navigation.goBack()}
                />
            )
        };
    };

    state = {
        token: '',
        long: '',
        lat: '',
        alt: '',
        tab: '',
        amount: 0
    };

    gotGeolocation = ({ coords: { longitude, latitude, altitude } }) =>
        this.setState({
            long: longitude,
            lat: latitude,
            alt: altitude
        });

    errorWhileGettingGeolocation = e => {
        Alert.alert('There was a problem', e.message);
    };

    setGeoLocationAndToken = () => {
        navigator.geolocation.getCurrentPosition(
            this.gotGeolocation,
            this.errorWhileGettingGeolocation
        );
        retrieveToken().then(token => this.setState({ token }));
    };

    async componentDidMount() {
        let token = await retrieveToken();
        this.props.resetTsevoCheckCompilanceStatus();
        this.props.tsevoCheckCompilanceStatus({ token });
        if (os()) {
            this.setGeoLocationAndToken();
        } else {
            if (permissionToGetGeolocation()) {
                this.setGeoLocationAndToken();
            } else {
                if (
                    getPermissionToGetGeolocation(
                        'Hello there',
                        "We need your geolocation, just to make sure that you're from US"
                    )
                ) {
                    this.setGeoLocationAndToken();
                } else {
                    Alert.alert(
                        'There was a problem',
                        'Sorry, but we need to know your location before we let you conduct any transaction'
                    );
                }
            }
        }
    }

    renderTextInput = () => {
        return (
            <View style={[globalStyles.highlightBorder, styles.inputContainer]}>
                <TextInput
                    ref={input => (this.input = input)}
                    placeholder="Enter withdrawal amount"
                    clearButtonMode="always"
                    keyboardType="numeric"
                    style={[
                        globalStyles.fontFace,
                        globalStyles.subTextFontSize,
                        globalStyles.fullWidth
                    ]}
                    onChangeText={value => (this.amount = value ? value : 0)}
                    underlineColorAndroid="rgba(0,0,0,0)"
                />
            </View>
        );
    };

    changeUrl = () => {
        Linking.openURL('https://live.postitplayit.com/app/login');
    };

    renderWebView = () => {
        const { token, long, lat, alt, tab, amount } = this.state;
        if (token)
            if (tab === 'PAY') {
                return (
                    <WebView
                        scalesPageToFit
                        source={{
                            uri: `${BASEURL_DEV}/api/v1/view/tsevo/add-funds?pay_type=${tab}&amount=null&latitude=${lat}&longitude=${long}&altitude=${alt}`,
                            headers: { Authorization: `Bearer ${token}` }
                        }}
                    />
                );
            } else if (tab === 'PAYOUT' /* && amount > 0 */) {
                console.log(
                    `${BASEURL_DEV}/api/v1/view/tsevo/add-funds?pay_type=${tab}&amount=${amount}&latitude=${lat}&longitude=${long}&altitude=${alt}`
                );
                return (
                    // <WebView
                    //     scalesPageToFit
                    //     source={{
                    //         uri: `${BASEURL_DEV}/api/v1/view/tsevo/add-funds?pay_type=${tab}&amount=${amount}&latitude=${lat}&longitude=${long}&altitude=${alt}`,
                    //         headers: { Authorization: `Bearer ${token}` },
                    //     }}
                    // />
                    <View
                        style={[
                            globalStyles.flexContainer,
                            globalStyles.flexHCenter,
                            globalStyles.flexVCenter
                        ]}
                    >
                        <TouchableOpacity onPress={this.changeUrl}>
                            <Text style={styles.withdrawLinkText}>
                                To withdraw the amount please visit {`\n`}
                                https://live.postitplayit.com
                            </Text>
                        </TouchableOpacity>
                    </View>
                );
            }
        return null;
    };

    onPressPay = () => {
        const { tsevoCheckCompliances } = this.props;
        if (
            tsevoCheckCompliances &&
            tsevoCheckCompliances.data &&
            tsevoCheckCompliances.data.compliance !== 'Allowed'
        ) {
            Alert.alert(
                'There was a problem',
                'The location detected has been set to be Blocked by the merchant or operator.'
            );
            this.setState({ playGameBtnPressed: false });
            return false;
        }
        if (tsevoCheckCompliances === '' || tsevoCheckCompliances.code >= 400) {
            Alert.alert(
                'There was a problem',
                'The location detected has been set to be Blocked by the merchant or operator.'
            );
            this.setState({ playGameBtnPressed: false });
            return false;
        }
        this.setState({ tab: 'PAY' });
    };
    onPressPayout = () => {
        const { tsevoCheckCompliances } = this.props;
        if (
            tsevoCheckCompliances &&
            tsevoCheckCompliances.data &&
            tsevoCheckCompliances.data.compliance !== 'Allowed'
        ) {
            Alert.alert(
                'There was a problem',
                'The location detected has been set to be Blocked by the merchant or operator.'
            );
            this.setState({ playGameBtnPressed: false });
            return false;
        }
        if (tsevoCheckCompliances === '' || tsevoCheckCompliances.code >= 400) {
            Alert.alert(
                'There was a problem',
                'The location detected has been set to be Blocked by the merchant or operator.'
            );
            this.setState({ playGameBtnPressed: false });
            return false;
        }
        this.setState({ tab: 'PAYOUT' });
    };
    onPressWithdraw = () => {
        if (this.props.user.data.amount < this.amount) {
            Alert.alert(
                'There was a problem',
                'your wallet amount is lesser than the withdrawal amount'
            );
        } else {
            if (this.amount > 0) this.setState({ amount: this.amount });
            else
                Alert.alert(
                    'There was a problem',
                    'Please enter an amount to withdraw'
                );
        }
    };

    render() {
        return (
            <View
                style={[
                    globalStyles.flexContainer,
                    { backgroundColor: 'white' },
                    globalStyles.mainContainer
                ]}
            >
                <Text
                    style={[
                        styles.headerText,
                        globalStyles.highlightColor,
                        { paddingTop: 10 }
                    ]}
                >
                    Funds
                </Text>
                <View style={styles.tabView}>
                    <SmallCards
                        text="Add Funds"
                        selected={this.state.tab === 'PAY'}
                        onPress={this.onPressPay}
                        style={{ width: '40%' }}
                        first
                    />
                    <SmallCards
                        text="Withdraw"
                        selected={this.state.tab === 'PAYOUT'}
                        onPress={this.onPressPayout}
                        style={{ width: '40%' }}
                        first
                    />
                </View>
                {/* {this.state.tab === 'PAYOUT' ? (
                    <View style={styles.tabView}>
                        {this.renderTextInput()}
                        <TouchableOpacity
                            style={{ borderRadius: 50 }}
                            onPress={this.onPressWithdraw}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    { padding: 9, borderRadius: 50 }
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor
                                    ]}
                                >
                                    Withdraw
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                ) : null} */}
                {this.renderWebView()}
            </View>
        );
    }
}
