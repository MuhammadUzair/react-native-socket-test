import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import CompetitionStyles from '../createCompetition/styles';
import styles from './styles';
import { globalStyles } from '../../assets/styles';
import {
    os,
    getDate,
    getTime,
    retrieveToken,
    getLocation,
    runLocationValidations,
    renderSubText,
    renderSpreadWithPlusSign,
    getTimeZone,
    getDashboardSelectedTeam,
    renderSubTextWithChangeSign
} from '../../utils';

import HeaderBackButton from '../../components/HeaderBackButton';
import OverlayIndicator from '../../components/OverlayIndicator';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import BlueHeader from '../../components/BlueHeader';
import CustomAvatar from '../../components/CustomAvatar';

export default class PlayIt extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingLeft: os() ? 10 : 15
            },
            headerLeft: (
                <HeaderBackButton
                    type="close"
                    onPress={() => {
                        navigation.dangerouslyGetParent().goBack();
                    }}
                />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    state = {
        enteredAmount: 0,
        totalAmount: 0,
        playGameBtnPressed: false,
        competitionDetail: [],
        subText: '',
        spreadTitle: '',
        amount: '',
        showLoader: true,
        fetchUser: true,
        teamStatus: {}
    };

    componentWillUnmount() {
        Keyboard.removeAllListeners('keyboardDidShow');
    }

    componentDidUpdate(prevProps, prevState) {
        const {
            competitionPlayed,
            navigation,
            fetchUser,
            resetPlayCompetition,
            screenProps,
            tsevoCheckCompliances,
            resetTsevoCheckCompilanceStatus
        } = this.props;
        if (competitionPlayed.code >= 200 && competitionPlayed.code < 300) {
            navigation.popToTop();
            navigation.navigate('MyCompetitions');
            resetPlayCompetition();
        }
        if (
            tsevoCheckCompliances.status == 'ERROR' &&
            tsevoCheckCompliances.messages[0]
        ) {
            resetTsevoCheckCompilanceStatus();
            Alert.alert(
                'There was a problem',
                tsevoCheckCompliances.messages[0]
            );
        }
    }

    componentDidMount = async () => {
        const {
            searchCompetition,
            fetchUser,
            resetTsevoCheckCompilanceStatus,
            tsevoCheckCompilanceStatus
        } = this.props;
        let token = await retrieveToken();
        resetTsevoCheckCompilanceStatus();
        tsevoCheckCompilanceStatus({ token });
        fetchUser(token);
        if (
            searchCompetition.data &&
            searchCompetition.data.competitions &&
            searchCompetition.data.competitions.length > 0
        ) {
            return false;
        } else {
            if (
                this.props.navigation.state.params &&
                this.props.navigation.state.params.item
            ) {
                let status = getDashboardSelectedTeam(
                    this.props.navigation.state.params.item
                );
                this.setState({
                    competitionDetail: this.props.navigation.state.params.item,
                    subText: this.props.navigation.state.params.subText,
                    spreadTitle: this.props.navigation.state.params.spreadTitle,
                    showLoader: false,
                    teamStatus: status
                });
            }
            Keyboard.addListener('keyboardDidShow', () =>
                this.scroll.props.scrollToEnd()
            );
        }
    };

    static getDerivedStateFromProps(newProps, prevState) {
        let competitionDetail = prevState.competitionDetail;
        let showLoader = prevState.showLoader;
        let subText = prevState.subText;
        let spreadTitle = prevState.spreadTitle;
        let amount = prevState.amount;

        if (
            newProps.searchCompetition.data &&
            newProps.searchCompetition.data.competitions &&
            newProps.searchCompetition.data.competitions.length > 0
        ) {
            let competition = newProps.searchCompetition.data.competitions[0];
            const subTextGenerate = `${
                competition.competitor.short_code
            } ${renderSubText(competition.spread, true)}`;
            const spreadTitleGenerate = `${
                competition.competitor.short_code
            } ${renderSpreadWithPlusSign(competition.spread)}`;

            competitionDetail = competition;
            subText = subTextGenerate;
            spreadTitle = spreadTitleGenerate;
            amount = competitionDetail.amount;
            showLoader = false;
        }
        if (
            newProps.searchCompetition.statue == 'successful' ||
            newProps.searchCompetition.statue == 'failed'
        ) {
            showLoader = false;
        }

        return { competitionDetail, showLoader, subText, spreadTitle, amount };
    }

    checkConstraints = () => {
        const { enteredAmount } = this.state;
        const { compliance, tsevoCheckCompliances } = this.props;
        const newEnteredAmount = parseFloat(enteredAmount);
        if (
            tsevoCheckCompliances &&
            tsevoCheckCompliances.data &&
            tsevoCheckCompliances.data.compliance !== 'Allowed'
        ) {
            Alert.alert(
                'There was a problem',
                'The location detected has been set to be Blocked by the merchant or operator.'
            );
            this.setState({ playGameBtnPressed: false });
            return false;
        }
        if (tsevoCheckCompliances === '' || tsevoCheckCompliances.code >= 400) {
            Alert.alert(
                'There was a problem',
                'The location detected has been set to be Blocked by the merchant or operator.'
            );
            this.setState({ playGameBtnPressed: false });
            return false;
        }

        if (!newEnteredAmount || Number.isNaN(newEnteredAmount)) {
            Alert.alert('Error', 'Please enter some amount.');
            this.setState({ playGameBtnPressed: false });
            return false;
        }

        return true;
    };

    dispatchPlayAction = async () => {
        let token = await retrieveToken();
        const { playCompetition } = this.props;
        const item = this.state.competitionDetail;
        const { enteredAmount } = this.state;
        const newEnteredAmount = parseFloat(enteredAmount);

        playCompetition({
            token,
            amount: newEnteredAmount,
            competition_id: item.id,
            sport_cid: item.sport_cid,
            owner_id: item.owner_id
        });
    };

    onPlayGameBtnClick = async () => {
        this.setState({ playGameBtnPressed: true });
        if (this.state.enteredAmount >= 5 && this.state.enteredAmount <= 1000) {
            if (!this.state.playGameBtnPressed) {
                if (this.checkConstraints()) {
                    await runLocationValidations(this.dispatchPlayAction);
                } else {
                    this.setState({ playGameBtnPressed: false });
                }
            }
        } else {
            Alert.alert(
                'Amount Limit',
                'Competition amount should be between $5 and $1,000'
            );
        }
    };

    onSeePaymentDetailBtnClick = () => {
        const { enteredAmount, subText, spreadTitle, teamStatus } = this.state;
        if (this.checkConstraints()) {
            const {
                navigation,
                settings: { serviceFee }
            } = this.props;
            const item = this.state.competitionDetail;
            const newEnteredAmount = parseFloat(enteredAmount);
            navigation.push('PaymentDetails', {
                item,
                enteredAmount: newEnteredAmount,
                totalAmount: (
                    newEnteredAmount +
                    newEnteredAmount * serviceFee
                ).toFixed(2),
                serviceFee,
                serviceFeeAmount: (newEnteredAmount * serviceFee).toFixed(2),
                subText,
                spreadTitle,
                teamStatus: teamStatus
            });
        }
    };

    inputHandler = enteredAmount => {
        this.setState({
            enteredAmount,
            playGameBtnPressed: false
        });
    };

    renderMyTeamRow = () => {
        const { competitionDetail, spreadTitle, teamStatus } = this.state;
        let title = '',
            uri = '',
            name = '',
            item = competitionDetail,
            spreadMsg = renderSubTextWithChangeSign(
                competitionDetail.spread,
                true
            );

        if (teamStatus.homeTeam) {
            title =
                item && item.competitor && item.competitor.short_code
                    ? item.competitor.short_code
                    : '';

            uri =
                item && item.competitor && item.competitor.avatar
                    ? item.competitor.avatar
                    : '';
            name =
                item && item.competitor && item.competitor.name
                    ? ' ' + item.competitor.name + ' '
                    : '';
        } else {
            title =
                item && item.team && item.team.short_code
                    ? item.team.short_code
                    : '';

            uri = item && item.team && item.team.avatar ? item.team.avatar : '';
            name =
                item && item.team && item.team.name
                    ? ' ' + item.team.name + ' '
                    : '';
        }

        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.flexSpaceBetween,
                    globalStyles.flexContainer
                ]}
            >
                <View style={[globalStyles.flexHorizontal]}>
                    <CustomAvatar
                        size="small"
                        checked={false}
                        title={title}
                        uri={uri}
                    />
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexVertical
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                styles.paddingLeftFive
                            ]}
                        >
                            {name + ' ' + renderSpreadWithPlusSign(item.spread)}
                        </Text>
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                globalStyles.baseBlueColor,
                                CompetitionStyles.confirmScreenSpread
                            ]}
                        >
                            {name && spreadMsg ? name + ' ' + spreadMsg : null}
                        </Text>
                    </View>
                </View>
                <View>
                    <Text
                        style={[
                            globalStyles.baseFontSize,
                            globalStyles.paleRedColor
                        ]}
                    >
                        {competitionDetail && competitionDetail.remaining_amount
                            ? '$' + competitionDetail.remaining_amount
                            : null}
                    </Text>
                </View>
            </View>
        );
    };
    render() {
        let {
            settings,
            competitionPlayed,
            searchCompetition,
            navigation
        } = this.props;
        let {
            competitionDetail,
            subText,
            spreadTitle,
            teamStatus
        } = this.state;
        const time =
            competitionDetail && competitionDetail.time_left
                ? competitionDetail.time_left.split(':')
                : '';
        const { enteredAmount } = this.state;
        const newEnteredAmount = parseFloat(enteredAmount);
        return (
            <KeyboardAwareScrollView
                innerRef={ref => (this.scroll = ref)}
                enableOnAndroid={true}
            >
                <BlueHeader
                    title="Play Competition"
                    navigation={this.props.navigation}
                    onPress={() =>
                        this.props.navigation.dangerouslyGetParent().goBack()
                    }
                />

                <View style={[globalStyles.whiteBackgroudColor]}>
                    <View
                        style={[
                            CompetitionStyles.rowContainer,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <View>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    CompetitionStyles.chooseTeamSection
                                ]}
                                opacity={!teamStatus.awayTeam ? 0.5 : 1}
                            >
                                <CustomAvatar
                                    size="small"
                                    checked={false}
                                    title={
                                        competitionDetail &&
                                        competitionDetail.team &&
                                        competitionDetail.team.short_code
                                            ? competitionDetail.team.short_code
                                            : ''
                                    }
                                    uri={
                                        competitionDetail &&
                                        competitionDetail.team &&
                                        competitionDetail.team.avatar
                                            ? competitionDetail.team.avatar
                                            : ''
                                    }
                                />
                                <View
                                    style={CompetitionStyles.teamNameButtonWrap}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseFontSize,
                                            this.state.selectedTeam == 'team'
                                                ? globalStyles.whiteColor
                                                : globalStyles.greyColor
                                        ]}
                                    >
                                        {competitionDetail &&
                                        competitionDetail.team &&
                                        competitionDetail.team.name
                                            ? ' ' +
                                              competitionDetail.team.name +
                                              ' '
                                            : null}
                                    </Text>
                                </View>
                            </View>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    CompetitionStyles.chooseTeamSection,
                                    styles.marginTopTen
                                ]}
                                opacity={!teamStatus.homeTeam ? 0.5 : 1}
                            >
                                <CustomAvatar
                                    size="small"
                                    checked={false}
                                    uri={
                                        competitionDetail &&
                                        competitionDetail.competitor &&
                                        competitionDetail.competitor.avatar
                                            ? competitionDetail.competitor
                                                  .avatar
                                            : ''
                                    }
                                    title={
                                        competitionDetail &&
                                        competitionDetail.competitor &&
                                        competitionDetail.competitor.short_code
                                            ? competitionDetail.competitor
                                                  .short_code
                                            : ''
                                    }
                                />

                                <View
                                    style={CompetitionStyles.teamNameButtonWrap}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseFontSize,
                                            this.state.selectedTeam ==
                                            'competitor'
                                                ? globalStyles.whiteColor
                                                : globalStyles.greyColor
                                        ]}
                                    >
                                        {competitionDetail &&
                                        competitionDetail.competitor &&
                                        competitionDetail.competitor.name
                                            ? ' ' +
                                              competitionDetail.competitor
                                                  .name +
                                              ' '
                                            : null}
                                    </Text>
                                </View>
                            </View>
                            <Text
                                style={[
                                    globalStyles.subTextFontSize,
                                    globalStyles.greyColor,
                                    CompetitionStyles.marginTop
                                ]}
                            >
                                {getDate(competitionDetail.start_date_time) +
                                    ' ' +
                                    getTime(competitionDetail.start_date_time) +
                                    ' ' +
                                    getTimeZone(competitionDetail.created_at)}
                            </Text>
                        </View>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                CompetitionStyles.locationSection,
                                globalStyles.flexVTop,
                                styles.timerMargin
                            ]}
                        >
                            <View style={globalStyles.flexVertical}>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        CompetitionStyles.locationText,
                                        CompetitionStyles.marginTopTen,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Competition starts in
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.baseBlueColor,
                                        globalStyles.subTextFontSize,
                                        CompetitionStyles.locationText
                                    ]}
                                >
                                    {competitionDetail.days_left} D, {time[0]}{' '}
                                    H, {time[1]} M Left
                                </Text>
                            </View>
                        </View>
                    </View>

                    {/* My Teams - start */}

                    <View
                        style={[
                            CompetitionStyles.rowContainer,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        {this.renderMyTeamRow()}
                    </View>

                    {/* My Teams - end */}

                    {/* Amount section - start */}

                    <View
                        style={[
                            CompetitionStyles.rowContainer,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <Text
                            style={[
                                CompetitionStyles.widthForty,
                                globalStyles.subTextFontSize
                            ]}
                        >
                            Amount
                        </Text>
                        <View style={[globalStyles.flexHorizontal]}>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.circularCircle,
                                    { width: 80, height: 30 }
                                ]}
                            >
                                <View
                                    style={{
                                        borderRadius: 50,
                                        overflow: 'hidden',
                                        zIndex: 10
                                    }}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseBlueColor,
                                            globalStyles.whiteBackgroudColor,
                                            globalStyles.circularCircle,
                                            styles.currencySignText,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        $
                                    </Text>
                                </View>
                                <TextInput
                                    style={[
                                        styles.inputText,
                                        globalStyles.baseFontSize
                                    ]}
                                    multiline={false}
                                    keyboardType="numeric"
                                    onChangeText={this.inputHandler}
                                    underlineColorAndroid="rgba(0,0,0,0)"
                                    value={`${
                                        this.state.enteredAmount !== 0
                                            ? this.state.enteredAmount
                                            : ''
                                    }`}
                                />
                            </View>
                        </View>
                    </View>

                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexVBottom,
                            styles.amountSection
                        ]}
                    >
                        {!this.state.enteredAmount ? (
                            <Text
                                style={[
                                    globalStyles.errorColor,
                                    globalStyles.subTextFontSize,
                                    styles.errorAmount
                                ]}
                            >
                                The amount field is required.
                            </Text>
                        ) : null}
                    </View>

                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexVBottom,
                            styles.amountSection
                        ]}
                    >
                        <View style={globalStyles.flexVertical}>
                            <View style={globalStyles.flexHorizontal}>
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        styles.totalHeading,
                                        globalStyles.subTextFontSize,
                                        styles.marginTopTen
                                    ]}
                                >
                                    Service Charges:
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.highlightPinkColor,
                                        styles.textAlignRight,
                                        styles.amountWidth,
                                        styles.marginTopTen
                                    ]}
                                >
                                    {settings &&
                                        settings.serviceFee &&
                                        settings.serviceFee * 100 + '%'}
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexVBottom,
                            styles.amountSection
                        ]}
                    >
                        <View style={globalStyles.flexHorizontal}>
                            <Text
                                style={[
                                    globalStyles.greyColor,
                                    styles.totalHeading,
                                    globalStyles.subTextFontSize,
                                    styles.marginTopTen,
                                    styles.paddingLeftForty
                                ]}
                            >
                                Total Amount:
                            </Text>

                            <Text
                                style={[
                                    globalStyles.highlightPinkColor,
                                    CompetitionStyles.totalAmountText,
                                    CompetitionStyles.textAlignRight,
                                    styles.marginTopTen
                                ]}
                            >
                                {Number.isNaN(newEnteredAmount)
                                    ? `0.00`
                                    : (
                                          newEnteredAmount +
                                          newEnteredAmount * settings.serviceFee
                                      ).toFixed(2)}
                            </Text>
                        </View>
                    </View>

                    {/* Amount Section - end */}

                    {/* Share with  - start*/}

                    <View
                        style={[
                            CompetitionStyles.rowContainer,
                            globalStyles.hairLineUnderBorder,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <View style={globalStyles.flexVTop}>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.dangerouslyGetParent().goBack()
                                }
                                activeOpacity={0.5}
                                style={[CompetitionStyles.marginTop]}
                            >
                                <LinearGradient
                                    colors={['#F5F5F5', '#F5F5F5']}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={[
                                        globalStyles.flexHCenter,
                                        globalStyles.flexSpaceBetween,
                                        CompetitionStyles.backButtonWrap
                                    ]}
                                >
                                    <Text
                                        style={[
                                            CompetitionStyles.footerButonText,
                                            globalStyles.greyColor
                                        ]}
                                    >
                                        Back
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>

                        <View style={globalStyles.flexVBottom}>
                            <TouchableOpacity
                                onPress={this.onSeePaymentDetailBtnClick}
                                activeOpacity={0.5}
                                style={[CompetitionStyles.marginTop]}
                                disabled={this.state.submitBtnPressed}
                            >
                                <LinearGradient
                                    colors={
                                        this.state.submitBtnPressed
                                            ? ['#b2bec3', '#b2bec3']
                                            : ['#4d6bc2', '#3695e3']
                                    }
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={[
                                        globalStyles.flexHCenter,
                                        globalStyles.flexSpaceBetween,
                                        CompetitionStyles.backButtonWrap
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.whiteColor,
                                            CompetitionStyles.footerButonText
                                        ]}
                                    >
                                        Play Game
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* Share with  - end */}
                </View>
                {competitionPlayed === 'inprogress' ||
                searchCompetition.status === 'inprogress' ||
                this.state.showLoader ? (
                    <OverlayIndicator />
                ) : null}
            </KeyboardAwareScrollView>
        );
    }
}
