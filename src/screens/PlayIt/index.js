import { connect } from 'react-redux';

import PlayIt from './PlayIt';
import {
    playCompetition,
    resetPlayCompetition
} from '../../redux/epics/play-competition-epic';
import { fetchUser } from '../../redux/epics/fetch-user-epic';
import {
    fetchSpecificCompetition,
    resetFetchSpecificCompetition
} from '../../redux/epics/fetch-specific-competition-epic';
import {
    tsevoCheckCompilanceStatus,
    resetTsevoCheckCompilanceStatus
} from '../../redux/epics/tsevo-check-compliances-epic';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({
    settings,
    competitionPlayed,
    compliance,
    searchCompetition,
    tsevoCheckCompliances
}) => ({
    settings,
    competitionPlayed,
    compliance,
    searchCompetition,
    tsevoCheckCompliances
});

const mapDispatchToProps = dispatch => ({
    playCompetition: data => dispatch(playCompetition(data)),
    fetchUser: data => dispatch(fetchUser(data)),
    resetPlayCompetition: () => dispatch(resetPlayCompetition()),
    fetchSpecificCompetition: data => dispatch(fetchSpecificCompetition(data)),
    resetFetchSpecificCompetition: data =>
        dispatch(resetFetchSpecificCompetition(data)),
    tsevoCheckCompilanceStatus: data =>
        dispatch(tsevoCheckCompilanceStatus(data)),
    resetTsevoCheckCompilanceStatus: () =>
        dispatch(resetTsevoCheckCompilanceStatus())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(PlayIt, [{ propName: 'competitionPlayed' }]));
