import { StyleSheet } from 'react-native';
import { os } from '../../utils';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
export default StyleSheet.create({
    container: {
        backgroundColor: '#fafafa'
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5
    },
    scrollView: {
        marginVertical: 10,
        paddingHorizontal: 20
    },
    circularCircle: {
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#EEE',
        padding: 5,
        textAlign: 'center',
        alignSelf: 'flex-end',
        position: 'relative',
        zIndex: 9
    },
    searchHolder: {
        backgroundColor: 'white',
        borderRadius: 50,
        marginVertical: 10,
        paddingHorizontal: 10
    },
    searchBar: {
        height: 50,
        elevation: 5
    },
    card: {
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 20
    },
    image: {
        // width: 35,
        // height: 35,
        // borderRadius: 50,
        marginRight: 5,
        marginLeft: 5
    },
    name: {
        color: 'black',
        fontWeight: 'bold',
        marginRight: 10
    },
    smallFont: {
        fontSize: 10
    },
    versus: {
        fontWeight: 'bold'
    },
    bigFontSize: {
        fontSize: 15
    },
    black: {
        color: '#000'
    },
    featuredBadge: {
        borderRadius: 50,
        backgroundColor: '#ff2458',
        // height: 15,
        color: 'white',
        paddingHorizontal: 10
    },
    btnHolder: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 50
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold'
    },
    blueBadge: {
        backgroundColor: '#2d94f6',
        borderRadius: 50,
        fontSize: 10,
        paddingHorizontal: 2,
        color: 'white'
    },
    amount: {
        color: '#ff59a7'
    },
    mb: {
        marginBottom: 20
    },
    scrollContainer: {
        // flexGrow: 1,flexShrink: 1, flexBasis: 1
        // paddingBottom: 40,
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    rowContainer: {
        paddingVertical: os() ? 18 : 12,
        borderBottomColor: '#bdc3c7'
    },
    postGameCard: {
        height: 50,
        width: '100%',
        position: 'absolute',
        top: 40,
        marginLeft: os() ? 20 : 10
    },
    timerMargin: {
        marginTop: verticalScale(-5)
    },
    amountSection: {
        paddingRight: 20
    },
    inputText: {
        width: 50,
        height: 20,
        paddingRight: 2,
        marginRight: 8,
        color: 'black',
        borderWidth: 0,
        textAlign: 'right',
        textAlignVertical: 'center'
    },
    currencySignText: {
        paddingTop: 0
    },
    totalHeading: {
        width: 150
        // marginRight: 20
    },
    marginTopTen: {
        marginTop: 10
    },
    paddingLeftForty: {
        paddingLeft: 40
    },
    paddingLeftFive: {
        paddingLeft: 5
    }
});
