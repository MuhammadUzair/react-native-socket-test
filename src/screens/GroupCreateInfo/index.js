import { connect } from 'react-redux';

import { createGroup } from '../../redux/epics/create-group-epic';
import GroupCreateInfo from './GroupCreateInfo';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ groupCreated }) => ({ groupCreated });

const mapDispatchToProps = dispatch => ({
    createGroup: data => dispatch(createGroup(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(GroupCreateInfo, [{ propName: 'groupCreated' }]));
