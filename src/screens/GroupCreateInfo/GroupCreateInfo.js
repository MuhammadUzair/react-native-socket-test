import React, { Component } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import { globalStyles } from '../../assets/styles';
import styles from './styles';

import { os, retrieveToken } from '../../utils';

import CustomAvatar from '../../components/CustomAvatar';
import HeaderBackButton from '../../components/HeaderBackButton';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import OverlayIndicator from '../../components/OverlayIndicator';
import HeaderRightIcon from '../../components/HeaderRightIcon';

export default class GroupCreateInfo extends Component {
    state = {
        token: ''
    };
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingLeft: os() ? 10 : 15
            },
            headerLeft: (
                <HeaderBackButton onPress={() => navigation.goBack()} />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    componentDidUpdate(prevProps, prevState) {
        const { groupCreated } = this.props;
        if (groupCreated.code >= 200 && groupCreated.code < 300)
            this.props.navigation.navigate('GroupScreen');
    }

    renderListItem = ({ item, index }) => {
        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.flexHCenter,
                    globalStyles.fullWidth,
                    globalStyles.hairLineUnderBorder,
                    { padding: 10 }
                ]}
            >
                <CustomAvatar
                    uri={item.avatar}
                    rounded
                    medium
                    containerStyle={{ marginRight: 10 }}
                />
                <View style={globalStyles.flexContainer}>
                    <Text style={globalStyles.blackColor}>{item.name}</Text>
                    <Text
                        style={[
                            globalStyles.greyColor,
                            globalStyles.subTextFontSize
                        ]}
                    >
                        {item.username}
                    </Text>
                </View>
                <Text style={globalStyles.baseBlueColor}>Added</Text>
            </View>
        );
    };

    create = () => {
        const {
            createGroup,
            navigation: {
                state: {
                    params: { name, description, privacy, friends }
                }
            }
        } = this.props;
        if (this.state.token) {
            createGroup({ token, name, description, privacy, friends });
        } else {
            retrieveToken().then(token => {
                this.setState({ token }, () => {
                    createGroup({ token, name, description, privacy, friends });
                });
            });
        }
    };

    emptyList = () => {
        return (
            <View style={styles.emptyGroupMembersView}>
                <EmptyListIndicator message="No Members added" />
            </View>
        );
    };

    render() {
        const {
            navigation: {
                state: {
                    params: { privacy, name, description, friends }
                }
            },
            groupCreated
        } = this.props;

        return (
            <View style={[globalStyles.flexContainer]}>
                <View style={[globalStyles.mainContainer, { marginTop: 5 }]}>
                    <View
                        style={[
                            globalStyles.greyBackgroudColor,
                            styles.postGameCard,
                            styles.borderRadius
                        ]}
                    />
                    <View
                        style={[
                            styles.bottomSpacing,
                            globalStyles.flexHorizontal,
                            globalStyles.flexVCenter
                        ]}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexHCenter,
                                styles.bigPrimaryButton
                            ]}
                        >
                            <Text
                                style={[
                                    globalStyles.fontFace,
                                    globalStyles.whiteColor,
                                    styles.bigPrimaryButtonText
                                ]}
                            >
                                Create Group
                            </Text>
                        </LinearGradient>
                    </View>
                    <View
                        style={[
                            globalStyles.whiteBackgroudColor,
                            styles.borderRadius,
                            { overflow: 'hidden' }
                        ]}
                    >
                        <View style={[globalStyles.greyBackgroudColor]}>
                            <View
                                style={[
                                    styles.rowContainer,
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween,
                                    globalStyles.hairLineUnderBorder
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Group Name
                                </Text>
                                <Text
                                    numberOfLines={1}
                                    ellipsizeMode="tail"
                                    style={[
                                        globalStyles.baseBlueColor,
                                        globalStyles.boldFontFace,
                                        globalStyles.baseFontSize,
                                        styles.textValueStyle
                                    ]}
                                >
                                    {name}
                                </Text>
                            </View>

                            <View
                                style={[
                                    styles.rowContainer,
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween,
                                    globalStyles.hairLineUnderBorder
                                ]}
                            >
                                <Text style={[globalStyles.baseGreyColor]}>
                                    Description
                                </Text>
                                <Text
                                    numberOfLines={1}
                                    ellipsizeMode="tail"
                                    style={[
                                        globalStyles.baseBlueColor,
                                        globalStyles.boldFontFace,
                                        globalStyles.baseFontSize,
                                        styles.textValueStyle
                                    ]}
                                >
                                    {description}
                                </Text>
                            </View>

                            <View
                                style={[
                                    styles.rowContainer,
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween,
                                    globalStyles.hairLineUnderBorder
                                ]}
                            >
                                <Text
                                    style={[
                                        styles.widthForty,
                                        globalStyles.baseGreyColor
                                    ]}
                                >
                                    Privacy
                                </Text>
                                <View style={[globalStyles.flexHorizontal]}>
                                    <Text
                                        style={globalStyles.highlightPinkColor}
                                    >
                                        {privacy}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View
                    style={[
                        { flexDirection: 'row', marginVertical: 10 },
                        globalStyles.flexSpaceBetween,
                        globalStyles.mainContainer,
                        globalStyles.fullWidth
                    ]}
                >
                    <Text style={[globalStyles.baseFontSize]}>Members</Text>
                </View>
                <View
                    style={[
                        globalStyles.mainContainer,
                        globalStyles.flexContainer
                    ]}
                >
                    <FlatList
                        data={friends}
                        renderItem={this.renderListItem}
                        style={[
                            globalStyles.flexContainer,
                            globalStyles.whiteBackgroudColor,
                            styles.borderRadiusTop
                        ]}
                        ListEmptyComponent={this.emptyList()}
                        keyExtractor={(item, index) =>
                            `id: ${item.friend_id}, index: ${index}`
                        }
                    />
                </View>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        styles.paddingAll,
                        styles.navIconGreyBackColor
                    ]}
                >
                    <TouchableOpacity
                        onPress={() =>
                            this.props.navigation.navigate('GroupScreen')
                        }
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween,
                            styles.bottomSpacing
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.highlightPinkColor,
                                styles.cancelCenterButton
                            ]}
                        >
                            Cancel Post
                        </Text>
                    </TouchableOpacity>

                    <View>
                        <TouchableOpacity
                            onPress={this.create}
                            activeOpacity={0.5}
                            style={[globalStyles.flexHCenter]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        { width: '50%' },
                                        globalStyles.whiteColor,
                                        styles.bigPrimaryButtonText
                                    ]}
                                >
                                    Confirm
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {groupCreated === 'inprogress' ? <OverlayIndicator /> : null}
            </View>
        );
    }
}
