import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Picker } from 'react-native';
import { globalStyles, greyColor } from '../../assets/styles';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';

import { Icon } from 'react-native-elements';
import { os, retrieveToken, getDate } from '../../utils';

import RNPickerSelect from 'react-native-picker-select';
import DateTimePicker from 'react-native-modal-datetime-picker';
import HeaderBackButton from '../../components/HeaderBackButton';

export default class Notifications extends Component {
    static navigationOptions = args => {
        const { navigation, navigationOptions } = args;
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20,
            },
            headerLeft: (
                <HeaderBackButton
                    type="close"
                    onPress={() => navigation.goBack()}
                />
            ),
        };
    };

    static getDerivedStateFromProps(props, state) {
        const { fType, tType } = props.filters;
        let toDate = '';
        let fromDate = '';

        if (props.filters.toDate) {
            toDate = new Date(props.filters.toDate);
            toDate = toDate
                .toDateString()
                .substr(4, toDate.toDateString().lenght);
            toDate = toDate.slice(0, 6) + ',' + toDate.slice(6);
        }
        if (props.filters.fromDate) {
            fromDate = new Date(props.filters.fromDate);
            fromDate = fromDate
                .toDateString()
                .substr(4, fromDate.toDateString().lenght);
            fromDate = fromDate.slice(0, 6) + ',' + fromDate.slice(6);
        }

        return {
            fromDate: fromDate,
            toDate: toDate,
            fType,
            tType,
        };
    }

    pickerStyles = {
        inputIOS: { height: '100%', fontSize: 16, color: '#000' },
        inputAndroid: {
            height: '100%',
            // fontSize: 16,
            paddingLeft: 0,
            paddingHorizontal: 0,
            padding: 0,
            left: -8,
            color: '#000',
        },
        viewContainer: {
            paddingLeft: 0,
            paddingHorizontal: 0,
            padding: 0,
            height: os() ? 50 : 40,
            justifyContent: 'center',
            borderBottomWidth: 1,
            borderColor: 'rgba(163,163,163, 0.87)',
        },
        icon: {
            position: 'absolute',
            top: 22,
            borderTopWidth: 6,
            borderLeftWidth: 6,
            borderRightWidth: 6,
            right: 4,
        },
        underline: { borderTopWidth: 0 },
        placeholderColor: 'rgba(163,163,163, 0.87)',
    };

    transactionsType = [
        { label: 'Refund', value: 'Refund' },
        { label: 'Debit', value: 'Deduct' },
        { label: 'Credit', value: 'Deposit' },
    ];

    feeType = [
        { label: 'Service Charges', value: 'Service Charges' },
        { label: 'General', value: 'General' },
        { label: 'Competition', value: 'Competition' },
    ];

    state = {
        isFromDatePickerVisible: false,
        isToDatePickerVisible: false,
        fromDate: '',
        toDate: '',
        transactionType: '',
        feeType: '',
        fType: '',
        tType: '',
    };

    resetState = async () => {
        const token = await retrieveToken();
        this.props.applyFilter({
            fType: '',
            tType: '',
            toDate: '',
            fromDate: '',
            token,
        });
        // this.props.navigation.goBack();
        // this.setState({
        //     fromDate: '',
        //     toDate: '',
        //     fType: '',
        //     tType: ''
        // });
    };

    selectFType = val => this.setState({ fType: val });
    selectTType = val => this.setState({ tType: val });

    _showFromDatePicker = () =>
        this.setState({
            isFromDatePickerVisible: true,
        });

    _hideFromDatePicker = () =>
        this.setState({ isFromDatePickerVisible: false });

    _handleFromDatePicked = date => {
        var newDate = getDate(new Date(date), true);
        this._hideFromDatePicker();
        this.setState({ fromDate: newDate });
    };
    _showToDatePicker = () =>
        this.setState({
            isToDatePickerVisible: true,
        });

    _hideToDatePicker = () => this.setState({ isToDatePickerVisible: false });

    _handleToDatePicked = date => {
        var newDate = getDate(new Date(date), true);
        this._hideToDatePicker();
        this.setState({ toDate: newDate });
    };

    onCrossPressed = () => {
        this.props.navigation.goBack();
    };
    onApplyFilterPress = () => {
        const { fType, tType, token } = this.state;
        let toDate = new Date(this.state.toDate);
        let fromDate = new Date(this.state.fromDate);
        if (toDate) {
            toDate = `${toDate.getFullYear()}-${toDate.getMonth() +
                1}-${toDate.getDate()}`;
        }

        fromDate = `${fromDate.getFullYear()}-${fromDate.getMonth() +
            1}-${fromDate.getDate()}`;

        if (!token)
            retrieveToken().then(token => {
                this.setState({ token }, () => {
                    this.props.applyFilter({
                        fType,
                        tType,
                        toDate: this.state.toDate ? toDate : '',
                        fromDate: this.state.fromDate ? fromDate : '',
                        token,
                    });
                });
            });
        else this.props.applyFilter({ fType, tType, toDate, fromDate, token });
        this.props.navigation.goBack();
    };
    resetFilterButtonView = () => {
        const { fromDate, toDate, fType, tType } = this.state;
        if (fromDate || toDate || fType || tType) {
            return (
                <View style={{ paddingBottom: 10 }}>
                    <TouchableOpacity
                        onPress={this.resetState}
                        activeOpacity={0.5}
                        style={[globalStyles.flexHCenter, styles.bottomSpacing]}
                    >
                        <Text
                            style={[
                                globalStyles.fontFace,
                                globalStyles.blackColor,
                                styles.bigPrimaryButtonText,
                            ]}
                        >
                            Reset Filters
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        }
    };

    render() {
        return (
            <View
                style={[
                    globalStyles.flexContainer,
                    globalStyles.whiteBackgroudColor,
                ]}
            >
                <View style={[{ paddingBottom: 15 }]}>
                    <View style={[styles.hairLineUnderBorder]}>
                        <View
                            style={[
                                globalStyles.mainContainer,
                                { padding: 10 },
                            ]}
                        >
                            <Text
                                style={[
                                    styles.headerText,
                                    globalStyles.highlightColor,
                                ]}
                            >
                                Filter Your Serach
                            </Text>
                            <Text
                                style={styles.descriptionText}
                            >{`View all the recent transactions and funds updates in your account.`}</Text>
                        </View>
                    </View>
                    <TouchableOpacity
                        onPress={this.onCrossPressed}
                        activeOpacity={0.5}
                        style={{
                            marginRight: 15,
                            right: 0,
                            bottom: 0,
                            position: 'absolute',
                            zIndex: 10,
                        }}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 50,
                                padding: 8,
                            }}
                        >
                            <Icon
                                name={'close'}
                                type={'evillcons'}
                                size={18}
                                color="#fff"
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                <View
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.mainContainer,
                    ]}
                >
                    <View style={{ marginTop: 10 }}>
                        <RNPickerSelect
                            style={this.pickerStyles}
                            mode="dropdown"
                            placeholder={{
                                label: 'Select a transaction type',
                                value: '',
                            }}
                            items={this.transactionsType}
                            onValueChange={this.selectTType}
                            value={this.state.tType}
                            hideDoneBar
                        />
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <RNPickerSelect
                            placeholderColor="rgba(163,163,163, 0.87)"
                            style={this.pickerStyles}
                            mode="dropdown"
                            placeholder={{
                                label: 'Select fee type',
                                value: '',
                            }}
                            items={this.feeType}
                            onValueChange={this.selectFType}
                            value={this.state.fType}
                            hideDoneBar
                        />
                    </View>
                    <TouchableOpacity onPress={this._showFromDatePicker}>
                        <View
                            style={[
                                styles.rowContainer,
                                globalStyles.flexHorizontal,
                                globalStyles.flexSpaceBetween,
                                styles.hairLineBotom,
                                { marginTop: 0 },
                            ]}
                        >
                            <Text
                                style={[
                                    this.state.fromDate
                                        ? globalStyles.blackColor
                                        : globalStyles.greyColor,
                                    styles.buttonText,
                                ]}
                            >
                                {this.state.fromDate
                                    ? this.state.fromDate
                                    : 'From'}
                            </Text>

                            <Icon
                                name={'calendar'}
                                type={'evilicon'}
                                size={20}
                                color={greyColor}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._showToDatePicker}>
                        <View
                            style={[
                                styles.rowContainer,
                                globalStyles.flexHorizontal,
                                globalStyles.flexSpaceBetween,
                                styles.hairLineBotom,
                                { marginTop: 0 },
                            ]}
                        >
                            <Text
                                style={[
                                    this.state.toDate
                                        ? globalStyles.blackColor
                                        : globalStyles.greyColor,
                                    styles.buttonText,
                                ]}
                            >
                                {this.state.toDate ? this.state.toDate : 'To'}
                            </Text>
                            <Icon
                                name={'calendar'}
                                type={'evilicon'}
                                size={20}
                                color={greyColor}
                            />
                        </View>
                    </TouchableOpacity>
                    <DateTimePicker
                        mode="date"
                        isVisible={this.state.isFromDatePickerVisible}
                        onConfirm={this._handleFromDatePicked}
                        onCancel={this._hideFromDatePicker}
                        date={
                            this.state.fromDate
                                ? new Date(this.state.fromDate)
                                : new Date()
                        }
                        maximumDate={new Date()}
                    />
                    <DateTimePicker
                        mode="date"
                        isVisible={this.state.isToDatePickerVisible}
                        onConfirm={this._handleToDatePicked}
                        onCancel={this._hideToDatePicker}
                        date={
                            this.state.toDate
                                ? new Date(this.state.toDate)
                                : new Date()
                        }
                        minimumDate={
                            this.state.fromDate
                                ? new Date(this.state.fromDate)
                                : new Date()
                        }
                        maximumDate={new Date()}
                    />
                </View>

                <View style={{ paddingBottom: 5 }}>
                    <TouchableOpacity
                        onPress={this.onApplyFilterPress}
                        activeOpacity={0.5}
                        style={[globalStyles.flexHCenter, styles.bottomSpacing]}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexHCenter,
                                globalStyles.flexSpaceBetween,
                                styles.bigPrimaryButton,
                            ]}
                        >
                            <Text
                                style={[
                                    globalStyles.fontFace,
                                    globalStyles.whiteColor,
                                    styles.bigPrimaryButtonText,
                                ]}
                            >
                                Apply Filters
                            </Text>
                            <Icon color="#FFF" name="arrow-forward" />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                {this.resetFilterButtonView()}
            </View>
        );
    }
}
