import { connect } from 'react-redux';

import { applyFilter, resetFilter } from '../../redux/epics/filter-action';
import {} from '../../redux/epics/fetch-transactions-epic';

import MyWalletSearch from './MyWalletSearch';

const mapStateToProps = ({ filters }) => ({ filters });
const mapDispatchToProps = dispatch => ({
    applyFilter: data => dispatch(applyFilter(data)),
    resetFilter: () => dispatch(resetFilter())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyWalletSearch);
