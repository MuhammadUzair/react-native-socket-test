import { StyleSheet } from 'react-native';
import { greyColor, baseGrey } from '../../assets/styles';
import { os } from '../../utils';

export default StyleSheet.create({
    container: {
        paddingVertical: 10
    },
    card: {
        padding: 5
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    subText: {
        fontSize: 18,
        color: 'black',
        marginBottom: 5
    },
    mb: {
        marginBottom: 10
    },
    bottomSpacing: {
        marginBottom: os() ? 12 : 6
    },
    rowContainer: {
        paddingVertical: os() ? 18 : 12,
        borderBottomColor: '#bdc3c7'
    },
    bigPrimaryButtonText: {
        fontSize: 18
    },
    bigPrimaryButton: {
        paddingVertical: os() ? 20 : 15,
        paddingHorizontal: os() ? 25 : 20,
        borderRadius: 50,
        flexDirection: 'row',
        width: os() ? 60 + '%' : 50 + '%'
    },

    descriptionText: {
        color: 'black',
        fontSize: 12,
        marginBottom: 10
    },
    hairLineUnderBorder: {
        borderBottomWidth: 8,
        borderColor: '#f5f5f5'
    },
    hairLineBotom: {
        borderBottomWidth: 1,
        borderColor: '#f5f5f5'
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    buttonStyle: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row',
        width: 70 + '%'
    },
    cardContainer: {
        elevation: 3,
        flex: 1,
        borderBottomColor: baseGrey,
        borderBottomWidth: 1
    },
    buttonText: {
        fontSize: 16
    },
    nameHolder: {
        flex: 4
    },
    stateHolder: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    scrollContainer: {
        paddingHorizontal: 20
    },
    scrollViewWrapper: {
        backgroundColor: 'white',
        borderRadius: 5,
        paddingVertical: 10
    },
    scrollView: {
        marginTop: 5,
        paddingHorizontal: 0
    },
    paddedContainer: {
        paddingHorizontal: 0
    },
    searchHolder: {
        paddingHorizontal: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        marginVertical: 5
    },
    searchBar: {
        height: 40,
        backgroundColor: 'white'
    }
});
