import { connect } from 'react-redux';
import {
    fetchMyCompetitions,
    refreshMyCompetitions,
    resetMyCompetitions
} from '../../redux/epics/fetch-my-competition-epic';
import {
    searchMyCompetitions,
    resetSearchMyCompetitions
} from '../../redux/epics/search-my-competition-epic';
import { fetchUser } from '../../redux/epics/fetch-user-epic';

import MyCompetition from './MyCompetition';
import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ myCompetitions, searchedMyCompetitions }) => ({
    myCompetitions,
    searchedMyCompetitions
});

const mapDispatchToProps = dispatch => ({
    fetchMyCompetitions: data => dispatch(fetchMyCompetitions(data)),
    refreshMyCompetitions: data => dispatch(refreshMyCompetitions(data)),
    searchMyCompetitions: data => dispatch(searchMyCompetitions(data)),
    resetSearchMyCompetitions: () => resetSearchMyCompetitions(),
    resetMyCompetitions: () => dispatch(resetMyCompetitions()),
    fetchUser: data => dispatch(fetchUser(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(MyCompetition, [
        { propName: 'myCompetitions', actionOnFailure: 'resetMyCompetitions' },
        {
            propName: 'searchedMyCompetitions',
            actionOnFailure: 'resetSearchMyCompetitions'
        }
    ])
);
