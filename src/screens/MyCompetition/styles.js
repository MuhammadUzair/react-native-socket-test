import { StyleSheet } from 'react-native';
import { scale } from 'react-native-size-matters';

export default StyleSheet.create({
    conateinr: {
        flex: 1,
        backgroundColor: 'red'
    },
    cardContainer: {
        justifyContent: 'space-around',
        marginVertical: 10
    },
    paddedContainer: {
        // paddingHorizontal: 20
    },
    searchHolder: {
        paddingHorizontal: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        marginVertical: 10
    },
    searchBar: {
        height: 40,
        backgroundColor: 'white'
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 20,
        overflow: 'hidden'
    },
    cardTop: {
        padding: 10,
        paddingBottom: 0
    },
    cardMid: {
        paddingHorizontal: 10,
        paddingBottom: 10,
        paddingTop: 0
    },
    cardBottom: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    image: {
        marginRight: 10
    },
    name: {
        color: 'black',
        fontWeight: 'bold',
        marginRight: 10
    },
    versus: {
        fontWeight: 'bold',
        color: '#000'
    },
    btnHolder: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 50
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold'
    },
    blueBadge: {
        backgroundColor: '#2d94f6',
        borderRadius: 50,
        fontSize: 10,
        paddingHorizontal: 2,
        color: 'white'
    },
    amount: {
        color: '#ff59a7'
    },
    mb: {
        marginBottom: 20
    },
    resultRow: {
        alignItems: 'center',
        paddingVertical: 5,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    noActionInfo: {
        marginLeft: scale(13)
    }
});
