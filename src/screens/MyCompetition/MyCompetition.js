import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    FlatList
} from 'react-native';
import { Icon } from 'react-native-elements';

import {
    os,
    retrieveToken,
    generateErrorMessage,
    generateAndPrintErrorMessage,
    getDate,
    getTime,
    renderSubText,
    getSelectedTeam
} from '../../utils';
import styles from './styles';

import { globalStyles } from '../../assets/styles';

import SmallCards from '../../components/SmallCards';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import DashboardCard from '../../components/DashboardCard';
import DrawerIconButton from '../../components/DrawerIconButton';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import HeaderBackButton from '../../components/HeaderBackButton';
import OverlayIndicator from '../../components/OverlayIndicator';
export default class MyCompetition extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        if (navigation.state.params && navigation.state.params.notification) {
            return {
                headerStyle: {
                    ...navigationOptions.headerStyle,
                    paddingHorizontal: os() ? 10 : 20
                },
                headerLeft: (
                    <HeaderBackButton
                        onPress={() => {
                            navigation.popToTop();
                            navigation.navigate('NotificationScreen', {
                                notification: true
                            });
                        }}
                    />
                ),
                headerRight: <HeaderRightIcon drawerProps={navigation} />
            };
        } else {
            return {
                headerStyle: {
                    ...navigationOptions.headerStyle,
                    paddingHorizontal: os() ? 10 : 20
                },
                headerLeft: (
                    <DrawerIconButton onPress={() => navigation.openDrawer()} />
                ),
                headerRight: <HeaderRightIcon drawerProps={navigation} />
            };
        }
    };

    state = {
        search: '',
        tab: 'open',
        buttonStates: [
            { tab: 'open', text: 'Open', state: true },
            { tab: 'inplay', text: 'In Play', state: false },
            { tab: 'settled', text: 'Settled', state: false },
            { tab: 'noactivity', text: 'No Action', state: false }
        ]
    };

    async componentDidMount() {
        const { refreshMyCompetitions, fetchUser } = this.props;
        const { buttonStates } = this.state;
        let token = await retrieveToken();
        fetchUser(token);
        this.setState({ token });
        refreshMyCompetitions({
            token,
            tab: buttonStates.filter(item => item.state)[0].tab
        });

        if (
            this.props.navigation.state.params &&
            this.props.navigation.state.params.showCompetitions
        ) {
            switch (this.props.navigation.state.params.showCompetitions) {
                case 'Open':
                    return this.onPressCard(0);
                case 'In-Play':
                    return this.onPressCard(1);
                case 'Settled':
                    return this.onPressCard(2);
                case 'No-Activity':
                    return this.onPressCard(3);

                default:
                    null;
            }
        }
    }

    onPressCard = index => {
        const { fetchMyCompetitions } = this.props;
        const buttonStates = this.state.buttonStates.map(({ text, tab }) => ({
            text,
            tab,
            state: false
        }));
        buttonStates[index].state = !buttonStates[index].state;
        this.setState({ buttonStates, tab: buttonStates[index].tab }, () => {
            const { myCompetitions } = this.props;
            const { tab } = this.state;
            if (myCompetitions.data[tab] === undefined)
                fetchMyCompetitions({
                    token: this.state.token,
                    tab: this.state.tab
                });
        });
    };

    onScrollEnd = ({ distanceFromEnd }) => {
        const { token, tab } = this.state;
        const { myCompetitions, fetchMyCompetitions } = this.props;
        if (!this.onEndReachedCalledDuringMomentum) {
            if (myCompetitions.data[tab].next_page)
                fetchMyCompetitions({
                    token,
                    tab,
                    nextPage: myCompetitions.data[tab].next_page
                });
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    onRefresh = () => {
        const { token, tab } = this.state;
        const { refreshMyCompetitions } = this.props;

        refreshMyCompetitions({ token, tab });
    };

    onViewPressed = item => {
        let status = getSelectedTeam(item);
        const spreadMsg = renderSubText(item.spread, false);
        this.props.navigation.push('CompetitionDetail', {
            item,
            spreadMsg,
            teamStatus: status
        });
    };

    onSearchChange = search => {
        this.setState({ search }, () => {
            const { search } = this.state;
            const { resetSearchMyCompetitions } = this.props;
            if (search === '') resetSearchMyCompetitions();
        });
    };

    onSearch = () => {
        const { token, search, tab } = this.state;
        this.props.searchMyCompetitions({ token, search, tab });
    };

    emptyList = () => (
        <View style={globalStyles.mainContainer}>
            <EmptyListIndicator message="No competitions available" />
        </View>
    );

    renderOpenCard = ({ item, index }) => {
        let status = getSelectedTeam(item);
        return (
            <DashboardCard
                isOpenCard
                item={item}
                onPress={this.onViewPressed}
                teamStatus={status}
            />
        );
    };

    renderInplayCard = ({ item, index }) => {
        let status = getSelectedTeam(item);
        return (
            <DashboardCard
                isInplayCard
                item={item}
                onPress={this.onViewPressed}
                teamStatus={status}
            />
        );
    };

    renderSettledCard = ({ item, index }) => {
        let status = getSelectedTeam(item);
        return (
            <DashboardCard
                isSettledCard
                item={item}
                onPress={this.onViewPressed}
                teamStatus={status}
            />
        );
    };

    renderNoActionCard = ({ item, index }) => {
        let status = getSelectedTeam(item);
        return (
            <DashboardCard
                isNoActionCard
                item={item}
                onPress={this.onViewPressed}
                teamStatus={status}
            />
        );
    };

    render() {
        const { myCompetitions, searchedMyCompetitions } = this.props;
        const { tab, search } = this.state;

        let cardToRender;
        if (this.state.buttonStates[0].state)
            cardToRender = this.renderOpenCard;
        if (this.state.buttonStates[1].state)
            cardToRender = this.renderInplayCard;
        if (this.state.buttonStates[2].state)
            cardToRender = this.renderSettledCard;
        if (this.state.buttonStates[3].state)
            cardToRender = this.renderNoActionCard;

        let compsToShow = [];
        let totalRecords = 0;

        if (
            search &&
            searchedMyCompetitions.status !== 'inprogress' &&
            searchedMyCompetitions.status !== ''
        ) {
            compsToShow = searchedMyCompetitions.data.competitions;
            totalRecords = searchedMyCompetitions.data.total_records;
        } else if (myCompetitions.data[tab] !== undefined) {
            compsToShow = myCompetitions.data[tab].competitions;
            totalRecords = myCompetitions.data[tab].total_records;
        }

        // if (
        //     search &&
        //     searchedMyCompetitions.status !== 'inprogress' &&
        //     searchedMyCompetitions.status !== ''
        // ) {
        //     totalRecords = searchedMyCompetitions.data.total_records;
        // } else if (
        //     myCompetitions.data[tab] !== undefined &&
        //     myCompetitions.data[tab].total_records
        // ) {
        //     totalRecords = myCompetitions.data[tab].total_records;
        // }
        return (
            <View style={globalStyles.flexContainer}>
                <View>
                    <View
                        style={[
                            styles.cardContainer,
                            globalStyles.flexHorizontal
                        ]}
                    >
                        {this.state.buttonStates.map(({ text, state }, i) => {
                            return (
                                <SmallCards
                                    key={i}
                                    style={{ marginLeft: 0 }}
                                    text={text}
                                    selected={state}
                                    onPress={this.onPressCard.bind(this, i)}
                                    first={i === 0}
                                />
                            );
                        })}
                    </View>
                </View>
                <View
                    style={[
                        { marginVertical: 10 },
                        globalStyles.mainContainer,
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        globalStyles.fullWidth
                    ]}
                >
                    <Text style={[globalStyles.baseFontSize]}>
                        {totalRecords} Competition(s) Available
                    </Text>
                </View>
                <View style={[globalStyles.flexContainer]}>
                    <FlatList
                        data={compsToShow}
                        renderItem={cardToRender}
                        keyExtractor={(item, index) =>
                            `id:${item.sport_cid}, index:${index}`
                        }
                        ListEmptyComponent={this.emptyList()}
                        onEndReachedThreshold={0.1}
                        onEndReached={this.onScrollEnd}
                        onMomentumScrollBegin={() => {
                            this.onEndReachedCalledDuringMomentum = false;
                        }}
                        refreshing={false}
                        onRefresh={this.onRefresh}
                    />
                </View>
                {searchedMyCompetitions.status == 'inprogress' ||
                myCompetitions.status == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
