import { connect } from 'react-redux';
import { fetchFriends, reset } from '../../redux/epics/fetch-friends-epic';
import {
    addFriends,
    resetFriendsAdded
} from '../../redux/epics/add-friends-epic';

import MyFriends from './MyFriends';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ friends, friendsAdded, fbFriends }) => ({
    friends,
    friendsAdded,
    fbFriends
});

const mapDispatchToProps = dispatch => ({
    fetchFriends: data => dispatch(fetchFriends(data)),
    resetFetchFriends: () => dispatch(reset()),
    addFriends: data => dispatch(addFriends(data)),
    resetFriendsAddedState: () => dispatch(resetFriendsAdded())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(MyFriends, [
        { propName: 'friendsAdded', actionOnFailure: 'resetFriendsAddedState' }
    ])
);
