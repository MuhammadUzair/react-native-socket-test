import { StyleSheet } from 'react-native';
import { os } from '../../utils';

export default StyleSheet.create({
    container: {
        paddingLeft: 20,
        paddingRight: 20
    },
    searchHolder: {
        backgroundColor: '#fff',
        borderRadius: 50,
        paddingHorizontal: 15,
        // paddingVertical: 3,
        marginBottom: 10
    },
    search: {
        color: '#000',
        fontSize: 12,
        height: 40
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    subText: {
        fontSize: 18,
        color: 'black',
        marginBottom: 5
    },
    descriptionText: {
        color: 'black',
        fontSize: 12,
        marginBottom: 10
    },
    teamsCard: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: '100%',
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 40,
        paddingHorizontal: 15
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    buttonStyle: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row',
        width: 60 + '%'
    },
    buttonText: {
        fontSize: 18,
        paddingLeft: 15
    },
    nameHolder: {
        flex: 4
    },
    stateHolder: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    cardView: {
        flex: 0,
        justifyContent: 'space-between'
    }
});
