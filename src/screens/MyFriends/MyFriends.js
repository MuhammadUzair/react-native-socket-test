import React, { PureComponent } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    FlatList,
    Alert
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { retrieveToken, importFriends } from '../../utils';

import { globalStyles } from '../../assets/styles';
import friendsStyle from './styles';
import CustomCheckBox from '../../components/CustomCheckBox';
import SmallCards from '../../components/SmallCards';
import OverlayIndicator from '../../components/OverlayIndicator';

export default class MyFriends extends PureComponent {
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft:
                navigation.state.params &&
                navigation.state.params.disableBack &&
                null
        };
    };

    static getDerivedStateFromProps(props, state) {
        return { isFetching: props.friends.status === 'inprogress' };
    }

    componentDidMount() {
        const { fetchFriends } = this.props;
        const { page } = this.state;
        retrieveToken()
            .then(token => {
                this.setState({ token });
                fetchFriends({ page, token });
            })
            .catch(err => Alert.alert('There was a problem', err.message));
    }

    componentDidUpdate(prevProps, prevState) {
        const {
            navigation,
            friendsAdded,
            resetFriendsAddedState,
            fetchFriends
        } = this.props;

        if (friendsAdded !== '' && friendsAdded !== 'inprogress')
            if (friendsAdded.code >= 200 && friendsAdded.code < 300) {
                navigation.navigate('Dashboard');
                resetFriendsAddedState();
            }
    }

    state = {
        addedFriends: [],
        page: 0,
        totalPages: 0,
        tab: 'std',
        buttonStates: [
            {
                id: 'std',
                text: 'Application Users',
                state: true
            },
            {
                id: 'fb',
                text: 'Facebook Freinds',
                state: false
            }
        ],
        search: ''
    };

    searchHandler = search => this.setState({ search });

    onPressCheckbox = id => {
        const addedFriends = [...this.state.addedFriends];
        const indexOf = addedFriends.indexOf(id);

        if (indexOf >= 0) addedFriends.splice(indexOf, 1);
        else addedFriends.push(id);
        this.setState({ addedFriends });
    };

    onPressCard = index => {
        const buttonStates = this.state.buttonStates.map(tabObj => ({
            ...tabObj,
            state: false
        }));

        if (
            this.state.buttonStates[index].id === 'fb' &&
            !this.state.buttonStates[index].state &&
            (this.props.fbFriends.code >= 300 ||
                this.props.fbFriends.code === undefined)
        )
            importFriends(this.state.token);

        buttonStates[index].state = !buttonStates[index].state;
        this.setState({
            buttonStates,
            tab: buttonStates[index].id,
            search: ''
        });
    };

    onPressContinue = () => {
        const { addFriends } = this.props;
        retrieveToken()
            .then(token => {
                addFriends({ friend_id: this.state.addedFriends, token });
            })
            .catch(err => Alert.alert('There was a problem', addFriends));
    };

    renderListItem = ({ item, index }) => {
        const isAdded = this.state.addedFriends.includes(item.friend_id);
        return (
            <TouchableOpacity
                key={index}
                onPress={this.onPressCheckbox.bind(this, item.friend_id)}
                style={[
                    globalStyles.flexHorizontal,
                    friendsStyle.checkbox,
                    globalStyles.hairLineUnderBorder
                ]}
            >
                <CustomCheckBox
                    style={friendsStyle.checkboxStyle}
                    checked={isAdded}
                    uri={item.avatar}
                    title={item.name}
                />
                <View style={friendsStyle.nameHolder}>
                    <Text style={friendsStyle.checkboxText}>{item.name}</Text>
                    <Text
                        style={[
                            friendsStyle.checkboxText,
                            globalStyles.highlightColor
                        ]}
                    >
                        {item.name}
                    </Text>
                </View>
                <View style={friendsStyle.stateHolder}>
                    {this.renderStateText(isAdded)}
                </View>
            </TouchableOpacity>
        );
    };

    fetchPage = () => {
        const { friends } = this.props;
        const { isFetching, token } = this.state;
        if (friends.data.next_page && !isFetching) {
            this.setState({ isFetching: true }, () => {
                const { fetchFriends } = this.props;
                fetchFriends({ nextPage: friends.data.next_page, token });
            });
        }
        // if (
        //     friends.status !== 'inprogress' &&
        //     friends.status !== '' &&
        //     this.state.page <= this.state.totalPages &&
        //     this.state.tab !== 'fb'
        // )
        //     this.setState({
        //         page: this.state.page + 1,
        //     });
    };

    renderStateText = item => {
        return item ? (
            <Text
                style={[friendsStyle.checkboxText, globalStyles.highlightColor]}
            >
                Added
            </Text>
        ) : (
            <Text style={[friendsStyle.checkboxText]}>Add as friend</Text>
        );
    };

    render() {
        const { friends, friendsAdded, fbFriends } = this.props;
        let filteredFriends =
            this.state.tab === 'std' ? friends.data.friends : fbFriends.data;
        if (this.state.search)
            filteredFriends = filteredFriends.filter(
                friend =>
                    friend.name
                        .toLowerCase()
                        .indexOf(this.state.search.toLowerCase()) >= 0
            );

        return (
            <View
                style={[globalStyles.mainContainer, globalStyles.flexContainer]}
            >
                <Text
                    style={[
                        friendsStyle.headerText,
                        globalStyles.baseBlueColor
                    ]}
                >
                    My Friends
                </Text>
                <Text style={[friendsStyle.subText, globalStyles.boldFontFace]}>
                    Personalize Post It Play It
                </Text>
                <Text
                    style={friendsStyle.descriptionText}
                >{`Choose your favourite sports, teams and friends\nfor quick access across Post It Play It Competitions.`}</Text>
                <View style={friendsStyle.searchHolder}>
                    <TextInput
                        style={friendsStyle.search}
                        placeholder="Search With Name"
                        placeholderTextColor="#000"
                        underlineColorAndroid="transparent"
                        value={this.state.search}
                        onChangeText={this.searchHandler}
                    />
                </View>
                <View
                    style={[friendsStyle.cardView, globalStyles.flexHorizontal]}
                >
                    {this.state.buttonStates.map(({ text, state }, i) => {
                        return (
                            <SmallCards
                                key={i}
                                item={i}
                                text={text}
                                selected={state}
                                onPress={this.onPressCard}
                                first={true}
                            />
                        );
                    })}
                </View>
                <View style={[globalStyles.flexContainer]}>
                    <View
                        style={[
                            friendsStyle.teamsCard,
                            globalStyles.flexContainer
                        ]}
                    >
                        <FlatList
                            style={globalStyles.flexContainer}
                            data={filteredFriends}
                            extraData={[
                                ...this.state.addedFriends,
                                ...friends.data,
                                ...fbFriends.data
                            ]}
                            ListEmptyComponent={() => (
                                <Text>No application Users to show</Text>
                            )}
                            renderItem={this.renderListItem}
                            onEndReachedThreshold={0.1}
                            onEndReached={this.fetchPage}
                            keyExtractor={(item, index) =>
                                `index:${index}, id:${item.friend_id}`
                            }
                        />
                    </View>
                    <View style={{ top: -26 }}>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHCenter,
                                friendsStyle.bottomSpacing
                            ]}
                            onPress={this.onPressContinue}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    friendsStyle.buttonStyle
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        friendsStyle.buttonText
                                    ]}
                                >
                                    Done
                                </Text>
                                <Icon
                                    size={20}
                                    color="#FFF"
                                    name="arrow-forward"
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {friends.status === '' ||
                friendsAdded === 'inprogress' ||
                this.props.friends.status === 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
