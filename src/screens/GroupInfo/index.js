import { connect } from 'react-redux';

import GroupInfo from './GroupInfo';

import {
    fetchGroupMembers,
    resetGroupMembers
} from '../../redux/epics/fetch-group-members-epic';

import { resetLeaveGroup } from '../../redux/epics/leave-group-epic';

import alertOnError from '../../hoc/alertOnError';

const mapDisptachToProps = dispatch => ({
    fetchGroupMembers: data => dispatch(fetchGroupMembers(data)),
    resetGroupMembers: data => dispatch(resetGroupMembers()),
    resetLeaveGroup: () => dispatch(resetLeaveGroup())
});

const mapStateToProps = ({ groupMembers, groupLeft }) => ({
    groupMembers,
    groupLeft
});

export default connect(
    mapStateToProps,
    mapDisptachToProps
)(
    alertOnError(GroupInfo, [
        { propName: 'groupMembers', actionOnFailure: 'resetGroupMembers' },
        { propName: 'groupLeft' }
    ])
);
