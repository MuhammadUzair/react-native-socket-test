import React, { Component } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';

import { globalStyles, greyColor } from '../../assets/styles';
import styles from './styles';

import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import { os, retrieveToken, generateAndPrintErrorMessage } from '../../utils';
import CustomAvatar from '../../components/CustomAvatar';
import HeaderBackButton from '../../components/HeaderBackButton';
import OverlayIndicator from '../../components/OverlayIndicator';
import HeaderRightIcon from '../../components/HeaderRightIcon';

export default class GroupInfo extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingLeft: os() ? 10 : 15
            },
            headerLeft: (
                <HeaderBackButton onPress={() => navigation.goBack()} />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    state = { isFetching: false };

    static getDerivedStateFromProps(props, state) {
        return {
            isFetching: props.groupMembers.status === 'inprogress'
        };
    }

    componentDidUpdate(prevProps, prevState) {
        const {
            groupMembers,
            resetGroupMembers,
            groupLeft,
            navigation
        } = this.props;

        // console.log('GroupInfo groupLeft:', groupLeft);
        if (groupLeft.code === 204) {
            navigation.goBack();
        }
    }

    componentDidMount() {
        const {
            navigation: {
                state: {
                    params: { item }
                }
            },
            fetchGroupMembers
        } = this.props;
        retrieveToken().then(token => {
            this.setState({ token }, () => {
                fetchGroupMembers({ groupId: item.group_id, token });
            });
        });
    }

    goBack = () => this.props.navigation.goBack();

    onScrollEnd = () => {
        const { fetchGroupMembers, groupMembers } = this.props;
        const { token, isFetching } = this.state;
        if (!isFetching && groupMembers.data.next_page)
            this.setState({ isFetching: true }, () => {
                fetchGroupMembers({
                    token,
                    nextPage: groupMembers.data.next_page
                });
            });
    };

    onPressLeave = () => {
        const {
            navigation: {
                state: {
                    params: { leaveGroupFunction }
                }
            }
        } = this.props;
        leaveGroupFunction();
    };

    renderBottom = () => {
        const { item } = this.props.navigation.state.params;
        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.flexHCenter,
                    globalStyles.flexSpaceBetween,
                    styles.paddingAll,
                    styles.navIconGreyBackColor
                ]}
            >
                {item.is_owner ? (
                    <View style={globalStyles.flexContainer} />
                ) : (
                    <TouchableOpacity
                        onPress={this.onPressLeave}
                        style={[globalStyles.flexVCenter]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.highlightPinkColor,
                                styles.cancelCenterButton
                            ]}
                        >
                            Leave Group
                        </Text>
                    </TouchableOpacity>
                )}

                <View>
                    <TouchableOpacity
                        onPress={this.goBack}
                        activeOpacity={0.5}
                        style={[globalStyles.flexHCenter]}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={[
                                globalStyles.flexHCenter,
                                globalStyles.flexSpaceBetween,
                                styles.bigPrimaryButton
                            ]}
                        >
                            <Text
                                style={[
                                    globalStyles.fontFace,
                                    { width: '50%' },
                                    globalStyles.whiteColor,
                                    styles.bigPrimaryButtonText
                                ]}
                            >
                                Back
                            </Text>
                            <Icon color="#FFF" name="arrow-forward" />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    renderMemberCard = ({ item, index }) => {
        const { screen } = this.props.navigation.state.params;
        return (
            <View
                style={[
                    globalStyles.flexHorizontal,
                    globalStyles.flexHCenter,
                    globalStyles.fullWidth,
                    globalStyles.hairLineUnderBorder,
                    { padding: 10 }
                ]}
            >
                <CustomAvatar
                    rounded
                    medium
                    uri={item.avatar}
                    containerStyle={{ marginRight: 10 }}
                />
                <View style={globalStyles.flexContainer}>
                    <Text style={globalStyles.blackColor}>{item.name}</Text>
                    <Text
                        style={[
                            globalStyles.greyColor,
                            globalStyles.subTextFontSize
                        ]}
                    >
                        {item.username}
                    </Text>
                </View>
                {item.is_owner ? (
                    <Text style={globalStyles.baseBlueColor}>Owner</Text>
                ) : screen !== 'all' ? (
                    <Text style={globalStyles.baseBlueColor}>Added</Text>
                ) : null}
            </View>
        );
    };

    render() {
        const {
            navigation: {
                state: {
                    params: { item, screen }
                }
            },
            groupMembers,
            groupLeft
        } = this.props;
        return (
            <View style={[globalStyles.flexContainer]}>
                <View style={[globalStyles.mainContainer, { marginTop: 5 }]}>
                    <View
                        style={[
                            globalStyles.greyBackgroudColor,
                            styles.postGameCard,
                            styles.borderRadius
                        ]}
                    />
                    <View
                        style={[
                            styles.bottomSpacing,
                            globalStyles.flexHorizontal,
                            globalStyles.flexVCenter
                        ]}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexHCenter,
                                styles.bigPrimaryButton
                            ]}
                        >
                            <Text
                                style={[
                                    globalStyles.fontFace,
                                    globalStyles.whiteColor,
                                    styles.bigPrimaryButtonText
                                ]}
                            >
                                Group Information
                            </Text>
                        </LinearGradient>
                    </View>
                    <View
                        style={[
                            globalStyles.whiteBackgroudColor,
                            styles.borderRadius,
                            { overflow: 'hidden' }
                        ]}
                    >
                        <View style={[globalStyles.greyBackgroudColor]}>
                            <View
                                style={[
                                    styles.rowContainer,
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween,
                                    globalStyles.hairLineUnderBorder
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Group Name
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.boldFontFace,
                                        globalStyles.baseFontSize,
                                        globalStyles.blackColor
                                    ]}
                                >
                                    {item.name}
                                </Text>
                            </View>

                            <View
                                style={[
                                    styles.rowContainer,
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween,
                                    globalStyles.hairLineUnderBorder
                                ]}
                            >
                                <Text style={[globalStyles.baseGreyColor]}>
                                    Description
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.baseBlueColor,
                                        globalStyles.boldFontFace,
                                        globalStyles.baseFontSize
                                    ]}
                                >
                                    {item.description}
                                </Text>
                            </View>

                            <View
                                style={[
                                    styles.rowContainer,
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween,
                                    globalStyles.hairLineUnderBorder
                                ]}
                            >
                                <Text
                                    style={[
                                        styles.widthForty,
                                        globalStyles.baseGreyColor
                                    ]}
                                >
                                    Privacy
                                </Text>
                                <View style={[globalStyles.flexHorizontal]}>
                                    <Text
                                        style={globalStyles.highlightPinkColor}
                                    >
                                        {item.privicy}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View
                    style={[
                        { flexDirection: 'row', marginVertical: 10 },
                        globalStyles.flexSpaceBetween,
                        globalStyles.mainContainer,
                        globalStyles.fullWidth
                    ]}
                >
                    <Text style={[globalStyles.baseFontSize]}>
                        {groupMembers.data && groupMembers.data.total_records
                            ? groupMembers.data.total_records
                            : 0}{' '}
                        members
                    </Text>
                </View>
                <View
                    style={[
                        globalStyles.mainContainer,
                        globalStyles.flexContainer,
                        styles.borderRadiusTop
                    ]}
                >
                    <FlatList
                        style={[
                            globalStyles.flexContainer,
                            globalStyles.whiteBackgroudColor
                        ]}
                        data={
                            groupMembers.data ? groupMembers.data.members : []
                        }
                        renderItem={this.renderMemberCard}
                        onEndReachedThreshold={0.1}
                        onEndReached={this.onScrollEnd}
                        keyExtractor={(item, index) => `index: ${index}`}
                    />
                </View>
                {screen !== 'all' ? this.renderBottom() : null}
                {groupLeft === 'inprogress' ? <OverlayIndicator /> : null}
            </View>
        );
    }
}
