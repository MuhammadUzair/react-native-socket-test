import { connect } from 'react-redux';

import { editGroup } from '../../redux/epics/edit-group-epic';
import MyGroupsEdit from './MyGroupsEdit';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ groupEdited }) => ({ groupEdited });
const mapDispatchToProps = dispatch => ({
    editGroup: data => dispatch(editGroup(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(MyGroupsEdit, [{ propName: 'groupEdited' }]));
