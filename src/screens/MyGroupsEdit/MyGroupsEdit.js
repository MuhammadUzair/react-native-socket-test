import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import { globalStyles } from '../../assets/styles';
import GroupCreateStyles from './styles';
import { retrieveToken, generateAndPrintErrorMessage } from '../../utils';
import OverlayIndicator from '../../components/OverlayIndicator';

export default class MyGroupsEdit extends Component {
    constructor(props) {
        super(props);
        const { item } = props.navigation.state.params;
        this.state = {
            namePH: 'Enter Group Name',
            descriptionPH: 'Enter Group Description',
            name: item.name,
            description: item.description,
            privacy: item.privicy === 'Public' ? 'public' : 'private'
        };
    }
    componentDidUpdate(prevProps, prevState) {
        const { groupEdited } = this.props;
        if (groupEdited.code >= 200 && groupEdited.code < 300)
            this.props.navigation.navigate('GroupScreen');
    }

    onCancelBtnClick = () => {
        this.props.navigation.popToTop();
        this.props.navigation.navigate('GroupScreen');
    };

    onNextBtnClick = () => {
        const { name, description, privacy } = this.state;
        const { item } = this.props.navigation.state.params;
        retrieveToken().then(token => {
            this.props.editGroup({
                token,
                name,
                description,
                privacy,
                groupId: item.group_id,
                add: [],
                remove: []
            });
        });
    };

    onSelect = (index, value) => {
        this.setState({ privacy: value });
    };

    addMembers = () => {
        const { name, description, privacy } = this.state;
        this.props.navigation.push('GroupScreenListing', {
            isEditing: true,
            item: this.props.navigation.state.params.item,
            name,
            description,
            privacy
        });
    };

    render() {
        const { item } = this.props.navigation.state.params;
        const { groupEdited } = this.props;
        return (
            <ScrollView
                style={[globalStyles.mainContainer, { paddingTop: 10 }]}
            >
                <View
                    style={[
                        globalStyles.whiteBackgroudColor,
                        GroupCreateStyles.borderRadiusTop,
                        GroupCreateStyles.postGameCard
                    ]}
                />
                <View
                    activeOpacity={0.5}
                    style={[
                        GroupCreateStyles.bottomSpacing,
                        globalStyles.flexHorizontal,
                        globalStyles.flexVCenter
                    ]}
                >
                    <LinearGradient
                        colors={['#4d6bc2', '#3695e3']}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexHCenter,
                            GroupCreateStyles.bigPrimaryButton
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.fontFace,
                                globalStyles.whiteColor,
                                GroupCreateStyles.bigPrimaryButtonText
                            ]}
                        >
                            Edit Group
                        </Text>
                    </LinearGradient>
                </View>

                <View
                    style={[
                        globalStyles.whiteBackgroudColor,
                        GroupCreateStyles.borderRadius
                    ]}
                >
                    <View
                        style={[
                            GroupCreateStyles.rowContainer,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            Name
                        </Text>
                        <TextInput
                            style={[globalStyles.hairLineUnderBorder]}
                            onChangeText={name => this.setState({ name })}
                            value={this.state.name}
                            placeholder={this.state.namePH}
                        />
                    </View>

                    <View
                        style={[
                            GroupCreateStyles.rowContainer,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            Description
                        </Text>
                        <TextInput
                            style={[globalStyles.hairLineUnderBorder]}
                            onChangeText={description =>
                                this.setState({ description })
                            }
                            placeholder={this.state.descriptionPH}
                            value={this.state.description}
                        />
                    </View>

                    <View
                        style={[
                            GroupCreateStyles.rowContainer,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            Privacy
                        </Text>
                        <View>
                            <RadioGroup
                                selectedIndex={
                                    item.privicy === 'Public' ? 0 : 1
                                }
                                onSelect={this.onSelect}
                            >
                                <RadioButton
                                    value="public"
                                    style={[globalStyles.hairLineUnderBorder]}
                                >
                                    <Text>Public</Text>
                                </RadioButton>
                                <RadioButton
                                    value="private"
                                    style={[globalStyles.hairLineUnderBorder]}
                                >
                                    <Text>Private</Text>
                                </RadioButton>
                            </RadioGroup>
                        </View>
                    </View>
                    <View
                        style={[
                            GroupCreateStyles.rowContainer,
                            globalStyles.hairLineUnderBorder,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            {item.total_members} Participants
                        </Text>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={this.addMembers}
                        >
                            <Text style={[globalStyles.baseBlueColor]}>
                                Add / Remove Friends
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        GroupCreateStyles.bottomSpacing
                    ]}
                >
                    <View>
                        <TouchableOpacity
                            onPress={() => this.onCancelBtnClick()}
                        >
                            <Text
                                style={[
                                    globalStyles.boldFontFace,
                                    globalStyles.highlightPinkColor,
                                    GroupCreateStyles.cancelCenterButton
                                ]}
                            >
                                Back
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.onNextBtnClick(this.props)}
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHCenter,
                                GroupCreateStyles.bottomSpacing,
                                GroupCreateStyles.marginTop
                            ]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    GroupCreateStyles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        { width: '50%' },
                                        globalStyles.whiteColor,
                                        GroupCreateStyles.bigPrimaryButtonText
                                    ]}
                                >
                                    Update
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {groupEdited === 'inprogress' ? <OverlayIndicator /> : null}
            </ScrollView>
        );
    }
}
