import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    TextInput,
    FlatList,
    RefreshControl
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements';

import DrawerIconButton from '../../components/DrawerIconButton';
import SmallCards from '../../components/SmallCards';

import OverlayIndicator from '../../components/OverlayIndicator';
import FriendsListingListItem from '../../components/FriendsListingListItem';
import FriendsListingAddCheckbox from '../../components/FriendsListingAddCheckbox';
import EmptyListIndicator from '../../components/EmptyListIndicator';

import { retrieveToken } from '../../utils';

import { os } from '../../utils';
import { globalStyles } from '../../assets/styles';
import styles from './styles';
import HeaderRightIcon from '../../components/HeaderRightIcon';

export default class MyFriendsListing extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20
            },
            headerLeft: (
                <DrawerIconButton onPress={() => navigation.openDrawer()} />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    pendingRequest = () => {
        this.props.resetFriendsPendingReceiveRequests();
        this.props.resetFriendsPendingSentRequests();
        this.props.navigation.navigate('PendingFriendList', {
            token: this.state.token
        });
    };

    state = {
        showPendingRequest: false,
        showTeams: false,
        showFriends: false,
        tabStates: [
            { text: 'My Friends', state: true, handler: () => {} },
            {
                text: 'Pending Requests',
                state: false,
                handler: this.pendingRequest
            }
        ],
        cards: [
            { text: 'Application Users', state: true },
            { text: 'Facebook Friends', state: false }
        ],
        check: [false, false, false, false, false, false, false, false],
        added: [],
        friends: [],
        myFriends: [],
        isRefreshingFriends: true,
        isRefreshingMyFriends: true,
        token: '',
        addedPropertyInFriends: false,
        addedPropertyInMyFriends: false,
        friendSearchInput: '',
        showFriendsError: true,
        showAllFriendsError: true
    };

    componentDidMount() {
        retrieveToken().then(token => {
            this.setState({ token });
            this.props.resetMyFriend();
            this.props.fetchMyFriends({
                token: token
            });
        });
    }

    static getDerivedStateFromProps(newProps, prevState) {
        // Api Error Handing Start
        if (
            newProps.allFriends.status == 'failed' &&
            prevState.showAllFriendsError
        ) {
            let friends = {
                status: 'failed'
            };
            return { showAllFriendsError: false, friends: friends };
        }
        if (
            newProps.myFriends.status == 'failed' &&
            prevState.showFriendsError
        ) {
            return { showFriendsError: false, myFriends: newProps.myFriends };
        }
        if (
            newProps.allFriends == 'inprogress' ||
            newProps.myFriends == 'inprogress'
        ) {
            return {
                friends: newProps.allFriends,
                myFriends: newProps.myFriends
            };
        }
        // Api Error Handing End

        // Fetch Friends Start
        let friends = newProps.allFriends,
            addedPropertyInFriends = prevState.addedPropertyInFriends,
            addedPropertyInMyFriends = prevState.addedPropertyInMyFriends,
            myFriends = newProps.myFriends;
        if (
            newProps.friends &&
            newProps.friends.data &&
            newProps.friends.data.friends &&
            newProps.friends.data.friends.length > 0
        ) {
            friends = newProps.friends.data.friends;
            if (!addedPropertyInFriends) {
                addedPropertyInFriends = true;
                // add isAdded property in friends
                friends.map(item => (item.isAdded = false));
            }
        }
        // Fetch Friends End

        // Fetch My Friends Start
        if (
            newProps.myFriends &&
            newProps.myFriends.data &&
            newProps.myFriends.data.friends &&
            newProps.myFriends.data.friends.length > 0
        ) {
            myFriends = newProps.myFriends.data.friends;
            if (!addedPropertyInMyFriends) {
                addedPropertyInMyFriends = true;
                // add isAdded property in Myfriends
                myFriends.map(item => (item.isAdded = false));
            }
        }
        // Fetch My Friends Start
        return {
            friends: friends,
            isRefreshingFriends: false,
            addedPropertyInFriends: addedPropertyInFriends,
            myFriends: myFriends,
            isRefreshingMyFriends: false,
            addedPropertyInMyFriends: addedPropertyInMyFriends
        };
    }

    plusPressed = () => {
        if (!this.state.showFriends) {
            this.setState({ showFriendsError: true });
            this.props.resetFetchFriends();
            this.props.fetchAllFriends({
                token: this.state.token
            });
        }
        this.setState(({ showFriends }) => ({
            showFriends: !showFriends
        }));
    };

    onPressCheckbox = friend => {
        // Add And Remove Friends
        if (friend.isAdded) {
            this.props.removeFriends({
                token: this.state.token,
                friend_id: friend.friend_id
            });
        } else {
            this.props.addFriends({
                friend_id: friend.friend_id,
                token: this.state.token
            });
        }

        let allFriends = this.state.friends,
            allFriendsList = this.state.friends.data.friends;
        allFriendsList.map(searchFriend => {
            if (friend.friend_id == searchFriend.friend_id) {
                searchFriend.isAdded = !searchFriend.isAdded;
            }
        });
        allFriends.data.friends = allFriendsList;
        this.setState({ friends: allFriends });
    };

    onPressCard = index => {
        const cards = this.state.cards.map(({ text }) => ({
            text,
            state: false
        }));
        cards[index].state = !cards[index].state;
        this.setState({ cards });
    };

    onScrollEnd = listName => {
        if (listName == 'MyFriends') {
            if (
                !this.onEndReachedCalledDuringMomentum &&
                this.state.myFriends &&
                this.props.myFriends &&
                this.props.myFriends.data &&
                this.props.myFriends.data.total_records &&
                this.props.myFriends.data.total_records >
                    this.state.myFriends.length
            ) {
                this.props.fetchMyFriends({
                    token: this.state.token,
                    nextPage: this.props.myFriends.data.next_page
                });
                this.onEndReachedCalledDuringMomentum = true;
            }
        } else {
            if (
                !this.onEndReachedCalledDuringMomentum &&
                this.state.friends &&
                this.state.friends.data &&
                this.state.friends.data.friends &&
                this.props.allFriends &&
                this.props.allFriends.data &&
                this.props.allFriends.data.total_records &&
                this.props.allFriends.data.total_records >
                    this.state.friends.data.friends.length
            ) {
                this.props.fetchAllFriends({
                    token: this.state.token,
                    nextPage: this.props.allFriends.data.next_page
                });
                this.onEndReachedCalledDuringMomentum = true;
            }
        }
    };

    onRefresh = listName => {
        if (listName == 'MyFriends') {
            this.setState({ showFriendsError: true });
            this.props.resetMyFriend();
            this.props.fetchMyFriends({
                token: this.state.token
            });
        } else {
            this.setState({ showAllFriendsError: true });
            this.props.resetFetchFriends();
            this.props.fetchAllFriends({
                token: this.state.token,
                page: 1
            });
        }
    };

    renderCard = item => {
        return (
            <FriendsListingListItem
                item={item}
                onPress={() => {
                    this.unFriend(item);
                }}
            />
        );
    };

    unFriend = friend => {
        this.props.removeFriends({
            token: this.state.token,
            friend_id: friend.friend_id
        });
        this.props.resetMyFriend();
        this.props.fetchMyFriends({
            token: this.state.token
        });
    };

    renderStateText = item => {
        return item ? (
            <Text
                style={[
                    styles.checkboxText,
                    globalStyles.boldFontFace,
                    globalStyles.paleRedColor
                ]}
            >
                Cancel
            </Text>
        ) : (
            <Text style={styles.checkboxText}>Add Friend</Text>
        );
    };

    renderAddedFriends = () => {
        if (this.state.cards[0].state == true) {
            return this.renderApplicationFriends();
        } else {
            return this.renderImportFriends();
        }
    };
    emptyList = () => {
        return (
            <EmptyListIndicator
                message="There are no Users."
                // showIndicator={this.state.friends.status === 'inprogress'}
            />
        );
    };

    renderApplicationFriends = () => {
        return (
            <FlatList
                style={[globalStyles.flexContainer]}
                data={this.state.friends.data.friends}
                renderItem={(item, i) => this.renderFriendsList(item.item, i)}
                onEndReachedThreshold={0.1}
                onEndReached={e => this.onScrollEnd('friends')}
                onRefresh={() => this.onRefresh('friends')}
                refreshing={false}
                ListEmptyComponent={this.emptyList()}
                keyExtractor={(item, index) =>
                    `id:${item.friend_id},index:${index}`
                }
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    renderImportFriends = () => {
        return (
            <View style={[globalStyles.flexHorizontal, styles.checkbox]}>
                <View
                    style={[
                        globalStyles.circularCircle,
                        globalStyles.flexVCenter,
                        styles.importFriendCircle
                    ]}
                >
                    <Icon
                        size={20}
                        name="facebook-f"
                        type="font-awesome"
                        color="#3695e3"
                        containerStyle={{ marginRight: 5 }}
                        style={[styles.checkboxStyle, globalStyles.flexVCenter]}
                    />
                </View>
                <View
                    style={[
                        styles.nameHolder,
                        globalStyles.boldFontFace,
                        globalStyles.baseFontSize,
                        globalStyles.blackColor
                    ]}
                >
                    <Text style={styles.checkboxText}>
                        Import friends from facebook
                    </Text>
                </View>
                <TouchableOpacity style={[styles.stateHolder]}>
                    <Text
                        style={[
                            globalStyles.boldFontFace,
                            globalStyles.highlightBlueColor
                        ]}
                    >
                        Import Friends
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };

    emptyMyFriendsList = () => {
        return (
            <EmptyListIndicator
                message="There are no Friends."
                // showIndicator={
                //     this.state.myFriends.status === 'inprogress' ||
                //     this.state.isRefreshingFriends
                // }
            />
        );
    };

    renderFriends = () => {
        if (this.state.myFriends && this.state.myFriends.length > 0) {
            return (
                <FlatList
                    data={this.state.myFriends}
                    renderItem={(item, i) =>
                        this.renderCard(item.item, item.index)
                    }
                    onEndReachedThreshold={0.1}
                    onEndReached={e => this.onScrollEnd('MyFriends')}
                    onRefresh={() => this.onRefresh('MyFriends')}
                    refreshing={false}
                    keyExtractor={(item, index) => `id:${index},index:${index}`}
                    onMomentumScrollBegin={() => {
                        this.onEndReachedCalledDuringMomentum = false;
                    }}
                />
            );
        } else {
            return (
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            onRefresh={() => this.onRefresh('MyFriends')}
                            refreshing={false}
                        />
                    }
                >
                    <Text style={styles.emptyListStyle}>
                        There are no Friends
                    </Text>
                </ScrollView>
            );
        }
    };

    onChangeName = text => {
        this.setState({
            friendSearchInput: text
        });
        if (!text) {
            this.searchBarHandler(false);
        }
    };

    renderShowFriendSearch = () => {
        if (this.state.showFriends) {
            return (
                <View>
                    <View style={styles.paddedContainer}>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.fullWidth,
                                styles.searchHolder
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexContainer,
                                    styles.searchBar
                                ]}
                            >
                                <View
                                    style={[
                                        globalStyles.flexContainer,
                                        globalStyles.flexVCenter
                                    ]}
                                >
                                    <TextInput
                                        placeholder="Search with name"
                                        underlineColorAndroid="transparent"
                                        style={globalStyles.fullHeight}
                                        onChangeText={e => this.onChangeName(e)}
                                    />
                                </View>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.searchBarHandler(true)}
                                style={styles.searchFriendBtn}
                            >
                                <Icon name="search" type="evilicon" size={20} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View>
                        <View
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexVCenter,
                                styles.scrollView
                            ]}
                        >
                            {this.state.cards.map(({ text, state }, i) => {
                                return (
                                    <SmallCards
                                        text={text}
                                        selected={state}
                                        containerStyle={{
                                            minWidth: '45%',
                                            paddingHorizontal: 5,
                                            paddingVertical: 3
                                        }}
                                        item={i}
                                        onPress={this.onPressCard}
                                        first={i === 0}
                                        key={i}
                                    />
                                );
                            })}
                        </View>
                    </View>
                </View>
            );
        }
    };

    searchBarHandler = searchApiCall => {
        this.setState({
            isRefreshingFriends: true,
            friends: 'inprogress'
        });
        this.props.resetFetchFriends();
        if (searchApiCall) {
            this.props.fetchAllFriends({
                token: this.state.token,
                page: 1,
                search: this.state.friendSearchInput
            });
        } else {
            this.props.fetchAllFriends({
                token: this.state.token,
                page: 1
            });
        }

        this.setState({
            isRefreshingFriends: false
        });
    };

    renderFriendsList = item => {
        return (
            <FriendsListingAddCheckbox
                item={item}
                onPress={() => this.onPressCheckbox(item)}
            />
        );
    };

    render() {
        const { showFriends } = this.state;
        const { allFriends, myFriends } = this.props;
        return (
            <View
                style={[
                    globalStyles.flexContainer,
                    globalStyles.mainContainer,
                    styles.container
                ]}
            >
                <View>
                    <Text
                        style={[styles.headerText, globalStyles.highlightColor]}
                    >
                        My Friends
                    </Text>
                    <Text
                        style={[
                            styles.descriptionText,
                            globalStyles.baseFontSize
                        ]}
                    >{`Add a new Friend for contest, and play against your friends all season.`}</Text>
                </View>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexHCenter
                    ]}
                >
                    <View style={[globalStyles.flexHorizontal, { flex: 3 }]}>
                        {this.state.tabStates.map(
                            ({ text, state, handler }, i) => (
                                <SmallCards
                                    item={i}
                                    selected={state}
                                    text={text}
                                    onPress={handler}
                                    containerStyle={{
                                        minWidth: '45%',
                                        paddingHorizontal: 5,
                                        paddingVertical: 3
                                    }}
                                    first={i === 0}
                                    key={i}
                                />
                            )
                        )}
                    </View>
                    <TouchableOpacity
                        onPress={this.plusPressed}
                        activeOpacity={0.5}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 50,
                                padding: 8
                            }}
                        >
                            <Icon
                                name="plus"
                                type="feather"
                                color="#fff"
                                size={18}
                                containerStyle={{
                                    transform: [
                                        {
                                            rotate: showFriends
                                                ? '45deg'
                                                : '0deg'
                                        }
                                    ]
                                }}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                {this.renderShowFriendSearch()}
                <View style={[globalStyles.flexContainer, { paddingTop: 10 }]}>
                    <View
                        style={[
                            globalStyles.flexContainer,
                            styles.scrollViewWrapper
                        ]}
                    >
                        <View
                            style={[
                                styles.scrollContainer,
                                globalStyles.flexContainer
                            ]}
                        >
                            {this.state.showFriends
                                ? this.renderAddedFriends()
                                : this.renderFriends()}
                        </View>
                    </View>
                </View>
                {allFriends.status == 'inprogress' ||
                myFriends.status == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
