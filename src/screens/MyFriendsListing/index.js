import { connect } from 'react-redux';
import {
    fetchAllFriends,
    reset
} from '../../redux/epics/fetch-all-friends-epic';
import {
    addFriends,
    resetFriendsAdded
} from '../../redux/epics/add-friends-epic';
import {
    fetchMyFriends,
    resetMyFriend
} from '../../redux/epics/fetch-my-friends-epic';
import { resetFriendsPendingReceiveRequests } from '../../redux/epics/fetch-friends-pending-receive-requests-epic';
import { resetFriendsPendingSentRequests } from '../../redux/epics/fetch-friends-pending-sent-requests-epic';

import { removeFriends } from '../../redux/epics/remove-friends-epic';

import MyFriendsListing from './MyFriendsListing';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({
    allFriends,
    friendsAdded,
    fbFriends,
    myFriends
}) => ({
    allFriends,
    friendsAdded,
    fbFriends,
    myFriends
});

const mapDispatchToProps = dispatch => ({
    fetchAllFriends: data => dispatch(fetchAllFriends(data)),
    resetFetchFriends: () => dispatch(reset()),
    addFriends: data => dispatch(addFriends(data)),
    resetFriendsAddedState: () => dispatch(resetFriendsAdded()),
    fetchMyFriends: data => dispatch(fetchMyFriends(data)),
    resetMyFriend: () => dispatch(resetMyFriend()),
    removeFriends: data => dispatch(removeFriends(data)),
    resetFriendsPendingReceiveRequests: () =>
        dispatch(resetFriendsPendingReceiveRequests()),
    resetFriendsPendingSentRequests: () =>
        dispatch(resetFriendsPendingSentRequests())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(MyFriendsListing, [
        { propName: 'allFriends' },
        { propName: 'myFriends' }
    ])
);
