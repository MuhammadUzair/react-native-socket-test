import { StyleSheet } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    container: {
        paddingVertical: 0
    },
    headerText: {
        fontSize: scale(24),
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    subText: {
        fontSize: 18,
        color: 'black',
        marginBottom: 5
    },
    descriptionText: {
        color: 'black',
        marginBottom: 10
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    buttonStyle: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row',
        width: 70 + '%'
    },
    buttonText: {
        fontSize: 14
    },
    nameHolder: {
        flex: 4
    },
    stateHolder: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    scrollContainer: {
        paddingHorizontal: 20
        // padding: 20
    },
    scrollViewWrapper: {
        backgroundColor: 'white',
        borderRadius: 5,
        paddingVertical: 10
    },
    scrollView: {
        marginTop: 5,
        paddingHorizontal: 0
    },
    paddedContainer: {
        paddingHorizontal: 0
    },
    searchHolder: {
        paddingHorizontal: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        marginVertical: 5
    },
    searchBar: {
        height: 40,
        backgroundColor: 'white'
    },
    importFriendCircle: {
        width: 35,
        height: 35,
        marginEnd: 10
    },
    emptyListStyle: {
        height: 100,
        paddingTop: 10
    },
    overlayIndicatorWrap: {
        marginTop: 20
    },
    unFriendBtn: {
        borderWidth: 1,
        borderColor: '#ff2458',
        padding: 8,
        borderRadius: 20
    },
    searchFriendBtn: {
        justifyContent: 'center'
    }
});
