import { StyleSheet } from 'react-native';
import { os } from '../../utils';

export default StyleSheet.create({
    container: {
        padding: os() ? 10 : 15,
    },
});
