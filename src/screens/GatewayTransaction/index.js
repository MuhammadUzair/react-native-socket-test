import { connect } from 'react-redux';

import GatewayTransaction from './GatewayTransaction';

import {
    fetchGatewayTransaction,
    refreshGatewayTransaction,
} from '../../redux/epics/gateway-transaction-epic';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ gatewayTransactions }) => ({ gatewayTransactions });
const mapDispatchToProps = dispatch => ({
    fetchGatewayTransaction: data => dispatch(fetchGatewayTransaction(data)),
    refreshGatewayTransaction: data =>
        dispatch(refreshGatewayTransaction(data)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(alertOnError(GatewayTransaction, [{ propName: 'gatewayTransactions' }]));
