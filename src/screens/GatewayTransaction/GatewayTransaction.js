import React, { Component } from 'react';
import { View, FlatList, ScrollView } from 'react-native';

import SmallCards from '../../components/SmallCards';

import { retrieveToken } from '../../utils';

import { globalStyles } from '../../assets/styles';
import styles from './style';

import Seperator from '../../components/Seperator';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import GatewayTransactionCard from '../../components/GatewayTransactionCard';
import OverlayIndicator from '../../components/OverlayIndicator';
export default class GatewayTransaction extends Component {
    state = {
        filter: 'All',
        isFetching: false,
        tabStates: [
            { text: 'All', state: true },
            { text: 'Pending', state: false },
            { text: 'Failed', state: false },
            { text: 'Processing', state: false },
            { text: 'Reversed', state: false }
        ]
    };

    componentDidMount() {
        retrieveToken().then(token =>
            this.setState({ token }, () => {
                const { token, filter } = this.state;
                this.props.refreshGatewayTransaction({ token, filter });
            })
        );
    }

    static getDerivedStateFromProps(props, state) {
        return {
            isFetching:
                props.gatewayTransactions.status === 'inprogress' ||
                props.gatewayTransactions.status === 'refreshing'
        };
    }

    onPressTab = i => {
        const newTabstates = [...this.state.tabStates].map(
            tab => ((tab.state = false), tab)
        );
        newTabstates[i].state = true;
        this.setState(
            {
                filter: this.state.tabStates[i].text,
                tabStates: newTabstates
            },
            () => {
                const { token, filter } = this.state;
                if (this.props.gatewayTransactions.data[filter] === undefined)
                    this.props.fetchGatewayTransaction({ token, filter });
            }
        );
    };

    onScrollEnd = () => {
        const { isFetching, filter, token } = this.state;
        const { fetchGatewayTransaction, gatewayTransactions } = this.props;
        if (!isFetching) {
            if (gatewayTransactions.data[filter].next_page) {
                this.setState({ isFetching: true }, () => {
                    fetchGatewayTransaction({
                        token,
                        nextPage: gatewayTransactions.data[filter].next_page,
                        filter
                    });
                });
            }
        }
    };

    onRefresh = () => {
        const { isFetching, filter, token } = this.state;
        const { refreshGatewayTransaction } = this.props;
        if (!isFetching) {
            refreshGatewayTransaction({ token, filter });
        }
    };

    render() {
        const { gatewayTransactions } = this.props;
        const { filter, token } = this.state;
        let dataToShow = [];

        if (gatewayTransactions.data[filter] !== undefined) {
            dataToShow = gatewayTransactions.data[filter].transactions;
        }

        return (
            <View style={globalStyles.flexContainer}>
                <View>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={[
                            globalStyles.mainContainer,
                            { paddingVertical: 10 }
                        ]}
                    >
                        {this.state.tabStates.map(({ text, state }, i) => (
                            <SmallCards
                                key={i}
                                selected={state}
                                text={text}
                                item={i}
                                first={i === 0}
                                onPress={this.onPressTab}
                            />
                        ))}
                    </ScrollView>
                </View>
                <View
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.mainContainer,
                        styles.container
                    ]}
                >
                    <FlatList
                        style={{
                            backgroundColor: 'white',
                            borderRadius: 10,
                            flex: 1
                        }}
                        data={dataToShow}
                        ListEmptyComponent={() => (
                            <EmptyListIndicator
                                // showIndicator={
                                //     gatewayTransactions.status === 'inprogress'
                                // }
                                message="No transactions to show"
                                style={{ padding: 10 }}
                            />
                        )}
                        ItemSeparatorComponent={() => <Seperator />}
                        renderItem={({ item }) => (
                            <GatewayTransactionCard item={item} token={token} />
                        )}
                        onEndReached={this.onScrollEnd}
                        onEndReachedThreshold={0.1}
                        onRefresh={this.onRefresh}
                        refreshing={false}
                        keyExtractor={(item, index) => item._id}
                    />
                </View>
                {gatewayTransactions.status == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
