import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    scrollView: {
        marginVertical: 10
    },
    searchHolder: {
        backgroundColor: 'white',
        borderRadius: 50,
        marginVertical: 5,
        paddingHorizontal: 20
    },
    searchBar: {
        height: 40,
        elevation: 5
    },
    card: {
        // padding: 10,
        justifyContent: 'space-between',
        overflow: 'hidden',
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 20,
        height: 150
    },
    avatarColumn: {
        flex: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 5
    },
    paddingHor: { paddingHorizontal: 10 },
    paddingTop: { paddingTop: 10 },
    paddingBottom: { paddingBottom: 5 },
    infoStrip: {
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    nameAndButton: {
        height: 40,
        marginBottom: 5,
        alignItems: 'flex-end'
    },
    versusHolder: {
        justifyContent: 'flex-end'
        // alignItems: 'space-between',
    },
    versusTeam: { flex: 4, fontSize: 12 },
    image: {
        // width: 35,
        // height: 35,
        // borderRadius: 50,
        // marginRight: 10
    },
    name: {
        color: 'black',
        fontWeight: 'bold',
        marginRight: 10
    },
    smallFont: {
        fontSize: 10
    },
    versus: {
        fontWeight: 'bold'
    },
    black: {
        color: '#000'
    },
    feturedBadgeHolder: {
        flex: 0,
        borderRadius: 50,
        overflow: 'hidden',
        paddingHorizontal: 10,
        marginRight: 5,
        // paddingTop: 1,
        backgroundColor: '#ff2458'
    },
    featuredBadge: {
        borderRadius: 50,
        backgroundColor: '#ff2458',
        // height: 15,
        color: 'white'
        // paddingHorizontal: 10
    },
    btnHolder: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    buttonStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderRadius: 50
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 10
    },
    blueBadge: {
        backgroundColor: '#2d94f6',
        borderRadius: 50,
        fontSize: 10,
        paddingHorizontal: 2,
        color: 'white'
    },
    amount: {
        color: '#ff59a7'
        // textAlign: 'right'
    },
    mb: {
        marginBottom: 20
    },
    scrollContainer: {
        // flexGrow: 1,flexShrink: 1, flexBasis: 1
        // paddingBottom: 40,
    }
});
