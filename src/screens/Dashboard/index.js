import { connect } from 'react-redux';
import Dashboard from './Dashboard';

import {
    fetchCompetitions,
    refreshCompetitions,
    resetCompetitions
} from '../../redux/epics/fetch-competitions-epic';
import {
    searchCompetitions,
    resetSearchCompetitions
} from '../../redux/epics/search-dashboard-epic';
import { fetchLeagues } from '../../redux/epics/fetch-leagues-epic';
import {
    activeNotification,
    unActiveNotification,
    getNotification
} from '../../redux/epics/get-notification';
import {
    fetchSpecificCompetition,
    resetFetchSpecificCompetition
} from '../../redux/epics/fetch-specific-competition-epic';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({
    competitions,
    leagues,
    searchedCompetitions,
    competitionPlayed,
    getNotification,
    searchCompetition
}) => ({
    competitions,
    leagues,
    searchedCompetitions,
    competitionPlayed,
    getNotification,
    searchCompetition
});

const mapDispatchToProps = dispatch => ({
    fetchCompetitions: data => dispatch(fetchCompetitions(data)),
    fetchLeagues: data => dispatch(fetchLeagues(data)),
    refreshCompetitions: data => dispatch(refreshCompetitions(data)),
    searchCompetitions: data => dispatch(searchCompetitions(data)),
    resetSearchCompetitions: () => dispatch(resetSearchCompetitions()),
    resetCompetitions: () => dispatch(resetCompetitions()),
    activeNotification: data => dispatch(activeNotification(data)),
    unActiveNotification: () => dispatch(unActiveNotification()),
    getNotification: data => dispatch(getNotification(data)),
    fetchSpecificCompetition: data => dispatch(fetchSpecificCompetition(data)),
    resetFetchSpecificCompetition: data =>
        dispatch(resetFetchSpecificCompetition(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(Dashboard, [
        {
            propName: 'searchedCompetitions',
            actionOnFailure: 'resetSearchCompetitions'
        },
        { propName: 'competitions', actionOnFailure: 'resetCompetitions' },
        { propName: 'leagues' }
    ])
);
