import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TextInput,
    FlatList,
    NetInfo,
    Alert,
    AsyncStorage
} from 'react-native';
import { Icon } from 'react-native-elements';

import {
    os,
    retrieveToken,
    renderSubText,
    renderSpreadWithPlusSign,
    PushNotificationControler,
    changeRoute,
    getDashboardSelectedTeam
} from '../../utils';

import { globalStyles } from '../../assets/styles';
import styles from './styles';

import SmallCards from '../../components/SmallCards';
import DrawerIconButton from '../../components/DrawerIconButton';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import DashboardCard from '../../components/DashboardCard';
import OverlayIndicator from '../../components/OverlayIndicator';
import io from 'socket.io-client';

const connectionConfig = {
    jsonp: false,
    reconnection: true,
    reconnectionDelay: 100,
    reconnectionAttempts: 100000,
    transports: ['websocket'] // you need to explicitly tell it to use websockets
};

export default class Dashboard extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20
            },
            headerLeft: (
                <DrawerIconButton onPress={() => navigation.openDrawer()} />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            fetchingPage: false,
            search: '',
            league: 'featured',
            buttonStates: [
                { text: 'Featured', state: true },
                { text: 'Favorite', state: false },
                { text: 'Friends', state: false },
                { text: 'Groups', state: false }
            ],
            fullCard: true,
            showLoader: true
        };

        this.socket = io('http://172.16.10.107:3000', connectionConfig);
        this.socket.on('connect', () => {
            console.log('dashboad connected to server');
        });
        this.socket.on('getMessage', data => {
            console.log('dashboad getMessage recive ', data);
        });
    }

    static getDerivedStateFromProps(nextProps, nextState) {
        const { competitions, leagues, searchedCompetitions } = nextProps;
        return {
            fetchingPage:
                competitions.status === 'inprogress' ||
                searchedCompetitions.status === 'inprogress'
        };
    }

    async componentDidMount() {
        let token = await retrieveToken();
        this.props.resetFetchSpecificCompetition();
        await PushNotificationControler(
            this.props.navigation,
            this.props.activeNotification,
            this.props.fetchSpecificCompetition
        );
        // Change Route base on notification - start
        await AsyncStorage.getItem('notification').then(async res => {
            if (res && res !== null) {
                res = JSON.parse(res);
                AsyncStorage.removeItem('notification');
                await changeRoute(
                    this.props.navigation,
                    token,
                    res,
                    this.props.fetchSpecificCompetition
                );
                return false;
            }
            AsyncStorage.removeItem('notification');
        });
        // Change Route base on notification - end
        const { leagues, refreshCompetitions, fetchLeagues } = this.props;
        const { league, buttonStates } = this.state;
        this.setState({ token });
        if (leagues === '') fetchLeagues(token);
        refreshCompetitions({ token, league });

        let newLeagues = [...buttonStates];

        if (leagues !== 'inprogress' && leagues !== '')
            newLeagues = [
                ...newLeagues,
                ...leagues.data.map(league => ({
                    text: league.short_code,
                    state: false
                }))
            ];

        this.setState({ buttonStates: newLeagues });
        // return this.props.navigation.navigate('createCompetition');
        // return this.props.navigation.navigate('ReferFriend');
    }

    componentDidUpdate = async (prevProps, prevState) => {
        const {
            leagues,
            searchedCompetitions,
            resetSearchCompetitions,
            refreshCompetitions,
            competitionPlayed,
            competitions,
            searchCompetition,
            navigation,
            resetFetchSpecificCompetition
        } = this.props;
        const { buttonStates, search, token, league } = this.state;

        if (
            navigation.state.routeName == 'DashboardScreen' &&
            searchCompetition &&
            searchCompetition.data &&
            searchCompetition.data.competitions &&
            searchCompetition.data.competitions.length > 0 &&
            searchCompetition.screen == 'dashboard'
        ) {
            let status = searchCompetition.data.competitions[0].status;
            let id = searchCompetition.data.competitions[0].id;
            let item = searchCompetition.data.competitions[0];
            const subText = `${item.competitor.short_code} ${renderSubText(
                item.spread,
                true
            )}`;
            const spreadTitle = `${
                item.competitor.short_code
            } ${renderSpreadWithPlusSign(item.spread)}`;
            resetFetchSpecificCompetition();
            // console.log('id ', id);
            switch (status) {
                case 'Open': {
                    navigation.popToTop();
                    return navigation.navigate('PlayIt', {
                        item,
                        subText,
                        spreadTitle
                    });
                }
                case 'In-Play':
                    return Alert.alert(
                        'There was a problem',
                        'The Competition has already started'
                    );
                case 'Settled':
                    return Alert.alert(
                        'There was a problem',
                        'The Competition has finished'
                    );
                case 'No-Activity':
                    return Alert.alert(
                        'There was a problem',
                        'The Competition went into No Action'
                    );
                default:
                    null;
            }
        }

        if (
            competitionPlayed !== '' &&
            competitionPlayed !== 'inprogress' &&
            competitionPlayed.data &&
            competitions.status !== 'refreshing'
        ) {
            refreshCompetitions({ token, league });
        }
        if (
            searchedCompetitions.status !== '' &&
            searchedCompetitions.status !== 'inprogress' &&
            !search
        ) {
            resetSearchCompetitions();
        }

        if (
            leagues.code >= 200 &&
            leagues.code < 300 &&
            buttonStates.length === 4
        ) {
            let newLeagues = [...buttonStates];

            if (leagues !== 'inprogress' && leagues !== '')
                newLeagues = [
                    ...newLeagues,
                    ...leagues.data.map(league => ({
                        text: league.short_code,
                        state: false
                    }))
                ];
            this.setState({ buttonStates: newLeagues });
        }
    };

    onPressPlayIt = async (item, status) => {
        const { navigation, screenProps } = this.props;
        const subText = `${item.competitor.short_code} ${renderSubText(
            item.spread,
            true
        )}`;
        const spreadTitle = `${
            item.competitor.short_code
        } ${renderSpreadWithPlusSign(item.spread)}`;
        navigation.push('PlayIt', {
            item,
            subText,
            spreadTitle,
            teamStatus: status
        });
    };

    onPressCard = index => {
        const { fetchCompetitions, competitions } = this.props;
        const { buttonStates, token } = this.state;
        let fullCard = false;
        // const connectionInfo = await NetInfo.getConnectionInfo().then(
        //     connectionInfo => connectionInfo,
        // );
        // if (
        //     connectionInfo.type !== 'unknown' &&
        //     connectionInfo.type !== 'none'
        // ) {
        if (!buttonStates[index].state) {
            const newButtonStates = buttonStates.map(({ text }) => ({
                text,
                state: false
            }));

            newButtonStates[index].state = !newButtonStates[index].state;

            if (
                newButtonStates[index].text == 'Featured' ||
                newButtonStates[index].text == 'Favorite' ||
                newButtonStates[index].text == 'Friends' ||
                newButtonStates[index].text == 'Groups'
            ) {
                fullCard = true;
            }
            this.setState(
                {
                    buttonStates: newButtonStates,
                    league: newButtonStates[index].text.toLowerCase(),
                    fullCard: fullCard
                },
                () => {
                    const { token, search, league } = this.state;
                    const { refreshCompetitions } = this.props;
                    if (search) {
                        this.props.searchCompetitions({
                            token,
                            search,
                            league
                        });
                    } else {
                        refreshCompetitions({
                            token,
                            league: this.state.league
                        });
                    }
                }
            );
        }
        // } else {
        //     Alert.alert('There was a problem', 'No internet connection.');
        // }
    };

    onScrollEnd = ({ distanceFromEnd }) => {
        const { league, token, fetchingPage, prevPage, search } = this.state;
        const {
            fetchCompetitions,
            competitions,
            searchedCompetitions,
            searchCompetitions
        } = this.props;
        if (!this.onEndReachedCalledDuringMomentum) {
            if (
                search &&
                searchedCompetitions.status !== '' &&
                searchedCompetitions.status != 'inprogress'
            ) {
                if (
                    searchedCompetitions.data.next_page &&
                    !fetchingPage &&
                    searchedCompetitions.data.next_page !== prevPage
                ) {
                    this.setState(
                        {
                            fetchingPage: true,
                            prevPage: searchedCompetitions.data.next_page
                        },
                        () => {
                            searchCompetitions({
                                token,
                                league,
                                nextPage: searchedCompetitions.data.next_page
                            });
                        }
                    );
                }
            } else if (
                competitions.data[league].next_page &&
                !fetchingPage &&
                competitions.data[league].next_page !== prevPage
            ) {
                this.setState(
                    {
                        fetchingPage: true,
                        prevPage: competitions.data[league].next_page
                    },
                    () => {
                        fetchCompetitions({
                            token,
                            league,
                            nextPage: competitions.data[league].next_page
                        });
                    }
                );
            }
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    onRefresh = () => {
        const { refreshCompetitions, searchCompetitions } = this.props;
        const { league, token, search } = this.state;
        if (search) {
            searchCompetitions({ token, league, search });
        } else {
            refreshCompetitions({ league, token });
        }
    };

    searchCompetitions = () => {
        const { token, search, league } = this.state;
        search && this.props.searchCompetitions({ token, search, league });
    };

    isFeatured = () => (
        <View style={styles.feturedBadgeHolder}>
            <Text style={[styles.featuredBadge, styles.smallFont]}>
                Featured
            </Text>
        </View>
    );

    emptyList = () => {
        const msg = `Currently there are no ${
            this.state.buttonStates.filter(item => item.state)[0].text
        } competitions to play.`;
        return (
            <View style={globalStyles.mainContainer}>
                <EmptyListIndicator
                    message={msg}
                    showIndicator={
                        this.props.competitions.status === 'inprogress' ||
                        this.props.searchedCompetitions === 'inprogress'
                    }
                />
            </View>
        );
    };

    onSearch = search => {
        const { competitions, refreshCompetitions } = this.props;
        const { league, token } = this.state;
        if (search === '')
            if (competitions.data[league] === undefined)
                refreshCompetitions({ token, league });

        this.setState({ search });
    };

    render() {
        const { competitions, searchedCompetitions } = this.props;
        const { search, league } = this.state;

        let dataToShow;
        let totalRecords = 0;
        if (search.length && searchedCompetitions.status !== '') {
            if (searchedCompetitions.status === 'inprogress') {
                dataToShow = [];
            } else {
                dataToShow = searchedCompetitions.data.competitions;
                totalRecords = searchedCompetitions.data.total_records;
            }
        } else if (competitions.data[league] !== undefined) {
            dataToShow = competitions.data[league].competitions;
            totalRecords =
                (competitions.data[league] &&
                    competitions.data[league].total_records) ||
                0;
        } else {
            dataToShow = [];
        }
        return (
            <View style={[globalStyles.flexContainer]}>
                <View>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={[
                            styles.scrollView,
                            globalStyles.mainContainer
                        ]}
                    >
                        {this.state.buttonStates.map(({ text, state }, i) => {
                            return (
                                <SmallCards
                                    key={i}
                                    text={text}
                                    selected={state}
                                    onPress={this.onPressCard.bind(this, i)}
                                    first={i === 0}
                                />
                            );
                        })}
                    </ScrollView>
                </View>
                <View style={[globalStyles.flexContainer]}>
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.fullWidth,
                            styles.searchHolder,
                            globalStyles.mainContainer
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.flexContainer,
                                styles.searchBar
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexContainer,
                                    globalStyles.flexVCenter
                                ]}
                            >
                                <TextInput
                                    underlineColorAndroid="transparent"
                                    placeholder="Search with Team name"
                                    style={[globalStyles.greyColor]}
                                    onChangeText={this.onSearch}
                                    onSubmitEditing={this.searchCompetitions}
                                    autoCorrect={false}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            style={[
                                globalStyles.flexHCenter,
                                globalStyles.flexVCenter
                            ]}
                            onPress={this.searchCompetitions}
                            activeOpacity={0.5}
                        >
                            <Icon name="search" type="evilicon" size={20} />
                        </TouchableOpacity>
                    </View>
                    <View
                        style={[
                            { paddingVertical: 10 },
                            globalStyles.fullWidth,
                            globalStyles.mainContainer
                        ]}
                    >
                        <Text style={[globalStyles.baseFontSize]}>
                            {totalRecords} Competition(s) Available
                        </Text>
                    </View>
                    <View
                        style={[globalStyles.flexContainer, { height: '100%' }]}
                    >
                        <FlatList
                            style={[globalStyles.flexContainer]}
                            data={dataToShow}
                            renderItem={({ item, index }) => {
                                let status = getDashboardSelectedTeam(item);
                                return (
                                    <DashboardCard
                                        isDashboardCard
                                        item={item}
                                        index={index}
                                        onPress={() =>
                                            this.onPressPlayIt(item, status)
                                        }
                                        teamStatus={status}
                                        fullCard={this.state.fullCard}
                                    />
                                );
                            }}
                            onEndReachedThreshold={0.1}
                            onEndReached={this.onScrollEnd}
                            onRefresh={this.onRefresh}
                            // refreshing={
                            //     competitions.status === 'refreshing' ||
                            //     searchedCompetitions.status === 'inprogress'
                            // }
                            refreshing={false}
                            ListEmptyComponent={this.emptyList()}
                            keyExtractor={(item, index) =>
                                `id:${item.id},index:${index}`
                            }
                            onMomentumScrollBegin={() => {
                                this.onEndReachedCalledDuringMomentum = false;
                            }}
                        />
                    </View>
                </View>
                {this.state.fetchingPage ||
                competitions.status === 'refreshing' ||
                searchedCompetitions.status === 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
