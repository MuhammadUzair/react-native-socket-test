import React, { Component } from 'react';
import { View, Text, Image, TextInput, ScrollView } from 'react-native';
import { Icon, Avatar } from 'react-native-elements';

import { globalStyles } from '../../assets/styles';
import searchStyles from './styles';

export default class Search extends Component {
    state = {
        cards: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    };
    render() {
        return (
            <View style={[globalStyles.flexContainer]}>
                <View style={searchStyles.paddedContainer}>
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.fullWidth,
                            searchStyles.searchHolder
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.flexContainer,
                                searchStyles.searchBar
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexContainer,
                                    globalStyles.flexVCenter
                                ]}
                            >
                                <TextInput
                                    placeholder="Search With Name"
                                    underlineColorAndroid="transparent"
                                    style={globalStyles.fullHeight}
                                />
                            </View>
                        </View>
                        <Icon name="search" type="evilicon" size={20} />
                    </View>
                </View>
                {/* <View> */}
                <ScrollView
                    style={searchStyles.scroll}
                    contentContainerStyle={searchStyles.paddedContainer}
                >
                    {this.state.cards.map(() => (
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexHCenter,
                                searchStyles.card
                            ]}
                        >
                            <Avatar
                                rounded
                                medium
                                containerStyle={searchStyles.image}
                                source={{
                                    uri:
                                        'https://images.pexels.com/photos/1020315/pexels-photo-1020315.jpeg?cs=srgb&dl=abstract-art-artistic-1020315.jpg&fm=jpg'
                                }}
                            />
                            <View>
                                <Text style={searchStyles.name}>NBA</Text>
                                <Text style={globalStyles.baseFontSize}>
                                    National Basket Ball Association
                                </Text>
                            </View>
                        </View>
                    ))}
                </ScrollView>
                {/* </View> */}
            </View>
        );
    }
}
