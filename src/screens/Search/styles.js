import { StyleSheet } from 'react-native';
import { verticalScale, scale, moderateScale } from 'react-native-size-matters';
export default StyleSheet.create({
    container: {
        backgroundColor: '#fafafa'
    },
    scroll: {
        flex: 1
    },
    paddedContainer: {
        marginTop: 10,
        paddingHorizontal: 20
    },
    searchHolder: {
        paddingHorizontal: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        marginBottom: 20
    },
    searchBar: {
        height: 50,
        backgroundColor: 'white'
    },
    card: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        paddingHorizontal: 10,
        paddingVertical: 15
    },
    image: {
        marginRight: 10
    },
    name: {
        color: '#000'
    }
});
