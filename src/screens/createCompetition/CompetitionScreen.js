import React, { Component } from 'react';
import {
    View,
    TextInput,
    ScrollView,
    Text,
    FlatList,
    TouchableOpacity
} from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';

import { os, retrieveToken } from '../../utils';
import CompetitionStyles from './styles';
import { globalStyles } from '../../assets/styles';

import {
    fetchCreateCompetitions,
    refreshCreateCompetitions,
    resetFetchCreateCompetitions
} from '../../redux/epics/fetch-create-competitions-epic';
import {
    searchCreateCompetitions,
    resetSearchCreateCompetitions,
    searchCreateCompetitionsPage
} from '../../redux/epics/search-create-competition-epic';

import SmallCards from '../../components/SmallCards';
import OverlayIndicator from '../../components/OverlayIndicator';
import Seperator from '../../components/Seperator';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import HeaderBackButton from '../../components/HeaderBackButton';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import { fetchUser } from '../../redux/epics/fetch-user-epic';
import DashboardCard from '../../components/DashboardCard';

import alertOnError from '../../hoc/alertOnError';

class CompetitionScreen extends Component {
    constructor() {
        super();
        this.state = {
            search: '',
            leagues: [],
            initialFetch: true
        };
    }

    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingLeft: os() ? 10 : 15
            },
            headerLeft: (
                <HeaderBackButton
                    onPress={() => navigation.navigate('Dashboard')}
                />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    static getDerivedStateFromProps(nextProps, nextState) {
        const { createCompetitions, searchedCreateCompetition } = nextProps;

        return createCompetitions.status === 'successful' ||
            createCompetitions.status === 'failed' ||
            searchedCreateCompetition.status === 'successful' ||
            searchedCreateCompetition.status === 'failed'
            ? { fetchingPage: false }
            : { fetchingPage: true };
    }

    componentDidMount = async () => {
        let token = await retrieveToken();
        this.props.fetchUser(token);
        this.initialComeptitionsFetch();
    };

    componentDidUpdate(prevProps, prevState) {
        this.initialComeptitionsFetch();
    }

    initialComeptitionsFetch = () => {
        const { fetchCreateCompetitionsAction, leagues } = this.props;
        const { initialFetch } = this.state;
        if (
            leagues !== '' &&
            leagues !== 'inprogress' &&
            leagues.code >= 200 &&
            leagues.code < 300 &&
            initialFetch
        )
            retrieveToken().then(token => {
                const newLeagues = leagues.data.map((league, i) => ({
                    text: league.short_code,
                    state: i === 0
                }));
                this.setState(
                    {
                        initialFetch: false,
                        token,
                        leagues: newLeagues,
                        league: leagues.data[0].short_code.toLowerCase()
                    },
                    () => {
                        fetchCreateCompetitionsAction({
                            token,
                            league: this.state.league
                        });
                    }
                );
            });
    };

    postIt = item => {
        const { league, search } = this.state;
        const {
            navigation,
            createCompetitions,
            searchedCreateCompetition
        } = this.props;
        if (search) {
            navigation.push('competitionDetail', {
                item,
                leagueSpread: searchedCreateCompetition.data.league_spread
            });
        } else {
            navigation.push('competitionDetail', {
                item,
                leagueSpread: createCompetitions.data[league].league_spread
            });
        }
    };

    onPressCard = index => {
        const { leagues, token } = this.state;
        const { createCompetitions } = this.props;
        const newLeagues = leagues.map(({ text }) => ({
            text,
            state: false
        }));
        newLeagues[index].state = !newLeagues[index].state;
        this.setState(
            { leagues: newLeagues, league: leagues[index].text.toLowerCase() },
            () => {
                const { league, search } = this.state;
                if (search) {
                    this.props.searchCreateCompetitions({
                        token,
                        league,
                        search
                    });
                } else if (createCompetitions.data[league] === undefined) {
                    this.props.fetchCreateCompetitionsAction({
                        token,
                        league
                    });
                }
            }
        );
    };

    onTextEntered = search => {
        if (!search.length) this.props.resetSearchCreateCompetitions();
        this.setState({ search });
    };

    onSearch = () => {
        const { token, league, search } = this.state;
        if (search.length) {
            this.props.searchCreateCompetitions({ token, league, search });
        } else {
            this.props.resetSearchCreateCompetitions();
        }
    };

    onScrollEnd = ({ distanceFromEnd }) => {
        const { search, league, token, fetchingPage } = this.state;
        const {
            searchCreateCompetitions,
            searchedCreateCompetition,
            fetchCreateCompetitionsAction,
            createCompetitions,
            searchCreateCompetitionsPage
        } = this.props;
        if (!this.onEndReachedCalledDuringMomentum) {
            if (
                search &&
                searchedCreateCompetition.data.next_page &&
                !fetchingPage
            ) {
                this.setState({ fetchingPage: true }, () => {
                    searchCreateCompetitionsPage({
                        token,
                        league,
                        nextPage: searchedCreateCompetition.data.next_page
                    });
                });
            } else if (
                !search &&
                createCompetitions.data[league].next_page &&
                !fetchingPage
            ) {
                this.setState({ fetchingPage: true }, () => {
                    fetchCreateCompetitionsAction({
                        token,
                        league,
                        nextPage: createCompetitions.data[league].next_page
                    });
                });
            }
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    onRefresh = () => {
        const {
            refreshCreateCompetitions,
            searchCreateCompetitions
        } = this.props;
        const { league, token, search } = this.state;
        if (search) {
            searchCreateCompetitions({ league, token, search });
        } else {
            refreshCreateCompetitions({ league, token });
        }
    };

    emptyList = () => (
        <View style={globalStyles.mainContainer}>
            <EmptyListIndicator message="Currently there are no competitions in this league." />
        </View>
    );

    render() {
        const { league, search } = this.state;
        const {
            createCompetitions,
            leagues,
            searchedCreateCompetition
        } = this.props;

        let compsToShow;
        let totalRecords = 0;

        if (search && searchedCreateCompetition.status !== '') {
            compsToShow = searchedCreateCompetition.data.competitions;
            /* searchedCreateCompetition.status !== 'inprogress'
                    ? searchedCreateCompetition.data.competitions
                    : []; */
        } else {
            compsToShow =
                createCompetitions.data[league] === undefined
                    ? []
                    : createCompetitions.data[league].competitions;
        }

        if (
            createCompetitions.data[league] !== undefined &&
            createCompetitions.data[league].total_records !== undefined
        ) {
            totalRecords = createCompetitions.data[league].total_records;
        }
        if (
            search &&
            searchedCreateCompetition.status !== '' &&
            searchedCreateCompetition.status !== 'inprogress'
        ) {
            totalRecords = searchedCreateCompetition.data.total_records;
        }

        if (leagues === 'inprogress') {
            return <OverlayIndicator />;
        }

        return (
            <View style={[globalStyles.flexContainer]}>
                <View style={[globalStyles.mainContainer]}>
                    <Text
                        style={[
                            globalStyles.highlightBlueColor,
                            globalStyles.headingFontFace,
                            CompetitionStyles.heading
                        ]}
                    >
                        Create Competition{' '}
                    </Text>
                    <Text
                        style={[
                            globalStyles.baseFontSize,
                            globalStyles.blackColor,
                            globalStyles.boldFontFace
                        ]}
                    >
                        Pick a new team for contest and play against your
                        friends all season.
                    </Text>
                </View>

                <View style={[CompetitionStyles.paddingBottom]}>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={globalStyles.mainContainer}
                    >
                        {this.state.leagues.map(({ text, state }, i) => {
                            return (
                                <SmallCards
                                    key={i}
                                    text={text}
                                    selected={state}
                                    onPress={this.onPressCard}
                                    item={i}
                                    first={i === 0}
                                />
                            );
                        })}
                    </ScrollView>
                </View>
                <View
                    style={[
                        globalStyles.mainContainer,
                        CompetitionStyles.paddedContainer
                    ]}
                >
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.fullWidth,
                            CompetitionStyles.searchHolder
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.flexContainer,
                                CompetitionStyles.searchBar
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexContainer,
                                    globalStyles.flexVCenter
                                ]}
                            >
                                <TextInput
                                    onChangeText={this.onTextEntered}
                                    onEndEditing={this.onSearch}
                                    placeholder="Search with Team name"
                                    underlineColorAndroid="transparent"
                                    style={[
                                        globalStyles.fullHeight,
                                        globalStyles.greyColor
                                    ]}
                                    autoCorrect={false}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={this.onSearch}
                            style={globalStyles.flexVCenter}
                        >
                            <Icon name="search" type="evilicon" size={20} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View
                    style={[
                        globalStyles.mainContainer,
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        { paddingRight: 10, paddingBottom: 10 }
                    ]}
                >
                    <Text
                        style={[
                            globalStyles.subTextFontSize,
                            globalStyles.cardBoxGreyText
                        ]}
                    >
                        {totalRecords} Competition(s) Available
                    </Text>
                </View>
                <FlatList
                    ref={list => (this.flatList = list)}
                    // contentContainerStyle={[
                    //     { paddingBottom: 20 },
                    //     globalStyles.mainContainer
                    // ]}
                    // ItemSeparatorComponent={Seperator}
                    data={compsToShow}
                    renderItem={({ item }) => (
                        <DashboardCard
                            item={item}
                            onPress={this.postIt}
                            teamStatus={{ homeTeam: false, awayTeam: false }}
                            isCreateCompetitionCard
                        />
                    )}
                    ListEmptyComponent={this.emptyList()}
                    onEndReachedThreshold={0.01}
                    onEndReached={this.onScrollEnd}
                    keyExtractor={(item, index) =>
                        `id:${item.id}, index:${index}`
                    }
                    onRefresh={this.onRefresh}
                    refreshing={false}
                    // numColumns={2}
                    onMomentumScrollBegin={() => {
                        this.onEndReachedCalledDuringMomentum = false;
                    }}
                />
                {searchedCreateCompetition.status == 'inprogress' ||
                createCompetitions.status == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}

const mapStateToProps = ({
    leagues,
    createCompetitions,
    searchedCreateCompetition
}) => ({
    leagues,
    createCompetitions,
    searchedCreateCompetition
});

const mapDispatchToProps = dispatch => ({
    fetchCreateCompetitionsAction: data =>
        dispatch(fetchCreateCompetitions(data)),
    searchCreateCompetitions: data => dispatch(searchCreateCompetitions(data)),
    searchCreateCompetitionsPage: data =>
        dispatch(searchCreateCompetitionsPage(data)),
    resetSearchCreateCompetitions: () =>
        dispatch(resetSearchCreateCompetitions()),
    refreshCreateCompetitions: data =>
        dispatch(refreshCreateCompetitions(data)),
    resetFetchCreateCompetitions: () =>
        dispatch(resetFetchCreateCompetitions()),
    fetchUser: data => dispatch(fetchUser(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(CompetitionScreen, [
        {
            propName: 'searchedCreateCompetition',
            actionOnFailure: 'resetSearchCreateCompetitions'
        },
        {
            propName: 'createCompetitions',
            actionOnFailure: 'resetFetchCreateCompetitions'
        }
    ])
);
