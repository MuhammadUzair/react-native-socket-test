import React, { Component } from 'react';
import {
    Text,
    View,
    Slider,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    FlatList,
    ScrollView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { Dropdown } from 'react-native-material-dropdown';
import {
    fetchMyFriends,
    resetMyFriend
} from '../../redux/epics/fetch-my-friends-epic';

import { globalStyles, baseBlue } from '../../assets/styles';
import {
    getTime,
    getDate,
    renderSpreadWithPlusSign,
    getTimeZone,
    retrieveToken
} from '../../utils';
import BlueHeader from '../../components/BlueHeader';
import EmptyListIndicator from '../../components/EmptyListIndicator';

import CustomCheckbox from '../../components/CustomCheckBox';
import styles from './styles';
import ShareWith from './ShareWith';

class competitionDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sliderData: 0,
            team: false,
            competitor: false,
            enteredAmount: 0,
            spreadMsg: '',
            showSign: '',
            submitBtnPressed: false,
            selectedTeam: '',
            selectedShare: 'Public',
            shareOption: [
                {
                    name: 'Public'
                },
                {
                    name: 'Friend(s)'
                },
                {
                    name: 'Group(s)'
                }
            ],
            selectedItems: [],
            myFriends: [],
            token: retrieveToken()
        };
    }

    componentDidMount = () => {
        // let token = await retrieveToken();
        // this.setState({ token });
        // this.props.fetchMyFriends({ token, isCreateCompetition: true });
        Keyboard.addListener('keyboardDidShow', () =>
            this.scroll.props.scrollToEnd()
        );
    };
    // componentDidUpdate() {
    //     console.log('friends ', this.props.myFriends);
    // }

    // static getDerivedStateFromProps(newProps, prevState) {
    //     const { myFriends } = newProps;
    //     let myFriendsStateList = prevState.myFriends;

    //     // Fetch My Friends Start
    //     if (
    //         myFriends &&
    //         myFriends.data &&
    //         myFriends.data.friends &&
    //         myFriends.data.friends.length > 0
    //     ) {
    //         myFriendsStateList = myFriends.data.friends;
    //         // add isAdded property in Myfriends
    //         myFriendsStateList.map(item => (item.isAdded = false));
    //     }
    //     // Fetch My Friends - End
    //     console.log('myFriendsStateList ', myFriendsStateList);
    //     return {
    //         myFriends: myFriendsStateList
    //     };
    // }

    componentWillUnmount() {
        Keyboard.removeAllListeners('keyboardDidShow');
    }

    sliderHandler = val => {
        const { team, competitor } = this.state;
        const {
            navigation: {
                state: {
                    params: { item }
                }
            }
        } = this.props;
        this.setState({ submitBtnPressed: false });

        if (val > 0) {
            this.setState({ showSign: '+' + val.toString() });
        } else {
            this.setState({ showSign: val.toString() });
        }

        this.setState({ sliderData: +val });
        if (team || competitor) {
            this.setSpreadMsg(+val, item.team, item.competitor);
        }
    };

    checkForProblems = () => {
        const {
            navigation: {
                state: {
                    params: { item }
                }
            },
            settings: { serviceFee }
        } = this.props;
        const { spreadMsg, team, competitor, enteredAmount } = this.state;
        this.setState({ submitBtnPressed: true });
        if (!competitor && !team) {
            Alert.alert('Error', 'Please select a team');
            return false;
        }
        if (this.state.sliderData % 1 !== 0) {
            let sliderData = this.state.sliderData % 1;
            sliderData = Math.abs(sliderData);
            sliderData = sliderData.toFixed(2);
            sliderData = sliderData.toString();
            sliderData = sliderData.substr(2, 1);
            if (sliderData > 5) {
                Alert.alert(
                    'Error',
                    'Spread must be an integer value or with .5'
                );
                return false;
            }
        }
        if (
            !parseFloat(enteredAmount) ||
            Number.isNaN(parseFloat(enteredAmount))
        ) {
            Alert.alert('Error', 'Please enter an amount');
            return false;
        }
        if (parseFloat(enteredAmount) < 5 || parseFloat(enteredAmount) > 1000) {
            Alert.alert(
                'Amount Limit',
                'Competition amount should be between $5 and $1,000'
            );
            return false;
        }
        if (Math.abs(this.state.sliderData) > 90) {
            Alert.alert('Error', 'Spread Should be -90 to +90');
            return false;
        }
        return true;
    };

    onClickAction = (privacy_type, ids) => {
        const {
            navigation: {
                state: {
                    params: { item, leagueSpread }
                }
            },
            settings: { serviceFee }
        } = this.props;
        const {
            spreadMsg,
            team,
            competitor,
            enteredAmount,
            sliderData
        } = this.state;
        const newEnteredAmount = parseFloat(enteredAmount);
        if (!this.state.submitBtnPressed && this.checkForProblems()) {
            this.setState({ submitBtnPressed: false });
            this.props.navigation.push('competitionConfirmation', {
                selectedTeam: competitor ? 'competitor' : 'team',
                item,
                serviceFee,
                spreadMsg,
                spread: sliderData,
                enteredAmount: newEnteredAmount,
                serviceFeeAmount: (newEnteredAmount * serviceFee).toFixed(2),
                totalAmount: (
                    newEnteredAmount +
                    newEnteredAmount * serviceFee
                ).toFixed(2),
                privacy_type,
                ids
            });
        }
    };

    amountInput = enteredAmount => {
        this.setState({ enteredAmount, submitBtnPressed: false });
    };

    teamSelect = () => {
        this.setState(
            { team: !this.state.team, competitor: this.state.team },
            this.setSpreadOnTeamSelect
        );
    };
    competitorSelect = name => {
        this.setState(
            {
                competitor: !this.state.competitor,
                team: this.state.competitor,
                submitBtnPressed: false,
                selectedTeam: name
            },
            this.setSpreadOnTeamSelect
        );
    };

    setSpreadOnTeamSelect = () => {
        const {
            navigation: {
                state: {
                    params: { item }
                }
            },
            settings: { serviceFee }
        } = this.props;
        const { sliderData, team } = this.state;
        this.setSpreadMsg(sliderData, item.team, item.competitor);
    };

    setSpreadMsg = (spread, teamObj, competitorObj) => {
        const { team, competitor } = this.state;
        if (!team && !competitor) {
            this.setState({ spreadMsg: 'Please select a team first' });
        } else if (spread < 0) {
            this.setState({
                spreadMsg: `${
                    competitor ? teamObj.short_code : competitorObj.short_code
                } to win by more than ${Math.abs(spread)}`
            });
        } else if (spread > 0) {
            this.setState({
                spreadMsg: `${
                    competitor ? teamObj.short_code : competitorObj.short_code
                } to win or lose by less than ${spread}`
            });
        } else {
            this.setState({
                spreadMsg: `${
                    competitor ? teamObj.short_code : competitorObj.short_code
                } to win the match`
            });
        }
    };

    onChangeSpreadInput = text => {
        const {
            navigation: {
                state: {
                    params: { item }
                }
            }
        } = this.props;
        this.setState({ submitBtnPressed: false });
        if (text && text !== '-' && text !== '+') {
            this.setState({
                sliderData: parseFloat(text),
                showSign: ''
            });
            this.setSpreadMsg(+text, item.team, item.competitor);
        } else {
            if (text == '-' && text == '+') {
                this.setState({
                    sliderData: 0,
                    showSign: text
                });
                this.setSpreadMsg(0, item.team, item.competitor);
            } else {
                this.setState({
                    sliderData: parseFloat(text),
                    showSign: ''
                });
                this.setSpreadMsg(+text, item.team, item.competitor);
            }
        }
    };

    spreadInputValue = () => {
        if (this.state.showSign) {
            return this.state.showSign;
        } else {
            if (typeof this.state.sliderData == 'number') {
                return renderSpreadWithPlusSign(this.state.sliderData);
            } else {
                return '';
            }
        }
    };

    onChangeDropdown = (index, data) => {
        this.setState({ selectedShare: data[index].name });
    };

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };

    onScrollEnd = item => {
        console.log('onScrollEnd call');
    };

    onMyFrirendScrollEnd = () => {
        const { myFriends } = this.props;
        if (
            !this.onEndReachedCalledDuringMomentum &&
            myFriends &&
            myFriends.data &&
            myFriends.data.next_page
        ) {
            this.props.fetchMyFriends({
                token: this.state.token,
                nextPage: myFriends.data.next_page,
                isCreateCompetition: true
            });
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    onRefresh = item => {
        console.log('onRefresh call');
    };
    emptyList = () => {
        return <EmptyListIndicator message="There are no Users." />;
    };

    renderFriendsList = item => {
        return (
            <TouchableOpacity
                onPress={() => console.log('name ', item.name)}
                style={[
                    styles.listItem,
                    item.isAdded && globalStyles.blueBackgroudColor
                ]}
            >
                <Text>{item.name && item.name}</Text>
            </TouchableOpacity>
        );
    };

    renderApplicationFriends = () => {
        return (
            <FlatList
                style={[globalStyles.flexContainer]}
                data={this.state.myFriends}
                renderItem={(item, i) => this.renderFriendsList(item.item, i)}
                onEndReachedThreshold={0.1}
                onEndReached={e => this.onMyFrirendScrollEnd()}
                onRefresh={() => this.onRefresh('friends')}
                refreshing={false}
                ListEmptyComponent={this.emptyList()}
                keyExtractor={(item, index) =>
                    `id:${item.friend_id},index:${index}`
                }
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    onChangeName = text => {
        console.log('text ', text);
    };

    render() {
        const {
            navigation: {
                state: {
                    params: { item, leagueSpread }
                }
            },
            settings
        } = this.props;
        const time = item.time_left.split(':');
        const { enteredAmount } = this.state;
        const newEnteredAmount = parseFloat(enteredAmount);
        return (
            <KeyboardAwareScrollView
                innerRef={ref => (this.scroll = ref)}
                enableOnAndroid={true}
            >
                <BlueHeader
                    title="Create Competition"
                    navigation={this.props.navigation}
                    onPress={() => this.props.navigation.goBack()}
                />

                <View style={[globalStyles.whiteBackgroudColor]}>
                    <View
                        style={[
                            styles.rowContainer,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <View>
                            <Text
                                style={[
                                    styles.topSpacing,
                                    globalStyles.subTextFontSize,
                                    globalStyles.greyColor
                                ]}
                            >
                                Choose Team
                            </Text>
                            <TouchableOpacity
                                onPress={() =>
                                    this.competitorSelect('competitor')
                                }
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.chooseTeamSection
                                ]}
                            >
                                <CustomCheckbox
                                    size="small"
                                    checked={false}
                                    uri={
                                        item &&
                                        item.competitor &&
                                        item.competitor.avatar
                                            ? item.competitor.avatar
                                            : ''
                                    }
                                    title={
                                        item &&
                                        item.competitor &&
                                        item.competitor.short_code
                                            ? item.competitor.short_code
                                            : ''
                                    }
                                    style={styles.avatarMarigin}
                                />
                                <LinearGradient
                                    colors={
                                        this.state.selectedTeam !== 'competitor'
                                            ? ['#F5F5F5', '#F5F5F5']
                                            : ['#4d6bc2', '#3695e3']
                                    }
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={[
                                        globalStyles.flexHCenter,
                                        globalStyles.flexSpaceBetween,
                                        styles.teamNameButtonWrap
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseFontSize,
                                            this.state.selectedTeam ==
                                            'competitor'
                                                ? globalStyles.whiteColor
                                                : globalStyles.greyColor
                                        ]}
                                    >
                                        {item &&
                                        item.competitor &&
                                        item.competitor.name
                                            ? ' ' + item.competitor.name + ' '
                                            : null}
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.competitorSelect('team')}
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.chooseTeamSection
                                ]}
                            >
                                <CustomCheckbox
                                    size="small"
                                    checked={false}
                                    title={
                                        item &&
                                        item.team &&
                                        item.team.short_code
                                            ? item.team.short_code
                                            : ''
                                    }
                                    uri={
                                        item && item.team && item.team.avatar
                                            ? item.team.avatar
                                            : ''
                                    }
                                    style={styles.avatarMarigin}
                                />

                                <LinearGradient
                                    colors={
                                        this.state.selectedTeam !== 'team'
                                            ? ['#F5F5F5', '#F5F5F5']
                                            : ['#4d6bc2', '#3695e3']
                                    }
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={[
                                        globalStyles.flexHCenter,
                                        globalStyles.flexSpaceBetween,
                                        styles.teamNameButtonWrap
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseFontSize,
                                            this.state.selectedTeam == 'team'
                                                ? globalStyles.whiteColor
                                                : globalStyles.greyColor
                                        ]}
                                    >
                                        {item && item.team && item.team.name
                                            ? ' ' + item.team.name + ' '
                                            : null}
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            {!this.state.selectedTeam ? (
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        styles.selectTeamError
                                    ]}
                                >
                                    Please select your team first.
                                </Text>
                            ) : null}
                            <Text
                                style={[
                                    globalStyles.subTextFontSize,
                                    globalStyles.greyColor,
                                    styles.marginTop
                                ]}
                            >
                                {getDate(item.start_date_time) +
                                    ' ' +
                                    getTime(item.start_date_time) +
                                    ' ' +
                                    getTimeZone(item.start_date_time)}
                            </Text>
                        </View>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                styles.locationSection
                            ]}
                        >
                            {item &&
                            item.payload &&
                            item.payload.name &&
                            item.payload.city &&
                            item.payload.country ? (
                                <Text
                                    style={[
                                        globalStyles.baseBlueColor,
                                        globalStyles.subTextFontSize,
                                        styles.locationText,
                                        styles.marginTopTen
                                    ]}
                                >
                                    {item.payload.name}, {item.payload.city}{' '}
                                    {item.payload.country}
                                </Text>
                            ) : null}
                        </View>
                    </View>

                    {/* Spread  - start*/}
                    <View
                        style={[
                            styles.rowContainer,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexSpaceBetween
                            ]}
                        >
                            <View style={[styles.widthSixty]}>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Spread
                                </Text>
                            </View>
                            <Text
                                style={[
                                    globalStyles.greyColor,
                                    globalStyles.subTextFontSize,
                                    styles.spreadInputHeading
                                ]}
                            >
                                Enter here
                            </Text>
                            <View
                                style={[
                                    styles.widthForty,
                                    globalStyles.flexSpaceBetween,
                                    styles.roundedAmount,
                                    styles.spreadInputWrap
                                ]}
                            >
                                <TextInput
                                    style={globalStyles.baseFontSize}
                                    underlineColorAndroid="transparent"
                                    value={this.spreadInputValue()}
                                    onChangeText={text =>
                                        this.onChangeSpreadInput(text)
                                    }
                                    placeholder="0"
                                    placeholderTextColor="rgba(0, 0, 0, 0.87)"
                                />
                            </View>
                        </View>

                        <View>
                            <Slider
                                thumbTintColor="#eee"
                                minimumTrackTintColor={baseBlue}
                                style={{ width: '100%' }}
                                step={0.5}
                                minimumValue={-leagueSpread}
                                maximumValue={leagueSpread}
                                value={this.state.sliderData}
                                onSlidingComplete={this.sliderHandler}
                            />
                        </View>
                        {leagueSpread ? (
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexSpaceBetween,
                                    styles.leagueSpreadMarign
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyLightColor,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    -{leagueSpread}
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.greyLightColor,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    +{leagueSpread}
                                </Text>
                            </View>
                        ) : null}
                    </View>
                    {/* Spread  - end */}

                    {/* Amount section - start */}

                    <View
                        style={[
                            styles.rowContainer,
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween
                        ]}
                    >
                        <Text
                            style={[
                                styles.widthForty,
                                globalStyles.subTextFontSize,
                                globalStyles.greyColor
                            ]}
                        >
                            Amount
                        </Text>
                        <View style={[globalStyles.flexHorizontal]}>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.circularCircle,
                                    { width: 100 }
                                ]}
                            >
                                <View
                                    style={{
                                        borderRadius: 50,
                                        overflow: 'hidden',
                                        zIndex: 10
                                    }}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseBlueColor,
                                            globalStyles.whiteBackgroudColor,
                                            globalStyles.circularCircle,
                                            globalStyles.smallWidth,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        $
                                    </Text>
                                </View>
                                <TextInput
                                    keyboardType="numeric"
                                    underlineColorAndroid={'transparent'}
                                    style={[
                                        globalStyles.whiteColor,
                                        styles.inputText,
                                        globalStyles.baseFontSize
                                    ]}
                                    onChangeText={this.amountInput}
                                    value={`${
                                        this.state.enteredAmount !== 0
                                            ? this.state.enteredAmount
                                            : ''
                                    }`}
                                />
                            </View>
                        </View>
                    </View>

                    <View
                        style={[
                            styles.totalAmountSection,
                            globalStyles.hairLineUnderBorder,
                            styles.teamBorder
                        ]}
                    >
                        <View style={globalStyles.flexVertical}>
                            <View style={globalStyles.flexHorizontal}>
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        styles.totalHeading,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    Service Charges:
                                </Text>
                                <Text
                                    style={[
                                        globalStyles.highlightPinkColor,
                                        styles.textAlignRight,
                                        styles.amountWidth
                                    ]}
                                >
                                    3%
                                </Text>
                            </View>
                            <View style={globalStyles.flexHorizontal}>
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        styles.totalHeading,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    Total Amount:
                                </Text>

                                <Text
                                    style={[
                                        globalStyles.highlightPinkColor,
                                        styles.totalAmountText,
                                        styles.textAlignRight
                                    ]}
                                >
                                    {Number.isNaN(newEnteredAmount)
                                        ? `0.00`
                                        : (
                                              newEnteredAmount +
                                              newEnteredAmount *
                                                  settings.serviceFee
                                          ).toFixed(2)}
                                </Text>
                            </View>
                            {!this.state.enteredAmount ? (
                                <View
                                    style={[
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexVBottom
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.errorColor,
                                            globalStyles.subTextFontSize,
                                            styles.errorAmount
                                        ]}
                                    >
                                        Please enter an amount to create
                                        competition.
                                    </Text>
                                </View>
                            ) : null}
                        </View>
                    </View>

                    {/* Amount Section - end */}

                    {/* Share with  - start*/}
                    <ShareWith
                        navigation={this.props.navigation}
                        onClickAction={this.onClickAction}
                    />

                    {/* Share with  - end */}
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = ({ settings, myFriends }) => ({ settings, myFriends });
const mapDispatchToProps = dispatch => ({
    fetchMyFriends: data => dispatch(fetchMyFriends(data)),
    resetMyFriend: () => dispatch(resetMyFriend())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(competitionDetail);
