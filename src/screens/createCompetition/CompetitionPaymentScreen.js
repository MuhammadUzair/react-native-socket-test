import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { scale, verticalScale } from 'react-native-size-matters';

import { Icon } from 'react-native-elements';
import { globalStyles, greyColor } from '../../assets/styles';

import { os, getDate, getTime } from '../../utils';

import CompetitionStyles from '../createCompetition/styles';
import LinearGradient from 'react-native-linear-gradient';
import CustomAvatar from '../../components/CustomAvatar';
import HeaderBackButton from '../../components/HeaderBackButton';
import HeaderRightIcon from '../../components/HeaderRightIcon';

export default class PaymentDetails extends Component {
    state = { check: [true, false] };
    onPressCheckBox = index => {
        const check = [...this.state.check];
        check[index] = !check[index];
        this.setState({ check });
    };

    onSubmitClick = () => {
        const {
            navigation: {
                push,
                state: {
                    params: {
                        item,
                        spreadMsg,
                        enteredAmount,
                        serviceFee,
                        selectedTeam,
                        spread,
                        serviceFeeAmount,
                        totalAmount
                    }
                }
            }
        } = this.props;
        push('competitionConfirmation', {
            item,
            spreadMsg,
            enteredAmount,
            serviceFee,
            selectedTeam,
            spread,
            serviceFeeAmount,
            totalAmount
        });
    };

    onCrossIconClick = () => {
        this.props.navigation.push('competitionDetail');
    };
    static navigationOptions = args => {
        const { navigation, navigationOptions } = args;
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                height: verticalScale(40),
                paddingLeft: os() ? 10 : 15,
                backgroundColor: '#E9E9EF'
            },
            headerTitle: null,
            headerRight: null,
            headerLeft: (
                <HeaderBackButton
                    type="close"
                    onPress={() => navigation.goBack()}
                />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    render() {
        const {
            navigation: {
                state: {
                    params: {
                        item,
                        spreadMsg,
                        enteredAmount,
                        serviceFee,
                        selectedTeam,
                        spread,
                        serviceFeeAmount,
                        totalAmount
                    }
                }
            }
        } = this.props;
        const time = item.time_left.split(':');
        return (
            <View style={[globalStyles.flexContainer]}>
                <View
                    style={[
                        globalStyles.mainContainer,
                        globalStyles.flexContainer
                    ]}
                >
                    <Text
                        style={[
                            globalStyles.highlightBlueColor,
                            globalStyles.headingFontFace,
                            CompetitionStyles.heading
                        ]}
                    >
                        Payment Details
                    </Text>
                    <Text
                        style={[
                            globalStyles.blackColor,
                            globalStyles.boldFontFace,
                            CompetitionStyles.amountTextSize
                        ]}
                    >
                        {spreadMsg}
                    </Text>
                    <View
                        style={[
                            globalStyles.flexContainer,
                            globalStyles.flexVBottom
                        ]}
                    >
                        <View
                            style={[
                                globalStyles.greyBackgroudColor,
                                CompetitionStyles.borderRadiusTop,
                                {
                                    width: '100%',
                                    height: '10%',
                                    position: 'absolute',
                                    top: '2.5%'
                                }
                            ]}
                        />
                        <View style={[globalStyles.flexHCenter, { top: '1%' }]}>
                            <View
                                style={[
                                    globalStyles.blueBackgroudColor,
                                    {
                                        flex: 0,
                                        borderRadius: 50,
                                        overflow: 'hidden'
                                    }
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.blueBackgroudColor,
                                        globalStyles.whiteColor,
                                        globalStyles.baseFontSize,
                                        {
                                            paddingTop: 1,
                                            paddingBottom: 1,
                                            paddingLeft: 5,
                                            paddingRight: 5
                                        }
                                    ]}
                                >
                                    {item.days_left} D, {time[0]} H, {time[1]} M
                                    Left
                                </Text>
                            </View>
                        </View>

                        <View
                            style={[
                                globalStyles.whiteBackgroudColor,
                                CompetitionStyles.marginTop
                            ]}
                        >
                            <View style={[globalStyles.greyBackgroudColor]}>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseGreyColor,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        Teams
                                    </Text>
                                    <Text
                                        style={[
                                            globalStyles.boldFontFace,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        <Text
                                            style={[globalStyles.baseBlueColor]}
                                        >
                                            {' '}
                                            {item.competitor.name}{' '}
                                        </Text>
                                        VS
                                        <Text
                                            style={[globalStyles.baseBlueColor]}
                                        >
                                            {' '}
                                            {item.team.name}{' '}
                                        </Text>
                                    </Text>
                                </View>

                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.baseGreyColor,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        Location
                                    </Text>
                                    <Text
                                        style={[
                                            {
                                                width: '60%',
                                                textAlign: 'right'
                                            },
                                            globalStyles.highlightPinkColor,
                                            globalStyles.boldFontFace,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        {item.payload.name}, {item.payload.city}{' '}
                                        {item.payload.country}
                                    </Text>
                                </View>

                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            CompetitionStyles.widthForty,
                                            globalStyles.baseGreyColor,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        Date & Time
                                    </Text>
                                    <View style={[globalStyles.flexHorizontal]}>
                                        <Text
                                            style={[
                                                globalStyles.blackColor,
                                                globalStyles.baseFontSize
                                            ]}
                                        >
                                            {getDate(item.start_date_time)},{' '}
                                            {getTime(item.start_date_time)}
                                        </Text>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.baseGreyColor,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        Spread
                                    </Text>
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.baseBlueColor,
                                            globalStyles.baseFontSize,
                                            globalStyles.boldFontFace
                                        ]}
                                    >
                                        {spread}
                                    </Text>
                                </View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.flexHCenter,
                                        globalStyles.hairLineUnderBorder,
                                        { marginTop: -7 }
                                    ]}
                                >
                                    <View
                                        style={[CompetitionStyles.widthSixty]}
                                    >
                                        <Text
                                            style={[
                                                globalStyles.blackColor,
                                                CompetitionStyles.rowHeading
                                            ]}
                                        >
                                            Selected Team
                                        </Text>
                                        <Text
                                            style={[
                                                globalStyles.baseGreyColor,
                                                globalStyles.baseFontSize
                                            ]}
                                        >
                                            {spreadMsg}
                                        </Text>
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            globalStyles.flexHCenter
                                        ]}
                                    >
                                        <CustomAvatar
                                            small
                                            uri={item[selectedTeam].avatar}
                                            title={
                                                item[selectedTeam].short_code
                                            }
                                            rounded
                                        />
                                    </View>
                                </View>
                            </View>

                            <View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.hairLineUnderBorder
                                    ]}
                                >
                                    <Text
                                        style={[
                                            CompetitionStyles.widthForty,
                                            globalStyles.fontFace,
                                            globalStyles.blackColor,
                                            CompetitionStyles.rowHeading
                                        ]}
                                    >
                                        Amount
                                    </Text>
                                    <View style={[globalStyles.flexHorizontal]}>
                                        <View
                                            style={[
                                                globalStyles.flexHorizontal
                                            ]}
                                        >
                                            <Text
                                                style={[
                                                    globalStyles.fontFace,
                                                    globalStyles.baseBlueColor,
                                                    globalStyles.baseFontSize,
                                                    globalStyles.boldFontFace
                                                ]}
                                            >
                                                ${enteredAmount}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        CompetitionStyles.rowContainer,
                                        globalStyles.flexHorizontal,
                                        globalStyles.flexSpaceBetween,
                                        globalStyles.flexHCenter,
                                        { marginTop: -10 }
                                    ]}
                                >
                                    <View
                                        style={[CompetitionStyles.widthSixty]}
                                    >
                                        <Text
                                            style={[
                                                globalStyles.boldFontFace,
                                                CompetitionStyles.rowHeading
                                            ]}
                                        >
                                            Service Fee {serviceFee * 100}%
                                        </Text>
                                        <Text
                                            style={[
                                                globalStyles.baseGreyColor,
                                                globalStyles.subTextFontSize
                                            ]}
                                        >
                                            This help us run our platform and
                                            offer service like 24/7 support on
                                            your game
                                        </Text>
                                    </View>
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            globalStyles.flexHCenter
                                        ]}
                                    >
                                        <View
                                            style={[
                                                globalStyles.flexHorizontal
                                            ]}
                                        >
                                            <Text
                                                style={[
                                                    globalStyles.fontFace,
                                                    globalStyles.baseBlueColor,
                                                    globalStyles.baseFontSize,
                                                    globalStyles.boldFontFace
                                                ]}
                                            >
                                                ${serviceFeeAmount}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View
                            style={[
                                globalStyles.flexContainer,
                                globalStyles.whiteBackgroudColor
                            ]}
                        />
                    </View>
                </View>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        CompetitionStyles.paddingAll,
                        globalStyles.greyBackgroudColor
                    ]}
                >
                    <View style={{ marginTop: 6 }}>
                        <Text
                            style={[
                                CompetitionStyles.amountTextSize,
                                globalStyles.baseBlueColor,
                                globalStyles.boldFontFace
                            ]}
                        >
                            ${totalAmount}
                        </Text>
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                globalStyles.blackColor
                            ]}
                        >
                            Total Amount{' '}
                        </Text>
                    </View>

                    <View>
                        <TouchableOpacity
                            onPress={this.onSubmitClick}
                            activeOpacity={0.5}
                            style={[
                                CompetitionStyles.bottomSpacing,
                                CompetitionStyles.marginTopPlaybtn
                            ]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    CompetitionStyles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        CompetitionStyles.bigPrimaryButtonText
                                    ]}
                                >
                                    Submit
                                </Text>
                                <Icon
                                    size={scale(20)}
                                    color="#FFF"
                                    name="arrow-forward"
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
