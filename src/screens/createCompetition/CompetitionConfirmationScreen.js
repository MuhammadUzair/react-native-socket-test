import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { scale } from 'react-native-size-matters';
import { refreshMyCompetitions } from '../../redux/epics/fetch-my-competition-epic';
import {
    tsevoCheckCompilanceStatus,
    resetTsevoCheckCompilanceStatus
} from '../../redux/epics/tsevo-check-compliances-epic';

import { globalStyles } from '../../assets/styles';
import CompetitionStyles from './styles';

import {
    getTime,
    getDate,
    retrieveToken,
    getLocation,
    runLocationValidations,
    renderSpreadWithPlusSign,
    getTimeZone
} from '../../utils';

import { postCompetition } from '../../redux/epics/post-competition-epic';

import CustomAvatar from '../../components/CustomAvatar';
import CustomCheckbox from '../../components/CustomCheckBox';

import alertOnError from '../../hoc/alertOnError';
import OverlayIndicator from '../../components/OverlayIndicator';
import ConfirmationScreen from '../../components/ConfirmationScreen';

import BlueHeader from '../../components/BlueHeader';
import styles from './styles';

class competitionDetail extends Component {
    state = {
        playGameBtnPressed: false
    };
    dispatchCreateAction = () => {
        const {
            postCompetition,
            navigation: {
                state: {
                    params: {
                        item,
                        enteredAmount,
                        spread,
                        selectedTeam,
                        privacy_type,
                        ids
                    }
                }
            }
        } = this.props;
        retrieveToken().then(token => {
            this.setState({ token: token });
            let data = {
                token,
                sport_cid: item.id,
                team_id: item[selectedTeam].id,
                competitor_id:
                    selectedTeam === 'team' ? item.competitor.id : item.team.id,
                amount: enteredAmount,
                spread,
                privacy_type
            };
            if (privacy_type == 'Friends' || 'Groups') {
                data.ids = ids;
            }
            postCompetition(data);
        });
    };
    onConfirmBtnClick = async () => {
        const { compliance, tsevoCheckCompliances } = this.props;

        if (!this.state.playGameBtnPressed) {
            this.setState({ playGameBtnPressed: true }, () => {
                if (
                    tsevoCheckCompliances === '' ||
                    tsevoCheckCompliances.code >= 400
                ) {
                    Alert.alert(
                        'There was a problem',
                        'The location detected has been set to be Blocked by the merchant or operator.'
                    );
                    this.setState({ playGameBtnPressed: false });
                    return false;
                }
                if (
                    tsevoCheckCompliances &&
                    tsevoCheckCompliances.data &&
                    tsevoCheckCompliances.data.compliance == 'Allowed'
                ) {
                    this.dispatchCreateAction();
                } else {
                    Alert.alert(
                        'There was a problem',
                        'The location detected has been set to be Blocked by the merchant or operator.'
                    );
                    this.setState({ playGameBtnPressed: false });
                    return false;
                }
            });
        }
    };

    componentDidMount = async () => {
        let token = await retrieveToken();
        this.props.resetTsevoCheckCompilanceStatus();
        this.props.tsevoCheckCompilanceStatus({ token });
    };

    componentDidUpdate(prevProps, prevState) {
        const {
            navigation,
            competitionPosted,
            fetchUser,
            refreshMyCompetitions,
            tsevoCheckCompliances,
            resetTsevoCheckCompilanceStatus
        } = this.props;
        // console.log('didupdate ', tsevoCheckCompliances);
        if (competitionPosted.code >= 200 && competitionPosted.code < 300) {
            Alert.alert(
                'Competition Created successfully',
                '',
                [
                    {
                        text: 'Create more',
                        onPress: () => {
                            navigation.popToTop();
                            navigation.navigate('createCompetition');
                        }
                    },
                    {
                        text: 'Go back to Dashboard',
                        onPress: () => {
                            navigation.popToTop();
                            navigation.navigate('Dashboard');
                        }
                    }
                ],
                { cancelable: false }
            );
            // navigation.popToTop();
            // refreshMyCompetitions({
            //     token: this.state.token,
            //     tab: 'open'
            // });
            // navigation.navigate('MyCompetition');
        }
        if (
            tsevoCheckCompliances.status == 'ERROR' &&
            tsevoCheckCompliances.messages[0]
        ) {
            this.setState({ playGameBtnPressed: false });
            resetTsevoCheckCompilanceStatus();
            Alert.alert(
                'There was a problem',
                tsevoCheckCompliances.messages[0]
            );
        }
    }

    onCancelBtnClick = () => {
        const { navigation } = this.props;
        navigation.popToTop();
        navigation.navigate('createCompetition');
    };

    renderMyTeamRow = () => {
        const {
            navigation: {
                state: {
                    params: { item, selectedTeam, spreadMsg }
                }
            },
            competitionPosted
        } = this.props;
        let title = '',
            uri = '',
            name = '';

        if (selectedTeam !== 'competitor') {
            title =
                item && item.competitor && item.competitor.short_code
                    ? item.competitor.short_code
                    : '';

            uri =
                item && item.competitor && item.competitor.avatar
                    ? item.competitor.avatar
                    : '';
            name =
                item && item.competitor && item.competitor.name
                    ? ' ' + item.competitor.name + ' '
                    : '';
        } else {
            title =
                item && item.team && item.team.short_code
                    ? item.team.short_code
                    : '';

            uri = item && item.team && item.team.avatar ? item.team.avatar : '';
            name =
                item && item.team && item.team.name
                    ? ' ' + item.team.name + ' '
                    : '';
        }

        return (
            <View style={[globalStyles.flexHorizontal, styles.teamRow]}>
                <View>
                    <CustomCheckbox
                        size="small"
                        checked={false}
                        title={title}
                        uri={uri}
                        style={styles.avatarMarigin}
                    />
                </View>
                <View style={styles.alignSelfCenter}>
                    <Text style={[globalStyles.baseFontSize]}>{name}</Text>
                    <Text
                        style={[
                            globalStyles.baseFontSize,
                            globalStyles.baseBlueColor,
                            styles.confirmScreenSpread
                        ]}
                    >
                        {spreadMsg && spreadMsg}
                    </Text>
                </View>
            </View>
        );
    };

    render() {
        const {
            navigation: {
                state: {
                    params: {
                        item,
                        spreadMsg,
                        enteredAmount,
                        serviceFee,
                        selectedTeam,
                        spread,
                        serviceFeeAmount,
                        totalAmount
                    }
                }
            },
            competitionPosted
        } = this.props;

        return (
            <ConfirmationScreen
                title="Create Competition"
                navigation={this.props.navigation}
                onPressBackButton={() => this.props.navigation.goBack()}
                onPressSubmit={this.onConfirmBtnClick}
                isSubmitBtnPressed={
                    this.state.playGameBtnPressed ||
                    competitionPosted === 'inprogress'
                        ? true
                        : false
                }
                item={item}
                totalAmount={totalAmount}
                enteredAmount={enteredAmount}
                serviceFee={serviceFee}
                serviceFeeAmount={serviceFeeAmount}
                selectedTeam={selectedTeam}
            />
        );
    }
}

const mapStateToProps = ({
    competitionPosted,
    compliance,
    tsevoCheckCompliances
}) => ({
    competitionPosted,
    compliance,
    tsevoCheckCompliances
});
const mapDispatchToProps = dispatch => ({
    postCompetition: data => dispatch(postCompetition(data)),
    fetchUser: data => dispatch(fetchUser(data)),
    refreshMyCompetitions: data => dispatch(refreshMyCompetitions(data)),
    tsevoCheckCompilanceStatus: data =>
        dispatch(tsevoCheckCompilanceStatus(data)),
    resetTsevoCheckCompilanceStatus: data =>
        dispatch(resetTsevoCheckCompilanceStatus(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(competitionDetail, [{ propName: 'competitionPosted' }]));
