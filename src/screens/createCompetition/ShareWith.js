import React, { Component, Fragment } from 'react';
import {
    Text,
    View,
    Slider,
    TextInput,
    TouchableOpacity,
    Alert,
    Keyboard,
    FlatList,
    ScrollView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { Dropdown } from 'react-native-material-dropdown';
import {
    fetchMyFriends,
    resetMyFriend
} from '../../redux/epics/fetch-my-friends-epic';
import {
    fetchCompetitionGroups,
    resetMyGroups
} from '../../redux/epics/competition-groups-epic';

import { globalStyles, baseBlue } from '../../assets/styles';
import {
    getTime,
    getDate,
    renderSpreadWithPlusSign,
    getTimeZone,
    retrieveToken
} from '../../utils';
import BlueHeader from '../../components/BlueHeader';
import EmptyListIndicator from '../../components/EmptyListIndicator';

import CustomCheckbox from '../../components/CustomCheckBox';
import styles from './styles';
import BlueSmallCards from '../../components/BlueSmallCards';
import OverlayIndicator from '../../components/OverlayIndicator';

class ShareWith extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sliderData: 0,
            team: false,
            competitor: false,
            enteredAmount: 0,
            spreadMsg: '',
            showSign: '',
            submitBtnPressed: false,
            selectedTeam: '',
            selectedShare: 'Public',
            shareOption: [
                {
                    name: 'Public'
                },
                {
                    name: 'Friend(s)'
                },
                {
                    name: 'Group(s)'
                }
            ],
            selectedItems: [],
            myFriends: [],
            myGroups: [],
            selectedmyFriends: [],
            selectedmyGroups: [],
            token: '',
            updatestate: true
        };
    }
    componentDidMount = async () => {
        let token = await retrieveToken();
        this.setState({ token });
        this.props.fetchMyFriends({
            token,
            isCreateCompetition: true,
            isRefresh: true
        });
        this.props.fetchCompetitionGroups({ token, isRefresh: true });
    };
    // componentDidUpdate() {
    //     console.log('share update props  --', this.props.competitionGroups);
    // }

    static getDerivedStateFromProps(newProps, prevState) {
        const { myFriends, competitionGroups } = newProps;
        let myFriendsStateList = [];
        let myGroupsList = [];
        if (myFriends == 'inprogress') {
            return {
                myFriends: myFriends,
                myGroups: competitionGroups
            };
        }

        // Fetch My Friends Start
        if (
            myFriends &&
            myFriends.data &&
            myFriends.data.friends &&
            myFriends.data.friends.length > 0
        ) {
            myFriendsStateList = myFriends.data.friends;
            // add isAdded property in Myfriends
            myFriendsStateList.map(item => (item.isAdded = false));
        }
        // Fetch My Friends - End

        // Fetch My Groups Start
        if (
            competitionGroups &&
            competitionGroups.data &&
            competitionGroups.data.groups &&
            competitionGroups.data.groups.length > 0
        ) {
            myGroupsList = competitionGroups.data.groups;
            // add isAdded property in MyGroups
            myGroupsList.map(item => (item.isAdded = false));
        }
        // Fetch My Groups - End

        return {
            myFriends: myFriendsStateList,
            myGroups: myGroupsList
        };
    }

    onChangeDropdown = (index, data) => {
        this.setState({ selectedShare: data[index].name });
    };

    onMyFrirendScrollEnd = async () => {
        let token = await retrieveToken();
        const { myFriends } = this.props;
        if (
            !this.onEndReachedCalledDuringMomentum &&
            myFriends &&
            myFriends.data &&
            myFriends.data.next_page
        ) {
            this.props.fetchMyFriends({
                token: token,
                nextPage: myFriends.data.next_page,
                isCreateCompetition: true
            });
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    onMyGroupsScrollEnd = async () => {
        let token = await retrieveToken();
        const { competitionGroups } = this.props;
        if (
            !this.onEndReachedCalledDuringMomentum &&
            competitionGroups &&
            competitionGroups.data &&
            competitionGroups.data.next_page
        ) {
            this.props.fetchCompetitionGroups({
                token: token,
                nextPage: competitionGroups.data.next_page
            });
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    emptyList = () => {
        let message = 'There are no Friends.';
        if (this.state.selectedShare == 'Group(s)') {
            message = 'There are no Groups.';
        }
        return <EmptyListIndicator message={message} />;
    };

    selectFriend = item => {
        let selectedmyFriends = [...this.state.selectedmyFriends];
        if (item.isAdded) {
            this.removeSelectedFriend(item);
        }
        let allFriends = this.state.myFriends;
        allFriends.map(friend => {
            if (friend.friend_id == item.friend_id) {
                friend.isAdded = !friend.isAdded;
            }
        });
        if (item.isAdded) {
            selectedmyFriends.push(item);
            this.setState({ selectedmyFriends });
        }
    };

    removeSelectedFriend = item => {
        if (item.isAdded) {
            let selectedmyFriends = [];
            let allSelectedmyFriends = [...this.state.selectedmyFriends];
            allSelectedmyFriends.map(friend => {
                if (item.friend_id !== friend.friend_id) {
                    selectedmyFriends.push(friend);
                }
            });
            this.setState({ selectedmyFriends });
        }
    };

    selectGroup = item => {
        let selectedmyGroups = [...this.state.selectedmyGroups];
        if (item.isAdded) {
            this.removeSelectedGroup(item);
        }
        let allGroups = this.state.myGroups;
        allGroups.map(group => {
            if (group.group_id == item.group_id) {
                group.isAdded = !group.isAdded;
            }
        });
        if (item.isAdded) {
            selectedmyGroups.push(item);
            this.setState({ selectedmyGroups });
        }
    };

    removeSelectedGroup = item => {
        if (item.isAdded) {
            let selectedmyGroups = [];
            let allSelectedGroups = [...this.state.selectedmyGroups];
            allSelectedGroups.map(group => {
                if (item.group_id !== group.group_id) {
                    selectedmyGroups.push(group);
                }
            });
            this.setState({ selectedmyGroups });
        }
    };

    deleteSelectedFriend = item => {
        let selectedmyFriends = [];
        let allSelectedmyFriends = [...this.state.selectedmyFriends];
        allSelectedmyFriends.map(friend => {
            if (item.friend_id !== friend.friend_id) {
                selectedmyFriends.push(friend);
            }
        });
        this.setState({ selectedmyFriends });
    };

    deleteSelectedGroup = item => {
        let selectedmyGroups = [];
        let allSelectedGroups = [...this.state.selectedmyGroups];
        allSelectedGroups.map(group => {
            if (item.group_id !== group.group_id) {
                selectedmyGroups.push(group);
            }
        });
        this.setState({ selectedmyGroups });
    };

    renderShareList = (item, listName) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    if (listName == 'friends') {
                        this.selectFriend(item);
                    } else {
                        this.selectGroup(item);
                    }
                }}
                style={[
                    styles.listItem,
                    item.isAdded && globalStyles.blueBackgroudColor
                ]}
            >
                <Text>{item.name && item.name}</Text>
            </TouchableOpacity>
        );
    };

    renderApplicationFriends = () => {
        return (
            <FlatList
                style={[globalStyles.flexContainer]}
                data={this.state.myFriends}
                renderItem={(item, i) =>
                    this.renderShareList(item.item, 'friends')
                }
                onEndReachedThreshold={0.1}
                onEndReached={e => this.onMyFrirendScrollEnd()}
                ListEmptyComponent={this.emptyList()}
                keyExtractor={(item, index) =>
                    `id:${item.friend_id},index:${index}`
                }
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    renderGroups = () => {
        return (
            <FlatList
                style={[globalStyles.flexContainer]}
                data={this.state.myGroups}
                renderItem={(item, i) =>
                    this.renderShareList(item.item, 'groups')
                }
                onEndReachedThreshold={0.1}
                onEndReached={e => this.onMyFrirendScrollEnd()}
                ListEmptyComponent={this.emptyList()}
                keyExtractor={(item, index) =>
                    `id:${item.group_id},index:${index}`
                }
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    onChangeName = text => {
        if (this.state.selectedShare == 'Friend(s)') {
            this.props.fetchMyFriends({
                token: this.state.token,
                isCreateCompetition: true,
                isSearch: text ? true : false,
                search: text,
                isRefresh: true
            });
        }
        if (this.state.selectedShare == 'Group(s)') {
            this.props.fetchCompetitionGroups({
                token: this.state.token,
                isCreateCompetition: true,
                isSearch: text ? true : false,
                search: text,
                isRefresh: true
            });
        }
    };

    selectedmyFriendsList = () => {
        if (this.state.selectedmyFriends.length > 0) {
            return this.state.selectedmyFriends.map((friend, i) => {
                return (
                    <BlueSmallCards
                        key={i}
                        text={friend.name}
                        onPress={() => this.selectFriend(friend)}
                        item={i}
                        first={i === 0}
                        index={i}
                    />
                );
            });
        }
        return null;
    };

    selectedmyGroupList = () => {
        if (this.state.selectedmyGroups.length > 0) {
            return this.state.selectedmyGroups.map((group, i) => {
                return (
                    <BlueSmallCards
                        key={i}
                        text={group.name}
                        onPress={() => this.selectGroup(group)}
                        item={i}
                        first={i === 0}
                        index={i}
                    />
                );
            });
        }
        return null;
    };

    getFriendAndGroupIds = () => {
        let selectedIDs = [];
        let allSelectedItem;
        if (this.state.selectedShare == 'Friend(s)') {
            allSelectedItem = [...this.state.selectedmyFriends];
            allSelectedItem.map(friend => selectedIDs.push(friend.friend_id));
            this.props.onClickAction('Friends', selectedIDs);
        } else if (this.state.selectedShare == 'Group(s)') {
            allSelectedItem = [...this.state.selectedmyGroups];
            allSelectedItem.map(group => selectedIDs.push(group.group_id));
            this.props.onClickAction('Groups', selectedIDs);
        } else {
            this.props.onClickAction('Public');
        }
    };

    render() {
        const { myFriends } = this.props;
        let placeholder = 'Add ' + this.state.selectedShare;
        return (
            <Fragment>
                {/* Share with  - start*/}
                <View
                    style={[
                        styles.shareRow,
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween
                    ]}
                >
                    <Text
                        style={[
                            styles.widthForty,
                            globalStyles.subTextFontSize,
                            globalStyles.greyColor
                        ]}
                    >
                        Share with
                    </Text>
                    <View style={[globalStyles.flexHorizontal]}>
                        <Dropdown
                            value={this.state.selectedShare}
                            rippleOpacity={0}
                            labelFontSize={0}
                            fontSize={14}
                            labelHeight={0}
                            textColor={'rgba(0, 0, 0, 0.87)'}
                            data={this.state.shareOption}
                            onChangeText={(value, index, data) =>
                                this.onChangeDropdown(index, data)
                            }
                            containerStyle={styles.smallDropDown}
                            valueExtractor={value => value.name}
                        />
                    </View>
                </View>

                {/* share options - start */}
                {this.state.selectedShare == 'Friend(s)' ||
                this.state.selectedShare == 'Group(s)' ? (
                    <Fragment>
                        <View style={[styles.teamRow]}>
                            <View>
                                <Text
                                    style={[
                                        globalStyles.subTextFontSize,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Select {this.state.selectedShare}
                                </Text>
                            </View>
                        </View>

                        <View
                            style={[
                                styles.rowContainer,
                                styles.searchInputWrap,
                                globalStyles.flexVertical,
                                (this.state.selectedmyFriends.length == 2 &&
                                    this.state.selectedShare == 'Friend(s)') ||
                                (this.state.selectedmyGroups.length == 2 &&
                                    this.state.selectedShare == 'Group(s)')
                                    ? styles.sharePaddingBottom
                                    : null
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexContainer,
                                    styles.wrapView
                                ]}
                            >
                                {
                                    {
                                        'Friend(s)': this.selectedmyFriendsList(),
                                        'Group(s)': this.selectedmyGroupList()
                                    }[this.state.selectedShare]
                                }
                            </View>
                            <TextInput
                                placeholder={placeholder}
                                underlineColorAndroid="transparent"
                                style={globalStyles.fullHeight}
                                onChangeText={e => this.onChangeName(e)}
                                style={
                                    (this.state.selectedmyFriends.length == 0 &&
                                        this.state.selectedShare ==
                                            'Friend(s)') ||
                                    (this.state.selectedmyGroups.length == 0 &&
                                        this.state.selectedShare == 'Group(s)')
                                        ? styles.defualtAddFriendTextInput
                                        : styles.addFriendTextInput
                                }
                            />
                        </View>

                        <View
                            contentContainerStyle={[
                                styles.teamRow,
                                globalStyles.flexHorizontal,
                                globalStyles.flexSpaceBetween
                            ]}
                        >
                            <ScrollView style={styles.listScrolView}>
                                {
                                    {
                                        'Friend(s)': this.renderApplicationFriends(),
                                        'Group(s)': this.renderGroups()
                                    }[this.state.selectedShare]
                                }
                            </ScrollView>
                        </View>
                    </Fragment>
                ) : null}

                {/* share options - end */}

                <View
                    style={[
                        styles.rowContainer,
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween
                    ]}
                />

                <View
                    style={[
                        styles.rowContainer,
                        globalStyles.hairLineUnderBorder,
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween
                    ]}
                >
                    <View style={globalStyles.flexVTop}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            activeOpacity={0.5}
                            style={[styles.marginTop]}
                        >
                            <LinearGradient
                                colors={['#F5F5F5', '#F5F5F5']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.backButtonWrap
                                ]}
                            >
                                <Text
                                    style={[
                                        styles.footerButonText,
                                        globalStyles.greyColor
                                    ]}
                                >
                                    Back
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>

                    <View style={globalStyles.flexVBottom}>
                        <TouchableOpacity
                            onPress={() => this.getFriendAndGroupIds()}
                            activeOpacity={0.5}
                            style={[styles.marginTop]}
                            disabled={this.state.submitBtnPressed}
                        >
                            <LinearGradient
                                colors={
                                    this.state.submitBtnPressed
                                        ? ['#b2bec3', '#b2bec3']
                                        : ['#4d6bc2', '#3695e3']
                                }
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.backButtonWrap
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.whiteColor,
                                        styles.footerButonText
                                    ]}
                                >
                                    Submit
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {myFriends.status == 'inprogress' ? <OverlayIndicator /> : null}

                {/* Share with  - end */}
            </Fragment>
        );
    }
}

const mapStateToProps = ({ settings, myFriends, competitionGroups }) => ({
    settings,
    myFriends,
    competitionGroups
});
const mapDispatchToProps = dispatch => ({
    fetchMyFriends: data => dispatch(fetchMyFriends(data)),
    resetMyFriend: () => dispatch(resetMyFriend()),
    fetchCompetitionGroups: data => dispatch(fetchCompetitionGroups(data)),
    resetMyGroups: () => dispatch(resetMyGroups())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShareWith);
