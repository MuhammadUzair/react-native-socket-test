import { connect } from 'react-redux';
import MyTeams from './MyTeams';

import { fetchTeams } from '../../redux/epics/fetch-teams-epic';
import { fetchLeagues } from '../../redux/epics/fetch-leagues-epic';
import { addTeams, reset } from '../../redux/epics/add-teams-epic';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ leagues, teams, teamsAdded }) => ({
    leagues,
    teams,
    teamsAdded
});

const mapDispatchToProps = dispatch => ({
    fetchLeagues: data => dispatch(fetchLeagues(data)),
    fetchTeams: data => dispatch(fetchTeams(data)),
    addTeams: data => dispatch(addTeams(data)),
    resetAddTeams: () => dispatch(reset())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(MyTeams, [
        { propName: 'teamsAdded', actionOnFailure: 'resetAddTeams' }
    ])
);
