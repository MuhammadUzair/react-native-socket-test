import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    FlatList
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { os, retrieveToken, generateErrorMessage } from '../../utils';

import { globalStyles, onboardGrey } from '../../assets/styles';
import teamsStyles from './styles';
import CustomCheckBox from '../../components/CustomCheckBox';
import SmallCards from '../../components/SmallCards';
import OverlayIndicator from '../../components/OverlayIndicator';
import OnboardTeamCheckbox from '../../components/OnboardTeamCheckbox';

export default class MyTeams extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft:
                navigation.state.params &&
                navigation.state.params.disableBack &&
                null,
            headerRight: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('MyFriends')}
                    style={{ paddingRight: os() ? 10 : 20 }}
                >
                    <Text style={{ color: onboardGrey }}>Skip Step</Text>
                </TouchableOpacity>
            )
        };
    };

    static getDerivedStateFromProps(nextProps, nextState) {
        let leagues = [];
        if (
            nextProps.leagues !== '' &&
            nextProps.leagues !== 'inprogress' &&
            toString.call(nextProps.leagues.data).indexOf('Array') >= 0
        )
            leagues = nextProps.leagues.data.map(
                (item, i) => ((item.state = i === 0), item)
            );
        let selectedLeague = 0;
        if (typeof nextProps.teams !== 'string')
            selectedLeague = Object.keys(nextProps.teams.data)[0];

        return { leagues, teams: nextProps.teams, selectedLeague };
    }

    componentDidMount() {
        const { fetchTeams, leagues, fetchLeagues } = this.props;
        retrieveToken()
            .then(token => {
                if (leagues === '') fetchLeagues(token);
                fetchTeams(token);
            })
            .catch(err => this.props.navigation.navigate('Login'));
    }

    componentDidUpdate(prevProps, prevState) {
        const { teamsAdded, navigation, resetAddTeams } = this.props;
        if (teamsAdded !== 'inprogress' && teamsAdded !== '') {
            if (teamsAdded.code >= 200 && teamsAdded.code < 300) {
                navigation.navigate('MyFriends');
                resetAddTeams();
            }
        }
    }

    state = {
        leagues: [],
        teams: [],
        selectedLeague: 0,
        search: '',
        selectedTeams: {}
    };

    onPressCheckbox = teamID => {
        const { teams, selectedTeams, selectedLeague } = this.state;
        const newTeams = { ...teams };
        let newSeletedTeams = { ...selectedTeams };

        if (newSeletedTeams[teamID]) newSeletedTeams[teamID] = null;
        else
            newSeletedTeams[teamID] = {
                league_id: selectedLeague,
                team_id: teamID
            };

        this.setState({ teams: newTeams, selectedTeams: newSeletedTeams });
    };

    onPressCard = index => {
        const leagues = this.state.leagues.map(
            league => ((league.state = false), league)
        );
        leagues[index].state = true;
        this.setState({ leagues, selectedLeague: leagues[index].id });
    };

    onPressContinue = () => {
        const { addTeams } = this.props;
        const { selectedTeams } = this.state;
        retrieveToken()
            .then(token => {
                addTeams({ team_ids: Object.values(selectedTeams), token });
            })
            .catch(err => Alert.alert('There was a problem', err.message));
    };

    renderListItems = ({ item, index }) => {
        const { selectedTeams } = this.state;
        return (
            <OnboardTeamCheckbox
                onPress={this.onPressCheckbox}
                item={item}
                index={index}
                isAdded={selectedTeams[item.id]}
            />
        );
    };

    renderLeagueItems = ({ item, index }) => (
        <SmallCards
            index={index}
            item={index}
            text={item.short_code}
            selected={item.state}
            onPress={this.onPressCard}
            first={index === 0}
        />
    );

    emptyList = () => <Text>There are no teams to show</Text>;

    searchBarHandler = search => {
        this.setState({ search });
    };

    render() {
        const {
            teams,
            leagues,
            selectedLeague,
            search,
            selectedTeams
        } = this.state;
        const { teamsAdded } = this.props;

        let filteredTeams = [];
        if (teams !== '' && teams !== 'inprogress') {
            filteredTeams = teams.data[selectedLeague].leagueTeams;
            if (search)
                filteredTeams = teams.data[selectedLeague].leagueTeams.filter(
                    item => {
                        if (
                            item.name
                                .toLowerCase()
                                .indexOf(search.toLowerCase()) >= 0 ||
                            item.name
                                .toLowerCase()
                                .indexOf(search.toLowerCase()) >= 0 ||
                            item.short_code.indexOf(search) >= 0 ||
                            item.address.city
                                .toLowerCase()
                                .indexOf(search.toLowerCase()) >= 0 ||
                            item.address.states
                                .toLowerCase()
                                .indexOf(search.toLowerCase()) >= 0 ||
                            item.address.country
                                .toLowerCase()
                                .indexOf(search.toLowerCase()) >= 0
                        )
                            return true;
                        return false;
                    }
                );
        }
        return (
            <View
                style={[globalStyles.mainContainer, globalStyles.flexContainer]}
            >
                <Text
                    style={[teamsStyles.headerText, globalStyles.baseBlueColor]}
                >
                    My Teams
                </Text>
                <Text style={[teamsStyles.subText, globalStyles.boldFontFace]}>
                    Personalize Post It Play It
                </Text>
                <Text
                    style={teamsStyles.descriptionText}
                >{`Choose your favourite sports, teams and friends\nfor quick access across Post It Play It Competitions.`}</Text>
                <View style={teamsStyles.searchHolder}>
                    <TextInput
                        style={teamsStyles.search}
                        placeholder="Search by Name, City, State and Country"
                        placeholderTextColor="#000"
                        underlineColorAndroid="transparent"
                        onChangeText={this.searchBarHandler}
                    />
                </View>
                <View>
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        style={teamsStyles.scrollView}
                        data={leagues}
                        renderItem={this.renderLeagueItems}
                        keyExtractor={(item, index) =>
                            `index:${index}, id:${item.id}`
                        }
                    />
                </View>
                <View style={[globalStyles.flexContainer]}>
                    <View
                        style={[
                            teamsStyles.teamsCard,
                            globalStyles.flexContainer
                        ]}
                    >
                        <FlatList
                            style={globalStyles.flexContainer}
                            extraData={teams}
                            data={filteredTeams}
                            renderItem={this.renderListItems}
                            ListEmptyComponent={this.emptyList()}
                            keyExtractor={(item, index) =>
                                `index:${index}, id:${item.id}`
                            }
                        />
                    </View>
                    <View style={{ top: -26 }}>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHCenter,
                                teamsStyles.bottomSpacing
                            ]}
                            onPress={this.onPressContinue}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    teamsStyles.buttonStyle
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        teamsStyles.buttonText
                                    ]}
                                >
                                    Continue
                                </Text>
                                <Icon
                                    size={20}
                                    color="#FFF"
                                    name="arrow-forward"
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {teams === 'inprogress' ||
                teams === '' ||
                teamsAdded === 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
