import { StyleSheet, Dimensions } from 'react-native';
const { height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        paddingVertical: 10
    },
    card: {
        padding: 5
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    subText: {
        fontSize: 18,
        color: 'black',
        marginBottom: 5
    },
    mb: {
        marginBottom: 10
    },
    descriptionText: {
        color: 'black',
        fontSize: 12,
        marginBottom: 10
    },
    checkbox: {
        width: '100%',
        paddingLeft: 2.5,
        paddingRight: 2.5,
        marginBottom: 10,
        alignItems: 'center'
    },
    checkboxText: {
        color: 'black',
        fontSize: 12
    },
    checkboxStyle: {
        marginRight: 10
    },
    buttonStyle: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row',
        width: 70 + '%'
    },
    buttonText: {
        fontSize: 14
    },
    nameHolder: {
        flex: 4
    },
    stateHolder: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    scrollContainer: {
        paddingHorizontal: 20
    },
    scrollViewWrapper: {
        backgroundColor: 'white',
        borderRadius: 5,
        paddingVertical: 10
    },
    scrollView: {
        marginTop: 5,
        paddingHorizontal: 0
    },
    paddedContainer: {
        paddingHorizontal: 0
    },
    searchHolder: {
        paddingHorizontal: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        marginVertical: 5
    },
    searchBar: {
        height: 40,
        backgroundColor: 'white'
    },
    linkWrap: {
        marginTop: height * 0.3
    },
    linkText: {
        textAlign: 'center'
    }
});
