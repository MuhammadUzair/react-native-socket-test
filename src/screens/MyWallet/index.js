import { connect } from 'react-redux';

import MyWallet from './MyWallet';

import {
    fetchTransactions,
    refreshTransactions
} from '../../redux/epics/fetch-transactions-epic';
import { fetchUser } from '../../redux/epics/fetch-user-epic';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ transactions, compliance }) => ({
    transactions,
    compliance
});

const mapDispatchToProps = dispatch => ({
    fetchTransactions: data => dispatch(fetchTransactions(data)),
    refreshTransactions: data => dispatch(refreshTransactions(data)),
    fetchUser: data => dispatch(fetchUser(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(MyWallet, [{ propName: 'transactions' }]));
