import React, { Component, Fragment } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    FlatList,
    Alert,
    Platform,
    Linking
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon } from 'react-native-elements';
import DrawerIconButton from '../../components/DrawerIconButton';
import SmallCards from '../../components/SmallCards';
import Transaction from '../../components/Transaction';

import { os, retrieveToken, complianceCheck, getLocation } from '../../utils';

import { globalStyles } from '../../assets/styles';
import styles from './styles';

import Seperator from '../../components/Seperator';
import EmptyListIndicator from '../../components/EmptyListIndicator';
import OverlayIndicator from '../../components/OverlayIndicator';

export default class MyWallet extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20
            },
            headerLeft: (
                <DrawerIconButton onPress={() => navigation.openDrawer()} />
            )
        };
    };

    static getDerivedStateFromProps(props, state) {
        if (props.transactions.status !== 'inprogress')
            return { isFetching: false };
        return null;
    }

    state = {
        tabStates: [
            { text: 'Funds', state: false },
            { text: 'Transactions', state: true },
            { text: 'Pending Transactions', state: false }
        ],
        showLoader: false
    };

    componentDidMount() {
        const { fetchTransactions, fetchUser } = this.props;
        retrieveToken().then(token => {
            this.setState({ token }, () => {
                fetchUser(token);
                fetchTransactions({ token });
            });
        });
    }

    onSearchPressed = () => {
        this.props.navigation.push('MyWalletSearchScreen');
    };

    onPressTransactionItems = item => {
        this.props.navigation.push('WalletTransationScreen', { item });
    };

    navigateToFundsScreen = () => {
        this.props.navigation.navigate('AddFundsScreen');
    };

    onPressCheckbox = index => {
        const check = [...this.state.check];
        check[index] = !check[index];
        const added = [...check].filter(i => i);
        this.setState({ check, added });
    };

    onTabPress = async i => {
        const { tabStates } = this.state;
        if (tabStates[i].text === 'Pending Transactions') {
            this.props.navigation.navigate('GatewayTransaction');
        }
        if (tabStates[i].text === 'Funds') {
            try {
                this.setState({ showLoader: true });
                const token = await retrieveToken();
                // const res = await complianceCheck(token);
                let res;
                if (this.props.compliance === true) res = true;
                else res = false;
                if (!res) {
                    getLocation(this.navigateToFundsScreen);
                } else {
                    Alert.alert(
                        'There was a problem',
                        'The location detected has been set to be Blocked by the merchant or operator.'
                    );
                }
            } catch (e) {
                Alert.alert('There was a problem', e.message);
            } finally {
                this.setState({ showLoader: false });
            }
        }
    };

    onRefresh = () => {
        const { refreshTransactions } = this.props;
        const { token } = this.state;
        refreshTransactions({ token });
    };

    onScrollEnd = () => {
        const { fetchTransactions, transactions } = this.props;
        const { token, isFetching } = this.state;
        if (!isFetching) {
            if (transactions.data.next_page)
                this.setState({ isFetching: true }, () => {
                    fetchTransactions({
                        token,
                        nextPage: transactions.data.next_page
                    });
                });
        }
    };

    renderCard = ({ item, index }) => (
        <Transaction item={item} onPress={this.onPressTransactionItems} />
    );

    changeUrl = () => {
        Linking.openURL('https://live.postitplayit.com/app/login');
    };

    renderIOSMyWallet = () => (
        <TouchableOpacity style={styles.linkWrap} onPress={this.changeUrl}>
            <Text style={[styles.descriptionText, styles.linkText]}>
                Go to {`\n`}
                https://live.postitplayit.com
            </Text>
        </TouchableOpacity>
    );
    renderTransactionTypes = transactions => {
        return (
            <Fragment>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexHCenter
                    ]}
                >
                    <View style={[globalStyles.flexHorizontal, { flex: 1 }]}>
                        {this.state.tabStates.map(({ text, state }, i) => (
                            <SmallCards
                                key={i}
                                style={{ marginLeft: 0, marginRight: 5 }}
                                containerStyle={{
                                    paddingHorizontal: 8
                                }}
                                selected={state}
                                text={text}
                                item={i}
                                onPress={this.onTabPress}
                                first={i === 0}
                                textStyle={{ fontSize: 11.7 }}
                            />
                        ))}
                    </View>
                    <TouchableOpacity
                        onPress={this.onSearchPressed}
                        activeOpacity={0.5}
                    >
                        <LinearGradient
                            colors={['#4d6bc2', '#3695e3']}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 50,
                                padding: 8
                            }}
                        >
                            <Icon
                                name="search"
                                type="evillcons"
                                size={18}
                                color="#fff"
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                <View
                    style={[
                        globalStyles.flexContainer,
                        { paddingVertical: 10 }
                    ]}
                >
                    <View
                        style={[
                            globalStyles.flexContainer,
                            styles.scrollViewWrapper
                        ]}
                    >
                        <FlatList
                            data={transactions.data.wallet}
                            renderItem={this.renderCard}
                            ItemSeparatorComponent={Seperator}
                            ListEmptyComponent={
                                <EmptyListIndicator
                                    // showIndicator={
                                    //     transactions.status === '' ||
                                    //     transactions.status === 'inprogress'
                                    // }
                                    message="No transactions to show"
                                />
                            }
                            onRefresh={this.onRefresh}
                            refreshing={false}
                            onEndReachedThreshold={0.1}
                            onEndReached={this.onScrollEnd}
                            keyExtractor={(item, index) => item.transaction_id}
                        />
                    </View>
                </View>
            </Fragment>
        );
    };

    render() {
        const { transactions } = this.props;
        const { showLoader } = this.state;
        return (
            <View
                style={[
                    globalStyles.flexContainer,
                    globalStyles.mainContainer,
                    styles.container
                ]}
            >
                <View>
                    <Text
                        style={[styles.headerText, globalStyles.highlightColor]}
                    >
                        Wallet
                    </Text>
                    <Text style={styles.descriptionText}>
                        View all the recent transactions and funds updates in
                        your account.
                    </Text>
                </View>
                {this.renderTransactionTypes(transactions)}
                {showLoader ||
                transactions.status == 'refreshing' ||
                transactions.status == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
