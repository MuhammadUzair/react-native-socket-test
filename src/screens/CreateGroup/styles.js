import { StyleSheet } from 'react-native';
import { os } from '../../utils';

export default StyleSheet.create({
    heading: {
        fontSize: 24
    },

    postGameCard: {
        height: 50,
        width: '100%',
        position: 'absolute',
        top: 40,
        marginLeft: os() ? 20 : 10
    },

    borderRadiusTop: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },

    bigPrimaryButtonText: {
        fontSize: 18
    },

    bigPrimaryButton: {
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },

    borderRadius: {
        borderRadius: 10
    },

    rowContainer: {
        paddingVertical: os() ? 18 : 12,
        paddingHorizontal: os() ? 20 : 10
    },

    bottomSpacing: {
        marginBottom: 12
    },

    marginTop: {
        marginTop: 10
    },

    inputText: {
        width: 70,
        height: '100%',
        padding: 12,
        paddingBottom: 0,
        paddingTop: 0,
        backgroundColor: '#EEE',
        color: 'black',
        marginLeft: -20,
        position: 'relative',
        zIndex: 0
    },

    cancelCenterButton: {
        marginTop: 25,
        fontSize: 15
    }
});
