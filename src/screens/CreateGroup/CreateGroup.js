import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import { globalStyles } from '../../assets/styles';
import GroupCreateStyles from './styles';

export default class CreateGroup extends Component {
    state = {
        namePH: 'Enter Group Name',
        descriptionPH: 'Enter Group Description',
        name: '',
        description: '',
        privacy: 'Public'
    };

    checkConstraints = () => {
        const { name, description } = this.state;
        if (!name.length && !description.length) {
            Alert.alert('Error', 'Group name and description is required');
            return false;
        }
        return true;
    };

    onCancelBtnClick = () => {
        // this.props.navigation.popToTop();
        this.props.navigation.goBack();
    };

    onNextBtnClick = () => {
        if (this.checkConstraints()) {
            const { privacy, name, description } = this.state;
            this.props.navigation.push('GroupScreenListing', {
                privacy,
                name,
                description
            });
        }
    };

    onSelect = (index, value) => {
        this.setState({ privacy: value });
    };
    render() {
        return (
            <View
                style={[
                    globalStyles.mainContainer,
                    GroupCreateStyles.rowContainer
                ]}
            >
                <View
                    style={[
                        globalStyles.whiteBackgroudColor,
                        GroupCreateStyles.borderRadiusTop,
                        GroupCreateStyles.postGameCard
                    ]}
                />
                <View
                    activeOpacity={0.5}
                    style={[
                        GroupCreateStyles.bottomSpacing,
                        globalStyles.flexHorizontal,
                        globalStyles.flexVCenter
                    ]}
                >
                    <LinearGradient
                        colors={['#4d6bc2', '#3695e3']}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexHCenter,
                            GroupCreateStyles.bigPrimaryButton
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.fontFace,
                                globalStyles.whiteColor,
                                GroupCreateStyles.bigPrimaryButtonText
                            ]}
                        >
                            Create Group{' '}
                        </Text>
                    </LinearGradient>
                </View>

                <View
                    style={[
                        globalStyles.whiteBackgroudColor,
                        GroupCreateStyles.borderRadius
                    ]}
                >
                    <View
                        style={[
                            GroupCreateStyles.rowContainer,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            Name
                        </Text>
                        <TextInput
                            style={[globalStyles.hairLineUnderBorder]}
                            onChangeText={name => this.setState({ name })}
                            value={this.state.name}
                            placeholder={this.state.namePH}
                        />
                    </View>

                    <View
                        style={[
                            GroupCreateStyles.rowContainer,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            Description
                        </Text>
                        <TextInput
                            style={[globalStyles.hairLineUnderBorder]}
                            onChangeText={description =>
                                this.setState({ description })
                            }
                            placeholder={this.state.descriptionPH}
                            value={this.state.description}
                        />
                    </View>

                    <View
                        style={[
                            GroupCreateStyles.rowContainer,
                            globalStyles.hairLineUnderBorder
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.boldFontFace,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            Privacy{' '}
                        </Text>
                        <View>
                            <RadioGroup
                                selectedIndex={0}
                                onSelect={this.onSelect}
                            >
                                <RadioButton
                                    value="Public"
                                    style={[globalStyles.hairLineUnderBorder]}
                                >
                                    <Text style={[globalStyles.blackColor]}>
                                        Public
                                    </Text>
                                </RadioButton>
                                <RadioButton
                                    value="Private"
                                    style={[globalStyles.hairLineUnderBorder]}
                                >
                                    <Text style={[globalStyles.blackColor]}>
                                        Private
                                    </Text>
                                </RadioButton>
                            </RadioGroup>
                        </View>
                    </View>
                </View>
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        GroupCreateStyles.bottomSpacing
                    ]}
                >
                    <View>
                        <TouchableOpacity onPress={this.onCancelBtnClick}>
                            <Text
                                style={[
                                    globalStyles.boldFontFace,
                                    globalStyles.highlightPinkColor,
                                    GroupCreateStyles.cancelCenterButton
                                ]}
                            >
                                Cancel Post
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={this.onNextBtnClick}
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHCenter,
                                GroupCreateStyles.bottomSpacing,
                                GroupCreateStyles.marginTop
                            ]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    GroupCreateStyles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        { width: '50%' },
                                        globalStyles.whiteColor,
                                        GroupCreateStyles.bigPrimaryButtonText
                                    ]}
                                >
                                    Next
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
