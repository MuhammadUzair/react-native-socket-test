import { connect } from 'react-redux';
import PaymentDetails from './PaymentDetails';

import {
    playCompetition,
    resetPlayCompetition
} from '../../redux/epics/play-competition-epic';
import { fetchUser } from '../../redux/epics/fetch-user-epic';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ competitionPlayed }) => ({
    competitionPlayed
});

const mapDispatchToProps = dispatch => ({
    playCompetition: data => dispatch(playCompetition(data)),
    fetchUser: data => dispatch(fetchUser(data)),
    resetPlayCompetition: () => dispatch(resetPlayCompetition())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PaymentDetails);
