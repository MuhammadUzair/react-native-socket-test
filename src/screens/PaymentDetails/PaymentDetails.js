import React, { Component } from 'react';
import { verticalScale } from 'react-native-size-matters';

import {
    os,
    retrieveToken,
    runLocationValidations,
    getLocation
} from '../../utils';
import HeaderBackButton from '../../components/HeaderBackButton';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import ConfirmationScreen from '../../components/ConfirmationScreen';
export default class PaymentDetails extends Component {
    state = { isSubmitButton: false };

    static navigationOptions = args => {
        const { navigation, navigationOptions } = args;
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                height: verticalScale(40),
                paddingLeft: os() ? 10 : 15,
                backgroundColor: '#E9E9EF'
            },
            headerTitle: null,
            headerRight: null,
            headerLeft: (
                <HeaderBackButton
                    type="close"
                    onPress={() => navigation.dangerouslyGetParent().goBack()}
                />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    componentDidMount() {
        retrieveToken().then(token => this.setState({ token }));
    }

    componentDidUpdate(prevProps, prevState) {
        const { competitionPlayed, fetchUser, navigation } = this.props;
        const { token, isSubmitButton } = this.state;
        // generateAndPrintErrorMessage(competitionPlayed);
        // not alerting on error becuase the previous screen i.e.
        // PlayIt.js is not being unmounted
        if (competitionPlayed.code >= 200 && competitionPlayed.code < 300) {
            fetchUser(token);
            navigation.popToTop();
            navigation.navigate('MyCompetitions');
        }
        if (competitionPlayed.code && isSubmitButton) {
            this.setState({ isSubmitButton: true });
        }
    }

    dispatchPlayAction = () => {
        getLocation(({ coords: { latitude, longitude, altitude } }) => {
            const {
                navigation: {
                    state: {
                        params: { item, enteredAmount }
                    }
                },
                playCompetition,
                resetPlayCompetition
            } = this.props;
            const { token } = this.state;
            this.setState({ isSubmitButton: true }, () => {
                resetPlayCompetition();
                playCompetition({
                    latitude,
                    longitude,
                    altitude,
                    token,
                    amount: enteredAmount,
                    competition_id: item.id,
                    sport_cid: item.sport_cid,
                    owner_id: item.owner_id
                });
            });
        });
    };

    onPlayGameBtnClick = () => {
        runLocationValidations(this.dispatchPlayAction);
    };

    render() {
        const {
            navigation: {
                state: {
                    params: {
                        item,
                        subText,
                        totalAmount,
                        enteredAmount,
                        serviceFee,
                        serviceFeeAmount,
                        spreadTitle,
                        teamStatus
                    }
                }
            }
        } = this.props;
        return (
            <ConfirmationScreen
                title="Play Competition"
                navigation={this.props.navigation}
                onPressBackButton={() => this.props.navigation.goBack()}
                onPressSubmit={this.onPlayGameBtnClick}
                isSubmitBtnPressed={this.state.isSubmitButton}
                item={item}
                subText={subText}
                totalAmount={totalAmount}
                enteredAmount={enteredAmount}
                serviceFee={serviceFee}
                serviceFeeAmount={serviceFeeAmount}
                spreadTitle={spreadTitle}
                isDashBoard
                teamStatus={teamStatus}
            />
        );
    }
}
