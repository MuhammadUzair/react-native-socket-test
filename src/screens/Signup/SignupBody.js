import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableWithoutFeedback,
    Linking,
    TextInput,
    SafeAreaView,
    Alert,
    TouchableOpacity,
    Platform
} from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import validator from 'validator';
import DateTimePicker from 'react-native-modal-datetime-picker';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';

import { globalStyles, baseGrey } from '../../assets/styles';
import styles from './styles';
import {
    validateDOB,
    loginFB,
    loginGoogle,
    onboard,
    retrieveNotificationTokenAndOS,
    getLocation,
    runLocationValidations
} from '../../utils';

import { signupUserAction, reset } from '../../redux/epics/signup-epic';

import OverlayIndicator from '../../components/OverlayIndicator';
import alertOnError from '../../hoc/alertOnError';

class SignupBody extends Component {
    state = {
        checked: false,
        username: '',
        email: '',
        password: '',
        name: '',
        code: '',
        dob: 'Date of birth',
        showDatePicker: false,
        device_token: '',
        device_type: Platform.OS == 'android' ? 'android' : 'ios',
        device_id: ''
    };

    componentDidUpdate(prevProps) {
        const { user } = this.props;
        if (user.code === 201 || user.code === 200) {
            onboard(user.data, this.props.navigation);
        }
    }
    componentDidMount() {
        let device_id = DeviceInfo.getUniqueID();
        retrieveNotificationTokenAndOS().then(res => {
            if (res && res.token && res.os) {
                this.setState({
                    device_token: res.token,
                    device_type: res.os,
                    device_id: device_id
                });
            }
        });
    }

    showDatePicker = () => this.setState({ showDatePicker: true });
    hideDatePicker = () => this.setState({ showDatePicker: false });
    dateSelected = date => {
        this.setState({
            dob: `${date.getMonth() +
                1}-${date.getDate()}-${date.getFullYear()}`
        });
        this.hideDatePicker();
    };

    onPressCheck = () => {
        this.setState({ checked: !this.state.checked });
    };

    onPressLogin = () => {
        const { navigation } = this.props;
        navigation.navigate('Login');
    };

    geolocationSuccess = ({
        coords: { longitude, latitude, altitude } = {
            longitude: null,
            latitude: null,
            altitude: null
        }
    }) => {
        const {
            checked,
            username,
            email,
            password,
            name,
            code: referral_code,
            dob: date_of_birth,
            device_token,
            device_type,
            device_id
        } = this.state;
        const { signupUser } = this.props;
        const data = {
            checked,
            username,
            email,
            password,
            password_confirmation: password,
            name,
            referral_code,
            date_of_birth,
            longitude,
            latitude,
            altitude,
            device_token,
            device_type,
            device_id,
            requestFrom: 'Mobile'
        };
        signupUser(data);
    };

    dispatchSignupAction = () => {
        getLocation(this.geolocationSuccess, this.geolocationSuccess);
    };

    onPressSignup = async () => {
        const {
            checked,
            username,
            email,
            password,
            name,
            code: referral_code,
            dob: date_of_birth
        } = this.state;

        try {
            if (!name.trim().length)
                throw new Error('Please enter your full name');
            if (!/[A-Za-z ]+/.test(name.trim()))
                throw new Error('Please enter your correct name');
            if (username.trim().length < 4)
                throw new Error(
                    'Username must be of 4 characters atleast\nNote: Username cannot contain any spaces'
                );
            if (!/[ A-Za-z0-9\.]+/.test(username.trim()))
                throw new Error(
                    "Enter a correct username.\nNote: Username cannot contain any special characters except '.'"
                );
            if (!validator.isEmail(email))
                throw new Error('Enter correct email address.');
            if (!password.length <= 16 && !password.length >= 8)
                throw new Error(
                    'Password should atleast be of 8 characters and maximum of 16 characters.'
                );
            if (!validateDOB(date_of_birth))
                throw new Error('Please enter your date of birth');
            if (!checked)
                throw new Error('You must agree to the Terms and Conditions.');

            runLocationValidations(this.dispatchSignupAction, true);
        } catch (e) {
            Alert.alert('', e.message);
        }
    };

    openLink = () => {
        Linking.openURL('https://beta.postitplayit.com/terms');
    };

    render() {
        return (
            <KeyboardAwareScrollView>
                <SafeAreaView>
                    <View style={[styles.body, globalStyles.mainContainer]}>
                        <View style={styles.header}>
                            <TouchableOpacity
                                onPress={this.onPressLogin}
                                style={styles.headerButton}
                            >
                                <Text style={[styles.btnText]}>Login Now</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.headerText}>Signup</Text>
                        <Text style={styles.descriptionText}>
                            {
                                'Here, you can create your own contests and face off \nagainst friends, family and the entire planet.'
                            }
                        </Text>
                        <View
                            style={[
                                styles.btnWrapper,
                                globalStyles.flexHorizontal
                            ]}
                        >
                            <TouchableOpacity
                                onPress={() =>
                                    loginGoogle({
                                        device_token: this.state.device_token,
                                        device_type: this.state.device_type,
                                        device_id: this.state.device_id
                                    })
                                }
                                activeOpacity={0.5}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    styles.secondaryButton
                                ]}
                            >
                                <Icon
                                    name="google"
                                    type="font-awesome"
                                    color="#e6452d"
                                    containerStyle={{ marginRight: 5 }}
                                />
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.baseFontSize
                                    ]}
                                >
                                    Signup With Google
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={0.5}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    styles.secondaryButton
                                ]}
                                onPress={() => {
                                    loginFB({
                                        device_token: this.state.device_token,
                                        device_type: this.state.device_type,
                                        device_id: this.state.device_id
                                    });
                                }}
                            >
                                <Icon
                                    name="facebook-f"
                                    type="font-awesome"
                                    color="#3695e3"
                                    containerStyle={{ marginRight: 5 }}
                                />
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.baseFontSize
                                    ]}
                                >
                                    Signup With Facebook
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.orTxt}>or</Text>
                        <View
                            style={[
                                styles.inputWrapper,
                                globalStyles.highlightBorder
                            ]}
                        >
                            <TextInput
                                placeholder="Your full name"
                                placeholderTextColor={baseGrey}
                                clearButtonMode="always"
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                value={this.state.name}
                                onChangeText={name => this.setState({ name })}
                            />
                        </View>
                        <View
                            style={[
                                styles.inputWrapper,
                                globalStyles.highlightBorder
                            ]}
                        >
                            <TextInput
                                placeholder="Enter your email"
                                placeholderTextColor={baseGrey}
                                keyboardType="email-address"
                                clearButtonMode="always"
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                value={this.state.email}
                                onChangeText={email => this.setState({ email })}
                            />
                        </View>
                        <View
                            style={[
                                styles.inputWrapper,
                                globalStyles.highlightBorder
                            ]}
                        >
                            <TextInput
                                placeholder="Username"
                                placeholderTextColor={baseGrey}
                                clearButtonMode="always"
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                value={this.state.username}
                                onChangeText={username =>
                                    this.setState({ username })
                                }
                            />
                        </View>
                        <View
                            style={[
                                styles.inputWrapper,
                                globalStyles.highlightBorder,
                                globalStyles.flexVCenter
                            ]}
                        >
                            <TextInput
                                secureTextEntry={true}
                                placeholder="Password"
                                placeholderTextColor={baseGrey}
                                clearButtonMode="always"
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                value={this.state.password}
                                onChangeText={password =>
                                    this.setState({ password })
                                }
                            />
                        </View>
                        <View
                            style={[
                                styles.horizontalInputWrapper,
                                globalStyles.flexHorizontal
                            ]}
                        >
                            <View
                                style={[
                                    styles.inputWrapper,
                                    styles.horizontalInputHolder,
                                    globalStyles.highlightBorder,
                                    globalStyles.flexVCenter
                                ]}
                            >
                                <TextInput
                                    placeholder="Referal Code"
                                    placeholderTextColor={baseGrey}
                                    clearButtonMode="always"
                                    style={styles.input}
                                    underlineColorAndroid="transparent"
                                    value={this.state.code}
                                    onChangeText={code =>
                                        this.setState({ code })
                                    }
                                />
                            </View>
                            <TouchableOpacity
                                activeOpacity={0.5}
                                style={[
                                    styles.inputWrapper,
                                    styles.horizontalInputHolder,
                                    globalStyles.highlightBorder,
                                    globalStyles.flexVCenter
                                ]}
                                onPress={this.showDatePicker}
                            >
                                <Text style={{ color: '#ddd', fontSize: 10 }}>
                                    {this.state.dob}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.checkboxHolder}>
                            <CheckBox
                                title="By signing up, you agree to our"
                                component={TouchableWithoutFeedback}
                                checked={this.state.checked}
                                checkedColor="#88c3ff"
                                containerStyle={styles.checkbox}
                                textStyle={[
                                    globalStyles.fontFace,
                                    globalStyles.baseFontSize,
                                    styles.checkboxTxt
                                ]}
                                onPress={this.onPressCheck}
                                size={20}
                            />
                            <View
                                style={{
                                    borderBottomWidth: 1,
                                    borderColor: 'white',
                                    marginLeft: -8
                                }}
                            >
                                <TouchableWithoutFeedback
                                    onPress={this.openLink}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.baseFontSize,
                                            styles.checkboxTxt
                                        ]}
                                    >
                                        Terms and Conditions
                                    </Text>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={this.onPressSignup}
                            activeOpacity={0.5}
                            style={[globalStyles.flexHCenter]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        styles.bigPrimaryButtonText
                                    ]}
                                >
                                    Signup Now
                                </Text>
                                <Icon
                                    size={20}
                                    color="#FFF"
                                    name="arrow-forward"
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                    <DateTimePicker
                        isVisible={this.state.showDatePicker}
                        onConfirm={this.dateSelected}
                        onCancel={this.hideDatePicker}
                    />
                </SafeAreaView>
                {this.props.user === 'inprogress' ? <OverlayIndicator /> : null}
            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = ({ user }) => ({
    user
});

const mapDispatchToProps = dispatch => ({
    signupUser: data => dispatch(signupUserAction(data)),
    resetSignupState: () => dispatch(reset())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(SignupBody, [
        { propName: 'user', actionOnFailure: 'resetSignupState' }
    ])
);
