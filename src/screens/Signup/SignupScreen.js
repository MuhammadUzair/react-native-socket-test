import React, { Component } from 'react';
import { View, ImageBackground, Platform } from 'react-native';

import { globalStyles } from '../../assets/styles';
import SignupBody from './SignupBody';

export default class SignupScreen extends Component {
    render() {
        return (
            <View style={[globalStyles.flexContainer]}>
                <ImageBackground
                    source={require('../../assets/images/signup.png')}
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.flexHCenter,
                        globalStyles.flexVBottom,
                        globalStyles.fullScreen
                    ]}
                >
                    <SignupBody navigation={this.props.navigation} />
                </ImageBackground>
            </View>
        );
    }
}
