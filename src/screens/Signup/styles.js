import { StyleSheet, Dimensions } from 'react-native';
import { verticalScale, scale } from 'react-native-size-matters';

export default StyleSheet.create({
    body: {
        // paddingTop: 20,
        width: Dimensions.get('window').width,
    },
    header: {
        height: verticalScale(40),
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    headerButton: {
        height: '100%',
        paddingHorizontal: verticalScale(5),
        justifyContent: 'flex-end',
    },
    btnText: {
        color: 'white',
    },
    headerText: {
        fontSize: 32,
        fontWeight: 'bold',
        marginBottom: verticalScale(2.5),
        color: 'white',
    },
    descriptionText: {
        fontSize: 12,
        color: 'white',
        marginBottom: verticalScale(10),
    },
    btnWrapper: {
        flex: 1,
        marginBottom: verticalScale(10),
        justifyContent: 'space-between',
    },
    secondaryButton: {
        borderRadius: 50,
        paddingVertical: 8,
        paddingHorizontal: 8,
        backgroundColor: 'white',
    },
    orTxt: {
        color: '#ddd',
        textAlign: 'center',
        marginBottom: verticalScale(10),
    },
    inputWrapper: {
        width: '100%',
        borderRadius: 50,
        borderWidth: StyleSheet.hairlineWidth,

        paddingHorizontal: 10,
        marginBottom: verticalScale(12),
    },
    input: {
        color: '#ddd',
        fontSize: 10,
        height: verticalScale(50),
    },
    horizontalInputWrapper: {
        justifyContent: 'space-between',
    },
    horizontalInputHolder: {
        width: '48%',
    },
    checkbox: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        marginBottom: 10,
        padding: 0,
        marginRight: 0,
        paddingRight: 0,
        // width: '50%',
    },
    checkboxTxt: {
        color: '#fff',
        fontSize: 12,
    },
    checkboxHolder: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },
    bigPrimaryButton: {
        paddingVertical: 14,
        paddingHorizontal: 25,
        borderRadius: 50,
        flexDirection: 'row',
        width: 60 + '%',
    },

    bigPrimaryButtonText: {
        fontSize: 14,
    },
});
