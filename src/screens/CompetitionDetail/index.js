import CompetitionDetail from './CompetitionDetail';
import { connect } from 'react-redux';
import {
    fetchSpecificCompetition,
    resetFetchSpecificCompetition
} from '../../redux/epics/fetch-specific-competition-epic';

const mapStateToProps = ({ searchCompetition }) => ({ searchCompetition });

const mapDispatchToProps = dispatch => ({
    fetchSpecificCompetition: data => dispatch(fetchSpecificCompetition(data)),
    resetFetchSpecificCompetition: data =>
        dispatch(resetFetchSpecificCompetition(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CompetitionDetail);
