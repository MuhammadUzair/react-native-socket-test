import React, { Component, Fragment } from 'react';
import { View, Text, FlatList, ScrollView } from 'react-native';
import { Icon } from 'react-native-elements';
import { globalStyles } from '../../assets/styles';
import styles from './styles';

import BlueBadge from '../../components/BlueBadge';
import CustomAvatar from '../../components/CustomAvatar';
import CompetitionDetailUserCard from '../../components/CompetitionDetailUserCard';
import HeaderBackButton from '../../components/HeaderBackButton';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import OverlayIndicator from '../../components/OverlayIndicator';
import BlueHeader from '../../components/BlueHeader';

import {
    getDate,
    getTime,
    renderSpreadWithPlusSign,
    renderSubText,
    os,
    retrieveToken,
    getTimeZone
} from '../../utils';

export default class CompetitionDetail extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingLeft: os() ? 10 : 15
            },
            headerLeft: (
                <HeaderBackButton
                    // onPress={() => navigation.goBack()}
                    onPress={() => {
                        if (navigation.state.params.notification) {
                            navigation.popToTop();
                            navigation.navigate('NotificationScreen', {
                                notification: true
                            });
                        } else if (navigation.state.params.isDashboard) {
                            navigation.popToTop();
                            navigation.navigate('DashboardScreen');
                        } else {
                            navigation.goBack();
                        }
                    }}
                />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    state = {
        competitionDetail: [],
        showLoader: true
    };

    componentDidMount = async (prevProps, prevState) => {
        if (
            this.props.navigation.state.params &&
            this.props.navigation.state.params.item
        ) {
            this.setState({
                competitionDetail: this.props.navigation.state.params.item,
                showLoader: false
            });
        } else {
            this.props.resetFetchSpecificCompetition();
            let token = await retrieveToken();
            let id =
                this.props.navigation.state.params &&
                this.props.navigation.state.params.competitionId
                    ? this.props.navigation.state.params.competitionId
                    : 0;
            this.props.fetchSpecificCompetition({
                token,
                competitionId: id
            });
        }
    };
    static getDerivedStateFromProps(newProps, prevState) {
        let competitionDetail = prevState.competitionDetail;
        let showLoader = prevState.showLoader;

        if (
            newProps.searchCompetition.data &&
            newProps.searchCompetition.data.competitions &&
            newProps.searchCompetition.data.competitions.length > 0
        ) {
            competitionDetail = newProps.searchCompetition.data.competitions[0];
            showLoader = false;
        }
        if (
            newProps.searchCompetition.statue == 'successful' ||
            newProps.searchCompetition.statue == 'failed'
        ) {
            showLoader = false;
        }

        return { competitionDetail, showLoader };
    }

    renderCard = content => {
        if (content.index > 0) {
            return (
                <CompetitionDetailUserCard
                    item={content.item}
                    index={content.index}
                />
            );
        }
        return null;
    };

    renderParticipants = item => {
        return (
            <Fragment>
                <View
                    style={[
                        globalStyles.flexContainer,
                        styles.cardContainer,
                        { height: '150%' }
                    ]}
                >
                    <FlatList
                        data={
                            item && item.participants ? item.participants : []
                        }
                        renderItem={this.renderCard}
                        keyExtractor={(item, index) =>
                            `id:${item.participant_id}, index: ${index}`
                        }
                    />
                </View>
            </Fragment>
        );
    };

    render() {
        let item = this.state.competitionDetail;
        let status = item.status;
        if (status) {
            const {
                navigation,
                navigation: {
                    state: {
                        params: { teamStatus }
                    }
                }
            } = this.props;
            return (
                <Fragment>
                    <BlueHeader
                        title="Competition Detail"
                        navigation={this.props.navigation}
                        onPress={() => {
                            if (navigation.state.params.notification) {
                                navigation.popToTop();
                                navigation.navigate('NotificationScreen', {
                                    notification: true
                                });
                            } else if (navigation.state.params.isDashboard) {
                                navigation.popToTop();
                                navigation.navigate('DashboardScreen');
                            } else {
                                navigation.goBack();
                            }
                        }}
                    />
                    <ScrollView>
                        <View style={styles.cardPadding}>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.competitionIdpadding
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.baseBlueColor,
                                        globalStyles.baseFontSize
                                    ]}
                                >
                                    Competition ID #{' '}
                                    {item && item.id ? item.id : ''}
                                </Text>
                            </View>

                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexContainer
                                ]}
                            >
                                <View
                                    style={[
                                        globalStyles.flexVTop,
                                        status == 'Settled'
                                            ? styles.settledleftContent
                                            : styles.leftContent
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.greyColor,
                                            globalStyles.subTextFontSize
                                        ]}
                                    >
                                        Game
                                    </Text>
                                </View>
                                {status == 'Settled' ? (
                                    <View style={styles.centerContent}>
                                        <Text
                                            style={[
                                                globalStyles.greyColor,
                                                globalStyles.subTextFontSize
                                            ]}
                                        >
                                            Score
                                        </Text>
                                    </View>
                                ) : null}
                                <View>
                                    <Text
                                        style={[
                                            globalStyles.greyColor,
                                            globalStyles.subTextFontSize
                                        ]}
                                    >
                                        Amount
                                    </Text>
                                </View>
                            </View>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexContainer,
                                    styles.marginTopFive
                                ]}
                            >
                                <View
                                    style={[
                                        globalStyles.flexVTop,
                                        status == 'Settled'
                                            ? styles.settledleftContent
                                            : styles.leftContent
                                    ]}
                                    opacity={!teamStatus.awayTeam ? 0.5 : 1}
                                >
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            status == 'Settled' &&
                                                !teamStatus.awaySpread &&
                                                styles.paddingLeftTen
                                        ]}
                                    >
                                        {teamStatus.awayTeam &&
                                            status == 'Settled' && (
                                                <Icon
                                                    type="FontAwesome"
                                                    name="check"
                                                    iconStyle={styles.checkIcon}
                                                />
                                            )}

                                        <CustomAvatar
                                            medium
                                            rounded
                                            uri={
                                                item &&
                                                item.awayTeam &&
                                                item.awayTeam &&
                                                item.awayTeam.avatar
                                                    ? item.awayTeam.avatar
                                                    : ''
                                            }
                                            title={
                                                item &&
                                                item.awayTeam &&
                                                item.awayTeam.short_code
                                                    ? item.awayTeam.short_code
                                                    : ''
                                            }
                                        />
                                        <Text
                                            style={[
                                                globalStyles.greyColor,
                                                globalStyles.subTextFontSize,
                                                styles.myTeamName,
                                                status == 'Settled'
                                                    ? teamStatus.awayTeam
                                                        ? styles.settledAcvtiveTeamNameWidth
                                                        : styles.settledTeamNameWidth
                                                    : null
                                            ]}
                                        >
                                            {item &&
                                            item.awayTeam &&
                                            item.awayTeam.name
                                                ? item.awayTeam.name + ' '
                                                : null}

                                            {teamStatus.awayTeam &&
                                                ' ' +
                                                    renderSpreadWithPlusSign(
                                                        item.participants[1]
                                                            .pivot.spread
                                                    )}
                                        </Text>
                                    </View>
                                </View>
                                {status == 'Settled' ? (
                                    <View style={styles.centerContent}>
                                        <Text
                                            style={[
                                                globalStyles.baseFontSize,
                                                styles.amountSectionTextPadding
                                            ]}
                                        >
                                            {item.detail.payload.awaySpread}
                                        </Text>
                                    </View>
                                ) : null}
                                <Text
                                    style={[
                                        globalStyles.paleRedColor,
                                        globalStyles.subTextFontSize,
                                        styles.amountSectionTextPadding
                                    ]}
                                >
                                    $
                                    {/* {item.status === 'Settled'
                                        ? item.result_amount
                                        : `${item.bidding_amount} / $${
                                              item.amount
                                          }`} */}
                                    {`${item.bidding_amount} / $${item.amount}`}
                                </Text>
                            </View>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexContainer,
                                    styles.marginTopFive
                                ]}
                            >
                                <View
                                    style={[
                                        globalStyles.flexVTop,
                                        status == 'Settled'
                                            ? styles.settledleftContent
                                            : styles.leftContent
                                    ]}
                                    opacity={!teamStatus.homeTeam ? 0.5 : 1}
                                >
                                    <View
                                        style={[
                                            globalStyles.flexHorizontal,
                                            status == 'Settled' &&
                                                !teamStatus.homeTeam &&
                                                styles.paddingLeftTen
                                        ]}
                                    >
                                        {teamStatus.homeTeam &&
                                            status == 'Settled' && (
                                                <Icon
                                                    type="FontAwesome"
                                                    name="check"
                                                    iconStyle={styles.checkIcon}
                                                />
                                            )}
                                        <CustomAvatar
                                            medium
                                            rounded
                                            uri={
                                                item &&
                                                item.homeTeam &&
                                                item.homeTeam.avatar
                                                    ? item.homeTeam.avatar
                                                    : ''
                                            }
                                            title={
                                                item &&
                                                item.my_team &&
                                                item[item.my_team] &&
                                                item[item.my_team].short_code
                                                    ? item[item.my_team]
                                                          .short_code
                                                    : ''
                                            }
                                            overlayContainerStyle={{
                                                backgroudColor: 'red'
                                            }}
                                        />
                                        <Text
                                            style={[
                                                globalStyles.greyColor,
                                                globalStyles.subTextFontSize,
                                                styles.myTeamName,
                                                styles.marginLeft,
                                                status == 'Settled'
                                                    ? teamStatus.homeTeam
                                                        ? styles.settledAcvtiveTeamNameWidth
                                                        : styles.settledTeamNameWidth
                                                    : null
                                            ]}
                                        >
                                            {item &&
                                            item.homeTeam &&
                                            item.homeTeam.name
                                                ? item.homeTeam.name
                                                : null}

                                            {teamStatus.homeTeam &&
                                                ' ' +
                                                    renderSpreadWithPlusSign(
                                                        item.participants[0]
                                                            .pivot.spread
                                                    )}
                                        </Text>
                                    </View>
                                </View>
                                {status == 'Settled' ? (
                                    <View style={styles.centerContent}>
                                        <Text
                                            style={[
                                                globalStyles.baseFontSize,
                                                styles.amountSectionTextPadding
                                            ]}
                                        >
                                            {item.detail.payload.homeSpread}
                                        </Text>
                                    </View>
                                ) : null}
                                <Text
                                    style={[
                                        globalStyles.baseBlueColor,
                                        globalStyles.subTextFontSize,
                                        status == 'Settled'
                                            ? styles.sattledSpreadTextWidth
                                            : styles.spreadTextWidth,
                                        styles.amountSectionTextPadding
                                    ]}
                                >
                                    {item && item.my_team == 'competitor'
                                        ? item.participants[1].spread_text
                                        : item.participants[1].spread_text}
                                </Text>
                            </View>
                            <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.competitionIdpadding
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.greyColor,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    {getDate(item.start_time) +
                                        ' ' +
                                        getTime(item.start_time) +
                                        ' ' +
                                        getTimeZone(item.start_time)}
                                </Text>
                            </View>
                        </View>

                        {/* Owner - start  */}
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.flexSpaceBetween,
                                globalStyles.greyBackgroudColor,
                                styles.paddedRow
                            ]}
                        >
                            <View
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexHorizontal,
                                    styles.ownerRowWidth
                                ]}
                            >
                                <CustomAvatar
                                    small
                                    rounded
                                    containerStyle={{ marginRight: 10 }}
                                    uri={
                                        item &&
                                        item.owner &&
                                        item.owner.avatar &&
                                        item.owner.avatar
                                    }
                                />
                                <View>
                                    <Text
                                        style={[
                                            globalStyles.blackColor,
                                            globalStyles.boldFontFace,
                                            globalStyles.baseFontSize
                                        ]}
                                    >
                                        {item &&
                                            item.owner &&
                                            item.owner.name &&
                                            item.owner.name}
                                    </Text>
                                    {item &&
                                    item.team &&
                                    item.team.short_code &&
                                    typeof item.spread == 'number' ? (
                                        <Text
                                            style={[
                                                globalStyles.baseBlueColor,
                                                globalStyles.subTextFontSize
                                            ]}
                                        >
                                            {item.team.name}{' '}
                                            {item.is_owner
                                                ? renderSubText(
                                                      item.spread,
                                                      true
                                                  )
                                                : renderSubText(
                                                      item.spread,
                                                      false
                                                  )}
                                        </Text>
                                    ) : null}

                                    <Text style={globalStyles.subTextFontSize}>
                                        {getDate(
                                            item.participants[0].pivot
                                                .created_at
                                        ) +
                                            ' ' +
                                            getTime(
                                                item.participants[0].pivot
                                                    .created_at
                                            ) +
                                            ' ' +
                                            getTimeZone(
                                                item.participants[0].pivot
                                                    .created_at
                                            )}
                                    </Text>
                                </View>
                            </View>
                            <View>
                                <BlueBadge
                                    textStyle={[
                                        globalStyles.baseFontSize,
                                        styles.badgeText
                                    ]}
                                    text="Owner"
                                />

                                <Text
                                    style={[
                                        styles.ownerAmount,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    {item.amount && '$ ' + item.amount}
                                </Text>
                            </View>
                        </View>
                        {this.renderParticipants(item)}

                        {this.state.showLoader && <OverlayIndicator />}

                        {/* Owner - end */}
                    </ScrollView>
                </Fragment>
            );
        }
        return null;
    }
}
