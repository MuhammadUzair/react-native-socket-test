import { StyleSheet, Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    accountTop: {
        top: -20
    },
    infoContainer: {
        borderRadius: 5,
        backgroundColor: 'white',
        paddingTop: 20,
        overflow: 'hidden'
    },
    paddedRow: {
        paddingVertical: verticalScale(8),
        paddingHorizontal: 10
    },
    card: {
        paddingHorizontal: 10,
        paddingVertical: verticalScale(10),
        marginVertical: verticalScale(5)
    },
    cardContainer: {
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: 'white'
    },
    cardPadding: {
        paddingRight: verticalScale(15),
        paddingLeft: verticalScale(15),
        backgroundColor: 'white'
    },
    competitionIdpadding: {
        paddingTop: 20,
        paddingBottom: 10
    },
    leftContent: {
        width: width * 0.5,
        marginRight: 10
    },
    settledleftContent: {
        width: width * 0.4,
        marginRight: 10
    },
    centerContent: {
        width: width * 0.15,
        marginRight: 10
    },
    marginTopFive: {
        marginTop: 5
    },
    marginTopTen: {
        marginTop: verticalScale(10)
    },
    myTeamName: {
        marginTop: verticalScale(10),
        textAlignVertical: 'center',
        width: width * 0.36,
        alignSelf: 'center'
    },
    settledAcvtiveTeamNameWidth: {
        width: width * 0.2
    },
    settledTeamNameWidth: {
        width: width * 0.21
    },
    checkIcon: {
        fontSize: scale(20),
        color: '#4879d5',
        paddingRight: 5
    },
    textAlignVerticalCenter: {
        textAlignVertical: 'center'
    },
    textAlignVerticalCenter: {
        textAlignVertical: 'center'
    },
    spreadTextWidth: {
        width: '45%'
    },
    sattledSpreadTextWidth: {
        width: '30%'
    },
    marginLeft: {
        marginLeft: 5
    },
    paddingLeftTen: {
        paddingLeft: width * 0.07
    },
    ownerAmount: {
        marginTop: 20,
        textAlign: 'right',
        paddingRight: 10
    },
    badgeText: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        textAlign: 'center'
    },
    ownerRowWidth: {
        width: width * 0.6
    },
    paddingLeft: {
        paddingLeft: 10
    },
    amountSectionTextPadding: {
        paddingTop: verticalScale(20)
    }
});
