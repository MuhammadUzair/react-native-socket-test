import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    FlatList,
    RefreshControl,
    Alert
} from 'react-native';

import SmallCards from '../../components/SmallCards';
import OverlayIndicator from '../../components/OverlayIndicator';
import PendingRequest from '../../components/PendingRequest';
import SentRequest from '../../components/SentRequest';
import EmptyListIndicator from '../../components/EmptyListIndicator';

import { globalStyles } from '../../assets/styles';
import styles from './styles';

export default class MyFriendsPendingRequestListing extends Component {
    state = {
        cards: [
            { text: 'Received Requests', state: true },
            { text: 'Sent Requests', state: false }
        ],

        check: [false, false, false, false, false, false, false, false],
        token: this.props.navigation.getParam('token'),
        receivedRequests: [],
        sentRequests: [],
        isRefreshingReceivedRequests: false,
        isRefreshingSentRequests: false,
        pageReceivedRequests: 1,
        pageSentRequests: 1,
        callReceiveApi: true,
        callSentApi: true,
        showError: true,
        showLoader: false,
        cancelRequestFriendId: 0
    };

    componentDidMount() {
        let token = this.state.token;
        this.props.fetchFriendsPendingReceiveRequests({
            token: token,
            page: this.state.pageReceivedRequests,
            isRefresh: true
        });
        this.props.fetchFriendsPendingSentRequests({
            token: token,
            page: this.state.pageSentRequests,
            isRefresh: true
        });
    }

    componentDidUpdate() {
        const {
            cancelFriendRequestApiRes,
            acceptFriendRequestApiRes,
            fetchFriendsPendingReceiveRequests,
            fetchFriendsPendingSentRequests,
            resetAcceptFriendRequestApi
        } = this.props;
        const { token } = this.state;

        // Show messages Start
        if (
            cancelFriendRequestApiRes.status &&
            cancelFriendRequestApiRes.status == 'ERROR'
        ) {
            this.setState({ showLoader: false });
            Alert.alert(
                'There was a problem ',
                cancelFriendRequestApiRes.messages[0],
                [{ text: 'OK', onPress: () => {} }],
                { cancelable: false }
            );
            this.props.resetFriendRequestApi();
            this.setState({ callSentApi: true });
            return null;
        }
        if (
            cancelFriendRequestApiRes.status &&
            cancelFriendRequestApiRes.status == 'OK'
        ) {
            this.setState({ showLoader: false });
            Alert.alert(
                '',
                cancelFriendRequestApiRes.messages[0],
                [
                    {
                        text: 'OK',
                        onPress: () => {
                            fetchFriendsPendingSentRequests({
                                token: token,
                                page: this.state.pageSentRequests,
                                isRefresh: true
                            });
                            fetchFriendsPendingReceiveRequests({
                                token: token,
                                page: this.state.pageReceivedRequests,
                                isRefresh: true
                            });
                        }
                    }
                ],
                { cancelable: false }
            );
            this.props.resetFriendRequestApi();
            this.setState({ callSentApi: true });
            return null;
        }

        if (
            acceptFriendRequestApiRes.status &&
            acceptFriendRequestApiRes.status == 'ERROR'
        ) {
            Alert.alert(
                'There was a problem ',
                acceptFriendRequestApiRes.messages[0],
                [{ text: 'OK', onPress: () => {} }],
                { cancelable: false }
            );
            resetAcceptFriendRequestApi();
            return null;
        }
        if (
            acceptFriendRequestApiRes.status &&
            acceptFriendRequestApiRes.status == 'OK'
        ) {
            Alert.alert(
                '',
                acceptFriendRequestApiRes.messages[0],
                [
                    {
                        text: 'OK',
                        onPress: () => {
                            fetchFriendsPendingReceiveRequests({
                                token: token,
                                page: this.state.pageReceivedRequests,
                                isRefresh: true
                            });
                        }
                    }
                ],
                { cancelable: false }
            );
            resetAcceptFriendRequestApi();
            return null;
        }

        // Show messages End
    }

    static getDerivedStateFromProps(newProps, prevState) {
        if (
            newProps.friendsPendingReceiveRequests &&
            newProps.friendsPendingReceiveRequests.data &&
            newProps.friendsPendingReceiveRequests.data.friends &&
            newProps.friendsPendingReceiveRequests.data.friends.length > 0
        ) {
            let receiveRequestList = newProps.friendsPendingReceiveRequests;
            receiveRequestList.data =
                newProps.friendsPendingReceiveRequests.data.friends;
            return {
                receivedRequests: receiveRequestList,
                isRefreshingReceivedRequests: false
            };
        }

        return {
            receivedRequests: newProps.friendsPendingReceiveRequests,
            sentRequests: newProps.friendsPendingSentRequests,
            isRefreshingReceivedRequests: false,
            isRefreshingSentRequests: false
        };
    }

    onPressCard = index => {
        const cards = this.state.cards.map(({ text }) => ({
            text,
            state: false
        }));
        cards[index].state = !cards[index].state;
        this.setState({ cards });
    };

    onScrollEnd = listName => {
        this.setState({
            isRefreshingReceivedRequests: true,
            isRefreshingSentRequests: true
        });
        if (listName == 'receivedRequests') {
            if (
                !this.onEndReachedCalledDuringMomentum &&
                this.state.callReceiveApi &&
                this.state.receivedRequests &&
                this.props.friendsPendingReceiveRequests &&
                this.props.friendsPendingReceiveRequests.count &&
                this.props.friendsPendingReceiveRequests.count >
                    this.state.receivedRequests.data.length
            ) {
                this.props.fetchFriendsPendingReceiveRequests({
                    token: this.state.token,
                    page: this.state.pageReceivedRequests + 1
                });
                this.setState({
                    pageReceivedRequests: this.state.pageReceivedRequests + 1
                });
                this.onEndReachedCalledDuringMomentum = true;
            }
        }
        if (listName == 'sentRequests') {
            if (
                !this.onEndReachedCalledDuringMomentum &&
                this.state.callSentApi &&
                this.state.sentRequests &&
                this.props.friendsPendingSentRequests &&
                this.props.friendsPendingSentRequests.count &&
                this.props.friendsPendingSentRequests.count >
                    this.state.sentRequests.data.length
            ) {
                this.props.fetchFriendsPendingSentRequests({
                    token: this.state.token,
                    page: this.state.pageSentRequests + 1
                });
                this.setState({
                    pageSentRequests: this.state.pageSentRequests + 1
                });
                this.onEndReachedCalledDuringMomentum = true;
            }
        }

        this.setState({
            isRefreshingReceivedRequests: false,
            isRefreshingSentRequests: false,
            callReceiveApi: true,
            callSentApi: true
        });
    };

    onRefresh = listName => {
        if (listName == 'receivedRequests') {
            this.setState({ pageReceivedRequests: 1 });
            this.props.fetchFriendsPendingReceiveRequests({
                token: this.state.token,
                page: 1,
                isRefresh: true
            });
        } else {
            this.setState({ pageSentRequests: 1 });
            this.props.fetchFriendsPendingSentRequests({
                token: this.state.token,
                page: 1,
                isRefresh: true
            });
        }
    };

    emptyReceivedRequestList = () => {
        return <EmptyListIndicator message="No Received Requests Found" />;
    };

    renderReceivedRequest = () => {
        if (
            this.state.receivedRequests.data.length == 0 ||
            this.state.receivedRequests.data.length == undefined
        ) {
            return (
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            onRefresh={() => this.onRefresh('receivedRequests')}
                            refreshing={this.state.isRefreshingReceivedRequests}
                        />
                    }
                >
                    <EmptyListIndicator message=" No Received Requests Found" />
                </ScrollView>
            );
        }

        return (
            <FlatList
                data={
                    this.state.receivedRequests.data.length > 0
                        ? this.state.receivedRequests.data
                        : []
                }
                renderItem={({ item }) => {
                    return (
                        <PendingRequest
                            item={item}
                            accept={this.acceptFriendRequest}
                            reject={this.cancelFriendReciveRequest}
                        />
                    );
                }}
                onEndReachedThreshold={0.1}
                onEndReached={e => this.onScrollEnd('receivedRequests')}
                onRefresh={() => this.onRefresh('receivedRequests')}
                refreshing={false}
                keyExtractor={(item, index) =>
                    `id:${
                        item.friend_id ? item.friend_id : index
                    },index:${index}`
                }
                ListEmptyComponent={this.emptyReceivedRequestList()}
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    renderSentRequest = () => {
        return (
            <FlatList
                data={this.state.sentRequests.data}
                renderItem={({ item }) => {
                    return (
                        <SentRequest
                            item={item}
                            cancel={this.cancelFriendSentRequest}
                        />
                    );
                }}
                onEndReachedThreshold={0.1}
                onEndReached={e => this.onScrollEnd('sentRequests')}
                onRefresh={() => this.onRefresh('sentRequests')}
                refreshing={false}
                keyExtractor={(item, index) =>
                    `id:${item.friend_id},index:${index}`
                }
                ListEmptyComponent={this.emptySentRequestList()}
                onMomentumScrollBegin={() => {
                    this.onEndReachedCalledDuringMomentum = false;
                }}
            />
        );
    };

    emptySentRequestList = () => {
        return <EmptyListIndicator message="No Sent Requests Found" />;
    };

    cancelFriendSentRequest = friend => {
        // write if condition because in android only  cancel Friend Request Api call multiple times
        if (this.state.cancelRequestFriendId !== friend.friend_id) {
            this.setState(
                {
                    callSentApi: false,
                    showLoader: true,
                    cancelRequestFriendId: friend.friend_id
                },
                () => {
                    this.props.cancelFriendRequest({
                        token: this.state.token,
                        friend_id: friend.friend_id
                    });
                }
            );
        }
    };

    cancelFriendReciveRequest = friend => {
        this.setState({ callReceiveApi: false });
        this.props.cancelFriendRequest({
            token: this.state.token,
            friend_id: friend.friend_id
        });
        this.setState({
            callReceiveApi: false
        });
    };

    renderFriends = () => {
        if (this.state.cards[0].state == true) {
            return this.renderReceivedRequest();
        } else {
            return this.renderSentRequest();
        }
    };

    acceptFriendRequest = friend => {
        this.setState({ callReceiveApi: false });
        this.props.acceptFriendRequest({
            token: this.state.token,
            friend_id: friend.friend_id
        });

        let filterReceiveRequests = {},
            search = this.state.receivedRequests.data,
            selected = [];
        search = search.filter(item => {
            if (item.friend_id == friend.friend_id) {
                return true;
            }
            selected.push(item);
            return true;
        });
        filterReceiveRequests = {
            data: selected,
            code: this.props.friendsPendingReceiveRequests.code,
            status: this.props.friendsPendingReceiveRequests.status
        };

        this.setState({
            receivedRequests: filterReceiveRequests,
            callReceiveApi: false
        });
    };

    render() {
        return (
            <View
                style={[
                    globalStyles.flexContainer,
                    globalStyles.mainContainer,
                    styles.container
                ]}
            >
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexHCenter
                    ]}
                >
                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexVCenter,
                            { flex: 3, paddingTop: 10 }
                        ]}
                    >
                        {this.state.cards.map(({ text, state }, i) => {
                            return (
                                <SmallCards
                                    containerStyle={{
                                        minWidth: '45%',
                                        paddingHorizontal: 5,
                                        paddingVertical: 3
                                    }}
                                    text={text}
                                    selected={state}
                                    onPress={this.onPressCard.bind(this, i)}
                                    first={i === 0}
                                    key={i}
                                />
                            );
                        })}
                    </View>
                </View>

                <View style={[globalStyles.flexContainer, { paddingTop: 10 }]}>
                    <View
                        style={[
                            globalStyles.flexContainer,
                            styles.scrollViewWrapper
                        ]}
                    >
                        <View style={styles.scrollContainer}>
                            {this.renderFriends()}
                        </View>
                    </View>
                </View>
                {this.state.showLoader ||
                this.props.friendsPendingReceiveRequests.status ==
                    'inprogress' ||
                this.props.friendsPendingSentRequests.status ===
                    'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
