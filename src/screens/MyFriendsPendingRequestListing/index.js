import { connect } from 'react-redux';

import { fetchFriendsPendingReceiveRequests } from '../../redux/epics/fetch-friends-pending-receive-requests-epic';
import { fetchFriendsPendingSentRequests } from '../../redux/epics/fetch-friends-pending-sent-requests-epic';
import {
    acceptFriendRequest,
    resetAcceptFriendRequestApi
} from '../../redux/epics/accept-friend-request-epic';
import {
    cancelFriendRequest,
    resetFriendRequestApi
} from '../../redux/epics/cancel-friend-request-epic';

import MyFriendsPendingRequestListing from './MyFriendsPendingRequestListing';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({
    friendsPendingReceiveRequests,
    friendsPendingSentRequests,
    cancelFriendRequestApiRes,
    acceptFriendRequestApiRes
}) => ({
    friendsPendingReceiveRequests,
    friendsPendingSentRequests,
    cancelFriendRequestApiRes,
    acceptFriendRequestApiRes
});

const mapDispatchToProps = dispatch => ({
    fetchFriendsPendingReceiveRequests: data =>
        dispatch(fetchFriendsPendingReceiveRequests(data)),
    fetchFriendsPendingSentRequests: data =>
        dispatch(fetchFriendsPendingSentRequests(data)),
    acceptFriendRequest: data => dispatch(acceptFriendRequest(data)),
    cancelFriendRequest: data => dispatch(cancelFriendRequest(data)),
    resetFriendRequestApi: () => dispatch(resetFriendRequestApi()),
    resetAcceptFriendRequestApi: () => dispatch(resetAcceptFriendRequestApi())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(MyFriendsPendingRequestListing, [
        { propName: 'friendsPendingReceiveRequests' },
        { propName: 'friendsPendingSentRequests' },
        { propName: 'cancelFriendRequestApiRes' }
    ])
);
