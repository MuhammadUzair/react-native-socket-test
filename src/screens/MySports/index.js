import { connect } from 'react-redux';

import { fetchLeagues } from '../../redux/epics/fetch-leagues-epic';
import { addLeagues, reset } from '../../redux/epics/add-leagues-epic';
import MySports from './MySports';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ leagues, user, leaguesAdded }) => ({
    leagues,
    user,
    leaguesAdded
});

const mapDispatchToProps = dispatch => ({
    fetchLeagues: data => dispatch(fetchLeagues(data)),
    addLeagues: data => dispatch(addLeagues(data)),
    resetAddLeagues: () => dispatch(reset())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(MySports, [
        { propName: 'leaguesAdded', actionOnFailure: 'resetAddLeagues' }
    ])
);
