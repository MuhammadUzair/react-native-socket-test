import { StyleSheet } from 'react-native';

import { os } from '../../utils';

export default StyleSheet.create({
    container: {
        paddingLeft: 20,
        paddingRight: 20
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    subText: {
        fontSize: 18,
        color: 'black',
        marginBottom: 5
    },
    descriptionText: {
        color: 'black',
        fontSize: 12,
        marginBottom: 10
    },
    teamsCard: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: '100%',
        // height: 50,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 40,
        paddingHorizontal: 10,
        flexWrap: 'wrap'
    },
    checkbox: {
        paddingVertical: 5,
        paddingHorizontal: 2.5
    },
    checkboxText: {
        color: 'black',
        textAlign: 'center',
        fontSize: 12
    },
    buttonStyle: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderRadius: 50,
        flexDirection: 'row',
        width: 60 + '%'
    },
    buttonText: {
        fontSize: 18,
        paddingLeft: 15
    },
    listHolder: {
        flex: 1,

        paddingBottom: 30,
        borderRadius: 10
    }
});
