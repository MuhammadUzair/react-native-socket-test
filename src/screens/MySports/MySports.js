import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    AsyncStorage,
    FlatList,
    Alert
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { os, retrieveToken } from '../../utils';

import { globalStyles, onboardGrey } from '../../assets/styles';
import sportsStyles from './styles';

import CustomCheckBox from '../../components/CustomCheckBox';
import OverlayIndicator from '../../components/OverlayIndicator';
import Seperator from '../../components/Seperator';
import OnboardSportCheckbox from '../../components/OnboardSportCheckbox';

export default class MySports extends Component {
    state = {
        leagues: []
    };

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('MyTeams')}
                    style={{ paddingRight: os() ? 10 : 20 }}
                >
                    <Text style={{ color: onboardGrey }}>Skip Step</Text>
                </TouchableOpacity>
            )
        };
    };

    componentDidMount() {
        const { user, fetchLeagues } = this.props;
        retrieveToken().then(token => fetchLeagues(token));
    }

    componentDidUpdate(prevProps, prevState) {
        const { leaguesAdded, navigation, resetAddLeagues } = this.props;
        if (leaguesAdded !== 'inprogress' && leaguesAdded !== '') {
            if (leaguesAdded.code >= 200 && leaguesAdded.code <= 300) {
                navigation.navigate('MyTeams');
                resetAddLeagues();
            }
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (typeof props.leagues !== 'string')
            if (props.leagues.data.length)
                return { leagues: props.leagues.data };
        return null;
    }

    renderItem = ({ item, index }) => {
        return (
            <OnboardSportCheckbox
                onPress={this.onPressCheckbox}
                item={item}
                index={index}
            />
        );
    };

    onPressCheckbox = index => {
        const leagues = [...this.state.leagues];
        leagues[index].isAdded = !leagues[index].isAdded;
        this.setState({ leagues });
    };

    onPressContinue = async () => {
        let leagueIds = this.state.leagues.filter(league => league.isAdded);
        if (leagueIds.length) {
            const token = JSON.parse(await AsyncStorage.getItem('token'));
            leagueIds = leagueIds.map(league => league.id);
            this.props.addLeagues({ token: token.data, leagueIds });
        } else {
            Alert.alert(
                'There was a problem',
                'Please select some teams before you continue'
            );
        }
    };

    render() {
        const { leagues, leaguesAdded } = this.props;
        return (
            <View
                style={[globalStyles.mainContainer, globalStyles.flexContainer]}
            >
                <Text
                    style={[
                        sportsStyles.headerText,
                        globalStyles.baseBlueColor
                    ]}
                >
                    My Sports
                </Text>
                <Text style={[sportsStyles.subText, globalStyles.boldFontFace]}>
                    Personalize Post It Play It
                </Text>
                <Text
                    style={sportsStyles.descriptionText}
                >{`Choose your favourite sports, teams and friends\nfor quick access across Post It Play It Competitions.`}</Text>
                <View style={[globalStyles.flexContainer]}>
                    <View
                        style={[
                            globalStyles.whiteBackgroudColor,
                            sportsStyles.listHolder
                        ]}
                    >
                        <FlatList
                            data={this.state.leagues}
                            renderItem={this.renderItem}
                            numColumns={2}
                            ItemSeparatorComponent={Seperator}
                            style={{ flex: 1 }}
                            contentContainerStyle={{ paddingTop: 5 }}
                            keyExtractor={(item, index) =>
                                `index:${index}, id:${item.id}`
                            }
                        />
                    </View>
                    <View style={{ top: -26 }}>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHCenter,
                                sportsStyles.bottomSpacing
                            ]}
                            onPress={this.onPressContinue}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    sportsStyles.buttonStyle
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        sportsStyles.buttonText
                                    ]}
                                >
                                    Continue
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {leagues === 'inprogress' || leaguesAdded === 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
