import { connect } from 'react-redux';
import ReferFriend from './ReferFriend';

import {
    inviteFriendsSubmit,
    resetInviteFriendsSubmit
} from '../../redux/epics/invite-friends';
import { fetchInvitesFriend } from '../../redux/epics/fetch-invites-friend';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ inviteFriends, inviteFriendsDetails, user }) => ({
    inviteFriends,
    inviteFriendsDetails,
    user
});
const mapDispatchToProps = dispatch => ({
    inviteFriendsSubmit: data => dispatch(inviteFriendsSubmit(data)),
    fetchInvitesFriend: data => dispatch(fetchInvitesFriend(data)),
    resetInviteFriendsSubmit: () => dispatch(resetInviteFriendsSubmit())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(ReferFriend, [{ propName: 'inviteFriends' }]));
