import { StyleSheet, Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
const { height, width } = Dimensions.get('window');

export default StyleSheet.create({
    mainContainer: {
        height: height
    },
    container: {
        paddingTop: verticalScale(10)
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 2.5
    },
    subText: {
        fontSize: 18,
        color: 'black',
        marginBottom: 5
    },
    bodyContainer: {
        backgroundColor: 'white',
        borderRadius: 5,
        marginTop: verticalScale(5)
    },
    holder: {
        paddingVertical: verticalScale(25),
        paddingHorizontal: 10
    },
    linkHolder: {
        paddingTop: verticalScale(15),
        paddingHorizontal: 10
    },
    link: {
        borderRadius: 50,
        borderWidth: StyleSheet.hairlineWidth,
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    emailButton: {
        justifyContent: 'space-around'
        // paddingVertical: 10
    },
    inviteText: {
        marginRight: 20
    },
    totalFriends: {
        position: 'absolute',
        top: 3,
        right: 40
    },
    inforCard: {
        padding: 20
    },
    marginTopTen: {
        marginTop: 10
    },
    marginTop20: {
        marginTop: 20
    },
    socialIconWrap: {
        marginTop: 10
    },
    socialIconMargin: {
        marginRight: 5
    },
    invitesWhiteBox: {
        backgroundColor: 'white',
        borderRadius: 5,
        marginTop: verticalScale(5),
        paddingBottom: 20
    },
    formNumber: {
        fontSize: scale(30),
        color: '#DBDBDB',
        fontWeight: 'bold'
    },
    userInput: {
        borderWidth: 0.3,
        padding: 10,
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 10,
        borderRadius: 30,
        width: width * 0.7
    },
    formRow: {
        padding: 20,
        paddingBottom: 0,
        borderColor: '#f5f5f5',
        borderWidth: 1
    },
    saveButtonWrap: {
        marginLeft: width * 0.04,
        marginTop: 50
    },
    bigPrimaryButton: {
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'center',
        width: width * 0.85
    },
    bigPrimaryButtonText: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    shareButton: {
        paddingVertical: 2,
        paddingHorizontal: 10,
        borderRadius: 50,
        flexDirection: 'row'
    },
    shareButtonText: {
        fontSize: 18,
        paddingLeft: 10
    },
    shareButtonwrap: {
        marginTop: 5
    },
    inviteFriendText: {
        paddingTop: 7
    },
    inviteFriendRow: {
        marginTop: 20,
        paddingLeft: 3,
        paddingRight: 5
    }
});
