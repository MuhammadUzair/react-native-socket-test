import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    ScrollView,
    Alert,
    Share
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import DrawerIconButton from '../../components/DrawerIconButton';

import { os, retrieveToken } from '../../utils';
import { BASEURL_DEV } from '../../redux/epics/base-url';

import { globalStyles, baseBlue } from '../../assets/styles';
import styles from './styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import OverlayIndicator from '../../components/OverlayIndicator';

export default class ReferFriend extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20
            },
            headerLeft: (
                <DrawerIconButton onPress={() => navigation.openDrawer()} />
            )
        };
    };

    state = {
        invitations: [
            {
                fullname: '',
                email: '',
                number: ''
            }
        ],
        isSendButtonDisabled: false,
        token: '',
        invitesSent: 0,
        friends: 0
    };

    componentDidMount = async () => {
        this.props.resetInviteFriendsSubmit();
        let token = await retrieveToken();
        this.props.fetchInvitesFriend({ token });
        this.setState({ token });
    };

    componentDidUpdate() {
        const {
            inviteFriends,
            resetInviteFriendsSubmit,
            inviteFriendsDetails
        } = this.props;
        console.log('inviteFriendsDetails ', inviteFriendsDetails);

        if (inviteFriends.status == 'OK') {
            resetInviteFriendsSubmit();
            this.setState({
                invitations: [
                    {
                        fullname: '',
                        email: '',
                        number: ''
                    }
                ]
            });
            Alert.alert(
                'Invite Friends Successfully',
                inviteFriends.messages[0],
                [
                    {
                        text: 'OK',
                        onPress: () => {}
                    }
                ],
                { cancelable: false }
            );
        }
    }

    static getDerivedStateFromProps(props, state) {
        const inviteFriendsDetails = props.inviteFriendsDetails;
        let invitesSent = state.invitesSent;
        let friends = state.invitesSent;
        if (inviteFriendsDetails.data && inviteFriendsDetails.data) {
            invitesSent = inviteFriendsDetails.data.invites;
            friends = inviteFriendsDetails.data.friends;
        }
        return {
            invitesSent,
            friends
        };
    }

    onFocusFullName = index => {
        index = index + 1;
        if (index == this.state.invitations.length) {
            let invitations = [...this.state.invitations],
                insertNewForm = {
                    fullname: '',
                    email: '',
                    number: ''
                };
            invitations.push(insertNewForm);
            this.setState({ invitations });
        }
    };

    onChangeTextInput = (key, value, index) => {
        let invitations = [...this.state.invitations];
        invitations[index][key] = value;
        this.setState({ invitations });
    };

    onSubmit = () => {
        let frindInvitations = this.state.invitations;
        frindInvitations = frindInvitations.filter(item => {
            if (item.fullname || item.email || item.number) return true;
        });
        this.props.inviteFriendsSubmit({
            token: this.state.token,
            invitations: frindInvitations
        });
    };

    socialShare = () => {
        let username = this.props.user.data.username;
        let url = `${BASEURL_DEV}app/register?ref=${username}&provider=b73c2d22763d1ce2143a3755c1d0ad3a`;
        let message = `Please Join Me On Post It Play It! ${BASEURL_DEV}app/register?ref=${username}&provider=b73c2d22763d1ce2143a3755c1d0ad3a`;
        Share.share(
            {
                message: message,
                url: url,
                title: 'PostItPlayIt'
            },
            {
                // Android only:
                dialogTitle: 'Share PostItPlayIt',
                // iOS only:
                excludedActivityTypes: [
                    'com.apple.UIKit.activity.PostToFacebook',
                    'com.apple.UIKit.activity.PostToTwitter'
                ]
            }
        );
    };

    renderInvitations = (text, number, last) => {
        return this.state.invitations.map((item, i) => {
            return (
                <View
                    style={[
                        globalStyles.flexHorizontal,
                        globalStyles.flexSpaceBetween,
                        styles.formRow
                    ]}
                    key={i}
                >
                    <View>
                        <Text style={styles.formNumber}>{i + 1}</Text>
                    </View>

                    <View>
                        <TextInput
                            style={[
                                globalStyles.baseFontSize,
                                styles.userInput
                            ]}
                            underlineColorAndroid="transparent"
                            value={item.fullname}
                            onChangeText={text =>
                                this.onChangeTextInput('fullname', text, i)
                            }
                            placeholder="Full Name"
                            placeholderTextColor="rgba(0, 0, 0, 0.87)"
                            onFocus={() => this.onFocusFullName(i)}
                        />
                        <TextInput
                            style={[
                                globalStyles.baseFontSize,
                                styles.userInput
                            ]}
                            underlineColorAndroid="transparent"
                            value={item.email}
                            onChangeText={text =>
                                this.onChangeTextInput('email', text, i)
                            }
                            placeholder="Email Address"
                            placeholderTextColor="rgba(0, 0, 0, 0.87)"
                            keyboardType="email-address"
                        />
                        <TextInput
                            style={[
                                globalStyles.baseFontSize,
                                styles.userInput
                            ]}
                            underlineColorAndroid="transparent"
                            value={item.number}
                            onChangeText={text =>
                                this.onChangeTextInput('number', text, i)
                            }
                            placeholder="Phone Number"
                            placeholderTextColor="rgba(0, 0, 0, 0.87)"
                            keyboardType="phone-pad"
                        />
                    </View>
                </View>
            );
        });
    };

    render() {
        const { inviteFriends } = this.props;
        return (
            <KeyboardAwareScrollView style={styles.mainContainer}>
                <View
                    style={[
                        globalStyles.flexContainer,
                        globalStyles.mainContainer,
                        styles.container
                    ]}
                >
                    <Text
                        style={[
                            styles.subText,
                            globalStyles.boldFontFace,
                            globalStyles.greyColor
                        ]}
                    >
                        Get A Chance To Earn More.
                    </Text>

                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween,
                            styles.bodyContainer,
                            styles.inforCard
                        ]}
                    >
                        <View>
                            <Text
                                style={[
                                    globalStyles.baseFontSize,
                                    globalStyles.highlightColor
                                ]}
                            >
                                Refer A Friend
                            </Text>

                            <TouchableOpacity
                                onPress={() => this.socialShare()}
                                activeOpacity={0.5}
                                disabled={this.state.isSendButtonDisabled}
                                style={styles.shareButtonwrap}
                            >
                                <LinearGradient
                                    colors={
                                        this.state.isSendButtonDisabled
                                            ? ['#b2bec3', '#b2bec3']
                                            : ['#4d6bc2', '#3695e3']
                                    }
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={[
                                        globalStyles.flexHCenter,
                                        globalStyles.flexSpaceBetween,
                                        styles.shareButton
                                    ]}
                                >
                                    <Text
                                        style={[
                                            globalStyles.fontFace,
                                            globalStyles.whiteColor,
                                            styles.shareButtonText
                                        ]}
                                    >
                                        Share
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>

                            {/* <View
                                style={[
                                    globalStyles.flexHorizontal,
                                    styles.socialIconWrap
                                ]}
                            >
                                <TouchableOpacity
                                    onPress={() => {
                                        Share.share(
                                            {
                                                message:
                                                    "BAM: we're helping your business with awesome React Native apps",
                                                url: 'http://bam.tech',
                                                title: 'Wow, did you see that?'
                                            },
                                            {
                                                // Android only:
                                                dialogTitle:
                                                    'Share BAM goodness',
                                                // iOS only:
                                                excludedActivityTypes: [
                                                    'com.apple.UIKit.activity.PostToTwitter'
                                                ]
                                            }
                                        );
                                    }}
                                >
                                    <Icon
                                        name="facebook-f"
                                        type="font-awesome"
                                        color="#6081C4"
                                        containerStyle={styles.socialIconMargin}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Icon
                                        name="google-plus"
                                        type="font-awesome"
                                        color="#E62B34"
                                        containerStyle={styles.socialIconMargin}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Icon
                                        name="twitter"
                                        type="font-awesome"
                                        color="#43BCF0"
                                        containerStyle={styles.socialIconMargin}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Icon
                                        name="linkedin"
                                        type="font-awesome"
                                        color="#2A98CF"
                                        containerStyle={styles.socialIconMargin}
                                    />
                                </TouchableOpacity>
                            </View> */}
                        </View>

                        <View>
                            <View style={[globalStyles.flexHorizontal]}>
                                <Text
                                    style={[
                                        styles.inviteText,
                                        globalStyles.baseFontSize
                                    ]}
                                >
                                    Invites Sent
                                </Text>
                                <Text style={[globalStyles.baseFontSize]}>
                                    Friends
                                </Text>
                            </View>
                            <View style={[globalStyles.flexHorizontal]}>
                                <Text
                                    style={[
                                        styles.marginTopTen,
                                        globalStyles.baseBlueColor
                                    ]}
                                >
                                    {this.state.invitesSent}
                                </Text>
                                <Text
                                    style={[
                                        styles.totalFriends,
                                        globalStyles.highlightGreenColor,
                                        styles.marginTopTen
                                    ]}
                                >
                                    {this.state.friends}
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexSpaceBetween,
                            styles.inviteFriendRow
                        ]}
                    >
                        <View style={globalStyles.flexHorizontal}>
                            <Icon
                                name="user"
                                type="font-awesome"
                                color="#2A98CF"
                                containerStyle={styles.socialIconMargin}
                            />
                            <Text
                                style={[
                                    globalStyles.baseFontSize,
                                    globalStyles.greyColor,
                                    styles.inviteFriendText
                                ]}
                            >
                                Invites Friends
                            </Text>
                        </View>

                        <View>
                            <Text
                                style={[
                                    globalStyles.baseBlueColor,
                                    globalStyles.baseFontSize,
                                    styles.inviteFriendText
                                ]}
                            >
                                Invite 1 or more then 1 friend
                            </Text>
                        </View>
                    </View>

                    <ScrollView contentContainerStyle={styles.invitesWhiteBox}>
                        {this.renderInvitations()}

                        <TouchableOpacity
                            onPress={() => this.onSubmit()}
                            activeOpacity={0.5}
                            disabled={this.state.isSendButtonDisabled}
                            style={styles.saveButtonWrap}
                        >
                            <LinearGradient
                                colors={
                                    this.state.isSendButtonDisabled
                                        ? ['#b2bec3', '#b2bec3']
                                        : ['#4d6bc2', '#3695e3']
                                }
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    styles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        styles.bigPrimaryButtonText
                                    ]}
                                >
                                    Send
                                </Text>
                                <Icon
                                    name="arrows-h"
                                    type="font-awesome"
                                    color="white"
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                    </ScrollView>

                    {inviteFriends == 'inprogress' ? (
                        <OverlayIndicator />
                    ) : null}
                </View>
            </KeyboardAwareScrollView>
        );
    }
}
