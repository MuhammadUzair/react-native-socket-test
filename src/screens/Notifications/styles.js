import { StyleSheet, Dimensions } from 'react-native';
import { os } from '../../utils';

const { height, width } = Dimensions.get('window');

export default StyleSheet.create({
    whiteBG: {
        backgroundColor: 'white'
    },
    heading: {
        fontSize: 24
    },

    flatListContainer: {
        flex: 0,
        margin: 7,
        width: '45%'
    },
    name: {
        marginRight: 10
    },
    flatList: {
        paddingTop: 35,
        padding: 10,
        borderRadius: 10
    },

    imagess: {
        marginLeft: 5,
        zIndex: 3,
        top: 15
    },

    notificationView: {
        height: '100%',
        width: '100%',
        marginTop: 40
    },

    noteFontFace: {
        fontSize: 8,
        color: 'grey'
    },

    roundTimer: {
        // backgroundColor:'#3695e3',
        // textAlign:'center',
        flex: 0,
        padding: 2
        // color: '#FFFFFF'
    },

    hairLineDivider: {
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 1.5
    },

    widthSixty: {
        width: '60%'
    },

    widthForty: {
        width: '40%'
    },

    paddingBottom: {
        paddingBottom: 20
    },

    paddingTop: {
        paddingTop: 40
    },

    rowContainer: {
        paddingVertical: os() ? 18 : 12,
        paddingHorizontal: 20,
        borderBottomColor: '#bdc3c7'
    },

    borderRadius: {
        borderRadius: 10
    },

    borderRadiusTop: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },

    roundedAmount: {
        borderRadius: 50,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#88c3ff',
        padding: 5,
        width: 60,
        textAlign: 'center',
        alignSelf: 'flex-end'
    },

    inputText: {
        width: 70,
        height: '100%',
        padding: 10,
        paddingBottom: 0,
        paddingTop: 0,
        backgroundColor: '#EEE',
        color: 'black',
        marginLeft: -20,
        position: 'relative',
        zIndex: 0
    },

    circularPosition: {
        position: 'absolute'
    },

    bottomSpacing: {
        marginBottom: 12
    },

    topSpacing: {
        marginTop: 12
    },

    bigPrimaryButtonText: {
        fontSize: 18
    },

    bigPrimaryButton: {
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 50,
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    card: {
        padding: 10
    },
    mainContainer: {
        paddingHorizontal: os() ? 10 : 15
    },

    marginTop: {
        marginTop: 10
    },

    amountTextSize: {
        fontSize: 18
    },

    cancelCenterButton: {
        marginTop: 25,
        fontSize: 15
    },
    notificationImage: {
        marginRight: 10
    },
    emptyNotificationView: {
        alignItems: 'center',
        flex: 1,
        height: height * 0.9,
        paddingTop: 20
    },
    notificationListHeading: {
        position: 'absolute',
        top: 0,
        left: width * 0.3
    }
});
