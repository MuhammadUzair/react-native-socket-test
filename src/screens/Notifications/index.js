import Notifications from './Notifications';

import { connect } from 'react-redux';

import { fetchNotification } from '../../redux/epics/fetch-notifications';
import {
    fetchSpecificCompetition,
    resetFetchSpecificCompetition
} from '../../redux/epics/fetch-specific-competition-epic';
import { unActiveNotification } from '../../redux/epics/get-notification';

import alertOnError from '../../hoc/alertOnError';

const mapStateToProps = ({ notifications, searchCompetition }) => ({
    notifications,
    searchCompetition
});
const mapDispatchToProps = dispatch => ({
    fetchNotification: data => dispatch(fetchNotification(data)),
    fetchSpecificCompetition: data => dispatch(fetchSpecificCompetition(data)),
    resetFetchSpecificCompetition: data =>
        dispatch(resetFetchSpecificCompetition(data)),
    unActiveNotification: data => dispatch(unActiveNotification(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertOnError(Notifications, [{ propName: 'notifications' }]));
