import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    FlatList,
    Platform,
    Alert,
    AsyncStorage
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';

import { globalStyles } from '../../assets/styles';
import NotificationsStyles from './styles';
import dashboardStyles from '../Dashboard/styles';

import {
    os,
    retrieveToken,
    renderSubText,
    renderSpreadWithPlusSign
} from '../../utils';

import EmptyListIndicator from '../../components/EmptyListIndicator';
import CustomAvatar from '../../components/CustomAvatar';
import HeaderBackButton from '../../components/HeaderBackButton';
import PushNotification from 'react-native-push-notification';
import HeaderRightIcon from '../../components/HeaderRightIcon';
import OverlayIndicator from '../../components/OverlayIndicator';
export default class Notifications extends Component {
    static navigationOptions = args => {
        const { navigation, navigationOptions } = args;
        return {
            headerStyle: {
                ...navigationOptions.headerStyle,
                paddingHorizontal: os() ? 10 : 20
            },
            headerLeft: (
                <HeaderBackButton
                    onPress={() => navigation.goBack()}
                    type="close"
                    isNotificationScreen={true}
                />
            ),
            headerRight: <HeaderRightIcon drawerProps={navigation} />
        };
    };

    state = {
        token: '',
        notifications: [],
        isNotificationRefresh: true,
        competitionCreated: false,
        competitionDetail: false
    };

    componentDidMount() {
        this.props.resetFetchSpecificCompetition();
        PushNotification.setApplicationIconBadgeNumber(0);
        PushNotification.cancelAllLocalNotifications();
        this.props.unActiveNotification();
        retrieveToken().then(token => {
            this.setState({
                token,
                competitionCreated: false,
                competitionDetail: false
            });
            this.props.fetchNotification({ token, isRefresh: true });
        });
    }

    componentDidUpdate = async (prevProps, prevState) => {
        const {
            searchCompetition,
            navigation,
            resetFetchSpecificCompetition
        } = this.props;

        // console.log('searchCompetition --', searchCompetition);

        if (
            navigation.state.routeName == 'NotificationScreen' &&
            searchCompetition &&
            searchCompetition.data &&
            searchCompetition.data.competitions &&
            searchCompetition.screen == 'notification'
        ) {
            if (prevState.competitionCreated) {
                this.setState({ competitionCreated: false });
                let status = searchCompetition.data.competitions[0].status;
                let id = searchCompetition.data.competitions[0].id;
                let item = searchCompetition.data.competitions[0];
                const subText = `${item.competitor.short_code} ${renderSubText(
                    item.spread,
                    true
                )}`;
                const spreadTitle = `${
                    item.competitor.short_code
                } ${renderSpreadWithPlusSign(item.spread)}`;
                resetFetchSpecificCompetition();

                if (status == 'Open') {
                    return navigation.navigate('PlayIt', {
                        item,
                        subText,
                        spreadTitle
                    });
                } else if (status == 'In-Play') {
                    Alert.alert(
                        'There was a problem',
                        'The Competition has already started'
                    );
                    return false;
                } else if (status == 'Settled') {
                    return Alert.alert(
                        'There was a problem',
                        'The Competition has finished'
                    );
                } else if (status == 'No-Activity') {
                    return Alert.alert(
                        'There was a problem',
                        'The Competition went into No Action'
                    );
                }
            }
        }
    };

    static getDerivedStateFromProps(nextProps, nextState) {
        const { notifications } = nextProps;

        if (
            notifications !== '' &&
            notifications !== 'inprogress' &&
            notifications.data &&
            notifications.data.length > 0
        ) {
            return {
                notifications: notifications.data,
                isNotificationRefresh: false
            };
        }
        return { isNotificationRefresh: false };
    }

    emptyList = () => {
        const msg = `Currently there are no notifications to show.`;
        return (
            <View style={NotificationsStyles.emptyNotificationView}>
                <EmptyListIndicator
                    message={msg}
                    // showIndicator={
                    //     this.props.notifications.status === 'inprogress' ||
                    //     this.props.notifications === 'inprogress'
                    // }
                />
            </View>
        );
    };

    renderNotification = item => {
        return (
            <TouchableOpacity
                style={NotificationsStyles.card}
                onPress={() => this.notificationOnPress(item)}
            >
                <View style={[globalStyles.flexHorizontal, dashboardStyles.mb]}>
                    <View>
                        <CustomAvatar
                            xsmall={Platform.OS === 'ios'}
                            small={Platform.OS === 'android'}
                            rounded
                            containerStyle={
                                NotificationsStyles.notificationImage
                            }
                            uri={
                                item.mobileContent &&
                                item.mobileContent.img !== undefined &&
                                item.mobileContent.img
                            }
                        />
                    </View>
                    <View style={globalStyles.flexContainer}>
                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                globalStyles.fullWidth,
                                globalStyles.flexSpaceBetween
                            ]}
                        >
                            <Text
                                style={[
                                    globalStyles.subTextFontSize,
                                    globalStyles.greyColor
                                ]}
                            >
                                {item.mobileContent && item.mobileContent.name
                                    ? item.mobileContent.name
                                    : '-'}
                            </Text>
                            <Text
                                style={[
                                    globalStyles.baseFontSize,
                                    globalStyles.greyColor,
                                    { textAlign: 'right' }
                                ]}
                            >
                                {item.created_at &&
                                    item.created_at.substr(0, 10) + ' ' + ' '}
                                {item.created_at
                                    ? moment(item.created_at).format('HH:mm')
                                    : '-'}
                            </Text>
                        </View>
                        <Text
                            style={[
                                globalStyles.baseFontSize,
                                globalStyles.blackColor,
                                globalStyles.fullWidth
                            ]}
                        >
                            {item.mobileContent && item.mobileContent.message
                                ? item.mobileContent.message
                                : '-'}
                        </Text>
                    </View>
                </View>
                <View style={NotificationsStyles.hairLineDivider} />
            </TouchableOpacity>
        );
    };

    changeRoute(item) {
        switch (item.notification_alias) {
            case 'pipi-friend-request-accepted':
                return this.props.navigation.navigate('MyFriendsList');
            case 'pipi-friend-request-sent':
                return this.props.navigation.navigate('PendingFriendList', {
                    token: this.state.token
                });
            case 'pipi-join-group':
                return this.props.navigation.navigate('GroupScreen', {
                    isMyGroup: true
                });
            case 'pipi-group-member-added':
                return this.props.navigation.navigate('GroupScreen', {
                    isMyGroup: true
                });
            case 'pipi-competition-settled':
                return this.props.navigation.navigate('MyCompetition', {
                    showCompetitions: 'Settled'
                });
            case 'pipi-competition-participated':
                let id =
                    item.mobileContent && item.mobileContent.detail
                        ? item.mobileContent.detail
                        : 0;
                return this.props.navigation.push('CompetitionDetail', {
                    competitionId: id,
                    notification: true
                });

            default:
                null;
        }
    }

    notificationOnPress(item) {
        const { fetchSpecificCompetition } = this.props;

        if (item.notification_alias === 'pipi-competition-created') {
            this.setState({ competitionCreated: true }, () => {
                let id =
                    item.mobileContent && item.mobileContent.detail
                        ? item.mobileContent.detail
                        : 0;
                fetchSpecificCompetition({
                    token: this.state.token,
                    competitionId: id,
                    isNotification: true
                });
            });
        } else {
            this.changeRoute(item);
        }

        // this.setState({ competitionCreated: true }, () => {
        //     let id =
        //         item.mobileContent && item.mobileContent.detail
        //             ? item.mobileContent.detail
        //             : 0;
        //     fetchSpecificCompetition({
        //         token: this.state.token,
        //         competitionId: 1273
        //     });
        // });
    }

    onRefresh = () => {
        this.setState({ isNotificationRefresh: true });
        this.props.fetchNotification({
            token: this.state.token,
            isRefresh: true
        });
    };

    render() {
        return (
            <View
                style={[globalStyles.mainContainer, globalStyles.flexContainer]}
            >
                <View
                    style={[
                        globalStyles.greyBackgroudColor,
                        NotificationsStyles.borderRadiusTop,
                        NotificationsStyles.notificationView,
                        NotificationsStyles.paddingTop,
                        NotificationsStyles.paddingBottom,
                        globalStyles.flexContainer
                    ]}
                >
                    <View>
                        <FlatList
                            data={this.state.notifications}
                            // data={[
                            //     {
                            //         mobileContent: {
                            //             name: 'uzair',
                            //             message: 'notification msg'
                            //         }
                            //     }
                            // ]}
                            renderItem={({ item }) => {
                                return this.renderNotification(item);
                            }}
                            onEndReachedThreshold={0.1}
                            onEndReached={this.onScrollEnd}
                            onRefresh={this.onRefresh}
                            refreshing={false}
                            ListEmptyComponent={this.emptyList()}
                            keyExtractor={(item, index) =>
                                `id:${item._id},index:${index}`
                            }
                            onMomentumScrollBegin={() => {
                                this.onEndReachedCalledDuringMomentum = false;
                            }}
                        />
                    </View>
                </View>
                <View
                    activeOpacity={0.5}
                    style={[
                        NotificationsStyles.bottomSpacing,
                        NotificationsStyles.topSpacing,
                        globalStyles.flexHorizontal,
                        globalStyles.flexVCenter,
                        NotificationsStyles.notificationListHeading
                    ]}
                >
                    <LinearGradient
                        colors={['#4d6bc2', '#3695e3']}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={[
                            globalStyles.flexHorizontal,
                            globalStyles.flexHCenter,
                            NotificationsStyles.bigPrimaryButton
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.fontFace,
                                globalStyles.whiteColor,
                                NotificationsStyles.bigPrimaryButtonText
                            ]}
                        >
                            Notifications
                        </Text>
                    </LinearGradient>
                </View>
                {this.props.notifications.status == 'inprogress' ||
                this.props.searchCompetition.status == 'inprogress' ? (
                    <OverlayIndicator />
                ) : null}
            </View>
        );
    }
}
