import React, { Component } from 'react';
import {
    ImageBackground,
    View,
    Dimensions,
    AsyncStorage,
    Alert
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';

import { os, retrieveToken } from '../../utils';

import { globalStyles, loginStyles } from '../../assets/styles';

import LoginBody from './LoginScreenBody';
import OverlayIndicator from '../../components/OverlayIndicator';

import { fetchUser } from '../../redux/epics/fetch-user-epic';
import { loginUserAction } from '../../redux/epics/login-epic';
import { checkComplianceAction } from '../../redux/epics/compliance-epic';
import { reset } from '../../redux/epics/signup-epic';
import alertOnError from '../../hoc/alertOnError';

class LoginScreen extends Component {
    /*
     * state.showSpinner is being maintained in LoginScreen and is used to
     * show the spinner. It's to true when login button is pressed.
     * it's being used because the spinner is shown when the state inside the
     * redux store is updated it takes some time on some devices to get the
     * geolocation. login action is not dispatched untill the geolocation is
     * received and nothing happens on the screen after the user has pressed
     * the login button hence i've added this state to show the spinner as soon
     * as soon as the button is pressed.
     */
    state = {
        whereTo: 'inprogress',
        reset: false,
        atForgot: false,
        showSpinner: false
    };

    componentDidMount() {
        this.props.resetAuthState();
        retrieveToken()
            .then(token => {
                // this.props.checkCompliance({ token });
                this.props.fetchUser(token);
            })
            .catch(err => this.setState({ whereTo: 'login' }));
    }

    static getDerivedStateFromProps(nextProps, nextState) {
        const { user } = nextProps;
        if (user !== 'inprogress') {
            return { whereTo: 'login', showSpinner: false };
        }
        if (user) {
            if (user.code === 200 || user.code === 201) {
                return { showSpinner: false };
            } else if (user.code >= 300) {
                return { showSpinner: false };
            }
        }
        if (typeof user === 'string' && user !== 'inprogress' && user !== '') {
            return { showSpinner: false };
        }

        return null;
    }

    onResetClick = () => {
        this.setState({ reset: !this.state.reset });
    };

    setForgotState = bool => {
        this.setState({ atForgot: bool });
    };

    setShowSpinner = showSpinner => this.setState({ showSpinner });

    render() {
        return (
            <ImageBackground
                source={require('../../assets/images/loginBackground.png')}
                style={[globalStyles.flexContainer]}
            >
                <KeyboardAwareScrollView
                    style={{
                        height: Dimensions.get('window').height
                    }}
                >
                    <View
                        style={[
                            globalStyles.flexHCenter,
                            globalStyles.flexVBottom,
                            {
                                height:
                                    Dimensions.get('window').height -
                                    (os() ? 0 : 24)
                            }
                        ]}
                    >
                        <View style={[loginStyles.container]}>
                            <LoginBody
                                {...this.props}
                                setShowSpinner={this.setShowSpinner}
                                onResetClick={this.onResetClick}
                                loginRes={this.props.loginRes}
                                setForgotState={this.setForgotState}
                                forgotBool={this.state.atForgot}
                            />
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                {this.state.whereTo === 'inprogress' ||
                this.props.user === 'inprogress' ||
                this.state.reset === true ||
                this.state.showSpinner ? (
                    <OverlayIndicator />
                ) : null}
            </ImageBackground>
        );
    }
}

const mapStateToProps = ({ user }) => ({
    user
});

const mapDispatchToProps = dispatch => ({
    resetAuthState: () => dispatch(reset()),
    fetchUser: data => dispatch(fetchUser(data)),
    loginUser: data => dispatch(loginUserAction(data)),
    checkCompliance: data => dispatch(checkComplianceAction(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    alertOnError(LoginScreen, [
        { propName: 'user', actionOnFailure: 'resetAuthState' }
    ])
);
