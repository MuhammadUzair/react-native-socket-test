import React, { Component } from 'react';
import {
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Alert,
    BackHandler,
    Platform
} from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import DeviceInfo from 'react-native-device-info';

import { globalStyles, loginStyles } from '../../assets/styles';
import { BASEURL_DEV } from '../../redux/epics/base-url';
import {
    loginFB,
    loginGoogle,
    onboard,
    retrieveNotificationTokenAndOS,
    getLocation,
    runLocationValidations
} from '../../utils';

class LoginBody extends Component {
    constructor(props) {
        super(props);
        this.state = {
            forgot: false,
            email: '',
            password: '',
            showSpinner: false,
            device: {
                device_token: '',
                device_type: Platform.OS == 'android' ? 'android' : 'ios',
                device_id: DeviceInfo.getUniqueID()
            }
        };
        this.handler = (() => {
            const toReturn = this.state.forgot && true;
            this.setState({ forgot: false });
            this.props.setForgotState(false);
            return toReturn;
        }).bind(this);

        BackHandler.addEventListener('hardwareBackPress', this.handler);
    }

    componentDidUpdate(prevProps, prevState) {
        const { user, setShowSpinner, navigation } = this.props;
        if (user) {
            if (user.code === 200 || user.code === 201) {
                onboard(user.data, navigation);
            } else if (user.code >= 300) {
                setShowSpinner(false);
            }
        }
    }

    getFcmToken = async () => {
        await retrieveNotificationTokenAndOS().then(res => {
            if (res && res.token && res.os) {
                let device = {
                    ...this.state.device,
                    device_token: res.token,
                    device_type: res.os
                };
                this.setState({ device });
            }
        });
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handler);
    }

    _onSignupPress() {
        this.props.navigation.navigate('Signup');
    }

    geolocationSuccess = async ({
        coords: { longitude, latitude, altitude } = {
            longitude: null,
            latitude: null,
            altitude: null
        }
    }) => {
        const { email, password } = this.state;
        const { loginUser } = this.props;

        const data = {
            email,
            password,
            longitude,
            latitude,
            altitude,
            device_token: this.state.device.device_token,
            device_type: this.state.device.device_type,
            device_id: this.state.device.device_id
        };
        loginUser(data);
    };

    /* geolocationError = e => {
        this.props.setShowSpinner(false);
        alert(e.message);
    }; */

    dispatchLoginAction = async () => {
        this.props.setShowSpinner(true);
        getLocation(this.geolocationSuccess, this.geolocationSuccess);
    };

    _onLoginPress = async () => {
        const { email, password, device } = this.state;
        try {
            await this.getFcmToken();
            if (!email.trim().length && !password.length)
                throw new Error('Enter your Email or Username and Password.');
            if (!email.length) throw new Error('Enter Email or Username.');
            if (!password.length) throw new Error('Enter Password.');
            runLocationValidations(this.dispatchLoginAction, true);
        } catch (e) {
            this.props.setShowSpinner(false);
            Alert.alert('There was a problem', e.message);
        }
    };

    onResetPress = () => {
        const { email } = this.state;
        try {
            if (!email.trim().length) throw new Error('Enter correct email.');

            this.props.onResetClick();
            const data = { email };
            const headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(data)
            };

            const req = fetch(`${BASEURL_DEV}/api/password/email`, options)
                .then(res => res.json())
                .then(async res => {
                    if (res.code === 200) {
                        this.setState({ forgot: false });
                        this.setState({ email: '' });
                        this.props.onResetClick();
                        this.props.setForgotState(false);
                    } else {
                        Alert.alert('Error', res.messages[0]);
                        this.props.onResetClick();
                        this.props.setForgotState(true);
                        BackHandler.addEventListener(
                            'hardwareBackPress',
                            this.handler
                        );
                    }
                });
        } catch (e) {
            Alert.alert('', e.message);
        }
    };

    onPressForgot = () => {
        this.setState({ forgot: true });
        this.props.setForgotState(true);
    };

    onPressBack = () => {
        this.setState({ forgot: false });
    };

    renderForgotComponents = () => {
        if (!this.props.forgotBool) {
            return (
                <View>
                    <View
                        style={[
                            globalStyles.flexHCenter,
                            globalStyles.highlightBorder,
                            globalStyles.flexHorizontal,
                            loginStyles.inputContainer,
                            loginStyles.bottomSpacing
                        ]}
                    >
                        <TextInput
                            placeholder="Enter your Password"
                            autoCapitalize="none"
                            autoCorrect={false}
                            clearButtonMode="always"
                            keyboardType="default"
                            style={[
                                globalStyles.fontFace,
                                globalStyles.subTextFontSize,
                                loginStyles.passwordField
                            ]}
                            secureTextEntry={true}
                            value={this.state.password}
                            onChangeText={password =>
                                this.setState({ password })
                            }
                            underlineColorAndroid="rgba(0,0,0,0)"
                        />
                        <TouchableOpacity
                            onPress={this.onPressForgot}
                            activeOpacity={0.5}
                        >
                            <Text
                                style={[
                                    globalStyles.fontFace,
                                    globalStyles.subTextFontSize,
                                    globalStyles.boldFontFace,
                                    globalStyles.highlightColor
                                ]}
                            >
                                Forgot?
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View
                        style={[
                            globalStyles.flexSpaceBetween,
                            loginStyles.bottomSpacing,
                            { alignItems: 'flex-end' }
                        ]}
                    >
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={this._onSignupPress.bind(this)}
                        >
                            <Text
                                style={[
                                    globalStyles.fontFace,
                                    globalStyles.baseFontSize,
                                    globalStyles.boldFontFace
                                ]}
                            >
                                Signup for Free
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <TouchableOpacity
                            onPress={this._onLoginPress}
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHCenter,
                                loginStyles.bottomSpacing
                            ]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    loginStyles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        loginStyles.bigPrimaryButtonText
                                    ]}
                                >
                                    Login Now
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>

                    <View
                        style={[
                            globalStyles.flexHCenter,
                            loginStyles.bottomSpacing
                        ]}
                    >
                        <Text
                            style={[
                                globalStyles.fontFace,
                                globalStyles.baseFontSize,
                                loginStyles.bottomSpacing,
                                globalStyles.baseGreyColor
                            ]}
                        >
                            or
                        </Text>

                        <View
                            style={[
                                globalStyles.flexHorizontal,
                                loginStyles.lgBottomSpacing
                            ]}
                        >
                            <TouchableOpacity
                                activeOpacity={0.5}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.highlightBorder,
                                    loginStyles.secondaryButton
                                ]}
                                onPress={async () => {
                                    await this.getFcmToken();
                                    loginGoogle(this.state.device);
                                }}
                            >
                                <Icon
                                    name="google"
                                    type="font-awesome"
                                    color="#e6452d"
                                    containerStyle={{ marginRight: 5 }}
                                />
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    Login with Google
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={0.5}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.highlightBorder,
                                    loginStyles.secondaryButton
                                ]}
                                onPress={async () => {
                                    await this.getFcmToken();
                                    loginFB(this.state.device);
                                }}
                            >
                                <Icon
                                    name="facebook-f"
                                    type="font-awesome"
                                    color="#3695e3"
                                    containerStyle={{ marginRight: 5 }}
                                />
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.subTextFontSize
                                    ]}
                                >
                                    Login with Facebook
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        }

        return (
            <View>
                <Text style={[globalStyles.baseFontSize, loginStyles.resetMsg]}>
                    Enter your Email, We'll send you the Reset Link.
                </Text>
                <View style={[loginStyles.bottomSpacing]}>
                    <View style={loginStyles.lgBottomSpacing}>
                        <TouchableOpacity
                            onPress={this.onResetPress}
                            activeOpacity={0.5}
                            style={[
                                globalStyles.flexHCenter,
                                loginStyles.bottomSpacing
                            ]}
                        >
                            <LinearGradient
                                colors={['#4d6bc2', '#3695e3']}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={[
                                    globalStyles.flexHorizontal,
                                    globalStyles.flexHCenter,
                                    globalStyles.flexSpaceBetween,
                                    loginStyles.bigPrimaryButton
                                ]}
                            >
                                <Text
                                    style={[
                                        globalStyles.fontFace,
                                        globalStyles.whiteColor,
                                        loginStyles.bigPrimaryButtonText
                                    ]}
                                >
                                    Reset Password
                                </Text>
                                <Icon color="#FFF" name="arrow-forward" />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    };

    render() {
        return (
            <View>
                <View
                    style={[
                        globalStyles.highlightBorder,
                        loginStyles.inputContainer,
                        loginStyles.bottomSpacing
                    ]}
                >
                    <TextInput
                        placeholder={
                            this.props.forgotBool
                                ? 'Enter Your Email or Username'
                                : 'Enter Your Email or Username'
                        }
                        autoCapitalize="none"
                        autoCorrect={false}
                        clearButtonMode="always"
                        keyboardType="email-address"
                        style={[
                            globalStyles.fontFace,
                            globalStyles.subTextFontSize,
                            globalStyles.fullWidth,
                            loginStyles.emailField
                        ]}
                        value={this.state.email}
                        onChangeText={email =>
                            this.setState({ email: email.trim() })
                        }
                        underlineColorAndroid="rgba(0,0,0,0)"
                    />
                </View>
                {this.renderForgotComponents()}
            </View>
        );
    }
}

// const mapDispatchToProps = dispatch => ({
//     loginUser: data => dispatch(loginUserAction(data))
// });

// export default connect(
//     null,
//     mapDispatchToProps
// )(LoginBody);

export default LoginBody;
