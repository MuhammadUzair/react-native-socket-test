import {
    SEARCH_MY_COMPETITIONS,
    SEARCH_MY_COMPETITIONS_FAILED,
    SEARCH_MY_COMPETITIONS_SUCCESSFUL,
    RESET_SEARCH_MY_COMPETITIONS
} from '../epics/search-my-competition-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { competitions: [] } }, action) => {
    switch (action.type) {
        case SEARCH_MY_COMPETITIONS:
            return { status: 'inprogress', data: { competitions: [] } };
        case SEARCH_MY_COMPETITIONS_SUCCESSFUL: {
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...state.data,
                    ...action.data.data,
                    competitions: [
                        ...state.data.competitions,
                        ...action.data.data.competitions
                    ]
                }
            };
        }
        case SEARCH_MY_COMPETITIONS_FAILED:
            return {
                ...action.data,
                status: 'failed',
                data: { competitions: [] }
            };
        case ALL_RESET:
            return { status: '', data: { competitions: [] } };
        case RESET_SEARCH_MY_COMPETITIONS:
            return { ...state, status: '', messages: [], code: null };
        default:
            return state;
    }
};
