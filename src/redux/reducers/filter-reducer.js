import { APPLY_FILTER, RESET_FILTER } from '../epics/filter-action';
const initialState = {
    fType: '',
    tType: '',
    toDate: '',
    fromDate: ''
};

export default (state = initialState, action) => {
    switch (action.type) {
        case APPLY_FILTER:
            const { fType, tType, toDate, fromDate } = action.data;
            return { fType, tType, toDate, fromDate };
        case RESET_FILTER:
            return initialState;
        default:
            return state;
    }
};
