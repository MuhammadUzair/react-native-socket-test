import {
    SIGNUP,
    SIGNUP_SUCCESSUFL,
    SIGNUP_FAILED,
    RESET
} from '../epics/signup-epic';

import {
    SOCIAL_SIGNUP,
    SOCIAL_SIGNUP_SUCCESSFUL,
    SOCIAL_SIGNUP_FAILED
} from '../epics/social-signup-epic';

import {
    FETCH_USER,
    FETCH_USER_SUCCESSFUL,
    FETCH_USER_FAILED
} from '../epics/fetch-user-epic';

import { LOGIN, LOGIN_SUCCESSUFL, LOGIN_FAILED } from '../epics/login-epic';
import { ALL_RESET } from '../epics/allReset-epic';
export default (state = '', action) => {
    switch (action.type) {
        case RESET:
        case ALL_RESET:
            return '';
        case LOGIN:
        case SIGNUP:
        case SOCIAL_SIGNUP:
        case FETCH_USER:
            return state.data !== undefined ? state : 'inprogress';
        case LOGIN_SUCCESSUFL:
        case SIGNUP_SUCCESSUFL:
        case SOCIAL_SIGNUP_SUCCESSFUL:
        case FETCH_USER_SUCCESSFUL:
            return action.data;
        case LOGIN_FAILED:
        case SIGNUP_FAILED:
        case SOCIAL_SIGNUP_FAILED:
        case FETCH_USER_FAILED:
            return action.data;
        default:
            return state;
    }
};
