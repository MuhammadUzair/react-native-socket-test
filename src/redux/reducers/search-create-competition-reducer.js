import {
    SEARCH_CREATE_COMPETITIONS,
    SEARCH_CREATE_COMPETITIONS_SUCCESSFUL,
    SEARCH_CREATE_COMPETITIONS_FAILED,
    RESET_SEARCH_CREATE_COMPETITIONS,
    SEARCH_CREATE_COMPETITIONS_PAGE,
    SEARCH_CREATE_COMPETITIONS_PAGE_SUCCESSFUL,
} from '../epics/search-create-competition-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { competitions: [] } }, action) => {
    switch (action.type) {
        case SEARCH_CREATE_COMPETITIONS:
            return { data: { competitions: [] }, status: 'inprogress' };
        case SEARCH_CREATE_COMPETITIONS_PAGE:
            return { ...state, status: 'inprogress' };
        case SEARCH_CREATE_COMPETITIONS_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...action.data.data,
                    competitions: [...action.data.data.competitions],
                },
            };
        case SEARCH_CREATE_COMPETITIONS_PAGE_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...state.data,
                    ...action.data.data,
                    competitions: [
                        ...state.data.competitions,
                        ...action.data.data.competitions,
                    ],
                },
            };
        case SEARCH_CREATE_COMPETITIONS_FAILED:
            return {
                ...action.data,
                status: 'failed',
                data: { competitions: [] },
            };
        case ALL_RESET:
            return { status: '', data: { competitions: [] } };
        case RESET_SEARCH_CREATE_COMPETITIONS:
            return { ...state, status: '', messages: [], code: null };
        default:
            return state;
    }
};
