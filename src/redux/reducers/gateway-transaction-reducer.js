import {
    FETCH_GATEWAY_TRANSACTIONS,
    FETCH_GATEWAY_TRANSACTIONS_FAILED,
    FETCH_GATEWAY_TRANSACTIONS_SUCCESSFUL,
    REFRESH_GATEWAY_TRANSACTIONS,
    REFRESH_GATEWAY_TRANSACTIONS_SUCCESSFUL,
    RESET_GATEWAY_TRANSACTIONS
} from '../epics/gateway-transaction-epic';
import { ALL_RESET } from '../epics/allReset-epic';

const initialState = {
    status: '',
    code: null,
    data: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GATEWAY_TRANSACTIONS: {
            if (state.data[action.data.filter] === undefined) {
                return {
                    ...state,
                    status: 'inprogress',
                    code: null,
                    filter: action.data.filter,
                    data: {
                        ...state.data,
                        [action.data.filter]: { transactions: [] }
                    }
                };
            }
            return { ...state, status: 'inprogress', code: null };
        }
        case FETCH_GATEWAY_TRANSACTIONS_SUCCESSFUL: {
            return {
                ...state,
                status: 'successful',
                ...action.data,
                data: {
                    ...state.data,
                    [action.data.filter]: {
                        ...action.data.data,
                        transactions: [
                            ...state.data[action.data.filter].transactions,
                            ...action.data.data.transactions
                        ]
                    }
                }
            };
        }
        case FETCH_GATEWAY_TRANSACTIONS_FAILED: {
            return {
                ...state,
                status: 'failed',
                ...action.data,
                data: { ...state.data }
            };
        }
        case REFRESH_GATEWAY_TRANSACTIONS: {
            if (state.data[action.data.filter] === undefined) {
                return {
                    ...state,
                    status: 'refreshing',
                    data: { [action.data.filter]: { transactions: [] } },
                    code: null
                };
            }
            return {
                ...state,
                status: 'refreshing',
                code: null
            };
        }
        case REFRESH_GATEWAY_TRANSACTIONS_SUCCESSFUL: {
            return {
                ...state,
                status: 'successful',
                ...action.data,
                data: { [action.data.filter]: { ...action.data.data } }
            };
        }
        case RESET_GATEWAY_TRANSACTIONS: {
            return {
                ...state,
                code: null,
                status: '',
                data: { ...state.data }
            };
        }
        case ALL_RESET:
            return initialState;
        default:
            return state;
    }
};
