import {
    LOGOUT_REQUEST,
    LOGOUT_REQUEST_SUCCESSFUL,
    LOGOUT_REQUEST_FAILED
} from '../epics/logout-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '' }, action) => {
    switch (action.type) {
        case ALL_RESET:

        case LOGOUT_REQUEST:
            return { status: '' };
        case LOGOUT_REQUEST_FAILED:
            return { status: 'failed', ...action.data };
        case LOGOUT_REQUEST_SUCCESSFUL:
            return {
                status: 'successful',
                ...action.data
            };
        default:
            return state;
    }
};
