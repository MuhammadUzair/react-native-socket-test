import {
    FETCH_NOTIFICATIONS,
    FETCH_NOTIFICATIONS_SUCCESSFUL,
    FETCH_NOTIFICATIONS_FAILED,
    RESET_FETCH_NOTIFICATIONS,
    REFRESH_FETCH_NOTIFICATIONS_SUCCESSFUL
} from '../epics/fetch-notifications';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [] }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_NOTIFICATIONS:
            return { status: '', data: [] };
        case FETCH_NOTIFICATIONS:
            return {
                status: 'inprogress',
                ...action.data,
                data: [...state.data]
            };
        case FETCH_NOTIFICATIONS_SUCCESSFUL:
            return {
                status: 'successful',
                ...action.data,
                data: [...state.data, ...action.data.data]
            };
        case REFRESH_FETCH_NOTIFICATIONS_SUCCESSFUL:
            return {
                status: 'successful',
                ...action.data
            };
        case FETCH_NOTIFICATIONS_FAILED:
            return { status: 'failed', ...action.data };
        default:
            return state;
    }
};
