import {
    FETCH_TRANSACTIONS,
    FETCH_TRANSACTIONS_SUCCESSFUL,
    FETCH_TRANSACTIONS_FAILED,
    RESET_FETCH_TRANSACTIONS,
    REFRESH_TRANSACTIONS,
    REFRESH_TRANSACTIONS_SUCCESSFUL,
} from '../epics/fetch-transactions-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { wallet: [] } }, action) => {
    switch (action.type) {
        case FETCH_TRANSACTIONS:
            return {
                ...state,
                status: 'inprogress',
                code: null,
                data: {
                    wallet: action.data.nextPage ? [...state.data.wallet] : [],
                },
            };
        case FETCH_TRANSACTIONS_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...action.data.data,
                    wallet: [...state.data.wallet, ...action.data.data.wallet],
                },
            };
        case FETCH_TRANSACTIONS_FAILED:
            return {
                ...state,
                ...action.data,
                status: 'failed',
                data: { ...state.data },
            };
        case REFRESH_TRANSACTIONS:
            return {
                ...state,
                status: 'refreshing',
                code: null,
            };
        case REFRESH_TRANSACTIONS_SUCCESSFUL:
            return {
                ...state,
                status: 'successful',
                data: { ...action.data.data },
            };
        case RESET_FETCH_TRANSACTIONS:
            return {
                ...state,
                code: null,
                messages: [],
            };
        case ALL_RESET:
            return { status: '', data: { wallet: [] } };
        default:
            return state;
    }
};
