import {
    PROFILE_SUBMIT,
    PROFILE_SUBMIT_SUCCESSFUL,
    PROFILE_SUBMIT_FAILED,
    RESET_PROFILE_SUBMIT_FAILED
} from '../epics/profile-submit';

export default (state = '', action) => {
    switch (action.type) {
        case PROFILE_SUBMIT:
            return 'inprogress';
        case PROFILE_SUBMIT_SUCCESSFUL:
            return action.data;
        case PROFILE_SUBMIT_FAILED:
            return action.data;
        case RESET_PROFILE_SUBMIT_FAILED: {
            return '';
        }
        default:
            return state;
    }
};
