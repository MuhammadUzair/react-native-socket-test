const initState = {
    serviceFee: 0.03
};

export default (state = initState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
