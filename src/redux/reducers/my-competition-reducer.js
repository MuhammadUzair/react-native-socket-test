import {
    FETCH_MY_COMPETITIONS,
    FETCH_MY_COMPETITIONS_FAILED,
    FETCH_MY_COMPETITIONS_SUCCESSFUL,
    REFRESH_MY_COMPETITIONS,
    REFRESH_MY_COMPETITIONS_FAILED,
    REFRESH_MY_COMPETITIONS_SUCCESSFUL,
    RESET_MY_COMPETITIONS
} from '../epics/fetch-my-competition-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: {} }, action) => {
    switch (action.type) {
        case FETCH_MY_COMPETITIONS:
            if (state.data[action.data.tab] === undefined) {
                return {
                    ...state,
                    status: 'inprogress',
                    code: null,
                    data: {
                        ...state.data,
                        [action.data.tab]: { competitions: [] }
                    }
                };
            }
            return { ...state, code: null, status: 'inprogress' };
        case REFRESH_MY_COMPETITIONS:
            return {
                ...state,
                code: null,
                status: 'refreshing'
            };
        case FETCH_MY_COMPETITIONS_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...state.data,
                    [action.data.tab]: {
                        ...action.data.data,
                        competitions: [
                            ...state.data[action.data.tab].competitions,
                            ...action.data.data.competitions
                        ]
                    }
                }
            };
        case REFRESH_MY_COMPETITIONS_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...state.data,
                    [action.data.tab]: { ...action.data.data }
                }
            };
        case REFRESH_MY_COMPETITIONS_FAILED:
        case FETCH_MY_COMPETITIONS_FAILED:
            return {
                ...state,
                ...action.data,
                status: 'failed',
                data: {
                    ...state.data
                }
            };
        case ALL_RESET:
            return { status: '', data: {} };
        case RESET_MY_COMPETITIONS:
            return { ...state, status: '', messages: [], code: null };
        default:
            return state;
    }
};
