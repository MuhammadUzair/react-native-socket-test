import { combineReducers } from 'redux';

import authReducer from './auth-reducer';
import leaguesReducer from './leagues-reducer';
import addLeaguesReducer from './add-leagues-reducer';
import teamsReducer from './teams-reducer';
import addTeamsReducer from './add-teams-reducer';
import friendsReducer from './friends-reducer';
import addFriendsReducer from './add-friends-reducer';
import fbFriendsReducer from './fb-friends-reducer';
import competitionsReducer from './competitions-reducer';
import searchCompetitionsReducer from './search-competitions-reducer';
import createCompetitionsReducer from './create-competitions-reducer';
import adminSettingsReducer from './admin-settings-reducer';
import postCompetitionReducer from './post-competition-reducer';
import playCompetitionReducer from './play-competition-reducer';
import searchCreateCompetitionReducer from './search-create-competition-reducer';
import myCompetitionsReducer from './my-competition-reducer';
import searchMyCompetitionsReducer from './search-my-competition-reducer';
import myGroupsReducer from './my-groups-reducer';
import groupMembersReducer from './group-members-reducer';
import leaveGroupReducer from './leave-group-reducer';
import myFriendsReducer from './my-friends-reducer';
import removeFriendsReducer from './remove-friends-reducer';
import joinGroupReducer from './join-group-reducer';
import groupActionReducer from './group-action-reducer';
import friendsPendingReceiveRequestsReducer from './friends-pending-receive-requests-reducer';
import friendsPendingSentRequestsReducer from './friends-pending-sent-requests-reducer';
import acceptFriendRequestReducer from './accept-friend-request-reducer';
import cancelFriendRequestReducer from './cancel-friend-request-reducer';
import searchMyFriendsReducer from './search-my-friends-reducer';
import createGroupReducer from './create-group-reducer';
import allFriendsReducer from './all-friends-reducer';
import editGroupReducer from './edit-group-reducer';
import deleteGroupReducer from './delete-group-reducer';
import transactionsReducer from './transactions-reducer';
import filterReducer from './filter-reducer';
import fetchStateReducer from './fetch-state';
import fetchCitiesReducer from './fetch-cities';
import profileSubmitReducer from './profile-submit';
import uploadImageReducer from './upload-image';
import logoutReducer from './logout-reducer';
import notificationsReducer from './notifications-reducer';
import fetchSpacificCompetitionReducer from './fetch-spacific-competition';
import getNotificationReducer from './get-notification';
import gatewayTransactionReducer from './gateway-transaction-reducer';
import complianceReducer from './compliance-reducer';
import tsevoCheckCompliancesReducer from './tsevo-check_compliances-reducer';
import competitionGroupsReducer from './competition-groups-reducer';
import inviteFriendsReducer from './invite-friends-reducer';
import getInviteFriendsReducer from './get-friend-invites-reducer';

export default combineReducers({
    user: authReducer,
    leagues: leaguesReducer,
    leaguesAdded: addLeaguesReducer,
    teams: teamsReducer,
    teamsAdded: addTeamsReducer,
    friends: friendsReducer,
    friendsAdded: addFriendsReducer,
    fbFriends: fbFriendsReducer,
    competitions: competitionsReducer,
    searchedCompetitions: searchCompetitionsReducer,
    createCompetitions: createCompetitionsReducer,
    searchedCreateCompetition: searchCreateCompetitionReducer,
    settings: adminSettingsReducer,
    competitionPosted: postCompetitionReducer,
    competitionPlayed: playCompetitionReducer,
    myCompetitions: myCompetitionsReducer,
    searchedMyCompetitions: searchMyCompetitionsReducer,
    myGroups: myGroupsReducer,
    groupMembers: groupMembersReducer,
    groupLeft: leaveGroupReducer,
    myFriends: myFriendsReducer,
    removeFriendsReducer: removeFriendsReducer,
    groupJoined: joinGroupReducer,
    groupAction: groupActionReducer,
    friendsPendingReceiveRequests: friendsPendingReceiveRequestsReducer,
    friendsPendingSentRequests: friendsPendingSentRequestsReducer,
    acceptFriendRequestApiRes: acceptFriendRequestReducer,
    cancelFriendRequestApiRes: cancelFriendRequestReducer,
    searchedMyFriends: searchMyFriendsReducer,
    groupCreated: createGroupReducer,
    groupEdited: editGroupReducer,
    allFriends: allFriendsReducer,
    groupDeleted: deleteGroupReducer,
    transactions: transactionsReducer,
    filters: filterReducer,
    state: fetchStateReducer,
    cities: fetchCitiesReducer,
    profile: profileSubmitReducer,
    uploadImage: uploadImageReducer,
    logout: logoutReducer,
    notifications: notificationsReducer,
    searchCompetition: fetchSpacificCompetitionReducer,
    getNotification: getNotificationReducer,
    gatewayTransactions: gatewayTransactionReducer,
    compliance: complianceReducer,
    tsevoCheckCompliances: tsevoCheckCompliancesReducer,
    competitionGroups: competitionGroupsReducer,
    inviteFriends: inviteFriendsReducer,
    inviteFriendsDetails: getInviteFriendsReducer
});
