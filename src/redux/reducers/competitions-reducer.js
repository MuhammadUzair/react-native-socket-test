import {
    FETCH_COMPETITIONS,
    FETCH_COMPETITIONS_FAILED,
    FETCH_COMPETITIONS_SUCCESSFUL,
    RESET_FETCH_COMPETITIONS,
    REFRESH_COMPETITIONS,
    REFRESH_COMPETITIONS_SUCCESSFUL,
    REFRESH_COMPETITIONS_FAILED
} from '../epics/fetch-competitions-epic';

import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: {} }, action) => {
    switch (action.type) {
        case FETCH_COMPETITIONS:
            if (state.data[action.data.league] === undefined)
                return {
                    ...state,
                    code: null,
                    status: 'inprogress',
                    league: action.data.league,
                    data: {
                        ...state.data,
                        [action.data.league]: { competitions: [] }
                    }
                };
            else return { ...state, code: null, status: 'inprogress' };
        case REFRESH_COMPETITIONS:
            return { ...state, code: null, status: 'refreshing' };
        case FETCH_COMPETITIONS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                league: action.data.league,
                data: {
                    ...state.data,
                    [action.data.league]: {
                        ...action.data.data,
                        competitions: [
                            ...(state.data[action.data.league]
                                ? state.data[action.data.league].competitions
                                : []),
                            ...(action.data.data
                                ? action.data.data.competitions
                                : [])
                        ]
                    }
                }
            };
        case REFRESH_COMPETITIONS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                league: action.data.league,
                data: {
                    ...state.data,
                    [action.data.league]: {
                        ...action.data.data
                        // competitions: [...action.data.data.competitions]
                    }
                }
            };
        case REFRESH_COMPETITIONS_FAILED:
        case FETCH_COMPETITIONS_FAILED:
            return {
                ...action.data,
                status: 'failed',
                league: action.data.league,
                data: {
                    ...state.data,
                    [action.data.league]: {
                        ...state.data[action.data.league],
                        competitions: [
                            ...(state.data[action.data.league]
                                ? state.data[action.data.league].competitions
                                : [])
                        ]
                    }
                }
            };
        case ALL_RESET:
            return { status: '', data: {} };
        case RESET_FETCH_COMPETITIONS:
            return { ...state, status: '', messages: [], code: null };
        default:
            return state;
    }
};
