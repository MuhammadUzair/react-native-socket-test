import {
    ADD_FRIENDS,
    ADD_FRIENDS_SUCCESSFUL,
    ADD_FRIENDS_FAILED,
    RESET_ADD_FRIENDS
} from '../epics/add-friends-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case ALL_RESET:

        case RESET_ADD_FRIENDS:
            return '';
        case ADD_FRIENDS:
            return 'inprogress';
        case ADD_FRIENDS_SUCCESSFUL:
            return action.data;
        case ADD_FRIENDS_FAILED:
            return action.data;
        default:
            return state;
    }
};
