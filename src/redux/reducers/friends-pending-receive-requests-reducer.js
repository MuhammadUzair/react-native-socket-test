import {
    FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS,
    FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_SUCCESSFUL,
    FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_FAILED,
    RESET_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS,
    REFRESH_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS
} from '../epics/fetch-friends-pending-receive-requests-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [], count: 0 }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS:
            return { status: '', data: [] };
        case FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS: {
            return {
                status: 'inprogress',
                ...action.data,
                data: state.data,
                count: state.count
            };
        }

        case FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_SUCCESSFUL: {
            return {
                status: 'successful',
                ...action.data,
                data: [...state.data, ...action.data.data.friends],
                count: action.data.data.total_records
            };
        }

        case REFRESH_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS: {
            return {
                status: 'successful',
                data: action.data.data.friends,
                count: action.data.data.total_records
            };
        }

        case FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_FAILED:
            return { status: 'failed', ...action.data };
        default:
            return state;
    }
};
