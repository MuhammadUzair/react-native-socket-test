import {
    LOGIN,
    LOGIN_SUCCESSUFL,
    LOGIN_FAILED,
    RESET
} from '../epics/login-epic';

import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case RESET:
        case ALL_RESET:
            return '';
        case LOGIN:
            return 'inprogress';
        case LOGIN_SUCCESSUFL:
            return action.data;
        case LOGIN_FAILED:
            return action.data;
        default:
            return state;
    }
};
