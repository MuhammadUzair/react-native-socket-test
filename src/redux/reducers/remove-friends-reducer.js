import {
    REMOVE_FRIENDS,
    REMOVE_FRIENDS_SUCCESSFUL,
    REMOVE_FRIENDS_FAILED
} from '../epics/remove-friends-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case REMOVE_FRIENDS:
            return 'inprogress';
        case REMOVE_FRIENDS_SUCCESSFUL:
            return action.data;
        case REMOVE_FRIENDS_FAILED:
            return action.data;
        default:
            return state;
    }
};
