import {
    FETCH_MY_GROUPS,
    FETCH_MY_GROUPS_FAILED,
    FETCH_MY_GROUPS_SUCCESSFUL,
    RESET_FETCH_MY_GROUPS,
    REFRESH_MY_GROUPS,
    REFRESH_MY_GROUPS_SUCCESSFUL,
    REFRESH_MY_GROUPS_FAILED
} from '../epics/fetch-my-groups-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: {} }, action) => {
    switch (action.type) {
        case FETCH_MY_GROUPS:
            if (state.data[action.data.group] === undefined)
                return {
                    ...state,
                    code: null,
                    status: 'inprogress',
                    data: {
                        ...state.data,
                        [action.data.group]: { total_records: 0, groups: [] }
                    }
                };
            return { ...state, code: null, status: 'inprogress' };
        case REFRESH_MY_GROUPS:
            return { ...state, code: null, status: 'refreshing' };
        case FETCH_MY_GROUPS_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...state.data,
                    [action.data.group]: {
                        ...action.data.data,
                        groups: [
                            ...state.data[action.data.group].groups,
                            ...action.data.data.groups
                        ]
                    }
                }
            };
        case REFRESH_MY_GROUPS_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...state.data,
                    [action.data.group]: {
                        ...action.data.data
                    }
                }
            };
        case FETCH_MY_GROUPS_FAILED:
            return {
                ...state,
                ...action.data,
                status: 'failed',
                data: { ...state.data }
            };
        case REFRESH_MY_GROUPS_FAILED:
            return {
                ...state,
                ...action.data,
                status: 'failed',
                data: { ...state.data }
            };
        case RESET_FETCH_MY_GROUPS:
            return { ...state, status: '', code: null, messages: [] };
        case ALL_RESET:
            return { status: '', data: {} };

        default:
            return state;
    }
};
