import {
    PLAY_COMPETITION,
    PLAY_COMPETITION_FAILED,
    PLAY_COMPETITION_SUCCESSFUL,
    RESET_PLAY_COMPETITION
} from '../epics/play-competition-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case PLAY_COMPETITION:
            return 'inprogress';
        case PLAY_COMPETITION_FAILED:
        case PLAY_COMPETITION_SUCCESSFUL:
            return action.data;
        case ALL_RESET:
        case RESET_PLAY_COMPETITION:
            return '';
        default:
            return state;
    }
};
