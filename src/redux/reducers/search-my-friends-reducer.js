import {
    SEARCH_MY_FRIENDS,
    SEARCH_MY_FRIENDS_SUCCESSFUL,
    SEARCH_MY_FRIENDS_FAILED,
    RESET_SEARCH_MY_FRIENDS
} from '../epics/search-my-friends-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { friends: [] } }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_SEARCH_MY_FRIENDS:
            return { status: '', data: { friends: [] }, code: null };
        case SEARCH_MY_FRIENDS:
            return {
                status: 'inprogress',
                code: null,
                data: { friends: [] }
            };
        case SEARCH_MY_FRIENDS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                data: {
                    ...action.data.data,
                    friends: [
                        ...state.data.friends,
                        ...action.data.data.friends
                    ]
                }
            };
        case SEARCH_MY_FRIENDS_FAILED:
            return {
                ...action.data,
                status: 'failed',
                data: { ...state.data }
            };
        default:
            return state;
    }
};
