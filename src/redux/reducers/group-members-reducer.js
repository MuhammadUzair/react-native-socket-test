import {
    FETCH_GROUP_MEMBERS,
    FETCH_GROUP_MEMBERS_SUCCESSFUL,
    FETCH_GROUP_MEMBERS_FAILED,
    FETCH_GROUP_MEMBERS_PAGE,
    FETCH_GROUP_MEMBERS_PAGE_SUCCESSFUL,
    RESET_GROUP_MEMBERS
} from '../epics/fetch-group-members-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { members: [] } }, action) => {
    switch (action.type) {
        case FETCH_GROUP_MEMBERS:
            return {
                ...state,
                code: null,
                status: 'inprogress',
                data: { ...state.data, members: [...state.data.members] }
            };
        case FETCH_GROUP_MEMBERS_SUCCESSFUL:
            return { ...action.data, status: 'successful' };
        case FETCH_GROUP_MEMBERS_FAILED:
            return { ...action.data, data: { members: [] } };
        case FETCH_GROUP_MEMBERS_PAGE_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                data: {
                    ...state.data,
                    ...action.data.data,
                    members: [
                        ...state.data.members,
                        ...action.data.data.members
                    ]
                }
            };
        case RESET_GROUP_MEMBERS:
            return { ...state, status: '', code: null, data: { members: [] } };
        case ALL_RESET:
            return { status: '', data: { members: [] } };

        default:
            return state;
    }
};
