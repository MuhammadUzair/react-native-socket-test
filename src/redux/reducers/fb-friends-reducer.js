import {
    FETCH_FB_FRIENDS,
    FETCH_FB_FRIENDS_SUCCESSFUL,
    FETCH_FB_FRIENDS_FAILED,
    RESET_FETCH_FB_FRIENDS
} from '../epics/fb-friends-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [] }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_FB_FRIENDS:
            return { status: '', data: [] };
        case FETCH_FB_FRIENDS:
            return {
                status: 'inprogress',
                ...action.data,
                data: [...state.data]
            };
        case FETCH_FB_FRIENDS_SUCCESSFUL:
            return {
                status: 'successful',
                ...action.data,
                data: [...state.data, ...action.data.data]
            };
        case FETCH_FB_FRIENDS_FAILED:
            return { status: 'failed', ...action.data };
        default:
            return state;
    }
};
