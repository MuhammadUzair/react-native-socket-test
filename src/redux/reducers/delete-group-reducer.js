import {
    DELETE_GROUP,
    DELETE_GROUP_FAILED,
    DELETE_GROUP_SUCCESSFUL,
    RESET_DELETE_GROUP
} from '../epics/delete-group-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case DELETE_GROUP:
            return 'inprogress';
        case DELETE_GROUP_SUCCESSFUL:
            return action.data;
        case DELETE_GROUP_FAILED:
            return action.data;
        case RESET_DELETE_GROUP:
        case ALL_RESET:
            return '';
        default:
            return state;
    }
};
