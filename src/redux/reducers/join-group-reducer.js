import {
    JOIN_GROUP,
    JOIN_GROUP_SUCCESSFUL,
    JOIN_GROUP_FAILED,
    RESET_JOIN_GROUP
} from '../epics/join-group-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case JOIN_GROUP:
            return 'inprogress';
        case JOIN_GROUP_SUCCESSFUL:
        case JOIN_GROUP_FAILED:
            return action.data;
        case ALL_RESET:
        case RESET_JOIN_GROUP:
            return '';
        default:
            return state;
    }
};
