import {
    CHECK_COMPLIANCE,
    CHECK_COMPLIANCE_FAILED,
    CHECK_COMPLIANCE_SUCCESSFUL,
    RESET_CHECK_COMPLIANCE,
} from '../epics/compliance-epic';

import { ALL_RESET } from '../epics/allReset-epic';

export default (state = null, action) => {
    switch (action.type) {
        case CHECK_COMPLIANCE_SUCCESSFUL:
        case CHECK_COMPLIANCE_FAILED:
            return action.data;
        case ALL_RESET:
        case RESET_CHECK_COMPLIANCE:
            return null;
        default:
            return state;
    }
};
