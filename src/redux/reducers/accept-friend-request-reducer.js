import {
    ACCEPT_FRIEND_REQUEST,
    ACCEPT_FRIEND_REQUEST_SUCCESSFUL,
    ACCEPT_FRIEND_REQUEST_FAILED,
    RESET_ACCEPT_FRIEND_REQUEST
} from '../epics/accept-friend-request-epic';

export default (state = '', action) => {
    switch (action.type) {
        case ACCEPT_FRIEND_REQUEST:
            return 'inprogress';
        case ACCEPT_FRIEND_REQUEST_SUCCESSFUL:
            return action.data;
        case ACCEPT_FRIEND_REQUEST_FAILED:
            return action.data;
        case RESET_ACCEPT_FRIEND_REQUEST:
            return { state: '' };
        default:
            return state;
    }
};
