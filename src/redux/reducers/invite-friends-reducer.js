import {
    INVITE_FRIENDS,
    INVITE_FRIENDS_SUCCESSFUL,
    INVITE_FRIENDS_FAILED,
    RESET_INVITE_FRIENDS
} from '../epics/invite-friends';

export default (state = '', action) => {
    switch (action.type) {
        case INVITE_FRIENDS:
            return 'inprogress';
        case INVITE_FRIENDS_SUCCESSFUL:
            return action.data;
        case INVITE_FRIENDS_FAILED:
            return action.data;
        case RESET_INVITE_FRIENDS: {
            return '';
        }
        default:
            return state;
    }
};
