import {
    FETCH_MY_FRIENDS,
    FETCH_MY_FRIENDS_SUCCESSFUL,
    FETCH_MY_FRIENDS_FAILED,
    RESET_FETCH_MY_FRIENDS,
    REFRESH_FETCH_MY_FRIENDS_SUCCESSFUL
} from '../epics/fetch-my-friends-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { friends: [] } }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_MY_FRIENDS:
            return { status: '', data: { friends: [] }, code: null };
        case FETCH_MY_FRIENDS:
            return {
                ...state,
                status: 'inprogress',
                code: null
            };
        case FETCH_MY_FRIENDS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                data: {
                    ...action.data.data,
                    friends: [
                        ...state.data.friends,
                        ...action.data.data.friends
                    ]
                }
            };
        case FETCH_MY_FRIENDS_FAILED:
            return {
                ...action.data,
                status: 'failed',
                data: { ...state.data }
            };
        case REFRESH_FETCH_MY_FRIENDS_SUCCESSFUL:
            return {
                status: 'successful',
                data: {
                    ...action.data.data,
                    friends: action.data.data.friends
                }
            };
        default:
            return state;
    }
};
