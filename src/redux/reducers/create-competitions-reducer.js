import {
    FETCH_CREATE_COMPETITIONS,
    FETCH_CREATE_COMPETITIONS_FAILED,
    FETCH_CREATE_COMPETITIONS_SUCCESSFUL,
    RESET_FETCH_CREATE_COMPETITIONS,
    REFRESH_CREATE_COMPETITIONS,
    REFRESH_CREATE_COMPETITIONS_SUCCESSFUL,
    REFRESH_CREATE_COMPETITIONS_FAILED
} from '../epics/fetch-create-competitions-epic';

import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: {} }, action) => {
    switch (action.type) {
        case FETCH_CREATE_COMPETITIONS:
            if (state.data[action.data.league] === undefined)
                return {
                    ...state,
                    code: null,
                    status: 'inprogress',
                    league: action.data.league,
                    data: {
                        ...state.data,
                        [action.data.league]: { competitions: [] }
                    }
                };
            else return { ...state, code: null, status: 'inprogress' };
        case REFRESH_CREATE_COMPETITIONS:
            return { ...state, code: null, status: 'refreshing' };
        case FETCH_CREATE_COMPETITIONS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                league: action.data.league,
                data: {
                    ...state.data,
                    [action.data.league]: {
                        ...action.data.data,
                        competitions: [
                            ...(state.data[action.data.league]
                                ? state.data[action.data.league].competitions
                                : []),
                            ...(action.data.data !== undefined &&
                            action.data.data.competitions !== undefined
                                ? action.data.data.competitions
                                : [])
                        ]
                    }
                }
            };
        case REFRESH_CREATE_COMPETITIONS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                league: action.data.league,
                data: {
                    ...state.data,
                    [action.data.league]: { ...action.data.data }
                }
            };
        case REFRESH_CREATE_COMPETITIONS_FAILED:
        case FETCH_CREATE_COMPETITIONS_FAILED:
            return {
                ...action.data,
                status: 'failed',
                league: action.data.league,
                data: {
                    ...state.data,
                    [action.data.league]: {
                        ...state.data[action.data.league],
                        competitions: [
                            ...(state.data[action.data.league]
                                ? state.data[action.data.league].competitions
                                : [])
                        ]
                    }
                }
            };
        case ALL_RESET:
        case RESET_FETCH_CREATE_COMPETITIONS:
            return { status: '', data: {} };
        default:
            return state;
    }
};
