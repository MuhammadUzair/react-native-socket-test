import {
    FETCH_INVITES_FRIENDS,
    FETCH_INVITES_FRIENDS_SUCCESSFUL,
    FETCH_INVITES_FRIENDS_FAILED,
    RESET_FETCH_INVITES_FRIENDS
} from '../epics/fetch-invites-friend';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [] }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_INVITES_FRIENDS:
            return { status: '', data: [] };
        case FETCH_INVITES_FRIENDS:
            return {
                ...state,
                status: 'inprogress',
                data: action.data
            };
        case FETCH_INVITES_FRIENDS_SUCCESSFUL:
            return {
                ...state,
                status: 'successful',
                ...action.data
            };
        case FETCH_INVITES_FRIENDS_FAILED:
            return { ...state, status: 'failed', data: action.data };
        default:
            return state;
    }
};
