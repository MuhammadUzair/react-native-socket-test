import {
    ACCEPT_GROUP_REQUEST,
    REJECT_GROUP_REQUEST,
    GROUP_REQUEST_ACTION_FAILED,
    GROUP_REQUEST_ACTION_SUCCESSFUL,
    RESET_GROUP_REQUEST_ACTION
} from '../epics/group-action-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case ACCEPT_GROUP_REQUEST:
        case REJECT_GROUP_REQUEST:
            return 'inprogress';
        case GROUP_REQUEST_ACTION_SUCCESSFUL:
        case GROUP_REQUEST_ACTION_FAILED:
            return action.data;
        case ALL_RESET:
        case RESET_GROUP_REQUEST_ACTION:
            return '';
        default:
            return state;
    }
};
