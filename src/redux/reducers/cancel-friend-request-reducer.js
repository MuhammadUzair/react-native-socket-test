import {
    CANCEL_FRIEND_REQUEST,
    CANCEL_FRIEND_REQUEST_SUCCESSFUL,
    CANCEL_FRIEND_REQUEST_FAILED,
    RESET_CANCEL_FRIEND_REQUEST
} from '../epics/cancel-friend-request-epic';

export default (state = '', action) => {
    switch (action.type) {
        case CANCEL_FRIEND_REQUEST:
            return 'inprogress';
        case CANCEL_FRIEND_REQUEST_SUCCESSFUL:
            return action.data;
        case CANCEL_FRIEND_REQUEST_FAILED:
            return action.data;
        case RESET_CANCEL_FRIEND_REQUEST:
            return { state: '' };
        default:
            return state;
    }
};
