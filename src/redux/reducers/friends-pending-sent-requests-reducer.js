import {
    FETCH_FRIENDS_PENDING_SENT,
    FETCH_FRIENDS_PENDING_SENT_SUCCESSFUL,
    FETCH_FRIENDS_PENDING_SENT_FAILED,
    RESET_FETCH_FRIENDS_PENDING_SENT,
    REFRESH_FRIENDS_PENDING_SENT
} from '../epics/fetch-friends-pending-sent-requests-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [], count: 0 }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_FRIENDS_PENDING_SENT:
            return { status: '', data: [] };
        case FETCH_FRIENDS_PENDING_SENT:
            return {
                status: 'inprogress',
                ...action.data,
                data: state.data,
                count: state.count
            };
        case FETCH_FRIENDS_PENDING_SENT_SUCCESSFUL:
            return {
                status: 'successful',
                ...action.data,
                data: [...state.data, ...action.data.data.friends],
                count: action.data.data.total_records
            };

        case REFRESH_FRIENDS_PENDING_SENT:
            return {
                status: 'successful',
                data: action.data.data.friends,
                count: action.data.data.total_records
            };
        case FETCH_FRIENDS_PENDING_SENT_FAILED:
            return { status: 'failed', ...action.data };
        default:
            return state;
    }
};
