import {
    ADD_LEAGUES,
    ADD_LEAGUES_SUCCESSFUL,
    ADD_LEAGUES_FAILED,
    RESET_ADD_LEAGUES
} from '../epics/add-leagues-epic';

import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case ADD_LEAGUES:
            return 'inprogress';
        case ADD_LEAGUES_SUCCESSFUL:
        case ADD_LEAGUES_FAILED:
            return action.data;
        case RESET_ADD_LEAGUES:
        case ALL_RESET:
            return '';
        default:
            return state;
    }
};
