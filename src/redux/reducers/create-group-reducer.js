import {
    CREATE_GROUP,
    CREATE_GROUP_SUCCESSFUL,
    CREATE_GROUP_FAILED,
    RESET_CREATE_GROUP
} from '../epics/create-group-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case CREATE_GROUP:
            return 'inprogress';
        case CREATE_GROUP_SUCCESSFUL:
        case CREATE_GROUP_FAILED:
            return action.data;
        case RESET_CREATE_GROUP:
        case ALL_RESET:
            return '';
        default:
            return state;
    }
};
