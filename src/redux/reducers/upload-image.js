import {
    UPLOAD_IMAGE,
    UPLOAD_IMAGE_SUCCESSFUL,
    UPLOAD_IMAGE_FAILED,
    RESET_UPLOAD_IMAGE
} from '../epics/upload-image';

export default (state = '', action) => {
    switch (action.type) {
        case UPLOAD_IMAGE:
            return 'inprogress';
        case UPLOAD_IMAGE_SUCCESSFUL:
            return action.data;
        case UPLOAD_IMAGE_FAILED:
            return action.data;
        case RESET_UPLOAD_IMAGE:
            return '';
        default:
            return state;
    }
};
