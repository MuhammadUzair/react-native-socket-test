import {
    TSEVO_CHECK_COMPLIANCE_STATUS,
    TSEVO_CHECK_COMPLIANCE_STATUS_SUCCESSFUL,
    TSEVO_CHECK_COMPLIANCE_STATUS_FAILED,
    RESET_TSEVO_CHECK_COMPLIANCE_STATUS
} from '../epics/tsevo-check-compliances-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case TSEVO_CHECK_COMPLIANCE_STATUS:
            return 'inprogress';
        case TSEVO_CHECK_COMPLIANCE_STATUS_SUCCESSFUL:
        case TSEVO_CHECK_COMPLIANCE_STATUS_FAILED:
            return action.data;
        case RESET_TSEVO_CHECK_COMPLIANCE_STATUS:
        case ALL_RESET:
            return '';
        default:
            return state;
    }
};
