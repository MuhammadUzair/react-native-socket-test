import {
    FETCH_TEAMS,
    FETCH_TEAMS_SUCCESSFUL,
    FETCH_TEAMS_FAILED,
    FETCH_TEAMS_RESET
} from '../epics/fetch-teams-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case ALL_RESET:
        case FETCH_TEAMS_RESET:
            return '';
        case FETCH_TEAMS:
            return 'inprogress';
        case FETCH_TEAMS_SUCCESSFUL:
            const { data } = action.data;
            const restructredData = {};
            data.forEach(league => (restructredData[league.id] = league));
            action.data.data = restructredData;
            return action.data;
        case FETCH_TEAMS_FAILED:
            return action.data;
        default:
            return state;
    }
};
