import {
    GET_NOTIFICATION,
    GET_NOTIFICATION_SUCCESSFUL,
    GET_NOTIFICATION_FAILED,
    RESET_GET_NOTIFICATION,
    ACTIVE_NOTIFICATION,
    UNACTIVE_NOTIFICATION
} from '../epics/get-notification';
import { ALL_RESET } from '../epics/allReset-epic';

export default (
    state = { status: '', isNotification: false, count: 0 },
    action
) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_GET_NOTIFICATION:
            return { status: '', isNotification: false, count: 0 };
        case ACTIVE_NOTIFICATION:
            return {
                isNotification: true,
                ...action.data
            };
        case UNACTIVE_NOTIFICATION:
            return {
                isNotification: false,
                count: 0
            };
        case GET_NOTIFICATION:
            return {
                status: 'inprogress',
                isNotification: false,
                count: 0
            };
        case GET_NOTIFICATION_SUCCESSFUL:
            return {
                status: 'successful',
                isNotification: action.data.data.count > 0 ? true : false,
                count: action.data.data.count
            };
        case GET_NOTIFICATION_FAILED:
            return { status: 'failed', ...action.data };
        default:
            return state;
    }
};
