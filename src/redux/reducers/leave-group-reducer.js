import {
    LEAVE_GROUP,
    LEAVE_GROUP_SUCCESSFUL,
    LEAVE_GROUP_FAILED,
    RESET_LEAVE_GROUP
} from '../epics/leave-group-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case LEAVE_GROUP:
            return 'inprogress';
        case LEAVE_GROUP_SUCCESSFUL:
        case LEAVE_GROUP_FAILED:
            return action.data;
        case ALL_RESET:
        case RESET_LEAVE_GROUP:
            return '';
        default:
            return state;
    }
};
