import {
    ADD_TEAMS,
    ADD_TEAMS_SUCCESSFUL,
    ADD_TEAMS_FAILED,
    RESET_ADD_TEAMS
} from '../epics/add-teams-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case ADD_TEAMS:
            return 'inprogress';
        case ADD_TEAMS_SUCCESSFUL:
        case ADD_TEAMS_FAILED:
            return action.data;
        case RESET_ADD_TEAMS:
        case ALL_RESET:
            return '';
        default:
            return state;
    }
};
