import {
    FETCH_CITIES,
    FETCH_CITIES_SUCCESSFUL,
    FETCH_CITIES_FAILED,
    RESET_FETCH_CITIES
} from '../epics/fetch-cities';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [] }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_CITIES:
            return { status: '', data: [] };
        case FETCH_CITIES:
            return {
                ...state,
                status: 'inprogress',
                data: action.data
            };
        case FETCH_CITIES_SUCCESSFUL:
            return {
                ...state,
                status: 'successful',
                data: action.data
            };
        case FETCH_CITIES_FAILED:
            return { ...state, status: 'failed', data: action.data };
        default:
            return state;
    }
};
