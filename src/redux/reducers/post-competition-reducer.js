import {
    POST_COMPETITION,
    POST_COMPETITION_FAILED,
    POST_COMPETITION_SUCCESSFUL,
    RESET_POST_COMPETITION
} from '../epics/post-competition-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case POST_COMPETITION:
            return 'inprogress';
        case POST_COMPETITION_SUCCESSFUL:
        case POST_COMPETITION_FAILED:
            return action.data;
        case ALL_RESET:
        case RESET_POST_COMPETITION:
            return '';
        default:
            return state;
    }
};
