import {
    FETCH_SPECIFIC_COMPETITIONS,
    FETCH_SPECIFIC_COMPETITIONS_SUCCESSFUL,
    FETCH_SPECIFIC_COMPETITIONS_FAILED,
    RESET_FETCH_SPECIFIC_COMPETITIONS,
    FETCH_COMPETITIONS_DASHBOARD,
    FETCH_COMPETITIONS_NOTIFICATION
} from '../epics/fetch-specific-competition-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [] }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_SPECIFIC_COMPETITIONS:
            return { status: '', data: [] };
        case FETCH_SPECIFIC_COMPETITIONS:
            return {
                ...state,
                status: 'inprogress',
                data: action.data
            };
        case FETCH_SPECIFIC_COMPETITIONS_SUCCESSFUL:
            return {
                ...action,
                status: 'successful',
                data: action.data.data
            };
        case FETCH_COMPETITIONS_DASHBOARD:
            return {
                ...action,
                status: 'successful',
                data: action.data.data,
                screen: 'dashboard'
            };
        case FETCH_COMPETITIONS_NOTIFICATION:
            return {
                ...action,
                status: 'successful',
                data: action.data.data,
                screen: 'notification'
            };
        case FETCH_SPECIFIC_COMPETITIONS_FAILED:
            return { ...state, status: 'failed', data: action.data };
        default:
            return state;
    }
};
