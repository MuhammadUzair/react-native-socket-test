import {
    EDIT_GROUP,
    EDIT_GROUP_SUCCESSFUL,
    EDIT_GROUP_FAILED,
    RESET_EDIT_GROUP
} from '../epics/edit-group-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = '', action) => {
    switch (action.type) {
        case EDIT_GROUP:
            return 'inprogress';
        case EDIT_GROUP_SUCCESSFUL:
        case EDIT_GROUP_FAILED:
            return action.data;
        case RESET_EDIT_GROUP:
        case ALL_RESET:
            return '';
        default:
            return state;
    }
};
