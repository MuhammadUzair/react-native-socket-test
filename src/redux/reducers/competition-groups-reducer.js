import {
    CREATE_COMPETITION_GROUPS,
    CREATE_COMPETITION_GROUPS_FAILED,
    CREATE_COMPETITION_GROUPS_SUCCESSFUL,
    RESET_CREATE_COMPETITION_GROUPS,
    REFRESH_MY_GROUPS,
    REFRESH_MY_GROUPS_SUCCESSFUL,
    REFRESH_MY_GROUPS_FAILED
} from '../epics/competition-groups-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { groups: [] } }, action) => {
    switch (action.type) {
        case CREATE_COMPETITION_GROUPS:
            return { ...state, code: null, status: 'inprogress' };
        case REFRESH_MY_GROUPS:
            return { ...state, code: null, status: 'refreshing' };
        case CREATE_COMPETITION_GROUPS_SUCCESSFUL:
            return {
                ...state,
                ...action.data,
                status: 'successful',
                data: {
                    ...state.data,
                    ...action.data.data,
                    groups: [...state.data.groups, ...action.data.data.groups]
                }
            };
        case REFRESH_MY_GROUPS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                data: {
                    ...action.data.data
                }
            };
        case CREATE_COMPETITION_GROUPS_FAILED:
            return {
                ...state,
                ...action.data,
                status: 'failed',
                data: { ...state.data }
            };
        case RESET_CREATE_COMPETITION_GROUPS:
            return { ...state, status: '', code: null, messages: [] };
        case ALL_RESET:
            return { status: '', data: {} };

        default:
            return state;
    }
};
