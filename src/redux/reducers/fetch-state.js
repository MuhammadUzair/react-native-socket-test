import {
    FETCH_STATE,
    FETCH_STATE_SUCCESSFUL,
    FETCH_STATE_FAILED,
    RESET_FETCH_STATE
} from '../epics/fetch-states';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: [] }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_STATE:
            return { status: '', data: [] };
        case FETCH_STATE:
            return {
                ...state,
                status: 'inprogress',
                data: action.data
            };
        case FETCH_STATE_SUCCESSFUL:
            return {
                ...state,
                status: 'successful',
                data: action.data
            };
        case FETCH_STATE_FAILED:
            return { ...state, status: 'failed', data: action.data };
        default:
            return state;
    }
};
