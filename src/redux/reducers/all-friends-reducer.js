import {
    FETCH_ALL_FRIENDS,
    FETCH_ALL_FRIENDS_SUCCESSFUL,
    FETCH_ALL_FRIENDS_FAILED,
    RESET_FETCH_ALL_FRIENDS
} from '../epics/fetch-all-friends-epic';
import { ALL_RESET } from '../epics/allReset-epic';

export default (state = { status: '', data: { friends: [] } }, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_ALL_FRIENDS:
            return { status: '', data: { friends: [] } };
        case FETCH_ALL_FRIENDS: {
            return {
                status: 'inprogress',
                ...action.data,
                data: { ...state.data }
            };
        }
        case FETCH_ALL_FRIENDS_SUCCESSFUL: {
            return {
                status: 'successful',
                ...action.data,
                data: {
                    ...action.data.data,
                    friends: [
                        ...state.data.friends,
                        ...action.data.data.friends
                    ]
                }
            };
        }
        case FETCH_ALL_FRIENDS_FAILED:
            return {
                ...action.data,
                status: 'failed',
                data: { ...state.data }
            };
        default:
            return state;
    }
};
