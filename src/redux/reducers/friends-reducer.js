import {
    FETCH_FRIENDS,
    FETCH_FRIENDS_SUCCESSFUL,
    FETCH_FRIENDS_FAILED,
    RESET_FETCH_FRIENDS,
} from '../epics/fetch-friends-epic';
import { ALL_RESET } from '../epics/allReset-epic';

const initialState = { status: '', data: { friends: [] } };

export default (state = initialState, action) => {
    switch (action.type) {
        case ALL_RESET:
        case RESET_FETCH_FRIENDS:
            return initialState;
        case FETCH_FRIENDS:
            return {
                status: 'inprogress',
                data: { ...state.data },
            };
        case FETCH_FRIENDS_SUCCESSFUL:
            return {
                ...action.data,
                status: 'successful',
                data: {
                    ...action.data.data,
                    friends: [
                        ...state.data.friends,
                        ...action.data.data.friends,
                    ],
                },
            };
        case FETCH_FRIENDS_FAILED:
            return { ...action.data, status: 'failed', ...state };
        default:
            return state;
    }
};
