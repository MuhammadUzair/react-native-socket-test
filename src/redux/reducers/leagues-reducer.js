import {
    FETCH_LEAGUES,
    FETCH_LEAGUES_FAILED,
    FETCH_LEAGUES_SUCCESSFUL
} from '../epics/fetch-leagues-epic';
import { ALL_RESET } from '../epics/allReset-epic';
export default (state = '', action) => {
    switch (action.type) {
        case ALL_RESET:
            return '';
        case FETCH_LEAGUES:
            return 'inprogress';
        case FETCH_LEAGUES_SUCCESSFUL:
        case FETCH_LEAGUES_FAILED:
            return action.data;
        default:
            return state;
    }
};
