import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

import { refreshMyGroups } from './fetch-my-groups-epic';

export const ACCEPT_GROUP_REQUEST = 'ACCEPT_GROUP_REQUEST';
export const REJECT_GROUP_REQUEST = 'REJECT_GROUP_REQUEST';
export const GROUP_REQUEST_ACTION_SUCCESSFUL =
    'GROUP_REQUEST_ACTION_SUCCESSFUL';
export const GROUP_REQUEST_ACTION_FAILED = 'GROUP_REQUEST_ACTION_FAILED';
export const RESET_GROUP_REQUEST_ACTION = 'RESET_GROUP_REQUEST_ACTION';

export const acceptGroupRequest = data => ({
    type: ACCEPT_GROUP_REQUEST,
    data
});

export const rejectGroupRequest = data => ({
    type: REJECT_GROUP_REQUEST,
    data
});

const groupRequestActionSuccessful = data => ({
    type: GROUP_REQUEST_ACTION_SUCCESSFUL,
    data
});

const groupRequestActionFailed = data => ({
    type: GROUP_REQUEST_ACTION_FAILED,
    data
});

export const resetGroupAction = () => ({
    type: RESET_GROUP_REQUEST_ACTION
});

export default action$ =>
    action$.pipe(
        ofType(ACCEPT_GROUP_REQUEST, REJECT_GROUP_REQUEST),
        switchMap(action => {
            const { token, groupId, ownerId } = action.data;
            const headers = addBasicHeaders(token);

            const url =
                `${BASEURL_DEV}/api/v1/view/groups/request/${groupId}/${ownerId}` +
                (action.type === REJECT_GROUP_REQUEST
                    ? '/Canceled'
                    : '/Accepted');

            const options = {
                method: 'PATCH',
                headers
            };

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return groupRequestActionSuccessful(res);
                    return groupRequestActionFailed(res);
                });
            return from(req).pipe(
                mergeMap(action =>
                    of(
                        action,
                        resetGroupAction(),
                        refreshMyGroups({ token, group: 'pendinggroups' })
                    )
                ),
                catchError(err =>
                    of(
                        groupRequestActionFailed(err.message),
                        resetGroupAction()
                    )
                )
            );
        })
    );
