import { of, from } from 'rxjs';
import { ofType } from 'redux-observable';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

/**************************************************************************** */
export const FETCH_MY_COMPETITIONS = 'FETCH_MY_COMPETITIONS';
export const FETCH_MY_COMPETITIONS_SUCCESSFUL =
    'FETCH_MY_COMPETITIONS_SUCCESSFUL';
export const FETCH_MY_COMPETITIONS_FAILED = 'FETCH_MY_COMPETITIONS_FAILED';
export const RESET_MY_COMPETITIONS = 'RESET_MY_COMPETITIONS';

export const fetchMyCompetitions = data => ({
    type: FETCH_MY_COMPETITIONS,
    data
});

const fetchMyCompetitionsSuccessful = data => ({
    type: FETCH_MY_COMPETITIONS_SUCCESSFUL,
    data
});

const fetchMyCompetitionsFailed = data => ({
    type: FETCH_MY_COMPETITIONS_FAILED,
    data
});
/**************************************************************************** */

/**************************************************************************** */
export const REFRESH_MY_COMPETITIONS = 'REFRESH_MY_COMPETITIONS';
export const REFRESH_MY_COMPETITIONS_SUCCESSFUL =
    'REFRESH_MY_COMPETITIONS_SUCCESSFUL';
export const REFRESH_MY_COMPETITIONS_FAILED = 'REFRESH_MY_COMPETITIONS_FAILED';

export const refreshMyCompetitions = data => ({
    type: REFRESH_MY_COMPETITIONS,
    data
});

const refreshMyCompetitionsSuccessful = data => ({
    type: REFRESH_MY_COMPETITIONS_SUCCESSFUL,
    data
});

const refreshMyCompetitionsFailed = data => ({
    type: REFRESH_MY_COMPETITIONS_FAILED,
    data
});
/**************************************************************************** */

export const resetMyCompetitions = () => ({ type: RESET_MY_COMPETITIONS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_MY_COMPETITIONS, REFRESH_MY_COMPETITIONS),
        mergeMap(action => {
            const { token, tab, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };

            const url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/my-competitions/${tab}`;

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (action.type === FETCH_MY_COMPETITIONS) {
                        if (res.code >= 200 && res.code < 300)
                            return fetchMyCompetitionsSuccessful({
                                ...res,
                                tab
                            });
                        return fetchMyCompetitionsFailed({ ...res, tab });
                    } else {
                        if (res.code >= 200 && res.code < 300)
                            return refreshMyCompetitionsSuccessful({
                                ...res,
                                tab
                            });
                        return refreshMyCompetitionsFailed({ ...res, tab });
                    }
                });
            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchMyCompetitionsFailed({
                            tab,
                            code: 400,
                            messages: [err.message]
                        })
                    )
                )
            );
        })
    );
