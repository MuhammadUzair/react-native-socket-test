import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESSFUL = 'FETCH_USER_SUCCESSFUL';
export const FETCH_USER_FAILED = 'FETCH_USER_FAILED';

export const fetchUser = data => ({
    type: FETCH_USER,
    data
});

const fetchUserSuccessful = data => ({
    type: FETCH_USER_SUCCESSFUL,
    data
});

const fetchUserFailed = data => ({
    type: FETCH_USER_FAILED,
    data
});

export default (action$, state$) =>
    action$.pipe(
        ofType(FETCH_USER),
        switchMap(action => {
            const headers = addBasicHeaders(action.data);

            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(`${BASEURL_DEV}/api/v1/view/me`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code === 200) return fetchUserSuccessful(res);
                    return fetchUserFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(fetchUserFailed(err.message)))
            );
        })
    );
