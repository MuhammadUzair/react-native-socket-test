import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

import { refreshMyGroups } from './fetch-my-groups-epic';

export const DELETE_GROUP = 'DELETE_GROUP';
export const DELETE_GROUP_SUCCESSFUL = 'DELETE_GROUP_SUCCESSFUL';
export const DELETE_GROUP_FAILED = 'DELETE_GROUP_FAILED';
export const RESET_DELETE_GROUP = 'RESET_DELETE_GROUP';

export const deleteGroup = data => ({ type: DELETE_GROUP, data });
const deleteGroupSuccessful = data => ({ type: DELETE_GROUP_SUCCESSFUL, data });
const deleteGroupFailed = data => ({ type: DELETE_GROUP_FAILED, data });
const resetDeleteGroup = data => ({ type: RESET_DELETE_GROUP });

export default action$ =>
    action$.pipe(
        ofType(DELETE_GROUP),
        mergeMap(action => {
            const { token, groupId } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'DELETE',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/groups/${groupId}`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return deleteGroupSuccessful(res);
                    return deleteGroupFailed(res);
                });

            return from(req).pipe(
                mergeMap(action =>
                    of(
                        action,
                        resetDeleteGroup(),
                        refreshMyGroups({ token, group: 'mygroups' })
                    )
                ),
                catchError(err =>
                    of(
                        deleteGroupFailed({
                            code: 400,
                            messages: [err.message]
                        })
                    )
                )
            );
        })
    );
