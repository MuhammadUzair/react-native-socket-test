import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const GET_NOTIFICATION = 'GET_NOTIFICATION';
export const GET_NOTIFICATION_SUCCESSFUL = 'GET_NOTIFICATION_SUCCESSFUL';
export const GET_NOTIFICATION_FAILED = 'GET_NOTIFICATION_FAILED';
export const RESET_GET_NOTIFICATION = 'RESET_GET_NOTIFICATION';
export const ACTIVE_NOTIFICATION = 'ACTIVE_NOTIFICATION';
export const UNACTIVE_NOTIFICATION = 'UNACTIVE_NOTIFICATION';

export const getNotification = data => ({
    type: GET_NOTIFICATION,
    data
});

export const getNotificationSuccessful = data => ({
    type: GET_NOTIFICATION_SUCCESSFUL,
    data
});

export const activeNotification = data => ({
    type: ACTIVE_NOTIFICATION,
    data
});
export const unActiveNotification = () => ({
    type: UNACTIVE_NOTIFICATION
});

const getNotificationFailed = data => ({
    type: GET_NOTIFICATION_FAILED,
    data
});

export const resetGetNotification = () => ({ type: RESET_GET_NOTIFICATION });

export default action$ =>
    action$.pipe(
        ofType(GET_NOTIFICATION),
        switchMap(action => {
            const { token } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };
            const url = BASEURL_DEV + '/api/v1/view/notifications/count';

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return getNotificationSuccessful(res);
                    return getNotificationFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        getNotificationFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetGetNotification()
                    )
                )
            );
        }),
        catchError(err =>
            of(
                getNotificationFailed({ code: 400, messages: [err.message] }),
                resetGetNotification()
            )
        )
    );
