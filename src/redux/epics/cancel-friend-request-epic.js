import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const CANCEL_FRIEND_REQUEST = 'CANCEL_FRIEND_REQUEST';
export const CANCEL_FRIEND_REQUEST_SUCCESSFUL =
    'CANCEL_FRIEND_REQUEST_SUCCESSFUL';
export const CANCEL_FRIEND_REQUEST_FAILED = 'CANCEL_FRIEND_REQUEST_FAILED';
export const RESET_CANCEL_FRIEND_REQUEST = 'RESET_CANCEL_FRIEND_REQUEST';

export const cancelFriendRequest = data => ({
    type: CANCEL_FRIEND_REQUEST,
    data
});

const cancelFriendRequestSuccessful = data => ({
    type: CANCEL_FRIEND_REQUEST_SUCCESSFUL,
    data
});

const cancelFriendRequestFailed = data => ({
    type: CANCEL_FRIEND_REQUEST_FAILED,
    data
});

export const resetFriendRequestApi = data => ({
    type: RESET_CANCEL_FRIEND_REQUEST,
    data
});

export default action$ =>
    action$.pipe(
        ofType(CANCEL_FRIEND_REQUEST),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);
            const friend_id = action.data.friend_id;

            const options = {
                method: 'PUT',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/friends/request/${friend_id}/canceled`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return cancelFriendRequestSuccessful(res);
                    return cancelFriendRequestFailed(res);
                });

            return from(req).pipe(
                catchError(err => {
                    of(cancelFriendRequestFailed(err.message));
                })
            );
        })
    );
