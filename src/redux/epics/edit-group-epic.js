import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const EDIT_GROUP = 'EDIT_GROUP';
export const EDIT_GROUP_SUCCESSFUL = 'EDIT_GROUP_SUCCESSFUL';
export const EDIT_GROUP_FAILED = 'EDIT_GROUP_FAILED';
export const RESET_EDIT_GROUP = 'RESET_EDIT_GROUP';

export const editGroup = data => ({
    type: EDIT_GROUP,
    data
});

const editGroupSuccessful = data => ({
    type: EDIT_GROUP_SUCCESSFUL,
    data
});

const editGroupFailed = data => ({
    type: EDIT_GROUP_FAILED,
    data
});

const resetEditGroup = () => ({ type: RESET_EDIT_GROUP });

export default action$ =>
    action$.pipe(
        ofType(EDIT_GROUP),
        switchMap(action => {
            const {
                token,
                remove,
                add,
                name,
                description,
                privacy,
                groupId
            } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'PATCH',
                headers,
                body: JSON.stringify({
                    remove: remove ? remove : [],
                    add: add ? add : [],
                    data: {
                        name,
                        description,
                        privicy: privacy === 'private' ? 0 : 1
                    }
                })
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/groups/updategroup/${groupId}`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return editGroupSuccessful(res);
                    return editGroupFailed(res);
                });
            return from(req).pipe(
                mergeMap(action => of(action, resetEditGroup())),
                catchError(err =>
                    of(
                        editGroupFailed({ code: 400, messages: [err.message] }),
                        resetEditGroup()
                    )
                )
            );
        })
    );
