import { combineEpics } from 'redux-observable';

import signupEpic from './signup-epic';
import socialSignupEpic from './social-signup-epic';
import loginEpic from './login-epic';

import fetchUserEpic from './fetch-user-epic';

import fetchLeaguesEpic from './fetch-leagues-epic';
import addLeaguesEpic from './add-leagues-epic';

import fetchTeamsEpic from './fetch-teams-epic';
import addTeamsEpic from './add-teams-epic';

import fetchFriendsEpic from './fetch-friends-epic';
import addFriendsEpic from './add-friends-epic';
import fetchFBFriendsEpic from './fb-friends-epic';
import fetchMyFriendsEpic from './fetch-my-friends-epic';
import removeFriendsEpic from './remove-friends-epic';
import fetchFriendsPendingReceiveRequestsEpic from './fetch-friends-pending-receive-requests-epic';
import fetchFriendsPendingSentRequestsEpic from './fetch-friends-pending-sent-requests-epic';
import acceptFriendRequestEpic from './accept-friend-request-epic';
import cancelFriendRequestEpic from './cancel-friend-request-epic';
import searchMyFreindsEpic from './search-my-friends-epic';
import fetchAllFriendsEpic from './fetch-all-friends-epic';

import fetchCompetitionsEpic from './fetch-competitions-epic';
import searchCompetitionsEpic from './search-dashboard-epic';

import fetchCreateCompetitionsEpic from './fetch-create-competitions-epic';
import searchCreateCompetitionsEpic from './search-create-competition-epic';
import postCompetitionEpic from './post-competition-epic';

import playCompetitionEpic from './play-competition-epic';

import fetchMyCompetitionsEpic from './fetch-my-competition-epic';
import searchMyCompetitionsEpic from './search-my-competition-epic';

import fetchMyGroupsEpic from './fetch-my-groups-epic';
import fetchGroupMembersEpic from './fetch-group-members-epic';

import leaveGroupEpic from './leave-group-epic';
import joinGroupEpic from './join-group-epic';
import groupActionEpic from './group-action-epic';
import createGroupEpic from './create-group-epic';
import editGroupEpic from './edit-group-epic';
import deleteGroupEpic from './delete-group-epic';

import fetchTransactionsEpic from './fetch-transactions-epic';
import fetchGatewayTransactionEpic from './gateway-transaction-epic';

import fetchStatesEpic from './fetch-states';
import fetchCitiesEpic from './fetch-cities';
import profileSubmitEpic from './profile-submit';
import uploadImageSubmitEpic from './upload-image';
import notificationsEpic from './fetch-notifications';
import getNotificationCountEpic from './get-notification';
import readAllNotificationsEpic from './read-all-notifications';

import fetchSpecificCompetitionEpic from './fetch-specific-competition-epic';

import logoutEpic from './logout-epic';

import complianceEpic from './compliance-epic';
import tsevoCheckCompliancesEpic from './tsevo-check-compliances-epic';
import competitionGroupsEpic from './competition-groups-epic';
import inviteFriendsEpic from './invite-friends';
import fetchInvitesFriendEpic from './fetch-invites-friend';

export default combineEpics(
    signupEpic,
    socialSignupEpic,
    loginEpic,
    fetchUserEpic,
    fetchLeaguesEpic,
    addLeaguesEpic,
    fetchTeamsEpic,
    addTeamsEpic,
    fetchFriendsEpic,
    addFriendsEpic,
    fetchFBFriendsEpic,
    fetchCompetitionsEpic,
    fetchCreateCompetitionsEpic,
    searchCompetitionsEpic,
    postCompetitionEpic,
    playCompetitionEpic,
    searchCreateCompetitionsEpic,
    fetchMyCompetitionsEpic,
    searchMyCompetitionsEpic,
    fetchMyGroupsEpic,
    fetchGroupMembersEpic,
    leaveGroupEpic,
    fetchMyFriendsEpic,
    removeFriendsEpic,
    joinGroupEpic,
    groupActionEpic,
    fetchFriendsPendingReceiveRequestsEpic,
    fetchFriendsPendingSentRequestsEpic,
    acceptFriendRequestEpic,
    cancelFriendRequestEpic,
    searchMyFreindsEpic,
    createGroupEpic,
    editGroupEpic,
    fetchAllFriendsEpic,
    deleteGroupEpic,
    fetchTransactionsEpic,
    fetchStatesEpic,
    fetchCitiesEpic,
    profileSubmitEpic,
    uploadImageSubmitEpic,
    logoutEpic,
    notificationsEpic,
    fetchSpecificCompetitionEpic,
    getNotificationCountEpic,
    fetchGatewayTransactionEpic,
    complianceEpic,
    readAllNotificationsEpic,
    tsevoCheckCompliancesEpic,
    competitionGroupsEpic,
    inviteFriendsEpic,
    fetchInvitesFriendEpic
);
