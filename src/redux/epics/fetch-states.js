import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_STATE = 'FETCH_STATE';
export const FETCH_STATE_SUCCESSFUL = 'FETCH_STATE_SUCCESSFUL';
export const FETCH_STATE_FAILED = 'FETCH_STATE_FAILED';
export const RESET_FETCH_STATE = 'FETCH_STATE_FAILED';

export const fetchSate = data => ({
    type: FETCH_STATE,
    data
});

const fetchSateSuccessful = data => ({
    type: FETCH_STATE_SUCCESSFUL,
    data
});

const fetchSateFailed = data => ({
    type: FETCH_STATE_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_STATE),
        mergeMap(action => {
            const headers = addBasicHeaders(action.data.token);

            const options = {
                method: 'GET',
                headers
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/user/states?name=United%20States`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res && res.length > 0) return fetchSateSuccessful(res);
                    return fetchSateFailed(res.message);
                });
            return from(req).pipe(
                catchError(err => of(fetchSateFailed(err.message)))
            );
        })
    );
