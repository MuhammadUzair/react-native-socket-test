import { ofType } from 'redux-observable';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';
import { of, from } from 'rxjs';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

import { refreshMyGroups } from './fetch-my-groups-epic';

export const LEAVE_GROUP = 'LEAVE_GROUP';
export const LEAVE_GROUP_SUCCESSFUL = 'LEAVE_GROUP_SUCCESSFUL';
export const LEAVE_GROUP_FAILED = 'LEAVE_GROUP_FAILED';
export const RESET_LEAVE_GROUP = 'RESET_LEAVE_GROUP';

export const leaveGroup = data => ({ type: LEAVE_GROUP, data });
const leaveGroupSuccessful = data => ({ type: LEAVE_GROUP_SUCCESSFUL, data });
const leaveGroupFailed = data => ({ type: LEAVE_GROUP_FAILED, data });
export const resetLeaveGroup = () => ({ type: RESET_LEAVE_GROUP });

export default action$ =>
    action$.pipe(
        ofType(LEAVE_GROUP),
        switchMap(action => {
            const { token, groupId } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'PATCH',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/groups/leave/${groupId}`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return leaveGroupSuccessful(res);
                    throw new Error(res.messages[0]);
                });
            return from(req).pipe(
                mergeMap(action => {
                    return of(
                        action,
                        refreshMyGroups({ token, group: 'mygroups' }),
                        resetLeaveGroup()
                    );
                }),
                catchError(err =>
                    of(leaveGroupFailed(err.message), resetLeaveGroup())
                )
            );
        })
    );
