import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_SPECIFIC_COMPETITIONS = 'FETCH_SPECIFIC_COMPETITIONS';
export const FETCH_SPECIFIC_COMPETITIONS_SUCCESSFUL =
    'FETCH_SPECIFIC_COMPETITIONS_SUCCESSFUL';
export const FETCH_SPECIFIC_COMPETITIONS_FAILED =
    'FETCH_SPECIFIC_COMPETITIONS_FAILED';
export const RESET_FETCH_SPECIFIC_COMPETITIONS =
    'RESET_FETCH_SPECIFIC_COMPETITIONS';
export const FETCH_COMPETITIONS_DASHBOARD = 'FETCH_COMPETITIONS_DASHBOARD';
export const FETCH_COMPETITIONS_NOTIFICATION =
    'FETCH_COMPETITIONS_NOTIFICATION';

export const fetchSpecificCompetition = data => ({
    type: FETCH_SPECIFIC_COMPETITIONS,
    data
});
export const resetFetchSpecificCompetition = data => ({
    type: RESET_FETCH_SPECIFIC_COMPETITIONS,
    data
});
export const fetchCompetitionDashBoard = data => ({
    type: FETCH_COMPETITIONS_DASHBOARD,
    data
});
export const fetchCompetitionNotification = data => ({
    type: FETCH_COMPETITIONS_NOTIFICATION,
    data
});

const fetchSpecificCompetitionSuccessful = data => ({
    type: FETCH_SPECIFIC_COMPETITIONS_SUCCESSFUL,
    data
});

const fetchSpecificCompetitionFailed = data => ({
    type: FETCH_SPECIFIC_COMPETITIONS_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_SPECIFIC_COMPETITIONS),
        mergeMap(action => {
            const {
                token,
                competitionId,
                isDashboard,
                isNotification
            } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/my-competition/${competitionId}}`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (isDashboard) {
                            return fetchCompetitionDashBoard(res);
                        }
                        if (isNotification) {
                            return fetchCompetitionNotification(res);
                        }
                        return fetchSpecificCompetitionSuccessful(res);
                    }
                    return fetchSpecificCompetitionFailed(res);
                });
            return from(req).pipe(
                catchError(err =>
                    of(fetchSpecificCompetitionFailed(err.message))
                )
            );
        })
    );
