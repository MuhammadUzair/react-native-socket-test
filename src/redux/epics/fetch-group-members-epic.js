import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_GROUP_MEMBERS = 'FETCH_GROUP_MEMBERS';
export const FETCH_GROUP_MEMBERS_SUCCESSFUL = 'FETCH_GROUP_MEMBERS_SUCCESSFUL';
export const FETCH_GROUP_MEMBERS_FAILED = 'FETCH_GROUP_MEMBERS_FAILED';
export const RESET_GROUP_MEMBERS = 'RESET_GROUP_MEMBERS';

export const FETCH_GROUP_MEMBERS_PAGE_SUCCESSFUL =
    'FETCH_GROUP_MEMBERS_PAGE_SUCCESSFUL';
export const FETCH_GROUP_MEMBERS_PAGE = 'FETCH_GROUP_MEMBERS_PAGE';

export const fetchGroupMembers = data => ({
    type: FETCH_GROUP_MEMBERS,
    data
});

const fetchGroupMembersSuccessful = data => ({
    type: FETCH_GROUP_MEMBERS_SUCCESSFUL,
    data
});

const fetchPage = data => ({
    type: FETCH_GROUP_MEMBERS_PAGE,
    data
});

const fetchPageSuccessful = data => ({
    type: FETCH_GROUP_MEMBERS_PAGE_SUCCESSFUL,
    data
});

const fetchGroupMembersFailed = data => ({
    type: FETCH_GROUP_MEMBERS_FAILED,
    data
});

export const resetGroupMembers = () => ({ type: RESET_GROUP_MEMBERS });

export default (action$, state$) =>
    action$.pipe(
        ofType(FETCH_GROUP_MEMBERS),
        switchMap(action => {
            const { token, groupId, nextPage, filter } = action.data;
            const headers = addBasicHeaders(token);

            const url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/groups/members?group_id=${groupId}`;
            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    const newRes = { ...res };
                    if (filter) {
                        newRes.data.members = newRes.data.members.filter(
                            item =>
                                !(
                                    item.id === state$.value.user.data.id &&
                                    item.is_owner
                                )
                        );
                    }
                    if (res.code >= 200 && res.code < 300) {
                        if (nextPage)
                            return fetchPageSuccessful({ ...newRes, groupId });
                        return fetchGroupMembersSuccessful({
                            ...newRes,
                            groupId
                        });
                    }
                    return fetchGroupMembersFailed({ ...res, groupId });
                });
            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchGroupMembersFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetGroupMembers()
                    )
                )
            );
        })
    );
