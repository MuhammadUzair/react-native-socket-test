import { ofType } from 'redux-observable';
import { of, from } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const SEARCH_CREATE_COMPETITIONS = 'SEARCH_CREATE_COMPETITIONS';
export const SEARCH_CREATE_COMPETITIONS_PAGE =
    'SEARCH_CREATE_COMPETITIONS_PAGE';
export const SEARCH_CREATE_COMPETITIONS_SUCCESSFUL =
    'SEARCH_CREATE_COMPETITIONS_SUCCESSFUL';
export const SEARCH_CREATE_COMPETITIONS_PAGE_SUCCESSFUL =
    'SEARCH_CREATE_COMPETITIONS_PAGE_SUCCESSFUL';
export const SEARCH_CREATE_COMPETITIONS_FAILED =
    'SEARCH_CREATE_COMPETITIONS_FAILED';
export const RESET_SEARCH_CREATE_COMPETITIONS =
    'RESET_SEARCH_CREATE_COMPETITIONS';

export const searchCreateCompetitions = data => ({
    type: SEARCH_CREATE_COMPETITIONS,
    data,
});

export const searchCreateCompetitionsPage = data => ({
    type: SEARCH_CREATE_COMPETITIONS_PAGE,
    data,
});

const searchCreateCompetitionsSuccessful = data => ({
    type: SEARCH_CREATE_COMPETITIONS_SUCCESSFUL,
    data,
});

const searchCreateCompetitionsPageSuccessful = data => ({
    type: SEARCH_CREATE_COMPETITIONS_PAGE_SUCCESSFUL,
    data,
});

const searchCreateCompetitionsFailed = data => ({
    type: SEARCH_CREATE_COMPETITIONS_FAILED,
    data,
});

export const resetSearchCreateCompetitions = () => ({
    type: RESET_SEARCH_CREATE_COMPETITIONS,
});

export default action$ =>
    action$.pipe(
        ofType(SEARCH_CREATE_COMPETITIONS, SEARCH_CREATE_COMPETITIONS_PAGE),
        switchMap(action => {
            const { token, league, search, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers,
            };

            const url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/get-competitons-by-league?league=${league}&q=${search}`;

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (action.type === SEARCH_CREATE_COMPETITIONS)
                            return searchCreateCompetitionsSuccessful(res);
                        return searchCreateCompetitionsPageSuccessful(res);
                    }
                    throw new Error(res.messages[0]);
                });
            return from(req).pipe(
                catchError(err =>
                    of(
                        searchCreateCompetitionsFailed({
                            code: 400,
                            messages: [err.message],
                        }),
                        resetSearchCreateCompetitions(),
                    ),
                ),
            );
        }),
    );
