import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const TSEVO_CHECK_COMPLIANCE_STATUS = 'TSEVO_CHECK_COMPLIANCE_STATUS';
export const TSEVO_CHECK_COMPLIANCE_STATUS_SUCCESSFUL =
    'TSEVO_CHECK_COMPLIANCE_STATUS_SUCCESSFUL';
export const TSEVO_CHECK_COMPLIANCE_STATUS_FAILED =
    'TSEVO_CHECK_COMPLIANCE_STATUS_FAILED';
export const RESET_TSEVO_CHECK_COMPLIANCE_STATUS =
    'RESET_TSEVO_CHECK_COMPLIANCE_STATUS';

export const tsevoCheckCompilanceStatus = data => ({
    type: TSEVO_CHECK_COMPLIANCE_STATUS,
    data
});

const tsevoCheckCompilanceStatusSuccessful = data => ({
    type: TSEVO_CHECK_COMPLIANCE_STATUS_SUCCESSFUL,
    data
});

const tsevoCheckCompilanceStatusFailed = data => ({
    type: TSEVO_CHECK_COMPLIANCE_STATUS_FAILED,
    data
});

export const resetTsevoCheckCompilanceStatus = () => ({
    type: RESET_TSEVO_CHECK_COMPLIANCE_STATUS
});

export default action$ =>
    action$.pipe(
        ofType(TSEVO_CHECK_COMPLIANCE_STATUS),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);

            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/tsevo/check_compliances`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return tsevoCheckCompilanceStatusSuccessful(res);
                    return tsevoCheckCompilanceStatusFailed(res);
                })
                .catch(error => {
                    console.log('error ', error);
                });

            return from(req).pipe(
                catchError(err =>
                    of(tsevoCheckCompilanceStatusFailed(err.message))
                )
            );
        })
    );
