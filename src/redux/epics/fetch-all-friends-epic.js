import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_ALL_FRIENDS = 'FETCH_ALL_FRIENDS';
export const FETCH_ALL_FRIENDS_SUCCESSFUL = 'FETCH_ALL_FRIENDS_SUCCESSFUL';
export const FETCH_ALL_FRIENDS_FAILED = 'FETCH_ALL_FRIENDS_FAILED';
export const RESET_FETCH_ALL_FRIENDS = 'RESET_FETCH_ALL_FRIENDS';

export const fetchAllFriends = data => ({
    type: FETCH_ALL_FRIENDS,
    data
});

const fetchAllFriendsSuccessful = data => ({
    type: FETCH_ALL_FRIENDS_SUCCESSFUL,
    data
});

const fetchAllFriendsFailed = data => ({
    type: FETCH_ALL_FRIENDS_FAILED,
    data
});

export const reset = () => ({ type: RESET_FETCH_ALL_FRIENDS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_ALL_FRIENDS),
        switchMap(action => {
            const { token, nextPage, search, groupId } = action.data;
            const headers = addBasicHeaders(token);
            let url = '';
            if (search) {
                url = `${BASEURL_DEV}/api/v1/view/friends/mutual?per_page=10&orderBy=ASC&q=${search}`;
            } else {
                url = `${BASEURL_DEV}/api/v1/view/friends/mutual?per_page=10&orderBy=ASC`;
            }

            if (nextPage) {
                url = BASEURL_DEV + nextPage;
            }

            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return fetchAllFriendsSuccessful(res);
                    return fetchAllFriendsFailed(res);
                });

            return from(req).pipe(
                catchError(err => of(fetchAllFriendsFailed(err.message)))
            );
        })
    );
