import { ofType } from 'redux-observable';
import { mergeMap, switchMap, catchError } from 'rxjs/operators';
import { Observable, of, from } from 'rxjs';

import { BASEURL_DEV } from './base-url';
import { storeToStorage, addBasicHeaders } from '../../utils';

import { checkComplianceAction } from './compliance-epic';

export const SIGNUP = 'SIGNUP';
export const SIGNUP_SUCCESSUFL = 'SIGNUP_SUCCESSUFL';
export const SIGNUP_FAILED = 'SIGNUP_FAILED';
export const RESET = 'RESET';

export const signupUserAction = data => ({
    type: SIGNUP,
    data
});

const signupSuccessful = data => ({
    type: SIGNUP_SUCCESSUFL,
    data
});

const signupFailed = data => ({
    type: SIGNUP_FAILED,
    data
});

export const reset = () => ({ type: RESET });

export default action$ =>
    action$.pipe(
        ofType(SIGNUP),
        switchMap(action => {
            const headers = addBasicHeaders();

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(action.data)
            };

            const req = fetch(`${BASEURL_DEV}/api/register`, options)
                .then(res => res.json())
                .then(async res => {
                    if (res.code === 201) {
                        await storeToStorage(
                            'token',
                            JSON.stringify({
                                type: 'standard',
                                data: res.data.token
                            })
                        );
                        return signupSuccessful(res);
                    } else return signupFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(signupFailed(err.message)))
            );

            // return from(req).pipe(
            //     mergeMap(action =>
            //         of(
            //             action,
            //             checkComplianceAction({
            //                 token: action.data.data.token,
            //             }),
            //         ),
            //     ),
            //     catchError(err => {
            //         return of(signupFailed(err.message));
            //     }),
            // );
        })
    );
