import { ofType } from 'redux-observable';
import { of, from } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const SEARCH_MY_COMPETITIONS = 'SEARCH_MY_COMPETITIONS';
export const SEARCH_MY_COMPETITIONS_SUCCESSFUL =
    'SEARCH_MY_COMPETITIONS_SUCCESSFUL';
export const SEARCH_MY_COMPETITIONS_FAILED = 'SEARCH_MY_COMPETITIONS_FAILED';
export const RESET_SEARCH_MY_COMPETITIONS = 'RESET_SEARCH_MY_COMPETITIONS';

export const searchMyCompetitions = data => ({
    type: SEARCH_MY_COMPETITIONS,
    data
});

const searchMyCompetitionsSuccessful = data => ({
    type: SEARCH_MY_COMPETITIONS_SUCCESSFUL,
    data
});

const searchMyCompetitionsFailed = data => ({
    type: SEARCH_MY_COMPETITIONS_FAILED,
    data
});

export const resetSearchMyCompetitions = () => ({
    type: RESET_SEARCH_MY_COMPETITIONS
});

export default action$ =>
    action$.pipe(
        ofType(SEARCH_MY_COMPETITIONS),
        switchMap(action => {
            const { token, tab, search, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };

            const url = nextPage
                ? `${BASEURL_DEV}/${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/my-competitions/${tab}?q=${search}`;

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return searchMyCompetitionsSuccessful(res);
                    throw new Error(res.messages[0]);
                });
            return from(req).pipe(
                catchError(err =>
                    of(
                        searchMyCompetitionsFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetSearchMyCompetitions()
                    )
                )
            );
        })
    );
