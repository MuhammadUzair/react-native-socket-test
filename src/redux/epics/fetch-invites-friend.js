import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_INVITES_FRIENDS = 'FETCH_INVITES_FRIENDS';
export const FETCH_INVITES_FRIENDS_SUCCESSFUL =
    'FETCH_INVITES_FRIENDS_SUCCESSFUL';
export const FETCH_INVITES_FRIENDS_FAILED = 'FETCH_INVITES_FRIENDS_FAILED';
export const RESET_FETCH_INVITES_FRIENDS = 'FETCH_INVITES_FRIENDS_FAILED';

export const fetchInvitesFriend = data => ({
    type: FETCH_INVITES_FRIENDS,
    data
});

const fetchInvitesFriendSuccessful = data => ({
    type: FETCH_INVITES_FRIENDS_SUCCESSFUL,
    data
});

const fetchInvitesFriendFailed = data => ({
    type: FETCH_INVITES_FRIENDS_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_INVITES_FRIENDS),
        mergeMap(action => {
            const { token, state } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/friends/getInvitesCount`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return fetchInvitesFriendSuccessful(res);
                    return fetchInvitesFriendFailed(res);
                });
            return from(req).pipe(
                catchError(err => {
                    if (err && err.message && err.message[0])
                        return of(
                            fetchInvitesFriendFailed({
                                code: 400,
                                messages: err.message[0]
                            })
                        );
                    return of(
                        fetchInvitesFriendFailed({
                            code: 400,
                            messages: ['Some thing went wrong']
                        })
                    );
                })
            );
        })
    );
