import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const PROFILE_SUBMIT = 'PROFILE_SUBMIT';
export const PROFILE_SUBMIT_SUCCESSFUL = 'PROFILE_SUBMIT_SUCCESSFUL';
export const PROFILE_SUBMIT_FAILED = 'PROFILE_SUBMIT_FAILED';
export const RESET_PROFILE_SUBMIT_FAILED = 'RESET_PROFILE_SUBMIT_FAILED';

export const profileSubmit = data => ({
    type: PROFILE_SUBMIT,
    data
});

const profileSubmitSuccessful = data => ({
    type: PROFILE_SUBMIT_SUCCESSFUL,
    data
});

const profileSubmitFailed = data => ({
    type: PROFILE_SUBMIT_FAILED,
    data
});

export const resetProfileSubmit = data => ({
    type: RESET_PROFILE_SUBMIT_FAILED
});

export default action$ =>
    action$.pipe(
        ofType(PROFILE_SUBMIT),
        switchMap(action => {
            if (action.type == 'RESET_PROFILE_SUBMIT_FAILED') {
                resetProfileSubmit();
                return true;
            }
            const { token, profile } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'PUT',
                headers,
                body: JSON.stringify(profile)
            };
            const req = fetch(`${BASEURL_DEV}/api/v1/view/profile`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return profileSubmitSuccessful(res);
                    return profileSubmitFailed(res);
                });

            return from(req).pipe(
                catchError(err => {
                    if (err) return of(profileSubmitFailed(err.message));
                    return of(
                        profileSubmitFailed({
                            code: 400,
                            messages: ['Some thing went wrong']
                        })
                    );
                })
            );
        })
    );
