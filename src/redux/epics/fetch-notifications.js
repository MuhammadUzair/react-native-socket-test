import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_NOTIFICATIONS = 'FETCH_NOTIFICATIONS';
export const FETCH_NOTIFICATIONS_SUCCESSFUL = 'FETCH_NOTIFICATIONS_SUCCESSFUL';
export const FETCH_NOTIFICATIONS_FAILED = 'FETCH_NOTIFICATIONS_FAILED';
export const RESET_FETCH_NOTIFICATIONS = 'RESET_FETCH_NOTIFICATIONS';
export const REFRESH_FETCH_NOTIFICATIONS = 'REFRESH_FETCH_NOTIFICATIONS';
export const REFRESH_FETCH_NOTIFICATIONS_SUCCESSFUL =
    'REFRESH_FETCH_NOTIFICATIONS_SUCCESSFUL';

export const fetchNotification = data => ({
    type: FETCH_NOTIFICATIONS,
    data
});
export const refreshNotification = data => ({
    type: REFRESH_FETCH_NOTIFICATIONS_SUCCESSFUL,
    data
});

const fetchNotificationSuccessful = data => ({
    type: FETCH_NOTIFICATIONS_SUCCESSFUL,
    data
});

const fetchNotificationFailed = data => ({
    type: FETCH_NOTIFICATIONS_FAILED,
    data
});

export const resetNotification = () => ({ type: RESET_FETCH_NOTIFICATIONS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_NOTIFICATIONS),
        switchMap(action => {
            const { token, nextPage, isRefresh } = action.data;
            const headers = addBasicHeaders(token);
            const options = {
                method: 'GET',
                headers
            };
            const url =
                BASEURL_DEV +
                (nextPage ? nextPage : '/api/v1/view/notifications');
            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (isRefresh) {
                            return refreshNotification(res);
                        } else {
                            return fetchNotificationSuccessful(res);
                        }
                    }
                    return fetchNotificationFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchNotificationFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetNotification()
                    )
                )
            );
        })
    );
