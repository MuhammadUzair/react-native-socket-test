import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const ADD_TEAMS = 'ADD_TEAMS';
export const ADD_TEAMS_SUCCESSFUL = 'ADD_TEAMS_SUCCESSFUL';
export const ADD_TEAMS_FAILED = 'ADD_TEAMS_FAILED';
export const RESET_ADD_TEAMS = 'RESET_ADD_TEAMS';

export const addTeams = data => ({
    type: ADD_TEAMS,
    data
});

const addTeamsSuccessful = data => ({
    type: ADD_TEAMS_SUCCESSFUL,
    data
});

const addTeamsFailed = data => ({
    type: ADD_TEAMS_FAILED,
    data
});

export const reset = () => ({ type: RESET_ADD_TEAMS });

export default action$ =>
    action$.pipe(
        ofType(ADD_TEAMS),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify({ team_ids: action.data.team_ids })
            };

            const req = fetch(`${BASEURL_DEV}/api/v1/view/my-teams`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return addTeamsSuccessful(res);
                    return addTeamsFailed(res);
                });

            return from(req).pipe(
                catchError(err => of(addTeamsFailed(err.message)))
            );
        })
    );
