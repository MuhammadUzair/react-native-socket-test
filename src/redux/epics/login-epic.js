import { ofType } from 'redux-observable';
import { mergeMap, switchMap, catchError } from 'rxjs/operators';
import { Observable, of, from } from 'rxjs';

import { BASEURL_DEV } from './base-url';
import { storeToStorage, addBasicHeaders } from '../../utils';

import { checkComplianceAction } from './compliance-epic';

export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESSUFL = 'LOGIN_SUCCESSUFL';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const RESET = 'RESET';

export const loginUserAction = data => ({
    type: LOGIN,
    data
});

const loginSuccessful = data => ({
    type: LOGIN_SUCCESSUFL,
    data
});

const loginFailed = data => ({
    type: LOGIN_FAILED,
    data
});

export const reset = () => ({ type: RESET });

export default action$ =>
    action$.pipe(
        ofType(LOGIN),
        switchMap(action => {
            const headers = addBasicHeaders();

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(action.data)
            };

            const req = fetch(`${BASEURL_DEV}/api/v1/view/login`, options)
                .then(res => res.json())
                .then(async res => {
                    if (res.code === 200) {
                        await storeToStorage(
                            'token',
                            JSON.stringify({
                                type: 'standard',
                                data: res.data.token
                            })
                        );
                        return loginSuccessful(res);
                    } else return loginFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(loginFailed(err.message)))
            );
            // return from(req).pipe(
            //     mergeMap(action =>
            //         of(
            //             action,
            //             checkComplianceAction({ token: action.data.data.token })
            //         )
            //     ),
            //     catchError(err => {
            //         return of(loginFailed(err.message));
            //     })
            // );
        })
    );
