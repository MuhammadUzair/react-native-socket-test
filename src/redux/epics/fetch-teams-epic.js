import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_TEAMS = 'FETCH_TEAMS';
export const FETCH_TEAMS_SUCCESSFUL = 'FETCH_TEAMS_SUCCESSFUL';
export const FETCH_TEAMS_FAILED = 'FETCH_TEAMS_FAILED';
export const FETCH_TEAMS_RESET = 'RESET_FETCH_TEAMS';

export const fetchTeams = data => ({
    type: FETCH_TEAMS,
    data
});

const fetchTeamsSuccessful = data => ({
    type: FETCH_TEAMS_SUCCESSFUL,
    data
});

const fetchTeamsFailed = data => ({
    type: FETCH_TEAMS_FAILED,
    data
});

export const reset = data => ({ type: FETCH_TEAMS_RESET });

export default action$ =>
    action$.pipe(
        ofType(FETCH_TEAMS),
        switchMap(action => {
            const headers = addBasicHeaders(action.data);

            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/my-leagues/teams`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return fetchTeamsSuccessful(res);
                    return fetchTeamsFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(fetchTeamsFailed(err.message)))
            );
        })
    );
