import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { addBasicHeaders } from '../../utils';
import { BASEURL_DEV } from './base-url';

export const FETCH_MY_GROUPS = 'FETCH_MY_GROUPS';
export const FETCH_MY_GROUPS_SUCCESSFUL = 'FETCH_MY_GROUPS_SUCCESSFUL';
export const FETCH_MY_GROUPS_FAILED = 'FETCH_MY_GROUPS_FAILED';
export const RESET_FETCH_MY_GROUPS = 'RESET_FETCH_MY_GROUPS';
export const REFRESH_MY_GROUPS = 'REFRESH_MY_GROUPS';
export const REFRESH_MY_GROUPS_SUCCESSFUL = 'REFRESH_MY_GROUPS_SUCCESSFUL';
export const REFRESH_MY_GROUPS_FAILED = 'REFRESH_MY_GROUPS_FAILED';

export const fetchMyGroups = data => ({
    type: FETCH_MY_GROUPS,
    data
});

export const refreshMyGroups = data => ({
    type: REFRESH_MY_GROUPS,
    data
});

export const refreshMyGroupsSuccessful = data => ({
    type: REFRESH_MY_GROUPS_SUCCESSFUL,
    data
});

const refreshMyGroupsFailed = data => ({
    type: REFRESH_MY_GROUPS_FAILED,
    data
});

const fetchMyGroupsSuccessful = data => ({
    type: FETCH_MY_GROUPS_SUCCESSFUL,
    data
});

const fetchMyGroupsFailed = data => ({
    type: FETCH_MY_GROUPS_FAILED,
    data
});

export const resetMyGroups = () => ({ type: RESET_FETCH_MY_GROUPS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_MY_GROUPS, REFRESH_MY_GROUPS),
        switchMap(action => {
            const { token, nextPage, group } = action.data;
            const headers = addBasicHeaders(token);

            const url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/groups/${group}`;

            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (action.type === FETCH_MY_GROUPS)
                            return fetchMyGroupsSuccessful({ ...res, group });
                        return refreshMyGroupsSuccessful({ ...res, group });
                    } else {
                        if (action.type === FETCH_MY_GROUPS)
                            return fetchMyGroupsFailed({ ...res, group });
                        return refreshMyGroupsFailed({ ...res, group });
                    }
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchMyGroupsFailed({
                            code: 400,
                            messages: [err.message],
                            group
                        }),
                        resetMyGroups()
                    )
                )
            );
        })
    );
