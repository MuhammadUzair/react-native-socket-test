import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_FB_FRIENDS = 'FETCH_FB_FRIENDS';
export const FETCH_FB_FRIENDS_SUCCESSFUL = 'FETCH_FB_FRIENDS_SUCCESSFUL';
export const FETCH_FB_FRIENDS_FAILED = 'FETCH_FB_FRIENDS_FAILED';
export const RESET_FETCH_FB_FRIENDS = 'RESET_FETCH_FB_FRIENDS';

export const fetchFBFriends = data => ({
    type: FETCH_FB_FRIENDS,
    data
});

const fetchFBFriendsSuccessful = data => ({
    type: FETCH_FB_FRIENDS_SUCCESSFUL,
    data
});

const fetchFBFriendsFailed = data => ({
    type: FETCH_FB_FRIENDS_FAILED,
    data
});

export const reset = () => ({ type: RESET_FETCH_FB_FRIENDS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_FB_FRIENDS),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify({ data: action.data.friends })
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/friends/social`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return fetchFBFriendsSuccessful(res);
                    return fetchFBFriendsFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(fetchFBFriendsFailed(err.message)))
            );
        })
    );
