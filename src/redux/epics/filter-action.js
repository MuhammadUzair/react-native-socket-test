export const APPLY_FILTER = 'APPLY_FILTER';
export const RESET_FILTER = 'RESET_FILTER';

export const applyFilter = data => ({
    type: APPLY_FILTER,
    data
});

export const resetFilter = () => ({ type: RESET_FILTER });
