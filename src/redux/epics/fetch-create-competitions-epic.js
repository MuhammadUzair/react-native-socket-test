import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { addBasicHeaders } from '../../utils';
import { BASEURL_DEV } from './base-url';

export const FETCH_CREATE_COMPETITIONS = 'FETCH_CREATE_COMPETITIONS';
export const FETCH_CREATE_COMPETITIONS_SUCCESSFUL =
    'FETCH_CREATE_COMPETITIONS_SUCCESSFUL';
export const FETCH_CREATE_COMPETITIONS_FAILED =
    'FETCH_CREATE_COMPETITIONS_FAILED';
export const RESET_FETCH_CREATE_COMPETITIONS =
    'RESET_FETCH_CREATE_COMPETITIONS';

export const fetchCreateCompetitions = data => ({
    type: FETCH_CREATE_COMPETITIONS,
    data
});

const fetchCreateCompetitionsSuccessful = data => ({
    type: FETCH_CREATE_COMPETITIONS_SUCCESSFUL,
    data
});

const fetchCreateCompetitionsFailed = data => ({
    type: FETCH_CREATE_COMPETITIONS_FAILED,
    data
});

export const REFRESH_CREATE_COMPETITIONS = 'REFRESH_CREATE_COMPETITIONS';
export const REFRESH_CREATE_COMPETITIONS_SUCCESSFUL =
    'REFRESH_CREATE_COMPETITIONS_SUCCESSFUL';
export const REFRESH_CREATE_COMPETITIONS_FAILED =
    'REFRESH_CREATE_COMPETITIONS_FAILED';

export const refreshCreateCompetitions = data => ({
    type: REFRESH_CREATE_COMPETITIONS,
    data
});

const refreshCreateCompetitionsSuccessful = data => ({
    type: REFRESH_CREATE_COMPETITIONS_SUCCESSFUL,
    data
});

const refreshCreateCompetitionsFailed = data => ({
    type: REFRESH_CREATE_COMPETITIONS_FAILED,
    data
});

export const resetFetchCreateCompetitions = () => ({
    type: RESET_FETCH_CREATE_COMPETITIONS
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_CREATE_COMPETITIONS, REFRESH_CREATE_COMPETITIONS),
        switchMap(action => {
            const { token, league, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };

            const url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/get-competitons-by-league?league=${league}`;

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (action.type === REFRESH_CREATE_COMPETITIONS)
                            return refreshCreateCompetitionsSuccessful({
                                ...res,
                                league
                            });
                        else
                            return fetchCreateCompetitionsSuccessful({
                                ...res,
                                league
                            });
                    } else {
                        if (action.type === REFRESH_CREATE_COMPETITIONS)
                            return refreshCreateCompetitionsFailed({
                                ...res,
                                league
                            });
                        else
                            return fetchCreateCompetitionsFailed({
                                ...res,
                                league
                            });
                    }
                });
            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchCreateCompetitionsFailed({
                            league,
                            code: 400,
                            messages: [err.message]
                        })
                    )
                )
            );
        })
    );
