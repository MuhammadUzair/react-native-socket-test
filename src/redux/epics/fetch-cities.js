import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_CITIES = 'FETCH_CITIES';
export const FETCH_CITIES_SUCCESSFUL = 'FETCH_CITIES_SUCCESSFUL';
export const FETCH_CITIES_FAILED = 'FETCH_CITIES_FAILED';
export const RESET_FETCH_CITIES = 'FETCH_CITIES_FAILED';

export const fetchCities = data => ({
    type: FETCH_CITIES,
    data
});

const fetchCitiesSuccessful = data => ({
    type: FETCH_CITIES_SUCCESSFUL,
    data
});

const fetchCitiesFailed = data => ({
    type: FETCH_CITIES_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_CITIES),
        mergeMap(action => {
            const { token, state } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/user/cities/` + state,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res && res.length > 0)
                        return fetchCitiesSuccessful(res);
                    return fetchCitiesFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(fetchCitiesFailed(err.message)))
            );
        })
    );
