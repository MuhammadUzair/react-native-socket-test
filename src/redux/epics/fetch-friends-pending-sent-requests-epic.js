import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_FRIENDS_PENDING_SENT = 'FETCH_FRIENDS_PENDING_SENT';
export const FETCH_FRIENDS_PENDING_SENT_SUCCESSFUL =
    'FETCH_FRIENDS_PENDING_SENT_SUCCESSFUL';
export const FETCH_FRIENDS_PENDING_SENT_FAILED =
    'FETCH_FRIENDS_PENDING_SENT_FAILED';
export const RESET_FETCH_FRIENDS_PENDING_SENT =
    'RESET_FETCH_FRIENDS_PENDING_SENT';
export const REFRESH_FRIENDS_PENDING_SENT = 'REFRESH_FRIENDS_PENDING_SENT';

export const fetchFriendsPendingSentRequests = data => ({
    type: FETCH_FRIENDS_PENDING_SENT,
    data
});

const fetchFriendsPendingSentRequestsSuccessful = data => ({
    type: FETCH_FRIENDS_PENDING_SENT_SUCCESSFUL,
    data
});

const refreshPendingSentRequestsSuccessful = data => ({
    type: REFRESH_FRIENDS_PENDING_SENT,
    data
});

const fetchFriendsPendingSentRequestsFailed = data => ({
    type: FETCH_FRIENDS_PENDING_SENT_FAILED,
    data
});

export const resetFriendsPendingSentRequests = () => ({
    type: RESET_FETCH_FRIENDS_PENDING_SENT
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_FRIENDS_PENDING_SENT),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);

            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/friends/request/sent?page=${
                    action.data.page
                }&per_page=10&orderBy=ASC`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        if (action.data.isRefresh) {
                            return refreshPendingSentRequestsSuccessful(res);
                        } else {
                            return fetchFriendsPendingSentRequestsSuccessful(
                                res
                            );
                        }
                    return fetchFriendsPendingSentRequestsFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(fetchFriendsPendingSentRequestsFailed(err.message))
                )
            );
        })
    );
