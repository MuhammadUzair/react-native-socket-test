import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const REMOVE_FRIENDS = 'REMOVE_FRIENDS';
export const REMOVE_FRIENDS_SUCCESSFUL = 'REMOVE_FRIENDS_SUCCESSFUL';
export const REMOVE_FRIENDS_FAILED = 'REMOVE_FRIENDS_FAILED';

export const removeFriends = data => ({
    type: REMOVE_FRIENDS,
    data
});

const removeFriendsSuccessful = data => ({
    type: REMOVE_FRIENDS_SUCCESSFUL,
    data
});

const removeFriendsFailed = data => ({
    type: REMOVE_FRIENDS_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(REMOVE_FRIENDS),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);
            const friend_id = action.data.friend_id;

            const options = {
                method: 'DELETE',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/friends/${friend_id}`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return removeFriendsSuccessful(res);
                    return removeFriendsFailed(res);
                });

            return from(req).pipe(
                catchError(err => {
                    of(removeFriendsFailed(err.message));
                })
            );
        })
    );
