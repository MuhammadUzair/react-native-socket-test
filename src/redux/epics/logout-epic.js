import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_REQUEST_SUCCESSFUL = 'LOGOUT_REQUEST_SUCCESSFUL';
export const LOGOUT_REQUEST_FAILED = 'LOGOUT_REQUEST_FAILED';

export const logoutRequest = data => ({
    type: LOGOUT_REQUEST,
    data
});

const logoutRequestSuccessful = data => ({
    type: LOGOUT_REQUEST_SUCCESSFUL,
    data
});

const logoutRequestFailed = data => ({
    type: LOGOUT_REQUEST_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(LOGOUT_REQUEST),
        switchMap(action => {
            const { token, device } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(device)
            };
            const req = fetch(`${BASEURL_DEV}/api/logout`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return logoutRequestSuccessful(res);
                    return logoutRequestFailed(res);
                });

            return from(req).pipe(
                catchError(err => {
                    of(logoutRequestFailed(err.messages));
                })
            );
        })
    );
