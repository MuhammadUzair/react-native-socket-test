import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const ADD_FRIENDS = 'ADD_FRIENDS';
export const ADD_FRIENDS_SUCCESSFUL = 'ADD_FRIENDS_SUCCESSFUL';
export const ADD_FRIENDS_FAILED = 'ADD_FRIENDS_FAILED';
export const RESET_ADD_FRIENDS = 'RESET_ADD_FRIENDS';

export const addFriends = data => ({
    type: ADD_FRIENDS,
    data
});

const addFriendsSuccessful = data => ({
    type: ADD_FRIENDS_SUCCESSFUL,
    data
});

const addFriendsFailed = data => ({
    type: ADD_FRIENDS_FAILED,
    data
});

export const resetFriendsAdded = () => ({ type: RESET_ADD_FRIENDS });

export default action$ =>
    action$.pipe(
        ofType(ADD_FRIENDS),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify({
                    friend_id: action.data.friend_id,
                    request_from: 2
                })
            };

            const req = fetch(`${BASEURL_DEV}/api/v1/view/friends`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return addFriendsSuccessful(res);
                    return addFriendsFailed(res);
                });

            return from(req).pipe(
                catchError(err => of(addFriendsFailed(err.message)))
            );
        })
    );
