import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_GATEWAY_TRANSACTIONS = 'FETCH_GATEWAY_TRANSACTIONS';
export const FETCH_GATEWAY_TRANSACTIONS_FAILED =
    'FETCH_GATEWAY_TRANSACTIONS_FAILED';
export const FETCH_GATEWAY_TRANSACTIONS_SUCCESSFUL =
    'FETCH_GATEWAY_TRANSACTIONS_SUCCESSFUL';
export const REFRESH_GATEWAY_TRANSACTIONS = 'REFRESH_GATEWAY_TRANSACTIONS';
export const REFRESH_GATEWAY_TRANSACTIONS_SUCCESSFUL =
    'REFRESH_GATEWAY_TRANSACTIONS_SUCCESSFUL';
export const RESET_GATEWAY_TRANSACTIONS = 'RESET_GATEWAY_TRANSACTIONS';

export const fetchGatewayTransaction = data => ({
    type: FETCH_GATEWAY_TRANSACTIONS,
    data,
});

const fetchGatewayTransactionFailed = data => ({
    type: FETCH_GATEWAY_TRANSACTIONS_FAILED,
    data,
});

const fetchGatewayTransactionSuccessful = data => ({
    type: FETCH_GATEWAY_TRANSACTIONS_SUCCESSFUL,
    data,
});

export const refreshGatewayTransaction = data => ({
    type: REFRESH_GATEWAY_TRANSACTIONS,
    data,
});

const refreshGatewayTransactionSuccessful = data => ({
    type: REFRESH_GATEWAY_TRANSACTIONS_SUCCESSFUL,
    data,
});

export const resetGatewayTransaction = () => ({
    type: RESET_GATEWAY_TRANSACTIONS,
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_GATEWAY_TRANSACTIONS, REFRESH_GATEWAY_TRANSACTIONS),
        switchMap(action => {
            const { token, filter, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const url =
                BASEURL_DEV +
                (nextPage
                    ? nextPage
                    : `/api/v1/view/inprocess?ttype=${
                          filter === 'All' ? '' : filter
                      }`);
            const options = {
                method: 'GET',
                headers,
            };

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (action.type === REFRESH_GATEWAY_TRANSACTIONS) {
                            return refreshGatewayTransactionSuccessful({
                                ...res,
                                filter,
                            });
                        }
                        return fetchGatewayTransactionSuccessful({
                            ...res,
                            filter,
                        });
                    }
                    return fetchGatewayTransactionFailed({ ...res, filter });
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchGatewayTransactionFailed({
                            code: 400,
                            messages: [err.message],
                        }),
                        resetGatewayTransaction(),
                    ),
                ),
            );
        }),
    );
