import { ofType } from 'redux-observable';
import { of, from } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const SEARCH_COMPETITIONS = 'SEARCH_COMPETITIONS';
export const SEARCH_COMPETITIONS_SUCCESSFUL = 'SEARCH_COMPETITIONS_SUCCESSFUL';
export const SEARCH_COMPETITIONS_FAILED = 'SEARCH_COMPETITIONS_FAILED';
export const RESET_SEARCH_COMPETITIONS = 'RESET_SEARCH_COMPETITIONS';

export const searchCompetitions = data => ({
    type: SEARCH_COMPETITIONS,
    data
});

const searchCompetitionsSuccessful = data => ({
    type: SEARCH_COMPETITIONS_SUCCESSFUL,
    data
});

const searchCompetitionsFailed = data => ({
    type: SEARCH_COMPETITIONS_FAILED,
    data
});

export const resetSearchCompetitions = () => ({
    type: RESET_SEARCH_COMPETITIONS
});

export default action$ =>
    action$.pipe(
        ofType(SEARCH_COMPETITIONS),
        switchMap(action => {
            const { token, league, search, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };

            const url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/my-competitions/friends?league=${league}&q=${search}`;

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return searchCompetitionsSuccessful(res);
                    throw new Error(res.messages[0]);
                });
            return from(req).pipe(
                catchError(err =>
                    of(
                        searchCompetitionsFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetSearchCompetitions()
                    )
                )
            );
        })
    );
