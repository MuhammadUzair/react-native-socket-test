import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_FRIENDS = 'FETCH_FRIENDS';
export const FETCH_FRIENDS_SUCCESSFUL = 'FETCH_FRIENDS_SUCCESSFUL';
export const FETCH_FRIENDS_FAILED = 'FETCH_FRIENDS_FAILED';
export const RESET_FETCH_FRIENDS = 'RESET_FETCH_FRIENDS';

export const fetchFriends = data => ({
    type: FETCH_FRIENDS,
    data,
});

const fetchFriendsSuccessful = data => ({
    type: FETCH_FRIENDS_SUCCESSFUL,
    data,
});

const fetchFriendsFailed = data => ({
    type: FETCH_FRIENDS_FAILED,
    data,
});

export const reset = () => ({ type: RESET_FETCH_FRIENDS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_FRIENDS),
        switchMap(action => {
            const { token, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers,
            };

            const url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/friends/mutual`;

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return fetchFriendsSuccessful(res);
                    return fetchFriendsFailed(res);
                });

            return from(req).pipe(
                catchError(err => of(fetchFriendsFailed(err.message))),
            );
        }),
    );
