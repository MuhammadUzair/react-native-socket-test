import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS =
    'FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS';
export const FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_SUCCESSFUL =
    'FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_SUCCESSFUL';
export const FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_FAILED =
    'FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_FAILED';
export const RESET_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS =
    'RESET_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS';
export const REFRESH_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS =
    'REFRESH_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS';

export const fetchFriendsPendingReceiveRequests = data => ({
    type: FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS,
    data
});

const fetchFriendsPendingReceiveRequestsSuccessful = data => ({
    type: FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_SUCCESSFUL,
    data
});

const refreshFriendsPendingReceiveRequestsSuccessful = data => ({
    type: REFRESH_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS,
    data
});

const fetchFriendsPendingReceiveRequestsFailed = data => ({
    type: FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS_FAILED,
    data
});

export const resetFriendsPendingReceiveRequests = () => ({
    type: RESET_FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_FRIENDS_PENDING_RECEIVE_REQUESTS),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);
            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/friends/request/receive?page=${
                    action.data.page
                }&per_page=10&orderBy=ASC`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (action.data.isRefresh) {
                            refreshFriendsPendingReceiveRequestsSuccessful(
                                res.data
                            );
                        } else {
                            return fetchFriendsPendingReceiveRequestsSuccessful(
                                res
                            );
                        }
                    }
                    return fetchFriendsPendingReceiveRequestsFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(fetchFriendsPendingReceiveRequestsFailed(err.message))
                )
            );
        })
    );
