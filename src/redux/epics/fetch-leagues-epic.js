import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_LEAGUES = 'FETCH_LEAGUES';
export const FETCH_LEAGUES_SUCCESSFUL = 'FETCH_LEAGUES_SUCCESSFUL';
export const FETCH_LEAGUES_FAILED = 'FETCH_LEAGUES_FAILED';
export const RESET_FETCH_LEAGUES = 'FETCH_LEAGUES_FAILED';

export const fetchLeagues = data => ({
    type: FETCH_LEAGUES,
    data
});

const fetchLeaguesSuccessful = data => ({
    type: FETCH_LEAGUES_SUCCESSFUL,
    data
});

const fetchLeaguesFailed = data => ({
    type: FETCH_LEAGUES_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(FETCH_LEAGUES),
        switchMap(action => {
            const headers = addBasicHeaders(action.data);

            const options = {
                mehtod: 'GET',
                headers
            };
            const req = fetch(`${BASEURL_DEV}/api/v1/view/leagues`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code === 200) return fetchLeaguesSuccessful(res);
                    return fetchLeaguesFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(fetchLeaguesFailed(err.message)))
            );
        })
    );
