import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const READ_ALL_NOTIFICATION = 'READ_ALL_NOTIFICATION';
export const READ_ALL_NOTIFICATION_SUCCESSFUL =
    'READ_ALL_NOTIFICATION_SUCCESSFUL';
export const READ_ALL_NOTIFICATION_FAILED = 'READ_ALL_NOTIFICATION_FAILED';
export const RESET_READ_ALL_NOTIFICATION = 'RESET_READ_ALL_NOTIFICATION';

export const readAllNotification = data => ({
    type: READ_ALL_NOTIFICATION,
    data
});

export const readAllNotificationSuccessful = data => ({
    type: READ_ALL_NOTIFICATION_SUCCESSFUL,
    data
});

const readAllNotificationFailed = data => ({
    type: READ_ALL_NOTIFICATION_FAILED,
    data
});

export const resetreadAllNotification = () => ({
    type: RESET_READ_ALL_NOTIFICATION
});

export default action$ =>
    action$.pipe(
        ofType(READ_ALL_NOTIFICATION),
        switchMap(action => {
            const { token } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'POST',
                headers
            };
            const url =
                BASEURL_DEV + '/api/v1/view/notifications/mark-all-read';

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if ((res.code >= 200 && res.code < 300) || res.status)
                        return readAllNotificationSuccessful(res);
                    return readAllNotificationFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        readAllNotificationFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetreadAllNotification()
                    )
                )
            );
        }),
        catchError(err =>
            of(
                readAllNotificationFailed({
                    code: 400,
                    messages: [err.message]
                }),
                resetreadAllNotification()
            )
        )
    );
