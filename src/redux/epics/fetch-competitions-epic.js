import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_COMPETITIONS = 'FETCH_COMPETITIONS';
export const FETCH_COMPETITIONS_SUCCESSFUL = 'FETCH_COMPETITIONS_SUCCESSFUL';
export const FETCH_COMPETITIONS_FAILED = 'FETCH_COMPETITIONS_FAILED';
export const RESET_FETCH_COMPETITIONS = 'RESET_FETCH_COMPETITIONS';

export const fetchCompetitions = data => ({
    type: FETCH_COMPETITIONS,
    data,
});

const fetchCompetitionsSuccessful = data => ({
    type: FETCH_COMPETITIONS_SUCCESSFUL,
    data,
});

const fetchCompetitionsFailed = data => ({
    type: FETCH_COMPETITIONS_FAILED,
    data,
});

export const REFRESH_COMPETITIONS = 'REFRESH_COMPETITIONS';
export const REFRESH_COMPETITIONS_SUCCESSFUL =
    'REFRESH_COMPETITIONS_SUCCESSFUL';
export const REFRESH_COMPETITIONS_FAILED = 'REFRESH_COMPETITIONS_FAILED';

export const refreshCompetitions = data => ({
    type: REFRESH_COMPETITIONS,
    data,
});

const refreshCompetitionsSuccessful = data => ({
    type: REFRESH_COMPETITIONS_SUCCESSFUL,
    data,
});

const refreshCompetitionsFailed = data => ({
    type: REFRESH_COMPETITIONS_FAILED,
    data,
});

export const resetCompetitions = () => ({ type: RESET_FETCH_COMPETITIONS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_COMPETITIONS, REFRESH_COMPETITIONS),
        mergeMap(action => {
            const { league, token, nextPage } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers,
            };

            let url = nextPage
                ? `${BASEURL_DEV}${nextPage}`
                : `${BASEURL_DEV}/api/v1/view/my-competitions/friends?league=${league}`;

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (action.type === REFRESH_COMPETITIONS)
                            return refreshCompetitionsSuccessful({
                                ...res,
                                league,
                            });
                        else
                            return fetchCompetitionsSuccessful({
                                ...res,
                                league,
                            });
                    } else {
                        if (action.type === REFRESH_COMPETITIONS)
                            return refreshCompetitionsFailed({
                                ...res,
                                league,
                            });
                        else return fetchCompetitionsFailed({ ...res, league });
                    }
                });
            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchCompetitionsFailed({
                            league,
                            code: 400,
                            messages: [err.message],
                        }),
                    ),
                ),
            );
        }),
    );
