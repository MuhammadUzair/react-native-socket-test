import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const INVITE_FRIENDS = 'INVITE_FRIENDS';
export const INVITE_FRIENDS_SUCCESSFUL = 'INVITE_FRIENDS_SUCCESSFUL';
export const INVITE_FRIENDS_FAILED = 'INVITE_FRIENDS_FAILED';
export const RESET_INVITE_FRIENDS = 'RESET_INVITE_FRIENDS';

export const inviteFriendsSubmit = data => ({
    type: INVITE_FRIENDS,
    data
});

const inviteFriendsSubmitSuccessful = data => ({
    type: INVITE_FRIENDS_SUCCESSFUL,
    data
});

const inviteFriendsSubmitFailed = data => ({
    type: INVITE_FRIENDS_FAILED,
    data
});

export const resetInviteFriendsSubmit = () => ({
    type: RESET_INVITE_FRIENDS
});

export default action$ =>
    action$.pipe(
        ofType(INVITE_FRIENDS),
        switchMap(action => {
            if (action.type == 'RESET_INVITE_FRIENDS_FAILED') {
                resetInviteFriendsSubmit();
                return true;
            }
            const { token, invitations } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(invitations)
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/friends/invite_friends`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return inviteFriendsSubmitSuccessful(res);
                    return inviteFriendsSubmitFailed(res);
                });

            return from(req).pipe(
                catchError(err => {
                    if (err) return of(inviteFriendsSubmitFailed(err.message));
                    return of(
                        inviteFriendsSubmitFailed({
                            code: 400,
                            messages: ['Some thing went wrong']
                        })
                    );
                })
            );
        })
    );
