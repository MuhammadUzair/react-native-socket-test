import { ofType } from 'redux-observable';
import { mergeMap, switchMap, catchError } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { Platform } from 'react-native';

import { BASEURL_DEV } from './base-url';
import { storeToStorage, addBasicHeaders } from '../../utils';
import DeviceInfo from 'react-native-device-info';

import { checkComplianceAction } from './compliance-epic';

export const SOCIAL_SIGNUP = 'SOCIAL_SIGNUP';
export const SOCIAL_SIGNUP_SUCCESSFUL = 'SOCIAL_SIGNUP_SUCCESSFUL';
export const SOCIAL_SIGNUP_FAILED = 'SOCIAL_SIGNUP_FAILED';

export const socialSignup = data => ({
    type: SOCIAL_SIGNUP,
    data
});

const socialSignupSuccessful = data => ({
    type: SOCIAL_SIGNUP_SUCCESSFUL,
    data
});

const socialSignupFailed = data => ({
    type: SOCIAL_SIGNUP_FAILED,
    data
});

export default action$ =>
    action$.pipe(
        ofType(SOCIAL_SIGNUP),
        switchMap(action => {
            const headers = addBasicHeaders();

            const body = {
                email: action.data.social_data.email
                    ? action.data.social_data.email
                    : '',
                password: '',
                social_data: action.data.social_data,
                social_type: action.data.social_type,
                device_id: DeviceInfo.getUniqueID(),
                device_type: Platform.OS == 'android' ? 'android' : 'ios',
                device_token: action.data.device_token
            };

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(body)
            };

            const req = fetch(`${BASEURL_DEV}/api/v1/view/sociallogin`, options)
                .then(res => res.json())
                .then(async res => {
                    if (res.code === 200) {
                        await storeToStorage(
                            'token',
                            JSON.stringify({
                                type: action.data.social_type,
                                data: res.data.token
                            })
                        );

                        return socialSignupSuccessful(res);
                    }
                    return socialSignupFailed(res);
                });

            return from(req).pipe(
                mergeMap(action =>
                    of(
                        action,
                        checkComplianceAction({
                            token: action.data.data.token
                        })
                    )
                ),
                catchError(err => of(socialSignupFailed(err.message)))
            );
        })
    );
