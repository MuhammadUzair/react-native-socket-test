import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const UPLOAD_IMAGE = 'UPLOAD_IMAGE';
export const UPLOAD_IMAGE_SUCCESSFUL = 'UPLOAD_IMAGE_SUCCESSFUL';
export const UPLOAD_IMAGE_FAILED = 'UPLOAD_IMAGE_FAILED';
export const RESET_UPLOAD_IMAGE = 'RESET_UPLOAD_IMAGE';

export const uploadImageApi = data => ({
    type: UPLOAD_IMAGE,
    data
});

const uploadImageSuccessful = data => ({
    type: UPLOAD_IMAGE_SUCCESSFUL,
    data
});

const uploadImageFailed = data => ({
    type: UPLOAD_IMAGE_FAILED,
    data
});

export const resetUploadImageApi = data => ({
    type: RESET_UPLOAD_IMAGE
});

export default action$ =>
    action$.pipe(
        ofType(UPLOAD_IMAGE),
        switchMap(action => {
            const { token, image } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'post',
                headers,
                body: JSON.stringify(image)
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/profile/upload`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return uploadImageSuccessful(res);
                    return uploadImageFailed(res);
                });

            return from(req).pipe(
                catchError(err => {
                    of(uploadImageFailed(err.message));
                })
            );
        })
    );
