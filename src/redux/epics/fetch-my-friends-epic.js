import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const FETCH_MY_FRIENDS = 'FETCH_MY_FRIENDS';
export const FETCH_MY_FRIENDS_SUCCESSFUL = 'FETCH_MY_FRIENDS_SUCCESSFUL';
export const FETCH_MY_FRIENDS_FAILED = 'FETCH_MY_FRIENDS_FAILED';
export const RESET_FETCH_MY_FRIENDS = 'RESET_FETCH_MY_FRIENDS';
export const REFRESH_FETCH_MY_FRIENDS = 'REFRESH_FETCH_MY_FRIENDS';
export const REFRESH_FETCH_MY_FRIENDS_SUCCESSFUL =
    'REFRESH_FETCH_MY_FRIENDS_SUCCESSFUL';

export const fetchMyFriends = data => ({
    type: FETCH_MY_FRIENDS,
    data
});

const fetchMyFriendsSuccessful = data => ({
    type: FETCH_MY_FRIENDS_SUCCESSFUL,
    data
});
const refreshMyFriendsSuccessful = data => ({
    type: REFRESH_FETCH_MY_FRIENDS_SUCCESSFUL,
    data
});

const fetchMyFriendsFailed = data => ({
    type: FETCH_MY_FRIENDS_FAILED,
    data
});

export const resetMyFriend = () => ({ type: RESET_FETCH_MY_FRIENDS });

export default action$ =>
    action$.pipe(
        ofType(FETCH_MY_FRIENDS),
        switchMap(action => {
            const {
                token,
                nextPage,
                isCreateCompetition,
                isSearch,
                search,
                isRefresh
            } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };
            let url =
                BASEURL_DEV +
                (nextPage
                    ? nextPage + '&loadPending=1'
                    : '/api/v1/view/friends/myfriends?loadPending=1');

            let createCompetitionFriendUrl =
                BASEURL_DEV +
                (nextPage
                    ? nextPage
                    : isSearch
                    ? `/api/v1/view/friends/myfriends?page=1&q=` + search
                    : `/api/v1/view/friends/myfriends?page=1`);

            const req = fetch(
                isCreateCompetition ? createCompetitionFriendUrl : url,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (isRefresh) return refreshMyFriendsSuccessful(res);
                        return fetchMyFriendsSuccessful(res);
                    }
                    return fetchMyFriendsFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchMyFriendsFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetMyFriend()
                    )
                )
            );
        }),
        catchError(err =>
            of(
                fetchMyFriendsFailed({ code: 400, messages: [err.message] }),
                resetMyFriend()
            )
        )
    );
