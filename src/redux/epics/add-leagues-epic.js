import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const ADD_LEAGUES = 'ADD_LEAGUES';
export const ADD_LEAGUES_SUCCESSFUL = 'ADD_LEAGUES_SUCCESSFUL';
export const ADD_LEAGUES_FAILED = 'ADD_LEAGUES_FAILED';
export const RESET_ADD_LEAGUES = 'RESET_ADD_LEAGUES';

export const addLeagues = data => ({
    type: ADD_LEAGUES,
    data
});

const addLeaguesSuccessful = data => ({
    type: ADD_LEAGUES_SUCCESSFUL,
    data
});

const addLeaguesFailed = data => ({
    type: ADD_LEAGUES_FAILED,
    data
});

export const reset = () => ({ type: RESET_ADD_LEAGUES });

export default action$ =>
    action$.pipe(
        ofType(ADD_LEAGUES),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);

            const body = {
                league_ids: action.data.leagueIds
            };

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(body)
            };

            const req = fetch(`${BASEURL_DEV}/api/v1/view/my-leagues`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return addLeaguesSuccessful(res);
                    return addLeaguesFailed(res);
                });
            return from(req).pipe(
                catchError(err => of(addLeaguesFailed(err.message)))
            );
        })
    );
