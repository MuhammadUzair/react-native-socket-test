import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const ACCEPT_FRIEND_REQUEST = 'ACCEPT_FRIEND_REQUEST';
export const ACCEPT_FRIEND_REQUEST_SUCCESSFUL =
    'ACCEPT_FRIEND_REQUEST_SUCCESSFUL';
export const ACCEPT_FRIEND_REQUEST_FAILED = 'ACCEPT_FRIEND_REQUEST_FAILED';
export const RESET_ACCEPT_FRIEND_REQUEST = 'RESET_ACCEPT_FRIEND_REQUEST';

export const acceptFriendRequest = data => ({
    type: ACCEPT_FRIEND_REQUEST,
    data
});

const acceptFriendRequestSuccessful = data => ({
    type: ACCEPT_FRIEND_REQUEST_SUCCESSFUL,
    data
});

const acceptFriendRequestFailed = data => ({
    type: ACCEPT_FRIEND_REQUEST_FAILED,
    data
});
export const resetAcceptFriendRequestApi = () => ({
    type: RESET_ACCEPT_FRIEND_REQUEST
});

export default action$ =>
    action$.pipe(
        ofType(ACCEPT_FRIEND_REQUEST),
        switchMap(action => {
            const headers = addBasicHeaders(action.data.token);
            const friend_id = action.data.friend_id;

            const options = {
                method: 'PUT',
                headers
            };
            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/friends/request/${friend_id}/accepted`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return acceptFriendRequestSuccessful(res);
                    return acceptFriendRequestFailed(res);
                });

            return from(req).pipe(
                catchError(err => {
                    of(acceptFriendRequestFailed(err.message));
                })
            );
        })
    );
