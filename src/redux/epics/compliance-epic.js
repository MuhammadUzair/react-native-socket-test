import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders, complianceCheck } from '../../utils';

export const CHECK_COMPLIANCE = 'CHECK_COMPLIANCE';
export const CHECK_COMPLIANCE_SUCCESSFUL = 'CHECK_COMPLIANCE_SUCCESSFUL';
export const CHECK_COMPLIANCE_FAILED = 'CHECK_COMPLIANCE_FAILED';
export const RESET_CHECK_COMPLIANCE = 'RESET_CHECK_COMPLIANCE';

export const checkComplianceAction = data => ({
    type: CHECK_COMPLIANCE,
    data,
});

const checkComplianceSuccessful = data => ({
    type: CHECK_COMPLIANCE_SUCCESSFUL,
    data,
});

const checkComplianceFailed = data => ({
    type: CHECK_COMPLIANCE_FAILED,
    data,
});

export const resetCreateGroup = () => ({ type: RESET_CHECK_COMPLIANCE });

export default action$ =>
    action$.pipe(
        ofType(CHECK_COMPLIANCE),
        switchMap(action => {
            const { token } = action.data;
            const result = complianceCheck(token);
            return from(result).pipe(
                mergeMap(action => {
                    return of(checkComplianceSuccessful(action));
                }),
                catchError(err => of(checkComplianceFailed(err.message))),
            );
        }) /* , */,
        // catchError(err =>
        //     of(
        //         createGroupFailed({ code: 400, messages: [err.message] }),
        //         resetCreateGroup()
        //     )
        // )
    );
