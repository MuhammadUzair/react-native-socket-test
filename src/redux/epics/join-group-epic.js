import { ofType } from 'redux-observable';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';
import { of, from } from 'rxjs';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

import { refreshMyGroups } from './fetch-my-groups-epic';

export const JOIN_GROUP = 'JOIN_GROUP';
export const JOIN_GROUP_SUCCESSFUL = 'JOIN_GROUP_SUCCESSFUL';
export const JOIN_GROUP_FAILED = 'JOIN_GROUP_FAILED';
export const RESET_JOIN_GROUP = 'RESET_JOIN_GROUP';

export const joinGroup = data => ({
    type: JOIN_GROUP,
    data
});

const joinGroupSuccessful = data => ({
    type: JOIN_GROUP_SUCCESSFUL,
    data
});

const joinGroupFailed = data => ({
    type: JOIN_GROUP_FAILED,
    data
});

export const resetJoinGroup = () => ({ type: RESET_JOIN_GROUP });

export default action$ =>
    action$.pipe(
        ofType(JOIN_GROUP),
        switchMap(action => {
            const { token, groupId } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'PATCH',
                headers
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/groups/join/${groupId}`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return joinGroupSuccessful(res);
                    return joinGroupFailed(res);
                });

            return from(req).pipe(
                mergeMap(action =>
                    of(
                        action,
                        resetJoinGroup(),
                        refreshMyGroups({ token, group: 'all' })
                    )
                ),
                catchError(err => of(err.message, resetJoinGroup()))
            );
        })
    );
