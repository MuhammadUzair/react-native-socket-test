import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

export const CREATE_GROUP = 'CREATE_GROUP';
export const CREATE_GROUP_SUCCESSFUL = 'CREATE_GROUP_SUCCESSFUL';
export const CREATE_GROUP_FAILED = 'CREATE_GROUP_FAILED';
export const RESET_CREATE_GROUP = 'RESET_CREATE_GROUP';

export const createGroup = data => ({
    type: CREATE_GROUP,
    data
});

const createGroupSuccessful = data => ({
    type: CREATE_GROUP_SUCCESSFUL,
    data
});

const createGroupFailed = data => ({
    type: CREATE_GROUP_FAILED,
    data
});

export const resetCreateGroup = () => ({ type: RESET_CREATE_GROUP });

export default action$ =>
    action$.pipe(
        ofType(CREATE_GROUP),
        switchMap(action => {
            const { token, name, description, friends, privacy } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify({
                    name,
                    description,
                    friends: friends.map(item => item.friend_id),
                    privicy: privacy === 'Private' ? 0 : 1
                })
            };

            const req = fetch(`${BASEURL_DEV}/api/v1/view/groups`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return createGroupSuccessful(res);
                    return createGroupFailed(res);
                });
            return from(req).pipe(
                mergeMap(action => of(action, resetCreateGroup())),
                catchError(err =>
                    of(
                        createGroupFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetCreateGroup()
                    )
                )
            );
        }),
        catchError(err =>
            of(
                createGroupFailed({ code: 400, messages: [err.message] }),
                resetCreateGroup()
            )
        )
    );
