import { ofType } from 'redux-observable';
import { switchMap, catchError, mergeMap } from 'rxjs/operators';
import { of, from } from 'rxjs';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

import { refreshMyCompetitions } from './fetch-my-competition-epic';

export const PLAY_COMPETITION = 'PLAY_COMPETITION';
export const PLAY_COMPETITION_SUCCESSFUL = 'PLAY_COMPETITION_SUCCESSFUL';
export const PLAY_COMPETITION_FAILED = 'PLAY_COMPETITION_FAILED';
export const RESET_PLAY_COMPETITION = 'RESET_PLAY_COMPETITION';

export const playCompetition = data => ({
    type: PLAY_COMPETITION,
    data,
});

const playCompetitionSuccessful = data => ({
    type: PLAY_COMPETITION_SUCCESSFUL,
    data,
});

const playCompetitionFailed = data => ({
    type: PLAY_COMPETITION_FAILED,
    data,
});

export const resetPlayCompetition = () => ({ type: RESET_PLAY_COMPETITION });

export default action$ =>
    action$.pipe(
        ofType(PLAY_COMPETITION),
        switchMap(action => {
            const {
                token,
                sport_cid,
                owner_id,
                competition_id,
                amount,
                latitude,
                longitude,
                altitude,
            } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify({
                    sport_cid,
                    owner_id,
                    competition_id,
                    amount,
                    latitude,
                    longitude,
                    altitude,
                }),
            };

            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/competitions/play`,
                options,
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return playCompetitionSuccessful(res);
                    return playCompetitionFailed(res);
                });

            return from(req).pipe(
                mergeMap(action => {
                    return of(
                        action,
                        resetPlayCompetition(),
                        refreshMyCompetitions({ token, tab: 'open' }),
                        refreshMyCompetitions({ token, tab: 'inplay' }),
                    );
                }),
                catchError(err =>
                    of(
                        playCompetitionFailed(err.message),
                        resetPlayCompetition(),
                    ),
                ),
            );
        }),
    );
