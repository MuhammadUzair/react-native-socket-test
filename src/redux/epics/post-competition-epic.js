import { ofType } from 'redux-observable';
import { switchMap, catchError, concat, mergeMap } from 'rxjs/operators';
import { of, from } from 'rxjs';

import { BASEURL_DEV } from './base-url';
import { storeToStorage, addBasicHeaders } from '../../utils';

export const POST_COMPETITION = 'POST_COMPETITION';
export const POST_COMPETITION_SUCCESSFUL = 'POST_COMPETITION_SUCCESSFUL';
export const POST_COMPETITION_FAILED = 'POST_COMPETITION_FAILED';
export const RESET_POST_COMPETITION = 'RESET_POST_COMPETITION';

export const postCompetition = data => ({
    type: POST_COMPETITION,
    data
});

const postCompetitionSuccessful = data => ({
    type: POST_COMPETITION_SUCCESSFUL,
    data
});

const postCompetitionFailed = data => ({
    type: POST_COMPETITION_FAILED,
    data
});

export const resetPostCompetition = () => ({ type: RESET_POST_COMPETITION });

export default action$ =>
    action$.pipe(
        ofType(POST_COMPETITION),
        switchMap(action => {
            const {
                token,
                sport_cid,
                team_id,
                spread,
                competitor_id,
                amount,
                latitude,
                longitude,
                altitude,
                privacy_type,
                ids
            } = action.data;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify({
                    sport_cid,
                    team_id,
                    spread,
                    competitor_id,
                    amount,
                    latitude,
                    longitude,
                    altitude,
                    privacy_type,
                    ids
                })
            };

            // const req = await fetch(
            //     `${BASEURL_DEV}/api/v1/view/competitions/create`,
            //     options
            // )
            //     .then(res => res.json())
            //     .then(res => {
            //         if (res.code >= 200 && res.code < 300)
            //             return postCompetitionSuccessful(res);
            //         return postCompetitionFailed(res);
            //     });

            // return of(req, resetPostCompetition());
            const req = fetch(
                `${BASEURL_DEV}/api/v1/view/competitions/create`,
                options
            )
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return postCompetitionSuccessful(res);
                    return postCompetitionFailed(res);
                });

            return from(req).pipe(
                mergeMap(action => of(action, resetPostCompetition())),
                catchError(err =>
                    of(
                        postCompetitionFailed(err.message),
                        resetPostCompetition()
                    )
                )
            );
        })
        // mergeMap(prom => from(prom)),
        // catchError(err =>
        //     of(postCompetitionFailed(err.message), resetPostCompetition())
        // )
    );
