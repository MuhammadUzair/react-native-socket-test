import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';
import { mergeMap } from 'rxjs-compat/operator/mergeMap';

export const SEARCH_MY_FRIENDS = 'SEARCH_MY_FRIENDS';
export const SEARCH_MY_FRIENDS_SUCCESSFUL = 'SEARCH_MY_FRIENDS_SUCCESSFUL';
export const SEARCH_MY_FRIENDS_FAILED = 'SEARCH_MY_FRIENDS_FAILED';
export const RESET_SEARCH_MY_FRIENDS = 'RESET_SEARCH_MY_FRIENDS';

export const searchMyFriends = data => ({
    type: SEARCH_MY_FRIENDS,
    data
});

const searchMyFriendsSuccessful = data => ({
    type: SEARCH_MY_FRIENDS_SUCCESSFUL,
    data
});

const searchMyFriendsFailed = data => ({
    type: SEARCH_MY_FRIENDS_FAILED,
    data
});

export const resetSearchMyFriend = () => ({ type: RESET_SEARCH_MY_FRIENDS });

export default action$ =>
    action$.pipe(
        ofType(SEARCH_MY_FRIENDS),
        switchMap(action => {
            const { token, nextPage, search, groupId } = action.data;
            const headers = addBasicHeaders(token);
            const options = {
                method: 'GET',
                headers
            };
            let url =
                BASEURL_DEV +
                (nextPage
                    ? nextPage
                    : `/api/v1/view/friends/myfriends?q=${search}`);
            if (groupId)
                url =
                    BASEURL_DEV +
                    (nextPage
                        ? nextPage
                        : `/api/v1/view/friends/myfriends?group_id=${groupId}&q=${search}`);
            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300)
                        return searchMyFriendsSuccessful(res);
                    return searchMyFriendsFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        searchMyFriendsFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetSearchMyFriend()
                    )
                )
            );
        }),
        catchError(err =>
            of(
                searchMyFriendsFailed({ code: 400, messages: [err.message] }),
                resetSearchMyFriend()
            )
        )
    );
