export const ALL_RESET = 'ALL_RESET';

export const logoutUserAction = data => ({
    type: ALL_RESET,
    data
});
