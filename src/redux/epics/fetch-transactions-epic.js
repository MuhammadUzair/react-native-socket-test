import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { BASEURL_DEV } from './base-url';
import { addBasicHeaders } from '../../utils';

import { APPLY_FILTER, RESET_FILTER } from './filter-action';

export const FETCH_TRANSACTIONS = 'FETCH_TRANSACTIONS';
export const FETCH_TRANSACTIONS_SUCCESSFUL = 'FETCH_TRANSACTIONS_SUCCESSFUL';
export const FETCH_TRANSACTIONS_FAILED = 'FETCH_TRANSACTIONS_FAILED';
export const RESET_FETCH_TRANSACTIONS = 'RESET_FETCH_TRANSACTIONS';

export const REFRESH_TRANSACTIONS = 'REFRESH_TRANSACTIONS';
export const REFRESH_TRANSACTIONS_SUCCESSFUL =
    'REFRESH_TRANSACTIONS_SUCCESSFUL';

export const fetchTransactions = data => ({
    type: FETCH_TRANSACTIONS,
    data
});

const fetchTransactionsSuccessful = data => ({
    type: FETCH_TRANSACTIONS_SUCCESSFUL,
    data
});

const fetchTransactionsFailed = data => ({
    type: FETCH_TRANSACTIONS_FAILED,
    data
});

const resetTransactions = () => ({ type: RESET_FETCH_TRANSACTIONS });

export const refreshTransactions = data => ({
    type: REFRESH_TRANSACTIONS,
    data
});

const refreshTransactionsSuccessful = data => ({
    type: REFRESH_TRANSACTIONS_SUCCESSFUL,
    data
});

export default (action$, state$) =>
    action$.pipe(
        ofType(
            FETCH_TRANSACTIONS,
            REFRESH_TRANSACTIONS,
            APPLY_FILTER,
            RESET_FILTER
        ),
        switchMap(action => {
            const { token, nextPage } = action.data;
            const { fType, tType, fromDate, toDate } = state$.value.filters;
            const headers = addBasicHeaders(token);

            const options = {
                method: 'GET',
                headers
            };

            let url;
            if (nextPage) {
                url = `${BASEURL_DEV}${nextPage}`;
            } else {
                url = `${BASEURL_DEV}/api/v1/view/wallet/${
                    state$.value.user.data.id
                }?`;
                if (fType) url += `ftype=${fType}&`;
                if (tType) url += `ttype=${tType}&`;
                if (fromDate) url += `fdate=${fromDate}&`;
                if (toDate) url += `todate=${toDate}`;
            }

            // console.log(url.endsWith('&'));
            // if (url.endsWith('&')) {
            //     url = url.substring(0, url.length - 1);
            // }

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (
                            action.type === APPLY_FILTER &&
                            action.type === RESET_FILTER
                        )
                            return refreshTransactionsSuccessful(res);

                        if (action.type === FETCH_TRANSACTIONS)
                            return fetchTransactionsSuccessful(res);
                        return refreshTransactionsSuccessful(res);
                    }
                    return fetchTransactionsFailed(res);
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchTransactionsFailed({
                            code: 400,
                            messages: [err.message]
                        }),
                        resetTransactions()
                    )
                )
            );
        })
    );
