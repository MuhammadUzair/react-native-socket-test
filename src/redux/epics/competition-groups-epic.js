import { ofType } from 'redux-observable';
import { from, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { addBasicHeaders } from '../../utils';
import { BASEURL_DEV } from './base-url';

export const CREATE_COMPETITION_GROUPS = 'CREATE_COMPETITION_GROUPS';
export const CREATE_COMPETITION_GROUPS_SUCCESSFUL =
    'CREATE_COMPETITION_GROUPS_SUCCESSFUL';
export const CREATE_COMPETITION_GROUPS_FAILED =
    'CREATE_COMPETITION_GROUPS_FAILED';
export const RESET_CREATE_COMPETITION_GROUPS =
    'RESET_CREATE_COMPETITION_GROUPS';
export const REFRESH_MY_GROUPS = 'REFRESH_MY_GROUPS';
export const REFRESH_MY_GROUPS_SUCCESSFUL = 'REFRESH_MY_GROUPS_SUCCESSFUL';
export const REFRESH_MY_GROUPS_FAILED = 'REFRESH_MY_GROUPS_FAILED';

export const fetchCompetitionGroups = data => ({
    type: CREATE_COMPETITION_GROUPS,
    data
});

export const refreshCompetitionGroups = data => ({
    type: REFRESH_MY_GROUPS,
    data
});

export const refreshCompetitionGroupsSuccessful = data => ({
    type: REFRESH_MY_GROUPS_SUCCESSFUL,
    data
});

const fetchCompetitionGroupsSuccessful = data => ({
    type: CREATE_COMPETITION_GROUPS_SUCCESSFUL,
    data
});

const fetchCompetitionGroupsFailed = data => ({
    type: CREATE_COMPETITION_GROUPS_FAILED,
    data
});

export const resetMyGroups = () => ({ type: RESET_CREATE_COMPETITION_GROUPS });

export default action$ =>
    action$.pipe(
        ofType(CREATE_COMPETITION_GROUPS),
        switchMap(action => {
            const {
                token,
                nextPage,
                group,
                isSearch,
                search,
                isRefresh
            } = action.data;
            const headers = addBasicHeaders(token);

            const url =
                BASEURL_DEV +
                (nextPage
                    ? nextPage
                    : isSearch
                    ? `/api/v1/view/groups/mygroups?type=all&q=` + search
                    : `/api/v1/view/groups/mygroups?type=all`);

            const options = {
                method: 'GET',
                headers
            };

            const req = fetch(url, options)
                .then(res => res.json())
                .then(res => {
                    if (res.code >= 200 && res.code < 300) {
                        if (isRefresh)
                            return refreshCompetitionGroupsSuccessful({
                                ...res,
                                group
                            });
                        return fetchCompetitionGroupsSuccessful({
                            ...res,
                            group
                        });
                    } else {
                        return fetchCompetitionGroupsFailed({
                            ...res,
                            group
                        });
                    }
                });

            return from(req).pipe(
                catchError(err =>
                    of(
                        fetchCompetitionGroupsFailed({
                            code: 400,
                            messages: [err.message],
                            group
                        }),
                        resetMyGroups()
                    )
                )
            );
        })
    );
