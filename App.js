import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';
// import { Clipboard } from 'react-native';
import PushNotification from 'react-native-push-notification';
import { storeToStorage } from './src/utils';

import RootStack from './src/routers/root';

class App extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount = () => {
        SplashScreen.hide();
        PushNotification.configure({
            onRegister: async token => {
                // console.log('token', token);
                // Clipboard.setString(JSON.stringify(token));
                await storeToStorage('pushNotification', JSON.stringify(token));
                PushNotification.setApplicationIconBadgeNumber(0);
                PushNotification.cancelAllLocalNotifications();
            },
            onNotification: async notification => {
                // console.log('notification call in appjs ', notification);
                // Clipboard.setString(JSON.stringify(notification));
                if (notification) {
                    await storeToStorage(
                        'notification',
                        JSON.stringify(notification)
                    );
                }
            },
            senderID: '924289564409',
            permissions: {
                alert: true,
                badge: true,
                sound: true
            },
            popInitialNotification: true,
            requestPermissions: true
        });
    };
    render() {
        return <RootStack />;
    }
}

console.disableYellowBox = true;

export default App;
