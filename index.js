import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';

import store from './src/redux/store';
import App from './App';

const AppWrapper = () => (
    <Provider store={store}>
        <App />
    </Provider>
);

AppRegistry.registerComponent('PostItPlayIt', () => AppWrapper);
